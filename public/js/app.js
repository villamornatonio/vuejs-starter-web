webpackJsonp([0],[
/* 0 */,
/* 1 */
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file.
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate

    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),
/* 2 */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(45)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction) {
  isProduction = _isProduction

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[data-vue-ssr-id~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(43)
}
var normalizeComponent = __webpack_require__(1)
/* script */
var __vue_script__ = __webpack_require__(46)
/* template */
var __vue_template__ = __webpack_require__(47)
/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/vue/layouts/pages.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-23a00882", Component.options)
  } else {
    hotAPI.reload("data-v-23a00882", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */,
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(62)
}
var normalizeComponent = __webpack_require__(1)
/* script */
var __vue_script__ = __webpack_require__(64)
/* template */
var __vue_template__ = __webpack_require__(65)
/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/vue/layouts/admin.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-219942e6", Component.options)
  } else {
    hotAPI.reload("data-v-219942e6", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 17 */,
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(19);


/***/ }),
/* 19 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__bootstrap_js__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__routes_js__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__store_js__ = __webpack_require__(83);





new Vue({
    el: '#root',
    router: __WEBPACK_IMPORTED_MODULE_1__routes_js__["a" /* default */],
    store: __WEBPACK_IMPORTED_MODULE_2__store_js__["a" /* default */],
    data: {
        shared: __WEBPACK_IMPORTED_MODULE_2__store_js__["a" /* default */]
    },
    mounted: function mounted() {
        var that = this;
        that.$on('notifySuccess', function (msg) {
            that.$swal('Success', msg, "success");
        });
        that.$on('notifyFailed', function (msg) {
            that.$swal('Error', msg, "error");
        });
    },
    created: function created() {
        var accessToken = localStorage.getItem('access_token');
        if (accessToken !== null && accessToken !== 'undefined') {
            var that = this;
            axios.get('/api/user').then(function (response) {
                that.setLogin(response.data.data);
            }).catch(function (error) {
                that.destroyLogin();
            });
        }
    },

    methods: {
        setLogin: function setLogin(user) {
            var that = this;
            // Save login info in our data and set header in case it's not set already
            this.$store.state.user = user;
            this.$store.state.isAuthenticated = true;
            this.$store.state.access_token = localStorage.getItem('access_token');
        },
        destroyLogin: function destroyLogin() {
            var that = this;
            // Cleanup when token was invalid our user has logged out
            this.$store.state.user = {};
            this.$store.state.isAuthenticated = false;
            this.$store.state.access_token = null;
            localStorage.removeItem('access_token');
            localStorage.removeItem('refresh_token');
            that.$router.push('/dashboard');
        }
    }
});

/***/ }),
/* 20 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_router__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_axios__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_Form__ = __webpack_require__(40);





window.Vue = __WEBPACK_IMPORTED_MODULE_0_vue___default.a;
window.Form = __WEBPACK_IMPORTED_MODULE_3__core_Form__["a" /* default */];
__WEBPACK_IMPORTED_MODULE_0_vue___default.a.use(__WEBPACK_IMPORTED_MODULE_1_vue_router__["default"]);
window.axios = __WEBPACK_IMPORTED_MODULE_2_axios___default.a;

window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest'
    // window.axios.defaults.baseURL = 'https://api-matchup.herokuapp.com';
};window.axios.defaults.baseURL = 'http://api.matchup.com';

__WEBPACK_IMPORTED_MODULE_2_axios___default.a.interceptors.request.use(function (config) {

    var accessToken = void 0;
    accessToken = localStorage.getItem('access_token');

    if (accessToken !== null && accessToken !== 'undefined') {
        config.headers.common['Authorization'] = 'Bearer ' + accessToken;
    }
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

__WEBPACK_IMPORTED_MODULE_2_axios___default.a.interceptors.response.use(function (response) {
    return response;
}, function (error) {

    var originalRequest = error.config;

    if (error.response.status === 401 && !originalRequest._retry) {

        originalRequest._retry = true;

        var refreshToken = window.localStorage.getItem('refresh_token');
        return __WEBPACK_IMPORTED_MODULE_2_axios___default.a.post('/api/auth/refresh', { refreshToken: refreshToken }).then(function (_ref) {
            var data = _ref.data;

            window.localStorage.setItem('access_token', data.access_token);
            window.localStorage.setItem('refresh_token', data.refresh_token);
            __WEBPACK_IMPORTED_MODULE_2_axios___default.a.defaults.headers.common['Authorization'] = 'Bearer ' + data.access_token;
            originalRequest.headers['Authorization'] = 'Bearer ' + data.access_token;
            return __WEBPACK_IMPORTED_MODULE_2_axios___default()(originalRequest);
        });
    }

    return Promise.reject(error);
});

/***/ }),
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */,
/* 25 */,
/* 26 */,
/* 27 */,
/* 28 */,
/* 29 */,
/* 30 */,
/* 31 */,
/* 32 */,
/* 33 */,
/* 34 */,
/* 35 */,
/* 36 */,
/* 37 */,
/* 38 */,
/* 39 */,
/* 40 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Errors__ = __webpack_require__(41);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var Form = function () {
    /**
     * Create a new Form instance.
     *
     * @param {object} data
     */
    function Form(data) {
        _classCallCheck(this, Form);

        this.originalData = data;

        for (var field in data) {
            this[field] = data[field];
        }

        this.errors = new __WEBPACK_IMPORTED_MODULE_0__Errors__["a" /* default */]();
    }

    /**
     * Fetch all relevant data for the form.
     */


    _createClass(Form, [{
        key: 'data',
        value: function data() {
            var data = {};

            for (var property in this.originalData) {
                data[property] = this[property];
            }

            return data;
        }

        /**
         * Reset the form fields.
         */

    }, {
        key: 'reset',
        value: function reset() {
            for (var field in this.originalData) {
                this[field] = '';
            }

            this.errors.clear();
        }

        /**
         * Send a POST request to the given URL.
         * .
         * @param {string} url
         */

    }, {
        key: 'post',
        value: function post(url) {
            return this.submit('post', url);
        }

        /**
         * Send a PUT request to the given URL.
         * .
         * @param {string} url
         */

    }, {
        key: 'put',
        value: function put(url) {
            return this.submit('put', url);
        }

        /**
         * Send a PATCH request to the given URL.
         * .
         * @param {string} url
         */

    }, {
        key: 'patch',
        value: function patch(url) {
            return this.submit('patch', url);
        }

        /**
         * Send a DELETE request to the given URL.
         * .
         * @param {string} url
         */

    }, {
        key: 'delete',
        value: function _delete(url) {
            return this.submit('delete', url);
        }

        /**
         * Submit the form.
         *
         * @param {string} requestType
         * @param {string} url
         */

    }, {
        key: 'submit',
        value: function submit(requestType, url) {
            var _this = this;

            return new Promise(function (resolve, reject) {
                axios[requestType](url, _this.data()).then(function (response) {
                    _this.onSuccess(response.data);

                    resolve(response.data);
                }).catch(function (error) {
                    _this.onFail(error.response.data);

                    reject(error.response.data);
                });
            });
        }

        /**
         * Handle a successful form submission.
         *
         * @param {object} data
         */

    }, {
        key: 'onSuccess',
        value: function onSuccess(data) {
            //alert(data.message); // temporary

            this.reset();
        }

        /**
         * Handle a failed form submission.
         *
         * @param {object} errors
         */

    }, {
        key: 'onFail',
        value: function onFail(errors) {
            this.errors.record(errors);
        }
    }]);

    return Form;
}();

/* harmony default export */ __webpack_exports__["a"] = (Form);

/***/ }),
/* 41 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Errors = function () {
    /**
     * Create a new Errors instance.
     */
    function Errors() {
        _classCallCheck(this, Errors);

        this.errors = {};
    }

    /**
     * Determine if an errors exists for the given field.
     *
     * @param {string} field
     */


    _createClass(Errors, [{
        key: "has",
        value: function has(field) {
            return this.errors.hasOwnProperty(field);
        }

        /**
         * Determine if we have any errors.
         */

    }, {
        key: "any",
        value: function any() {
            return Object.keys(this.errors).length > 0;
        }

        /**
         * Retrieve the error message for a field.
         *
         * @param {string} field
         */

    }, {
        key: "get",
        value: function get(field) {
            if (this.errors[field]) {
                return this.errors[field][0];
            }
        }

        /**
         * Record the new errors.
         *
         * @param {object} errors
         */

    }, {
        key: "record",
        value: function record(errors) {
            this.errors = errors;
        }

        /**
         * Clear one or all error fields.
         *
         * @param {string|null} field
         */

    }, {
        key: "clear",
        value: function clear(field) {
            if (field) {
                delete this.errors[field];

                return;
            }

            this.errors = {};
        }
    }]);

    return Errors;
}();

/* harmony default export */ __webpack_exports__["a"] = (Errors);

/***/ }),
/* 42 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_router__ = __webpack_require__(5);


var routes = [{
    path: '/',
    component: __webpack_require__(7),
    meta: { requiresAuth: false },
    children: [{
        path: '',
        component: __webpack_require__(48),
        meta: { requiresAuth: false }
    }]
}, {
    path: '/about',
    component: __webpack_require__(7),
    meta: { requiresAuth: false },
    children: [{
        path: '',
        component: __webpack_require__(50),
        meta: { requiresAuth: false }
    }]
}, {
    path: '/contact',
    component: __webpack_require__(7),
    meta: { requiresAuth: false },
    children: [{
        path: '',
        component: __webpack_require__(52),
        meta: { requiresAuth: false }
    }]
}, {
    path: '/login',
    component: __webpack_require__(54),
    meta: { requiresAuth: false },
    children: [{
        path: '',
        component: __webpack_require__(57),
        meta: { requiresAuth: false }
    }]
}, {
    path: '/dashboard',
    component: __webpack_require__(16),
    meta: { requiresAuth: true },
    children: [{
        path: '',
        component: __webpack_require__(66),
        meta: { requiresAuth: true }
    }]
}, {
    path: '/users',
    component: __webpack_require__(16),
    meta: { requiresAuth: true },
    children: [{
        path: '',
        component: __webpack_require__(71),
        meta: { requiresAuth: true }
    }]
}, {
    path: '*',
    component: __webpack_require__(76),
    meta: { requiresAuth: false },
    children: [{
        path: '',
        component: __webpack_require__(81),
        meta: { requiresAuth: false }
    }]
}];

var router = new __WEBPACK_IMPORTED_MODULE_0_vue_router__["default"]({
    linkActiveClass: "active",
    mode: 'history',
    routes: routes
});

router.beforeEach(function (to, from, next) {
    var accessToken = localStorage.getItem('access_token');
    if (to.meta.requiresAuth) {
        if (!accessToken || accessToken === null) {
            next({
                path: '/login',
                query: { redirect: to.fullPath }
            });
        }
    }
    next();
});

/* harmony default export */ __webpack_exports__["a"] = (router);

/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(44);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("0452ebb8", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js?sourceMap!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-23a00882\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0&bustCache!./pages.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js?sourceMap!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-23a00882\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0&bustCache!./pages.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)(true);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n/*!\n * Start Bootstrap - Modern Business (http://startbootstrap.com/)\n * Copyright 2013-2016 Start Bootstrap\n * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap/blob/gh-pages/LICENSE)\n */\n\n/* Global Styles */\nhtml,\nbody {\n    height: 100%;\n}\nbody {\n    padding-top: 50px; /* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */\n}\n.img-portfolio {\n    margin-bottom: 30px;\n}\n.img-hover:hover {\n    opacity: 0.8;\n}\n\n/* Home Page Carousel */\nheader.carousel {\n    height: 50%;\n}\nheader.carousel .item,\nheader.carousel .item.active,\nheader.carousel .carousel-inner {\n    height: 100%;\n}\nheader.carousel .fill {\n    width: 100%;\n    height: 100%;\n    background-position: center;\n    background-size: cover;\n}\n\n/* 404 Page Styles */\n.error-404 {\n    font-size: 100px;\n}\n\n/* Pricing Page Styles */\n.price {\n    display: block;\n    font-size: 50px;\n    line-height: 50px;\n}\n.price sup {\n    top: -20px;\n    left: 2px;\n    font-size: 20px;\n}\n.period {\n    display: block;\n    font-style: italic;\n}\n\n/* Footer Styles */\nfooter {\n    margin: 50px 0;\n}\n\n/* Responsive Styles */\n@media(max-width:991px) {\n.customer-img,\n    .img-related {\n        margin-bottom: 30px;\n}\n}\n@media(max-width:767px) {\n.img-portfolio {\n        margin-bottom: 15px;\n}\nheader.carousel .carousel {\n        height: 70%;\n}\n}\n\n\t\n", "", {"version":3,"sources":["/Users/villamornantoniojr/Development/greyfox/matchup/web/resources/js/vue/layouts/resources/js/vue/layouts/pages.vue?2c3bdd97"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;AA0GA;;;;GAIA;;AAEA,mBAAA;AAEA;;IAEA,aAAA;CACA;AAEA;IACA,kBAAA,CAAA,yHAAA;CACA;AAEA;IACA,oBAAA;CACA;AAEA;IACA,aAAA;CACA;;AAEA,wBAAA;AAEA;IACA,YAAA;CACA;AAEA;;;IAGA,aAAA;CACA;AAEA;IACA,YAAA;IACA,aAAA;IACA,4BAAA;IACA,uBAAA;CACA;;AAEA,qBAAA;AAEA;IACA,iBAAA;CACA;;AAEA,yBAAA;AAEA;IACA,eAAA;IACA,gBAAA;IACA,kBAAA;CACA;AAEA;IACA,WAAA;IACA,UAAA;IACA,gBAAA;CACA;AAEA;IACA,eAAA;IACA,mBAAA;CACA;;AAEA,mBAAA;AAEA;IACA,eAAA;CACA;;AAEA,uBAAA;AAEA;AACA;;QAEA,oBAAA;CACA;CACA;AAEA;AACA;QACA,oBAAA;CACA;AAEA;QACA,YAAA;CACA;CACA","file":"pages.vue","sourcesContent":["<template>\n<div id=\"layouts-pages\">\n\n    <!-- Navigation -->\n    <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">\n        <div class=\"container\">\n            <!-- Brand and toggle get grouped for better mobile display -->\n            <div class=\"navbar-header\">\n                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">\n                    <span class=\"sr-only\">Toggle navigation</span>\n                    <span class=\"icon-bar\"></span>\n                    <span class=\"icon-bar\"></span>\n                    <span class=\"icon-bar\"></span>\n                </button>\n                <router-link to='/' class=\"navbar-brand\">VueJS Starter</router-link>\n            </div>\n            <!-- Collect the nav links, forms, and other content for toggling -->\n            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\n                <ul class=\"nav navbar-nav navbar-right\">\n                    <li>\n                        <router-link to='about'>About</router-link>\n                    </li>\n                    <li>\n                    \t<router-link to='contact'>Contact</router-link>\n                    </li>\n                    <li>\n                    \t<router-link to='login'>Login</router-link>\n                    </li>\n                    \n                </ul>\n            </div>\n            <!-- /.navbar-collapse -->\n        </div>\n        <!-- /.container -->\n    </nav>\n\n    <!-- Header Carousel -->\n    <header id=\"myCarousel\" class=\"carousel slide\">\n        <!-- Indicators -->\n        <ol class=\"carousel-indicators\">\n            <li data-target=\"#myCarousel\" data-slide-to=\"0\" class=\"\"></li>\n            <li data-target=\"#myCarousel\" data-slide-to=\"1\" class=\"active\"></li>\n            <li data-target=\"#myCarousel\" data-slide-to=\"2\"></li>\n        </ol>\n\n        <!-- Wrapper for slides -->\n        <div class=\"carousel-inner\">\n            <div class=\"item\">\n                <div class=\"fill\" style=\"background-image:url('http://placehold.it/1900x1080&amp;text=Slide One');\"></div>\n                <div class=\"carousel-caption\">\n                    <h2>Caption 1</h2>\n                </div>\n            </div>\n            <div class=\"item active\">\n                <div class=\"fill\" style=\"background-image:url('http://placehold.it/1900x1080&amp;text=Slide Two');\"></div>\n                <div class=\"carousel-caption\">\n                    <h2>Caption 2</h2>\n                </div>\n            </div>\n            <div class=\"item\">\n                <div class=\"fill\" style=\"background-image:url('http://placehold.it/1900x1080&amp;text=Slide Three');\"></div>\n                <div class=\"carousel-caption\">\n                    <h2>Caption 3</h2>\n                </div>\n            </div>\n        </div>\n\n        <!-- Controls -->\n        <a class=\"left carousel-control\" href=\"#myCarousel\" data-slide=\"prev\">\n            <span class=\"icon-prev\"></span>\n        </a>\n        <a class=\"right carousel-control\" href=\"#myCarousel\" data-slide=\"next\">\n            <span class=\"icon-next\"></span>\n        </a>\n    </header>\n\n    <!-- Page Content -->\n    <div class=\"container\">\n\n        \n\n        <router-view></router-view>\n\n        <hr>\n\n        <!-- Footer -->\n        <footer>\n            <div class=\"row\">\n                <div class=\"col-lg-12\">\n                    <p>Copyright © Your Website 2014</p>\n                </div>\n            </div>\n        </footer>\n\n    </div>\n    <!-- /.container -->\n\n\n\n</div>\n</template>\n\n<script>\n</script>\n\n<style type=\"text/css\" media=\"screen\">\n/*!\n * Start Bootstrap - Modern Business (http://startbootstrap.com/)\n * Copyright 2013-2016 Start Bootstrap\n * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap/blob/gh-pages/LICENSE)\n */\n\n/* Global Styles */\n\nhtml,\nbody {\n    height: 100%;\n}\n\nbody {\n    padding-top: 50px; /* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */\n}\n\n.img-portfolio {\n    margin-bottom: 30px;\n}\n\n.img-hover:hover {\n    opacity: 0.8;\n}\n\n/* Home Page Carousel */\n\nheader.carousel {\n    height: 50%;\n}\n\nheader.carousel .item,\nheader.carousel .item.active,\nheader.carousel .carousel-inner {\n    height: 100%;\n}\n\nheader.carousel .fill {\n    width: 100%;\n    height: 100%;\n    background-position: center;\n    background-size: cover;\n}\n\n/* 404 Page Styles */\n\n.error-404 {\n    font-size: 100px;\n}\n\n/* Pricing Page Styles */\n\n.price {\n    display: block;\n    font-size: 50px;\n    line-height: 50px;\n}\n\n.price sup {\n    top: -20px;\n    left: 2px;\n    font-size: 20px;\n}\n\n.period {\n    display: block;\n    font-style: italic;\n}\n\n/* Footer Styles */\n\nfooter {\n    margin: 50px 0;\n}\n\n/* Responsive Styles */\n\n@media(max-width:991px) {\n    .customer-img,\n    .img-related {\n        margin-bottom: 30px;\n    }\n}\n\n@media(max-width:767px) {\n    .img-portfolio {\n        margin-bottom: 15px;\n    }\n\n    header.carousel .carousel {\n        height: 70%;\n    }\n}\n\n\t\n</style>\n\n"],"sourceRoot":""}]);

// exports


/***/ }),
/* 45 */
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),
/* 46 */
/***/ (function(module, exports) {

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { attrs: { id: "layouts-pages" } }, [
    _c(
      "nav",
      {
        staticClass: "navbar navbar-inverse navbar-fixed-top",
        attrs: { role: "navigation" }
      },
      [
        _c("div", { staticClass: "container" }, [
          _c(
            "div",
            { staticClass: "navbar-header" },
            [
              _vm._m(0),
              _vm._v(" "),
              _c(
                "router-link",
                { staticClass: "navbar-brand", attrs: { to: "/" } },
                [_vm._v("VueJS Starter")]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "collapse navbar-collapse",
              attrs: { id: "bs-example-navbar-collapse-1" }
            },
            [
              _c("ul", { staticClass: "nav navbar-nav navbar-right" }, [
                _c(
                  "li",
                  [
                    _c("router-link", { attrs: { to: "about" } }, [
                      _vm._v("About")
                    ])
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "li",
                  [
                    _c("router-link", { attrs: { to: "contact" } }, [
                      _vm._v("Contact")
                    ])
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "li",
                  [
                    _c("router-link", { attrs: { to: "login" } }, [
                      _vm._v("Login")
                    ])
                  ],
                  1
                )
              ])
            ]
          )
        ])
      ]
    ),
    _vm._v(" "),
    _vm._m(1),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "container" },
      [_c("router-view"), _vm._v(" "), _c("hr"), _vm._v(" "), _vm._m(2)],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "navbar-toggle",
        attrs: {
          type: "button",
          "data-toggle": "collapse",
          "data-target": "#bs-example-navbar-collapse-1"
        }
      },
      [
        _c("span", { staticClass: "sr-only" }, [_vm._v("Toggle navigation")]),
        _vm._v(" "),
        _c("span", { staticClass: "icon-bar" }),
        _vm._v(" "),
        _c("span", { staticClass: "icon-bar" }),
        _vm._v(" "),
        _c("span", { staticClass: "icon-bar" })
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "header",
      { staticClass: "carousel slide", attrs: { id: "myCarousel" } },
      [
        _c("ol", { staticClass: "carousel-indicators" }, [
          _c("li", {
            attrs: { "data-target": "#myCarousel", "data-slide-to": "0" }
          }),
          _vm._v(" "),
          _c("li", {
            staticClass: "active",
            attrs: { "data-target": "#myCarousel", "data-slide-to": "1" }
          }),
          _vm._v(" "),
          _c("li", {
            attrs: { "data-target": "#myCarousel", "data-slide-to": "2" }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "carousel-inner" }, [
          _c("div", { staticClass: "item" }, [
            _c("div", {
              staticClass: "fill",
              staticStyle: {
                "background-image":
                  "url('http://placehold.it/1900x1080&text=Slide One')"
              }
            }),
            _vm._v(" "),
            _c("div", { staticClass: "carousel-caption" }, [
              _c("h2", [_vm._v("Caption 1")])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "item active" }, [
            _c("div", {
              staticClass: "fill",
              staticStyle: {
                "background-image":
                  "url('http://placehold.it/1900x1080&text=Slide Two')"
              }
            }),
            _vm._v(" "),
            _c("div", { staticClass: "carousel-caption" }, [
              _c("h2", [_vm._v("Caption 2")])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "item" }, [
            _c("div", {
              staticClass: "fill",
              staticStyle: {
                "background-image":
                  "url('http://placehold.it/1900x1080&text=Slide Three')"
              }
            }),
            _vm._v(" "),
            _c("div", { staticClass: "carousel-caption" }, [
              _c("h2", [_vm._v("Caption 3")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c(
          "a",
          {
            staticClass: "left carousel-control",
            attrs: { href: "#myCarousel", "data-slide": "prev" }
          },
          [_c("span", { staticClass: "icon-prev" })]
        ),
        _vm._v(" "),
        _c(
          "a",
          {
            staticClass: "right carousel-control",
            attrs: { href: "#myCarousel", "data-slide": "next" }
          },
          [_c("span", { staticClass: "icon-next" })]
        )
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("footer", [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-lg-12" }, [
          _c("p", [_vm._v("Copyright © Your Website 2014")])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-23a00882", module.exports)
  }
}

/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__(49)
/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/vue/pages/home.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-16de4d36", Component.options)
  } else {
    hotAPI.reload("data-v-16de4d36", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { attrs: { id: "home" } }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-lg-12" }, [
          _c("h1", { staticClass: "page-header" }, [
            _vm._v(
              "\n                    Welcome to Modern Business\n                "
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-4" }, [
          _c("div", { staticClass: "panel panel-default" }, [
            _c("div", { staticClass: "panel-heading" }, [
              _c("h4", [
                _c("i", { staticClass: "fa fa-fw fa-check" }),
                _vm._v(" Bootstrap v3.2.0")
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "panel-body" }, [
              _c("p", [
                _vm._v(
                  "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, optio corporis quae nulla aspernatur in alias at numquam rerum ea excepturi expedita tenetur assumenda voluptatibus eveniet incidunt dicta nostrum quod?"
                )
              ]),
              _vm._v(" "),
              _c(
                "a",
                { staticClass: "btn btn-default", attrs: { href: "#" } },
                [_vm._v("Learn More")]
              )
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-4" }, [
          _c("div", { staticClass: "panel panel-default" }, [
            _c("div", { staticClass: "panel-heading" }, [
              _c("h4", [
                _c("i", { staticClass: "fa fa-fw fa-gift" }),
                _vm._v(" Free & Open Source")
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "panel-body" }, [
              _c("p", [
                _vm._v(
                  "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, optio corporis quae nulla aspernatur in alias at numquam rerum ea excepturi expedita tenetur assumenda voluptatibus eveniet incidunt dicta nostrum quod?"
                )
              ]),
              _vm._v(" "),
              _c(
                "a",
                { staticClass: "btn btn-default", attrs: { href: "#" } },
                [_vm._v("Learn More")]
              )
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-4" }, [
          _c("div", { staticClass: "panel panel-default" }, [
            _c("div", { staticClass: "panel-heading" }, [
              _c("h4", [
                _c("i", { staticClass: "fa fa-fw fa-compass" }),
                _vm._v(" Easy to Use")
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "panel-body" }, [
              _c("p", [
                _vm._v(
                  "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, optio corporis quae nulla aspernatur in alias at numquam rerum ea excepturi expedita tenetur assumenda voluptatibus eveniet incidunt dicta nostrum quod?"
                )
              ]),
              _vm._v(" "),
              _c(
                "a",
                { staticClass: "btn btn-default", attrs: { href: "#" } },
                [_vm._v("Learn More")]
              )
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-lg-12" }, [
          _c("h2", { staticClass: "page-header" }, [
            _vm._v("Portfolio Heading")
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-4 col-sm-6" }, [
          _c("a", { attrs: { href: "portfolio-item.html" } }, [
            _c("img", {
              staticClass: "img-responsive img-portfolio img-hover",
              attrs: { src: "http://placehold.it/700x450", alt: "" }
            })
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-4 col-sm-6" }, [
          _c("a", { attrs: { href: "portfolio-item.html" } }, [
            _c("img", {
              staticClass: "img-responsive img-portfolio img-hover",
              attrs: { src: "http://placehold.it/700x450", alt: "" }
            })
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-4 col-sm-6" }, [
          _c("a", { attrs: { href: "portfolio-item.html" } }, [
            _c("img", {
              staticClass: "img-responsive img-portfolio img-hover",
              attrs: { src: "http://placehold.it/700x450", alt: "" }
            })
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-4 col-sm-6" }, [
          _c("a", { attrs: { href: "portfolio-item.html" } }, [
            _c("img", {
              staticClass: "img-responsive img-portfolio img-hover",
              attrs: { src: "http://placehold.it/700x450", alt: "" }
            })
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-4 col-sm-6" }, [
          _c("a", { attrs: { href: "portfolio-item.html" } }, [
            _c("img", {
              staticClass: "img-responsive img-portfolio img-hover",
              attrs: { src: "http://placehold.it/700x450", alt: "" }
            })
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-4 col-sm-6" }, [
          _c("a", { attrs: { href: "portfolio-item.html" } }, [
            _c("img", {
              staticClass: "img-responsive img-portfolio img-hover",
              attrs: { src: "http://placehold.it/700x450", alt: "" }
            })
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-lg-12" }, [
          _c("h2", { staticClass: "page-header" }, [
            _vm._v("Modern Business Features")
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-6" }, [
          _c("p", [
            _vm._v("The Modern Business template by Start Bootstrap includes:")
          ]),
          _vm._v(" "),
          _c("ul", [
            _c("li", [_c("strong", [_vm._v("Bootstrap v3.2.0")])]),
            _vm._v(" "),
            _c("li", [_vm._v("jQuery v1.11.0")]),
            _vm._v(" "),
            _c("li", [_vm._v("Font Awesome v4.1.0")]),
            _vm._v(" "),
            _c("li", [_vm._v("Working PHP contact form with validation")]),
            _vm._v(" "),
            _c("li", [_vm._v("Unstyled page elements for easy customization")]),
            _vm._v(" "),
            _c("li", [_vm._v("17 HTML pages")])
          ]),
          _vm._v(" "),
          _c("p", [
            _vm._v(
              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, omnis doloremque non cum id reprehenderit, quisquam totam aspernatur tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus unde."
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-6" }, [
          _c("img", {
            staticClass: "img-responsive",
            attrs: { src: "http://placehold.it/700x450", alt: "" }
          })
        ])
      ]),
      _vm._v(" "),
      _c("hr"),
      _vm._v(" "),
      _c("div", { staticClass: "well" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-8" }, [
            _c("p", [
              _vm._v(
                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi."
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4" }, [
            _c(
              "a",
              {
                staticClass: "btn btn-lg btn-default btn-block",
                attrs: { href: "#" }
              },
              [_vm._v("Call to Action")]
            )
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-16de4d36", module.exports)
  }
}

/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__(51)
/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/vue/pages/about.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0d1e7b74", Component.options)
  } else {
    hotAPI.reload("data-v-0d1e7b74", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { attrs: { id: "pages-about-component" } }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-lg-12" }, [
            _c("h1", { staticClass: "page-header" }, [
              _vm._v("About\n                    "),
              _c("small", [_vm._v("Subheading")])
            ]),
            _vm._v(" "),
            _c("ol", { staticClass: "breadcrumb" }, [
              _c("li", [
                _c("a", { attrs: { href: "index.html" } }, [_vm._v("Home")])
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "active" }, [_vm._v("About")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-6" }, [
            _c("img", {
              staticClass: "img-responsive",
              attrs: { src: "http://placehold.it/750x450", alt: "" }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-6" }, [
            _c("h2", [_vm._v("About Modern Business")]),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed voluptate nihil eum consectetur similique? Consectetur, quod, incidunt, harum nisi dolores delectus reprehenderit voluptatem perferendis dicta dolorem non blanditiis ex fugiat."
              )
            ]),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe, magni, aperiam vitae illum voluptatum aut sequi impedit non velit ab ea pariatur sint quidem corporis eveniet. Odit, temporibus reprehenderit dolorum!"
              )
            ]),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, consequuntur, modi mollitia corporis ipsa voluptate corrupti eum ratione ex ea praesentium quibusdam? Aut, in eum facere corrupti necessitatibus perspiciatis quis?"
              )
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-lg-12" }, [
            _c("h2", { staticClass: "page-header" }, [_vm._v("Our Team")])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4 text-center" }, [
            _c("div", { staticClass: "thumbnail" }, [
              _c("img", {
                staticClass: "img-responsive",
                attrs: { src: "http://placehold.it/750x450", alt: "" }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "caption" }, [
                _c("h3", [
                  _vm._v("John Smith"),
                  _c("br"),
                  _vm._v(" "),
                  _c("small", [_vm._v("Job Title")])
                ]),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste saepe et quisquam nesciunt maxime."
                  )
                ]),
                _vm._v(" "),
                _c("ul", { staticClass: "list-inline" }, [
                  _c("li", [
                    _c("a", { attrs: { href: "#" } }, [
                      _c("i", { staticClass: "fa fa-2x fa-facebook-square" })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c("a", { attrs: { href: "#" } }, [
                      _c("i", { staticClass: "fa fa-2x fa-linkedin-square" })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c("a", { attrs: { href: "#" } }, [
                      _c("i", { staticClass: "fa fa-2x fa-twitter-square" })
                    ])
                  ])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4 text-center" }, [
            _c("div", { staticClass: "thumbnail" }, [
              _c("img", {
                staticClass: "img-responsive",
                attrs: { src: "http://placehold.it/750x450", alt: "" }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "caption" }, [
                _c("h3", [
                  _vm._v("John Smith"),
                  _c("br"),
                  _vm._v(" "),
                  _c("small", [_vm._v("Job Title")])
                ]),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste saepe et quisquam nesciunt maxime."
                  )
                ]),
                _vm._v(" "),
                _c("ul", { staticClass: "list-inline" }, [
                  _c("li", [
                    _c("a", { attrs: { href: "#" } }, [
                      _c("i", { staticClass: "fa fa-2x fa-facebook-square" })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c("a", { attrs: { href: "#" } }, [
                      _c("i", { staticClass: "fa fa-2x fa-linkedin-square" })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c("a", { attrs: { href: "#" } }, [
                      _c("i", { staticClass: "fa fa-2x fa-twitter-square" })
                    ])
                  ])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4 text-center" }, [
            _c("div", { staticClass: "thumbnail" }, [
              _c("img", {
                staticClass: "img-responsive",
                attrs: { src: "http://placehold.it/750x450", alt: "" }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "caption" }, [
                _c("h3", [
                  _vm._v("John Smith"),
                  _c("br"),
                  _vm._v(" "),
                  _c("small", [_vm._v("Job Title")])
                ]),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste saepe et quisquam nesciunt maxime."
                  )
                ]),
                _vm._v(" "),
                _c("ul", { staticClass: "list-inline" }, [
                  _c("li", [
                    _c("a", { attrs: { href: "#" } }, [
                      _c("i", { staticClass: "fa fa-2x fa-facebook-square" })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c("a", { attrs: { href: "#" } }, [
                      _c("i", { staticClass: "fa fa-2x fa-linkedin-square" })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c("a", { attrs: { href: "#" } }, [
                      _c("i", { staticClass: "fa fa-2x fa-twitter-square" })
                    ])
                  ])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4 text-center" }, [
            _c("div", { staticClass: "thumbnail" }, [
              _c("img", {
                staticClass: "img-responsive",
                attrs: { src: "http://placehold.it/750x450", alt: "" }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "caption" }, [
                _c("h3", [
                  _vm._v("John Smith"),
                  _c("br"),
                  _vm._v(" "),
                  _c("small", [_vm._v("Job Title")])
                ]),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste saepe et quisquam nesciunt maxime."
                  )
                ]),
                _vm._v(" "),
                _c("ul", { staticClass: "list-inline" }, [
                  _c("li", [
                    _c("a", { attrs: { href: "#" } }, [
                      _c("i", { staticClass: "fa fa-2x fa-facebook-square" })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c("a", { attrs: { href: "#" } }, [
                      _c("i", { staticClass: "fa fa-2x fa-linkedin-square" })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c("a", { attrs: { href: "#" } }, [
                      _c("i", { staticClass: "fa fa-2x fa-twitter-square" })
                    ])
                  ])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4 text-center" }, [
            _c("div", { staticClass: "thumbnail" }, [
              _c("img", {
                staticClass: "img-responsive",
                attrs: { src: "http://placehold.it/750x450", alt: "" }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "caption" }, [
                _c("h3", [
                  _vm._v("John Smith"),
                  _c("br"),
                  _vm._v(" "),
                  _c("small", [_vm._v("Job Title")])
                ]),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste saepe et quisquam nesciunt maxime."
                  )
                ]),
                _vm._v(" "),
                _c("ul", { staticClass: "list-inline" }, [
                  _c("li", [
                    _c("a", { attrs: { href: "#" } }, [
                      _c("i", { staticClass: "fa fa-2x fa-facebook-square" })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c("a", { attrs: { href: "#" } }, [
                      _c("i", { staticClass: "fa fa-2x fa-linkedin-square" })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c("a", { attrs: { href: "#" } }, [
                      _c("i", { staticClass: "fa fa-2x fa-twitter-square" })
                    ])
                  ])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4 text-center" }, [
            _c("div", { staticClass: "thumbnail" }, [
              _c("img", {
                staticClass: "img-responsive",
                attrs: { src: "http://placehold.it/750x450", alt: "" }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "caption" }, [
                _c("h3", [
                  _vm._v("John Smith"),
                  _c("br"),
                  _vm._v(" "),
                  _c("small", [_vm._v("Job Title")])
                ]),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste saepe et quisquam nesciunt maxime."
                  )
                ]),
                _vm._v(" "),
                _c("ul", { staticClass: "list-inline" }, [
                  _c("li", [
                    _c("a", { attrs: { href: "#" } }, [
                      _c("i", { staticClass: "fa fa-2x fa-facebook-square" })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c("a", { attrs: { href: "#" } }, [
                      _c("i", { staticClass: "fa fa-2x fa-linkedin-square" })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c("a", { attrs: { href: "#" } }, [
                      _c("i", { staticClass: "fa fa-2x fa-twitter-square" })
                    ])
                  ])
                ])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-lg-12" }, [
            _c("h2", { staticClass: "page-header" }, [_vm._v("Our Customers")])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-2 col-sm-4 col-xs-6" }, [
            _c("img", {
              staticClass: "img-responsive customer-img",
              attrs: { src: "http://placehold.it/500x300", alt: "" }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-2 col-sm-4 col-xs-6" }, [
            _c("img", {
              staticClass: "img-responsive customer-img",
              attrs: { src: "http://placehold.it/500x300", alt: "" }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-2 col-sm-4 col-xs-6" }, [
            _c("img", {
              staticClass: "img-responsive customer-img",
              attrs: { src: "http://placehold.it/500x300", alt: "" }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-2 col-sm-4 col-xs-6" }, [
            _c("img", {
              staticClass: "img-responsive customer-img",
              attrs: { src: "http://placehold.it/500x300", alt: "" }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-2 col-sm-4 col-xs-6" }, [
            _c("img", {
              staticClass: "img-responsive customer-img",
              attrs: { src: "http://placehold.it/500x300", alt: "" }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-2 col-sm-4 col-xs-6" }, [
            _c("img", {
              staticClass: "img-responsive customer-img",
              attrs: { src: "http://placehold.it/500x300", alt: "" }
            })
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0d1e7b74", module.exports)
  }
}

/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__(53)
/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/vue/pages/contact.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-62c61d19", Component.options)
  } else {
    hotAPI.reload("data-v-62c61d19", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { attrs: { id: "pages-contact-component" } }, [
      _c("div", { staticClass: "container" }, [
        _c("h1", { staticClass: "mt-4 mb-3" }, [
          _vm._v("Contact\n        "),
          _c("small", [_vm._v("Subheading")])
        ]),
        _vm._v(" "),
        _c("ol", { staticClass: "breadcrumb" }, [
          _c("li", { staticClass: "breadcrumb-item" }, [
            _c("a", { attrs: { href: "index.html" } }, [_vm._v("Home")])
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "breadcrumb-item active" }, [
            _vm._v("Contact")
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-lg-8 mb-4" }, [
            _c("iframe", {
              attrs: {
                width: "100%",
                height: "400px",
                frameborder: "0",
                scrolling: "no",
                marginheight: "0",
                marginwidth: "0",
                src:
                  "http://maps.google.com/maps?hl=en&ie=UTF8&ll=37.0625,-95.677068&spn=56.506174,79.013672&t=m&z=4&output=embed"
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-lg-4 mb-4" }, [
            _c("h3", [_vm._v("Contact Details")]),
            _vm._v(" "),
            _c("p", [
              _vm._v("\n            3481 Melrose Place\n            "),
              _c("br"),
              _vm._v("Beverly Hills, CA 90210\n            "),
              _c("br")
            ]),
            _vm._v(" "),
            _c("p", [
              _c("abbr", { attrs: { title: "Phone" } }, [_vm._v("P")]),
              _vm._v(": (123) 456-7890\n          ")
            ]),
            _vm._v(" "),
            _c("p", [
              _c("abbr", { attrs: { title: "Email" } }, [_vm._v("E")]),
              _vm._v(":\n            "),
              _c("a", { attrs: { href: "mailto:name@example.com" } }, [
                _vm._v("name@example.com\n            ")
              ])
            ]),
            _vm._v(" "),
            _c("p", [
              _c("abbr", { attrs: { title: "Hours" } }, [_vm._v("H")]),
              _vm._v(": Monday - Friday: 9:00 AM to 5:00 PM\n          ")
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-lg-8 mb-4" }, [
            _c("h3", [_vm._v("Send us a Message")]),
            _vm._v(" "),
            _c(
              "form",
              {
                attrs: {
                  name: "sentMessage",
                  id: "contactForm",
                  novalidate: ""
                }
              },
              [
                _c("div", { staticClass: "control-group form-group" }, [
                  _c("div", { staticClass: "controls" }, [
                    _c("label", [_vm._v("Full Name:")]),
                    _vm._v(" "),
                    _c("input", {
                      staticClass: "form-control",
                      attrs: {
                        type: "text",
                        id: "name",
                        required: "",
                        "data-validation-required-message":
                          "Please enter your name."
                      }
                    }),
                    _vm._v(" "),
                    _c("p", { staticClass: "help-block" })
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "control-group form-group" }, [
                  _c("div", { staticClass: "controls" }, [
                    _c("label", [_vm._v("Phone Number:")]),
                    _vm._v(" "),
                    _c("input", {
                      staticClass: "form-control",
                      attrs: {
                        type: "tel",
                        id: "phone",
                        required: "",
                        "data-validation-required-message":
                          "Please enter your phone number."
                      }
                    }),
                    _vm._v(" "),
                    _c("div", { staticClass: "help-block" })
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "control-group form-group" }, [
                  _c("div", { staticClass: "controls" }, [
                    _c("label", [_vm._v("Email Address:")]),
                    _vm._v(" "),
                    _c("input", {
                      staticClass: "form-control",
                      attrs: {
                        type: "email",
                        id: "email",
                        required: "",
                        "data-validation-required-message":
                          "Please enter your email address."
                      }
                    }),
                    _vm._v(" "),
                    _c("div", { staticClass: "help-block" })
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "control-group form-group" }, [
                  _c("div", { staticClass: "controls" }, [
                    _c("label", [_vm._v("Message:")]),
                    _vm._v(" "),
                    _c("textarea", {
                      staticClass: "form-control",
                      staticStyle: { resize: "none" },
                      attrs: {
                        rows: "10",
                        cols: "100",
                        id: "message",
                        required: "",
                        "data-validation-required-message":
                          "Please enter your message",
                        maxlength: "999"
                      }
                    }),
                    _vm._v(" "),
                    _c("div", { staticClass: "help-block" })
                  ])
                ]),
                _vm._v(" "),
                _c("div", { attrs: { id: "success" } }),
                _vm._v(" "),
                _c(
                  "button",
                  { staticClass: "btn btn-primary", attrs: { type: "submit" } },
                  [_vm._v("Send Message")]
                )
              ]
            )
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-62c61d19", module.exports)
  }
}

/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */
var __vue_script__ = __webpack_require__(55)
/* template */
var __vue_template__ = __webpack_require__(56)
/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/vue/layouts/portal.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3e497cc4", Component.options)
  } else {
    hotAPI.reload("data-v-3e497cc4", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 55 */
/***/ (function(module, exports) {

//
//
//
//
//
//

/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { attrs: { id: "layouts-portal" } }, [_c("router-view")], 1)
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3e497cc4", module.exports)
  }
}

/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(58)
}
var normalizeComponent = __webpack_require__(1)
/* script */
var __vue_script__ = __webpack_require__(60)
/* template */
var __vue_template__ = __webpack_require__(61)
/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/vue/auth/login.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3a3db52e", Component.options)
  } else {
    hotAPI.reload("data-v-3a3db52e", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(59);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("7e837cb4", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js?sourceMap!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3a3db52e\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0&bustCache!./login.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js?sourceMap!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3a3db52e\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0&bustCache!./login.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)(true);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n/*!\n * Start Bootstrap - SB Admin 2 v3.3.7+1 (http://startbootstrap.com/template-overviews/sb-admin-2)\n * Copyright 2013-2016 Start Bootstrap\n * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap/blob/gh-pages/LICENSE)\n */\nbody {\n \tbackground-color: #f8f8f8;\n}\n#wrapper {\n \twidth: 100%;\n}\n#page-wrapper {\n \tpadding: 0 15px;\n \tmin-height: 568px;\n \tbackground-color: white;\n}\n@media (min-width: 768px) {\n#page-wrapper {\n \t\tposition: inherit;\n \t\tmargin: 0 0 0 250px;\n \t\tpadding: 0 30px;\n \t\tborder-left: 1px solid #e7e7e7;\n}\n}\n.navbar-top-links {\n \tmargin-right: 0;\n}\n.navbar-top-links li {\n \tdisplay: inline-block;\n}\n.navbar-top-links li:last-child {\n \tmargin-right: 15px;\n}\n.navbar-top-links li a {\n \tpadding: 15px;\n \tmin-height: 50px;\n}\n.navbar-top-links .dropdown-menu li {\n \tdisplay: block;\n}\n.navbar-top-links .dropdown-menu li:last-child {\n \tmargin-right: 0;\n}\n.navbar-top-links .dropdown-menu li a {\n \tpadding: 3px 20px;\n \tmin-height: 0;\n}\n.navbar-top-links .dropdown-menu li a div {\n \twhite-space: normal;\n}\n.navbar-top-links .dropdown-messages,\n .navbar-top-links .dropdown-tasks,\n .navbar-top-links .dropdown-alerts {\n \twidth: 310px;\n \tmin-width: 0;\n}\n.navbar-top-links .dropdown-messages {\n \tmargin-left: 5px;\n}\n.navbar-top-links .dropdown-tasks {\n \tmargin-left: -59px;\n}\n.navbar-top-links .dropdown-alerts {\n \tmargin-left: -123px;\n}\n.navbar-top-links .dropdown-user {\n \tright: 0;\n \tleft: auto;\n}\n.sidebar .sidebar-nav.navbar-collapse {\n \tpadding-left: 0;\n \tpadding-right: 0;\n}\n.sidebar .sidebar-search {\n \tpadding: 15px;\n}\n.sidebar ul li {\n \tborder-bottom: 1px solid #e7e7e7;\n}\n.sidebar ul li a.active {\n \tbackground-color: #eeeeee;\n}\n.sidebar .arrow {\n \tfloat: right;\n}\n.sidebar .fa.arrow:before {\n \tcontent: \"\\F104\";\n}\n.sidebar .active > a > .fa.arrow:before {\n \tcontent: \"\\F107\";\n}\n.sidebar .nav-second-level li,\n .sidebar .nav-third-level li {\n \tborder-bottom: none !important;\n}\n.sidebar .nav-second-level li a {\n \tpadding-left: 37px;\n}\n.sidebar .nav-third-level li a {\n \tpadding-left: 52px;\n}\n@media (min-width: 768px) {\n.sidebar {\n \t\tz-index: 1;\n \t\tposition: absolute;\n \t\twidth: 250px;\n \t\tmargin-top: 51px;\n}\n.navbar-top-links .dropdown-messages,\n \t.navbar-top-links .dropdown-tasks,\n \t.navbar-top-links .dropdown-alerts {\n \t\tmargin-left: auto;\n}\n}\n.btn-outline {\n \tcolor: inherit;\n \tbackground-color: transparent;\n \ttransition: all .5s;\n}\n.btn-primary.btn-outline {\n \tcolor: #428bca;\n}\n.btn-success.btn-outline {\n \tcolor: #5cb85c;\n}\n.btn-info.btn-outline {\n \tcolor: #5bc0de;\n}\n.btn-warning.btn-outline {\n \tcolor: #f0ad4e;\n}\n.btn-danger.btn-outline {\n \tcolor: #d9534f;\n}\n.btn-primary.btn-outline:hover,\n .btn-success.btn-outline:hover,\n .btn-info.btn-outline:hover,\n .btn-warning.btn-outline:hover,\n .btn-danger.btn-outline:hover {\n \tcolor: white;\n}\n.chat {\n \tmargin: 0;\n \tpadding: 0;\n \tlist-style: none;\n}\n.chat li {\n \tmargin-bottom: 10px;\n \tpadding-bottom: 5px;\n \tborder-bottom: 1px dotted #999999;\n}\n.chat li.left .chat-body {\n \tmargin-left: 60px;\n}\n.chat li.right .chat-body {\n \tmargin-right: 60px;\n}\n.chat li .chat-body p {\n \tmargin: 0;\n}\n.panel .slidedown .glyphicon,\n .chat .glyphicon {\n \tmargin-right: 5px;\n}\n.chat-panel .panel-body {\n \theight: 350px;\n \toverflow-y: scroll;\n}\n.login-panel {\n \tmargin-top: 25%;\n}\n.flot-chart {\n \tdisplay: block;\n \theight: 400px;\n}\n.flot-chart-content {\n \twidth: 100%;\n \theight: 100%;\n}\ntable.dataTable thead .sorting,\n table.dataTable thead .sorting_asc,\n table.dataTable thead .sorting_desc,\n table.dataTable thead .sorting_asc_disabled,\n table.dataTable thead .sorting_desc_disabled {\n \tbackground: transparent;\n}\ntable.dataTable thead .sorting_asc:after {\n \tcontent: \"\\F0DE\";\n \tfloat: right;\n \tfont-family: fontawesome;\n}\ntable.dataTable thead .sorting_desc:after {\n \tcontent: \"\\F0DD\";\n \tfloat: right;\n \tfont-family: fontawesome;\n}\ntable.dataTable thead .sorting:after {\n \tcontent: \"\\F0DC\";\n \tfloat: right;\n \tfont-family: fontawesome;\n \tcolor: rgba(50, 50, 50, 0.5);\n}\n.btn-circle {\n \twidth: 30px;\n \theight: 30px;\n \tpadding: 6px 0;\n \tborder-radius: 15px;\n \ttext-align: center;\n \tfont-size: 12px;\n \tline-height: 1.428571429;\n}\n.btn-circle.btn-lg {\n \twidth: 50px;\n \theight: 50px;\n \tpadding: 10px 16px;\n \tborder-radius: 25px;\n \tfont-size: 18px;\n \tline-height: 1.33;\n}\n.btn-circle.btn-xl {\n \twidth: 70px;\n \theight: 70px;\n \tpadding: 10px 16px;\n \tborder-radius: 35px;\n \tfont-size: 24px;\n \tline-height: 1.33;\n}\n.show-grid [class^=\"col-\"] {\n \tpadding-top: 10px;\n \tpadding-bottom: 10px;\n \tborder: 1px solid #ddd;\n \tbackground-color: #eee !important;\n}\n.show-grid {\n \tmargin: 15px 0;\n}\n.huge {\n \tfont-size: 40px;\n}\n.panel-green {\n \tborder-color: #5cb85c;\n}\n.panel-green > .panel-heading {\n \tborder-color: #5cb85c;\n \tcolor: white;\n \tbackground-color: #5cb85c;\n}\n.panel-green > a {\n \tcolor: #5cb85c;\n}\n.panel-green > a:hover {\n \tcolor: #3d8b3d;\n}\n.panel-red {\n \tborder-color: #d9534f;\n}\n.panel-red > .panel-heading {\n \tborder-color: #d9534f;\n \tcolor: white;\n \tbackground-color: #d9534f;\n}\n.panel-red > a {\n \tcolor: #d9534f;\n}\n.panel-red > a:hover {\n \tcolor: #b52b27;\n}\n.panel-yellow {\n \tborder-color: #f0ad4e;\n}\n.panel-yellow > .panel-heading {\n \tborder-color: #f0ad4e;\n \tcolor: white;\n \tbackground-color: #f0ad4e;\n}\n.panel-yellow > a {\n \tcolor: #f0ad4e;\n}\n.panel-yellow > a:hover {\n \tcolor: #df8a13;\n}\n.timeline {\n \tposition: relative;\n \tpadding: 20px 0 20px;\n \tlist-style: none;\n}\n.timeline:before {\n \tcontent: \" \";\n \tposition: absolute;\n \ttop: 0;\n \tbottom: 0;\n \tleft: 50%;\n \twidth: 3px;\n \tmargin-left: -1.5px;\n \tbackground-color: #eeeeee;\n}\n.timeline > li {\n \tposition: relative;\n \tmargin-bottom: 20px;\n}\n.timeline > li:before,\n .timeline > li:after {\n \tcontent: \" \";\n \tdisplay: table;\n}\n.timeline > li:after {\n \tclear: both;\n}\n.timeline > li:before,\n .timeline > li:after {\n \tcontent: \" \";\n \tdisplay: table;\n}\n.timeline > li:after {\n \tclear: both;\n}\n.timeline > li > .timeline-panel {\n \tfloat: left;\n \tposition: relative;\n \twidth: 46%;\n \tpadding: 20px;\n \tborder: 1px solid #d4d4d4;\n \tborder-radius: 2px;\n \t-webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);\n \tbox-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);\n}\n.timeline > li > .timeline-panel:before {\n \tcontent: \" \";\n \tdisplay: inline-block;\n \tposition: absolute;\n \ttop: 26px;\n \tright: -15px;\n \tborder-top: 15px solid transparent;\n \tborder-right: 0 solid #ccc;\n \tborder-bottom: 15px solid transparent;\n \tborder-left: 15px solid #ccc;\n}\n.timeline > li > .timeline-panel:after {\n \tcontent: \" \";\n \tdisplay: inline-block;\n \tposition: absolute;\n \ttop: 27px;\n \tright: -14px;\n \tborder-top: 14px solid transparent;\n \tborder-right: 0 solid #fff;\n \tborder-bottom: 14px solid transparent;\n \tborder-left: 14px solid #fff;\n}\n.timeline > li > .timeline-badge {\n \tz-index: 100;\n \tposition: absolute;\n \ttop: 16px;\n \tleft: 50%;\n \twidth: 50px;\n \theight: 50px;\n \tmargin-left: -25px;\n \tborder-radius: 50% 50% 50% 50%;\n \ttext-align: center;\n \tfont-size: 1.4em;\n \tline-height: 50px;\n \tcolor: #fff;\n \tbackground-color: #999999;\n}\n.timeline > li.timeline-inverted > .timeline-panel {\n \tfloat: right;\n}\n.timeline > li.timeline-inverted > .timeline-panel:before {\n \tright: auto;\n \tleft: -15px;\n \tborder-right-width: 15px;\n \tborder-left-width: 0;\n}\n.timeline > li.timeline-inverted > .timeline-panel:after {\n \tright: auto;\n \tleft: -14px;\n \tborder-right-width: 14px;\n \tborder-left-width: 0;\n}\n.timeline-badge.primary {\n \tbackground-color: #2e6da4 !important;\n}\n.timeline-badge.success {\n \tbackground-color: #3f903f !important;\n}\n.timeline-badge.warning {\n \tbackground-color: #f0ad4e !important;\n}\n.timeline-badge.danger {\n \tbackground-color: #d9534f !important;\n}\n.timeline-badge.info {\n \tbackground-color: #5bc0de !important;\n}\n.timeline-title {\n \tmargin-top: 0;\n \tcolor: inherit;\n}\n.timeline-body > p,\n .timeline-body > ul {\n \tmargin-bottom: 0;\n}\n.timeline-body > p + p {\n \tmargin-top: 5px;\n}\n@media (max-width: 767px) {\nul.timeline:before {\n \t\tleft: 40px;\n}\nul.timeline > li > .timeline-panel {\n \t\twidth: calc(10%);\n \t\twidth: -moz-calc(10%);\n \t\twidth: -webkit-calc(10%);\n}\nul.timeline > li > .timeline-badge {\n \t\ttop: 16px;\n \t\tleft: 15px;\n \t\tmargin-left: 0;\n}\nul.timeline > li > .timeline-panel {\n \t\tfloat: right;\n}\nul.timeline > li > .timeline-panel:before {\n \t\tright: auto;\n \t\tleft: -15px;\n \t\tborder-right-width: 15px;\n \t\tborder-left-width: 0;\n}\nul.timeline > li > .timeline-panel:after {\n \t\tright: auto;\n \t\tleft: -14px;\n \t\tborder-right-width: 14px;\n \t\tborder-left-width: 0;\n}\n}\n\n \n ", "", {"version":3,"sources":["/Users/villamornantoniojr/Development/greyfox/matchup/web/resources/js/vue/auth/resources/js/vue/auth/login.vue?3152aae8"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;AAyGA;;;;GAIA;AACA;EACA,0BAAA;CACA;AACA;EACA,YAAA;CACA;AACA;EACA,gBAAA;EACA,kBAAA;EACA,wBAAA;CACA;AACA;AACA;GACA,kBAAA;GACA,oBAAA;GACA,gBAAA;GACA,+BAAA;CACA;CACA;AACA;EACA,gBAAA;CACA;AACA;EACA,sBAAA;CACA;AACA;EACA,mBAAA;CACA;AACA;EACA,cAAA;EACA,iBAAA;CACA;AACA;EACA,eAAA;CACA;AACA;EACA,gBAAA;CACA;AACA;EACA,kBAAA;EACA,cAAA;CACA;AACA;EACA,oBAAA;CACA;AACA;;;EAGA,aAAA;EACA,aAAA;CACA;AACA;EACA,iBAAA;CACA;AACA;EACA,mBAAA;CACA;AACA;EACA,oBAAA;CACA;AACA;EACA,SAAA;EACA,WAAA;CACA;AACA;EACA,gBAAA;EACA,iBAAA;CACA;AACA;EACA,cAAA;CACA;AACA;EACA,iCAAA;CACA;AACA;EACA,0BAAA;CACA;AACA;EACA,aAAA;CACA;AACA;EACA,iBAAA;CACA;AACA;EACA,iBAAA;CACA;AACA;;EAEA,+BAAA;CACA;AACA;EACA,mBAAA;CACA;AACA;EACA,mBAAA;CACA;AACA;AACA;GACA,WAAA;GACA,mBAAA;GACA,aAAA;GACA,iBAAA;CACA;AACA;;;GAGA,kBAAA;CACA;CACA;AACA;EACA,eAAA;EACA,8BAAA;EACA,oBAAA;CACA;AACA;EACA,eAAA;CACA;AACA;EACA,eAAA;CACA;AACA;EACA,eAAA;CACA;AACA;EACA,eAAA;CACA;AACA;EACA,eAAA;CACA;AACA;;;;;EAKA,aAAA;CACA;AACA;EACA,UAAA;EACA,WAAA;EACA,iBAAA;CACA;AACA;EACA,oBAAA;EACA,oBAAA;EACA,kCAAA;CACA;AACA;EACA,kBAAA;CACA;AACA;EACA,mBAAA;CACA;AACA;EACA,UAAA;CACA;AACA;;EAEA,kBAAA;CACA;AACA;EACA,cAAA;EACA,mBAAA;CACA;AACA;EACA,gBAAA;CACA;AACA;EACA,eAAA;EACA,cAAA;CACA;AACA;EACA,YAAA;EACA,aAAA;CACA;AACA;;;;;EAKA,wBAAA;CACA;AACA;EACA,iBAAA;EACA,aAAA;EACA,yBAAA;CACA;AACA;EACA,iBAAA;EACA,aAAA;EACA,yBAAA;CACA;AACA;EACA,iBAAA;EACA,aAAA;EACA,yBAAA;EACA,6BAAA;CACA;AACA;EACA,YAAA;EACA,aAAA;EACA,eAAA;EACA,oBAAA;EACA,mBAAA;EACA,gBAAA;EACA,yBAAA;CACA;AACA;EACA,YAAA;EACA,aAAA;EACA,mBAAA;EACA,oBAAA;EACA,gBAAA;EACA,kBAAA;CACA;AACA;EACA,YAAA;EACA,aAAA;EACA,mBAAA;EACA,oBAAA;EACA,gBAAA;EACA,kBAAA;CACA;AACA;EACA,kBAAA;EACA,qBAAA;EACA,uBAAA;EACA,kCAAA;CACA;AACA;EACA,eAAA;CACA;AACA;EACA,gBAAA;CACA;AACA;EACA,sBAAA;CACA;AACA;EACA,sBAAA;EACA,aAAA;EACA,0BAAA;CACA;AACA;EACA,eAAA;CACA;AACA;EACA,eAAA;CACA;AACA;EACA,sBAAA;CACA;AACA;EACA,sBAAA;EACA,aAAA;EACA,0BAAA;CACA;AACA;EACA,eAAA;CACA;AACA;EACA,eAAA;CACA;AACA;EACA,sBAAA;CACA;AACA;EACA,sBAAA;EACA,aAAA;EACA,0BAAA;CACA;AACA;EACA,eAAA;CACA;AACA;EACA,eAAA;CACA;AACA;EACA,mBAAA;EACA,qBAAA;EACA,iBAAA;CACA;AACA;EACA,aAAA;EACA,mBAAA;EACA,OAAA;EACA,UAAA;EACA,UAAA;EACA,WAAA;EACA,oBAAA;EACA,0BAAA;CACA;AACA;EACA,mBAAA;EACA,oBAAA;CACA;AACA;;EAEA,aAAA;EACA,eAAA;CACA;AACA;EACA,YAAA;CACA;AACA;;EAEA,aAAA;EACA,eAAA;CACA;AACA;EACA,YAAA;CACA;AACA;EACA,YAAA;EACA,mBAAA;EACA,WAAA;EACA,cAAA;EACA,0BAAA;EACA,mBAAA;EACA,mDAAA;EACA,2CAAA;CACA;AACA;EACA,aAAA;EACA,sBAAA;EACA,mBAAA;EACA,UAAA;EACA,aAAA;EACA,mCAAA;EACA,2BAAA;EACA,sCAAA;EACA,6BAAA;CACA;AACA;EACA,aAAA;EACA,sBAAA;EACA,mBAAA;EACA,UAAA;EACA,aAAA;EACA,mCAAA;EACA,2BAAA;EACA,sCAAA;EACA,6BAAA;CACA;AACA;EACA,aAAA;EACA,mBAAA;EACA,UAAA;EACA,UAAA;EACA,YAAA;EACA,aAAA;EACA,mBAAA;EACA,+BAAA;EACA,mBAAA;EACA,iBAAA;EACA,kBAAA;EACA,YAAA;EACA,0BAAA;CACA;AACA;EACA,aAAA;CACA;AACA;EACA,YAAA;EACA,YAAA;EACA,yBAAA;EACA,qBAAA;CACA;AACA;EACA,YAAA;EACA,YAAA;EACA,yBAAA;EACA,qBAAA;CACA;AACA;EACA,qCAAA;CACA;AACA;EACA,qCAAA;CACA;AACA;EACA,qCAAA;CACA;AACA;EACA,qCAAA;CACA;AACA;EACA,qCAAA;CACA;AACA;EACA,cAAA;EACA,eAAA;CACA;AACA;;EAEA,iBAAA;CACA;AACA;EACA,gBAAA;CACA;AACA;AACA;GACA,WAAA;CACA;AACA;GACA,iBAAA;GACA,sBAAA;GACA,yBAAA;CACA;AACA;GACA,UAAA;GACA,WAAA;GACA,eAAA;CACA;AACA;GACA,aAAA;CACA;AACA;GACA,YAAA;GACA,YAAA;GACA,yBAAA;GACA,qBAAA;CACA;AACA;GACA,YAAA;GACA,YAAA;GACA,yBAAA;GACA,qBAAA;CACA;CACA","file":"login.vue","sourcesContent":["<template>\n\t<div id=\"auth-login-component\">\n\t\t\n\n\t\t<div class=\"container\">\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-md-4 col-md-offset-4\">\n\t\t\t\t\t<div class=\"login-panel panel panel-default\">\n\t\t\t\t\t\t<div class=\"panel-heading\">\n\t\t\t\t\t\t\t<h3 class=\"panel-title\">Please Sign In</h3>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"panel-body\">\n\t\t\t\t\t\t\t<form role=\"form\">\n\t\t\t\t\t\t\t\t<fieldset>\n\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t\t<input class=\"form-control\" v-model='form.username' placeholder=\"E-mail\" name=\"email\" type=\"email\" autofocus=\"\">\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t\t<input class=\"form-control\" v-model='form.password' placeholder=\"Password\" name=\"password\" type=\"password\" value=\"\">\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"checkbox\">\n\t\t\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t\t\t<input name=\"remember\" type=\"checkbox\" value=\"Remember Me\">Remember Me\n\t\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<!-- Change this to a button or input when using this as a form -->\n\t\t\t\t\t\t\t\t\t<a v-on:click=\"login\" class=\"btn btn-lg btn-success btn-block\">Login</a>\n\t\t\t\t\t\t\t\t</fieldset>\n\t\t\t\t\t\t\t</form>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\n\n\n\n\t\t\n\n\n\t\t\n\t</div>\n</template>\n\n<script>\nexport default {\n\n\tdata(){\n\t\treturn {\n            form : new Form({\n                username: '',\n                password: ''\n            }),\n            messages: {}\n        }\n    },\n    computed: {},\n\n    created(){\n        //DO SOMETHING HERE AT FIRST LOAD\n        let that = this;\n        if(that.$store.state.isAuthenticated){\n        \tthat.$router.push('/dashboard');\n\n        }\n    },\n    methods: {\n    \tlogin(){\n    \t\tlet that = this;\n    \t\tthat.form.post('/api/auth/token')\n    \t\t.then(function (response){\n    \t\t\tlocalStorage.setItem('access_token',response.access_token);\n    \t\t\tlocalStorage.setItem('refresh_token',response.refresh_token);\n    \t\t\tthat.getSetUserData();\n    \t\t})\n    \t\t.catch(function (error){\n    \t\t\tthat.form.reset();\n    \t\t\tthat.$router.app.$emit('notifyFailed','Login Failed!');\n\n    \t\t});\n    \t},\n    \tgetSetUserData: function () {\n    \t\tvar that = this;\n\n    \t\taxios.get('/api/user')\n    \t\t.then(function(response){\n    \t\t\tthat.$store.state.user = response.data.data;\n    \t\t\tthat.$store.state.isAuthenticated = true;\n    \t\t\tthat.$store.state.access_token = localStorage.getItem('access_token');\n    \t\t\tthat.$router.push('/dashboard');\n    \t\t}).catch(function(error){\n    \t\t\tconsole.log(error);\n\n    \t\t});\n\n    \t}\n    }\n\n\n};</script>\n\n\n<style type=\"text/css\" media=\"screen\">\n\n/*!\n * Start Bootstrap - SB Admin 2 v3.3.7+1 (http://startbootstrap.com/template-overviews/sb-admin-2)\n * Copyright 2013-2016 Start Bootstrap\n * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap/blob/gh-pages/LICENSE)\n */\n body {\n \tbackground-color: #f8f8f8;\n }\n #wrapper {\n \twidth: 100%;\n }\n #page-wrapper {\n \tpadding: 0 15px;\n \tmin-height: 568px;\n \tbackground-color: white;\n }\n @media (min-width: 768px) {\n \t#page-wrapper {\n \t\tposition: inherit;\n \t\tmargin: 0 0 0 250px;\n \t\tpadding: 0 30px;\n \t\tborder-left: 1px solid #e7e7e7;\n \t}\n }\n .navbar-top-links {\n \tmargin-right: 0;\n }\n .navbar-top-links li {\n \tdisplay: inline-block;\n }\n .navbar-top-links li:last-child {\n \tmargin-right: 15px;\n }\n .navbar-top-links li a {\n \tpadding: 15px;\n \tmin-height: 50px;\n }\n .navbar-top-links .dropdown-menu li {\n \tdisplay: block;\n }\n .navbar-top-links .dropdown-menu li:last-child {\n \tmargin-right: 0;\n }\n .navbar-top-links .dropdown-menu li a {\n \tpadding: 3px 20px;\n \tmin-height: 0;\n }\n .navbar-top-links .dropdown-menu li a div {\n \twhite-space: normal;\n }\n .navbar-top-links .dropdown-messages,\n .navbar-top-links .dropdown-tasks,\n .navbar-top-links .dropdown-alerts {\n \twidth: 310px;\n \tmin-width: 0;\n }\n .navbar-top-links .dropdown-messages {\n \tmargin-left: 5px;\n }\n .navbar-top-links .dropdown-tasks {\n \tmargin-left: -59px;\n }\n .navbar-top-links .dropdown-alerts {\n \tmargin-left: -123px;\n }\n .navbar-top-links .dropdown-user {\n \tright: 0;\n \tleft: auto;\n }\n .sidebar .sidebar-nav.navbar-collapse {\n \tpadding-left: 0;\n \tpadding-right: 0;\n }\n .sidebar .sidebar-search {\n \tpadding: 15px;\n }\n .sidebar ul li {\n \tborder-bottom: 1px solid #e7e7e7;\n }\n .sidebar ul li a.active {\n \tbackground-color: #eeeeee;\n }\n .sidebar .arrow {\n \tfloat: right;\n }\n .sidebar .fa.arrow:before {\n \tcontent: \"\\f104\";\n }\n .sidebar .active > a > .fa.arrow:before {\n \tcontent: \"\\f107\";\n }\n .sidebar .nav-second-level li,\n .sidebar .nav-third-level li {\n \tborder-bottom: none !important;\n }\n .sidebar .nav-second-level li a {\n \tpadding-left: 37px;\n }\n .sidebar .nav-third-level li a {\n \tpadding-left: 52px;\n }\n @media (min-width: 768px) {\n \t.sidebar {\n \t\tz-index: 1;\n \t\tposition: absolute;\n \t\twidth: 250px;\n \t\tmargin-top: 51px;\n \t}\n \t.navbar-top-links .dropdown-messages,\n \t.navbar-top-links .dropdown-tasks,\n \t.navbar-top-links .dropdown-alerts {\n \t\tmargin-left: auto;\n \t}\n }\n .btn-outline {\n \tcolor: inherit;\n \tbackground-color: transparent;\n \ttransition: all .5s;\n }\n .btn-primary.btn-outline {\n \tcolor: #428bca;\n }\n .btn-success.btn-outline {\n \tcolor: #5cb85c;\n }\n .btn-info.btn-outline {\n \tcolor: #5bc0de;\n }\n .btn-warning.btn-outline {\n \tcolor: #f0ad4e;\n }\n .btn-danger.btn-outline {\n \tcolor: #d9534f;\n }\n .btn-primary.btn-outline:hover,\n .btn-success.btn-outline:hover,\n .btn-info.btn-outline:hover,\n .btn-warning.btn-outline:hover,\n .btn-danger.btn-outline:hover {\n \tcolor: white;\n }\n .chat {\n \tmargin: 0;\n \tpadding: 0;\n \tlist-style: none;\n }\n .chat li {\n \tmargin-bottom: 10px;\n \tpadding-bottom: 5px;\n \tborder-bottom: 1px dotted #999999;\n }\n .chat li.left .chat-body {\n \tmargin-left: 60px;\n }\n .chat li.right .chat-body {\n \tmargin-right: 60px;\n }\n .chat li .chat-body p {\n \tmargin: 0;\n }\n .panel .slidedown .glyphicon,\n .chat .glyphicon {\n \tmargin-right: 5px;\n }\n .chat-panel .panel-body {\n \theight: 350px;\n \toverflow-y: scroll;\n }\n .login-panel {\n \tmargin-top: 25%;\n }\n .flot-chart {\n \tdisplay: block;\n \theight: 400px;\n }\n .flot-chart-content {\n \twidth: 100%;\n \theight: 100%;\n }\n table.dataTable thead .sorting,\n table.dataTable thead .sorting_asc,\n table.dataTable thead .sorting_desc,\n table.dataTable thead .sorting_asc_disabled,\n table.dataTable thead .sorting_desc_disabled {\n \tbackground: transparent;\n }\n table.dataTable thead .sorting_asc:after {\n \tcontent: \"\\f0de\";\n \tfloat: right;\n \tfont-family: fontawesome;\n }\n table.dataTable thead .sorting_desc:after {\n \tcontent: \"\\f0dd\";\n \tfloat: right;\n \tfont-family: fontawesome;\n }\n table.dataTable thead .sorting:after {\n \tcontent: \"\\f0dc\";\n \tfloat: right;\n \tfont-family: fontawesome;\n \tcolor: rgba(50, 50, 50, 0.5);\n }\n .btn-circle {\n \twidth: 30px;\n \theight: 30px;\n \tpadding: 6px 0;\n \tborder-radius: 15px;\n \ttext-align: center;\n \tfont-size: 12px;\n \tline-height: 1.428571429;\n }\n .btn-circle.btn-lg {\n \twidth: 50px;\n \theight: 50px;\n \tpadding: 10px 16px;\n \tborder-radius: 25px;\n \tfont-size: 18px;\n \tline-height: 1.33;\n }\n .btn-circle.btn-xl {\n \twidth: 70px;\n \theight: 70px;\n \tpadding: 10px 16px;\n \tborder-radius: 35px;\n \tfont-size: 24px;\n \tline-height: 1.33;\n }\n .show-grid [class^=\"col-\"] {\n \tpadding-top: 10px;\n \tpadding-bottom: 10px;\n \tborder: 1px solid #ddd;\n \tbackground-color: #eee !important;\n }\n .show-grid {\n \tmargin: 15px 0;\n }\n .huge {\n \tfont-size: 40px;\n }\n .panel-green {\n \tborder-color: #5cb85c;\n }\n .panel-green > .panel-heading {\n \tborder-color: #5cb85c;\n \tcolor: white;\n \tbackground-color: #5cb85c;\n }\n .panel-green > a {\n \tcolor: #5cb85c;\n }\n .panel-green > a:hover {\n \tcolor: #3d8b3d;\n }\n .panel-red {\n \tborder-color: #d9534f;\n }\n .panel-red > .panel-heading {\n \tborder-color: #d9534f;\n \tcolor: white;\n \tbackground-color: #d9534f;\n }\n .panel-red > a {\n \tcolor: #d9534f;\n }\n .panel-red > a:hover {\n \tcolor: #b52b27;\n }\n .panel-yellow {\n \tborder-color: #f0ad4e;\n }\n .panel-yellow > .panel-heading {\n \tborder-color: #f0ad4e;\n \tcolor: white;\n \tbackground-color: #f0ad4e;\n }\n .panel-yellow > a {\n \tcolor: #f0ad4e;\n }\n .panel-yellow > a:hover {\n \tcolor: #df8a13;\n }\n .timeline {\n \tposition: relative;\n \tpadding: 20px 0 20px;\n \tlist-style: none;\n }\n .timeline:before {\n \tcontent: \" \";\n \tposition: absolute;\n \ttop: 0;\n \tbottom: 0;\n \tleft: 50%;\n \twidth: 3px;\n \tmargin-left: -1.5px;\n \tbackground-color: #eeeeee;\n }\n .timeline > li {\n \tposition: relative;\n \tmargin-bottom: 20px;\n }\n .timeline > li:before,\n .timeline > li:after {\n \tcontent: \" \";\n \tdisplay: table;\n }\n .timeline > li:after {\n \tclear: both;\n }\n .timeline > li:before,\n .timeline > li:after {\n \tcontent: \" \";\n \tdisplay: table;\n }\n .timeline > li:after {\n \tclear: both;\n }\n .timeline > li > .timeline-panel {\n \tfloat: left;\n \tposition: relative;\n \twidth: 46%;\n \tpadding: 20px;\n \tborder: 1px solid #d4d4d4;\n \tborder-radius: 2px;\n \t-webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);\n \tbox-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);\n }\n .timeline > li > .timeline-panel:before {\n \tcontent: \" \";\n \tdisplay: inline-block;\n \tposition: absolute;\n \ttop: 26px;\n \tright: -15px;\n \tborder-top: 15px solid transparent;\n \tborder-right: 0 solid #ccc;\n \tborder-bottom: 15px solid transparent;\n \tborder-left: 15px solid #ccc;\n }\n .timeline > li > .timeline-panel:after {\n \tcontent: \" \";\n \tdisplay: inline-block;\n \tposition: absolute;\n \ttop: 27px;\n \tright: -14px;\n \tborder-top: 14px solid transparent;\n \tborder-right: 0 solid #fff;\n \tborder-bottom: 14px solid transparent;\n \tborder-left: 14px solid #fff;\n }\n .timeline > li > .timeline-badge {\n \tz-index: 100;\n \tposition: absolute;\n \ttop: 16px;\n \tleft: 50%;\n \twidth: 50px;\n \theight: 50px;\n \tmargin-left: -25px;\n \tborder-radius: 50% 50% 50% 50%;\n \ttext-align: center;\n \tfont-size: 1.4em;\n \tline-height: 50px;\n \tcolor: #fff;\n \tbackground-color: #999999;\n }\n .timeline > li.timeline-inverted > .timeline-panel {\n \tfloat: right;\n }\n .timeline > li.timeline-inverted > .timeline-panel:before {\n \tright: auto;\n \tleft: -15px;\n \tborder-right-width: 15px;\n \tborder-left-width: 0;\n }\n .timeline > li.timeline-inverted > .timeline-panel:after {\n \tright: auto;\n \tleft: -14px;\n \tborder-right-width: 14px;\n \tborder-left-width: 0;\n }\n .timeline-badge.primary {\n \tbackground-color: #2e6da4 !important;\n }\n .timeline-badge.success {\n \tbackground-color: #3f903f !important;\n }\n .timeline-badge.warning {\n \tbackground-color: #f0ad4e !important;\n }\n .timeline-badge.danger {\n \tbackground-color: #d9534f !important;\n }\n .timeline-badge.info {\n \tbackground-color: #5bc0de !important;\n }\n .timeline-title {\n \tmargin-top: 0;\n \tcolor: inherit;\n }\n .timeline-body > p,\n .timeline-body > ul {\n \tmargin-bottom: 0;\n }\n .timeline-body > p + p {\n \tmargin-top: 5px;\n }\n @media (max-width: 767px) {\n \tul.timeline:before {\n \t\tleft: 40px;\n \t}\n \tul.timeline > li > .timeline-panel {\n \t\twidth: calc(10%);\n \t\twidth: -moz-calc(10%);\n \t\twidth: -webkit-calc(10%);\n \t}\n \tul.timeline > li > .timeline-badge {\n \t\ttop: 16px;\n \t\tleft: 15px;\n \t\tmargin-left: 0;\n \t}\n \tul.timeline > li > .timeline-panel {\n \t\tfloat: right;\n \t}\n \tul.timeline > li > .timeline-panel:before {\n \t\tright: auto;\n \t\tleft: -15px;\n \t\tborder-right-width: 15px;\n \t\tborder-left-width: 0;\n \t}\n \tul.timeline > li > .timeline-panel:after {\n \t\tright: auto;\n \t\tleft: -14px;\n \t\tborder-right-width: 14px;\n \t\tborder-left-width: 0;\n \t}\n }\n\n \n </style>\n"],"sourceRoot":""}]);

// exports


/***/ }),
/* 60 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            form: new Form({
                username: '',
                password: ''
            }),
            messages: {}
        };
    },

    computed: {},

    created: function created() {
        //DO SOMETHING HERE AT FIRST LOAD
        var that = this;
        if (that.$store.state.isAuthenticated) {
            that.$router.push('/dashboard');
        }
    },

    methods: {
        login: function login() {
            var that = this;
            that.form.post('/api/auth/token').then(function (response) {
                localStorage.setItem('access_token', response.access_token);
                localStorage.setItem('refresh_token', response.refresh_token);
                that.getSetUserData();
            }).catch(function (error) {
                that.form.reset();
                that.$router.app.$emit('notifyFailed', 'Login Failed!');
            });
        },

        getSetUserData: function getSetUserData() {
            var that = this;

            axios.get('/api/user').then(function (response) {
                that.$store.state.user = response.data.data;
                that.$store.state.isAuthenticated = true;
                that.$store.state.access_token = localStorage.getItem('access_token');
                that.$router.push('/dashboard');
            }).catch(function (error) {
                console.log(error);
            });
        }
    }

});

/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { attrs: { id: "auth-login-component" } }, [
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-md-4 col-md-offset-4" }, [
          _c("div", { staticClass: "login-panel panel panel-default" }, [
            _vm._m(0),
            _vm._v(" "),
            _c("div", { staticClass: "panel-body" }, [
              _c("form", { attrs: { role: "form" } }, [
                _c("fieldset", [
                  _c("div", { staticClass: "form-group" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.form.username,
                          expression: "form.username"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        placeholder: "E-mail",
                        name: "email",
                        type: "email",
                        autofocus: ""
                      },
                      domProps: { value: _vm.form.username },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.form, "username", $event.target.value)
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.form.password,
                          expression: "form.password"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        placeholder: "Password",
                        name: "password",
                        type: "password",
                        value: ""
                      },
                      domProps: { value: _vm.form.password },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.form, "password", $event.target.value)
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _vm._m(1),
                  _vm._v(" "),
                  _c(
                    "a",
                    {
                      staticClass: "btn btn-lg btn-success btn-block",
                      on: { click: _vm.login }
                    },
                    [_vm._v("Login")]
                  )
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "panel-heading" }, [
      _c("h3", { staticClass: "panel-title" }, [_vm._v("Please Sign In")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "checkbox" }, [
      _c("label", [
        _c("input", {
          attrs: { name: "remember", type: "checkbox", value: "Remember Me" }
        }),
        _vm._v("Remember Me\n\t\t\t\t\t\t\t\t\t")
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3a3db52e", module.exports)
  }
}

/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(63);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("5a2e5cae", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js?sourceMap!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-219942e6\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0&bustCache!./admin.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js?sourceMap!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-219942e6\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0&bustCache!./admin.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)(true);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", "", {"version":3,"sources":[],"names":[],"mappings":"","file":"admin.vue","sourceRoot":""}]);

// exports


/***/ }),
/* 64 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {};
  },
  created: function created() {},

  methods: {
    logout: function logout() {
      var that = this;
      localStorage.removeItem('access_token');
      localStorage.removeItem('refresh_token');
      that.$store.state.user = {};
      that.$store.state.isAuthenticated = false;
      that.$store.state.access_token = null;
      that.$router.push('/login');
    }
  }

});

/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "layouts-admin" }, [
    _c("nav", { staticClass: "navbar navbar-default navbar-static-top" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "navbar-header" }, [
          _c(
            "button",
            {
              staticClass: "navbar-toggle navbar-toggle-sidebar collapsed",
              attrs: { type: "button" }
            },
            [_vm._v("\n\t\t\tMENU\n\t\t\t")]
          ),
          _vm._v(" "),
          _vm._m(0),
          _vm._v(" "),
          _c("a", { staticClass: "navbar-brand", attrs: { href: "#" } }, [
            _vm._v(
              "\n\t\t\t\t" +
                _vm._s(this.$store.state.user.first_name) +
                "\n\t\t\t"
            )
          ])
        ]),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass: "collapse navbar-collapse",
            attrs: { id: "bs-example-navbar-collapse-1" }
          },
          [
            _vm._m(1),
            _vm._v(" "),
            _c("ul", { staticClass: "nav navbar-nav navbar-right" }, [
              _c(
                "li",
                [_c("router-link", { attrs: { to: "/" } }, [_vm._v("Home")])],
                1
              ),
              _vm._v(" "),
              _c("li", { staticClass: "dropdown " }, [
                _vm._m(2),
                _vm._v(" "),
                _c(
                  "ul",
                  { staticClass: "dropdown-menu", attrs: { role: "menu" } },
                  [
                    _c("li", { staticClass: "dropdown-header" }, [
                      _vm._v("SETTINGS")
                    ]),
                    _vm._v(" "),
                    _c(
                      "li",
                      {},
                      [
                        _c("router-link", { attrs: { to: "profile" } }, [
                          _vm._v("Profile")
                        ])
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c("li", { staticClass: "divider" }),
                    _vm._v(" "),
                    _c("li", [
                      _c("a", { on: { click: _vm.logout } }, [_vm._v("Logout")])
                    ])
                  ]
                )
              ])
            ])
          ]
        )
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "container-fluid main-container" }, [
      _c("div", { staticClass: "col-md-2 sidebar" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "absolute-wrapper" }),
          _vm._v(" "),
          _c("div", { staticClass: "side-menu" }, [
            _c(
              "nav",
              {
                staticClass: "navbar navbar-default",
                attrs: { role: "navigation" }
              },
              [
                _c("div", { staticClass: "side-menu-container" }, [
                  _c("ul", { staticClass: "nav navbar-nav" }, [
                    _c(
                      "li",
                      [
                        _c("router-link", { attrs: { to: "dashboard" } }, [
                          _c("span", {
                            staticClass: "glyphicon glyphicon-dashboard"
                          }),
                          _vm._v(" Dashboard")
                        ])
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "li",
                      [
                        _c("router-link", { attrs: { to: "users" } }, [
                          _c("span", {
                            staticClass: "glyphicon glyphicon-user"
                          }),
                          _vm._v(" Users")
                        ])
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _vm._m(3),
                    _vm._v(" "),
                    _vm._m(4),
                    _vm._v(" "),
                    _vm._m(5)
                  ])
                ])
              ]
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-10 content" }, [_c("router-view")], 1),
      _vm._v(" "),
      _vm._m(6)
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "navbar-toggle collapsed",
        attrs: {
          type: "button",
          "data-toggle": "collapse",
          "data-target": "#bs-example-navbar-collapse-1"
        }
      },
      [
        _c("span", { staticClass: "sr-only" }, [_vm._v("Toggle navigation")]),
        _vm._v(" "),
        _c("span", { staticClass: "icon-bar" }),
        _vm._v(" "),
        _c("span", { staticClass: "icon-bar" }),
        _vm._v(" "),
        _c("span", { staticClass: "icon-bar" })
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "form",
      {
        staticClass: "navbar-form navbar-left",
        attrs: { method: "GET", role: "search" }
      },
      [
        _c("div", { staticClass: "form-group" }, [
          _c("input", {
            staticClass: "form-control",
            attrs: { type: "text", name: "q", placeholder: "Search" }
          })
        ]),
        _vm._v(" "),
        _c(
          "button",
          { staticClass: "btn btn-default", attrs: { type: "submit" } },
          [_c("i", { staticClass: "glyphicon glyphicon-search" })]
        )
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      {
        staticClass: "dropdown-toggle",
        attrs: {
          href: "#",
          "data-toggle": "dropdown",
          role: "button",
          "aria-expanded": "false"
        }
      },
      [
        _vm._v("\n\t\t\t\t\t\tAccount\n\t\t\t\t\t\t"),
        _c("span", { staticClass: "caret" })
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", [
      _c("a", { attrs: { href: "#" } }, [
        _c("span", { staticClass: "glyphicon glyphicon-cloud" }),
        _vm._v(" Link")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "li",
      { staticClass: "panel panel-default", attrs: { id: "dropdown" } },
      [
        _c(
          "a",
          { attrs: { "data-toggle": "collapse", href: "#dropdown-lvl1" } },
          [
            _c("span", { staticClass: "glyphicon glyphicon-user" }),
            _vm._v(" Sub Level "),
            _c("span", { staticClass: "caret" })
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass: "panel-collapse collapse",
            attrs: { id: "dropdown-lvl1" }
          },
          [
            _c("div", { staticClass: "panel-body" }, [
              _c("ul", { staticClass: "nav navbar-nav" }, [
                _c("li", [_c("a", { attrs: { href: "#" } }, [_vm._v("Link")])]),
                _vm._v(" "),
                _c("li", [_c("a", { attrs: { href: "#" } }, [_vm._v("Link")])]),
                _vm._v(" "),
                _c("li", [_c("a", { attrs: { href: "#" } }, [_vm._v("Link")])]),
                _vm._v(" "),
                _c(
                  "li",
                  {
                    staticClass: "panel panel-default",
                    attrs: { id: "dropdown" }
                  },
                  [
                    _c(
                      "a",
                      {
                        attrs: {
                          "data-toggle": "collapse",
                          href: "#dropdown-lvl2"
                        }
                      },
                      [
                        _c("span", { staticClass: "glyphicon glyphicon-off" }),
                        _vm._v(" Sub Level "),
                        _c("span", { staticClass: "caret" })
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "panel-collapse collapse",
                        attrs: { id: "dropdown-lvl2" }
                      },
                      [
                        _c("div", { staticClass: "panel-body" }, [
                          _c("ul", { staticClass: "nav navbar-nav" }, [
                            _c("li", [
                              _c("a", { attrs: { href: "#" } }, [
                                _vm._v("Link")
                              ])
                            ]),
                            _vm._v(" "),
                            _c("li", [
                              _c("a", { attrs: { href: "#" } }, [
                                _vm._v("Link")
                              ])
                            ]),
                            _vm._v(" "),
                            _c("li", [
                              _c("a", { attrs: { href: "#" } }, [
                                _vm._v("Link")
                              ])
                            ])
                          ])
                        ])
                      ]
                    )
                  ]
                )
              ])
            ])
          ]
        )
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", [
      _c("a", { attrs: { href: "#" } }, [
        _c("span", { staticClass: "glyphicon glyphicon-signal" }),
        _vm._v(" Link")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("footer", { staticClass: "pull-left footer" }, [
      _c("p", { staticClass: "col-md-12" }),
      _c("hr", { staticClass: "divider" }),
      _vm._v("\n  \t\t\t\tCopyright © 2015 "),
      _c("a", { attrs: { href: "http://www.pingpong-labs.com" } }, [
        _vm._v("Gravitano")
      ]),
      _vm._v(" "),
      _c("p")
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-219942e6", module.exports)
  }
}

/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(67)
}
var normalizeComponent = __webpack_require__(1)
/* script */
var __vue_script__ = __webpack_require__(69)
/* template */
var __vue_template__ = __webpack_require__(70)
/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/vue/dashboard/index.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-684a444a", Component.options)
  } else {
    hotAPI.reload("data-v-684a444a", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(68);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("69f533d8", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js?sourceMap!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-684a444a\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0&bustCache!./index.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js?sourceMap!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-684a444a\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0&bustCache!./index.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)(true);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", "", {"version":3,"sources":[],"names":[],"mappings":"","file":"index.vue","sourceRoot":""}]);

// exports


/***/ }),
/* 69 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            users: {}
        };
    },

    computed: {},

    created: function created() {
        //DO SOMETHING HERE AT FIRST LOAD
        var that = this;
        axios.get('/api/v1/users').then(function (response) {
            that.users = response.data.data;
        }).catch(function (error) {
            console.log(error);
        });
    },

    methods: {
        onSubmit: function onSubmit() {
            var that = this;
        }
    }

});

/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { attrs: { id: "admin-index-component" } }, [
      _c("div", { staticClass: "panel panel-default" }, [
        _c("div", { staticClass: "panel-heading" }, [
          _vm._v("\n\t\tDashboard\n\t")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "panel-body" }, [
          _c("p", [
            _vm._v(
              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste saepe et quisquam nesciunt maxime."
            )
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-684a444a", module.exports)
  }
}

/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(72)
}
var normalizeComponent = __webpack_require__(1)
/* script */
var __vue_script__ = __webpack_require__(74)
/* template */
var __vue_template__ = __webpack_require__(75)
/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/vue/users/index.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4309364f", Component.options)
  } else {
    hotAPI.reload("data-v-4309364f", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(73);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("7b7e9f15", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js?sourceMap!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4309364f\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0&bustCache!./index.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js?sourceMap!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4309364f\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0&bustCache!./index.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)(true);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", "", {"version":3,"sources":[],"names":[],"mappings":"","file":"index.vue","sourceRoot":""}]);

// exports


/***/ }),
/* 74 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            users: {}
        };
    },

    computed: {},

    created: function created() {
        //DO SOMETHING HERE AT FIRST LOAD
        var that = this;
        axios.get('/api/v1/users').then(function (response) {
            that.users = response.data.data;
        }).catch(function (error) {
            console.log(error);
        });
    },

    methods: {
        onSubmit: function onSubmit() {
            var that = this;
        }
    }

});

/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { attrs: { id: "admin-index-component" } }, [
    _c("div", { staticClass: "panel panel-default" }, [
      _c("div", { staticClass: "panel-heading" }, [_vm._v("\n\t\tUsers\n\t")]),
      _vm._v(" "),
      _c("div", { staticClass: "panel-body" }, [
        _c(
          "table",
          {
            staticClass: "table table-bordered table-responsive table-striped"
          },
          [
            _vm._m(0),
            _vm._v(" "),
            _c(
              "tbody",
              _vm._l(_vm.users, function(user) {
                return _c("tr", [
                  _c("td", [_vm._v(_vm._s(user.name))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(user.email))])
                ])
              })
            )
          ]
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("Name")]),
        _vm._v(" "),
        _c("th", [_vm._v("Email")])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-4309364f", module.exports)
  }
}

/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(77)
}
var normalizeComponent = __webpack_require__(1)
/* script */
var __vue_script__ = __webpack_require__(79)
/* template */
var __vue_template__ = __webpack_require__(80)
/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/vue/layouts/errors.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3366911d", Component.options)
  } else {
    hotAPI.reload("data-v-3366911d", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(78);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("2fd1c6d4", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js?sourceMap!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3366911d\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0&bustCache!./errors.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js?sourceMap!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3366911d\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0&bustCache!./errors.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)(true);
// imports


// module
exports.push([module.i, "\nbody { background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABZ0RVh0Q3JlYXRpb24gVGltZQAxMC8yOS8xMiKqq3kAAAAcdEVYdFNvZnR3YXJlAEFkb2JlIEZpcmV3b3JrcyBDUzVxteM2AAABHklEQVRIib2Vyw6EIAxFW5idr///Qx9sfG3pLEyJ3tAwi5EmBqRo7vHawiEEERHS6x7MTMxMVv6+z3tPMUYSkfTM/R0fEaG2bbMv+Gc4nZzn+dN4HAcREa3r+hi3bcuu68jLskhVIlW073tWaYlQ9+F9IpqmSfq+fwskhdO/AwmUTJXrOuaRQNeRkOd5lq7rXmS5InmERKoER/QMvUAPlZDHcZRhGN4CSeGY+aHMqgcks5RrHv/eeh455x5KrMq2yHQdibDO6ncG/KZWL7M8xDyS1/MIO0NJqdULLS81X6/X6aR0nqBSJcPeZnlZrzN477NKURn2Nus8sjzmEII0TfMiyxUuxphVWjpJkbx0btUnshRihVv70Bv8ItXq6Asoi/ZiCbU6YgAAAABJRU5ErkJggg==);\n}\n.error-template {padding: 40px 15px;text-align: center;\n}\n.error-actions {margin-top:15px;margin-bottom:15px;\n}\n.error-actions .btn { margin-right:10px;\n}\n\t\n", "", {"version":3,"sources":["/Users/villamornantoniojr/Development/greyfox/matchup/web/resources/js/vue/layouts/resources/js/vue/layouts/errors.vue?4c2d1278"],"names":[],"mappings":";AAYA,OAAA,8oBAAA;CAAA;AACA,iBAAA,mBAAA,mBAAA;CAAA;AACA,gBAAA,gBAAA,mBAAA;CAAA;AACA,sBAAA,kBAAA;CAAA","file":"errors.vue","sourcesContent":["<template>\n<div id=\"layouts-errors\">\n    <router-view></router-view>\n</div>\n</template>\n\n\n<script>\n</script>\n\n<style type=\"text/css\" media=\"screen\">\n\nbody { background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABZ0RVh0Q3JlYXRpb24gVGltZQAxMC8yOS8xMiKqq3kAAAAcdEVYdFNvZnR3YXJlAEFkb2JlIEZpcmV3b3JrcyBDUzVxteM2AAABHklEQVRIib2Vyw6EIAxFW5idr///Qx9sfG3pLEyJ3tAwi5EmBqRo7vHawiEEERHS6x7MTMxMVv6+z3tPMUYSkfTM/R0fEaG2bbMv+Gc4nZzn+dN4HAcREa3r+hi3bcuu68jLskhVIlW073tWaYlQ9+F9IpqmSfq+fwskhdO/AwmUTJXrOuaRQNeRkOd5lq7rXmS5InmERKoER/QMvUAPlZDHcZRhGN4CSeGY+aHMqgcks5RrHv/eeh455x5KrMq2yHQdibDO6ncG/KZWL7M8xDyS1/MIO0NJqdULLS81X6/X6aR0nqBSJcPeZnlZrzN477NKURn2Nus8sjzmEII0TfMiyxUuxphVWjpJkbx0btUnshRihVv70Bv8ItXq6Asoi/ZiCbU6YgAAAABJRU5ErkJggg==);}\n.error-template {padding: 40px 15px;text-align: center;}\n.error-actions {margin-top:15px;margin-bottom:15px;}\n.error-actions .btn { margin-right:10px; }\n\t\n</style>"],"sourceRoot":""}]);

// exports


/***/ }),
/* 79 */
/***/ (function(module, exports) {

//
//
//
//
//
//
//

/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { attrs: { id: "layouts-errors" } }, [_c("router-view")], 1)
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3366911d", module.exports)
  }
}

/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__(82)
/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/vue/errors/404.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0f827080", Component.options)
  } else {
    hotAPI.reload("data-v-0f827080", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "errors-404-component" }, [
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-md-12" }, [
          _c("div", { staticClass: "error-template" }, [
            _c("h1", [_vm._v("\n                    Oops!")]),
            _vm._v(" "),
            _c("h2", [_vm._v("\n                    404 Not Found")]),
            _vm._v(" "),
            _c("div", { staticClass: "error-details" }, [
              _vm._v(
                "\n                    Sorry, an error has occured, Requested page not found!\n                "
              )
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "error-actions" },
              [
                _c(
                  "router-link",
                  { staticClass: "btn btn-primary btn-lg", attrs: { to: "/" } },
                  [
                    _c("span", { staticClass: "glyphicon glyphicon-home" }),
                    _vm._v("\n                        Take Me Home ")
                  ]
                ),
                _vm._v(" "),
                _c(
                  "router-link",
                  {
                    staticClass: "btn btn-default btn-lg",
                    attrs: { to: "contact" }
                  },
                  [
                    _c("span", { staticClass: "glyphicon glyphicon-envelope" }),
                    _vm._v(" Contact Support")
                  ]
                )
              ],
              1
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0f827080", module.exports)
  }
}

/***/ }),
/* 83 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export store */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vuex__ = __webpack_require__(17);



__WEBPACK_IMPORTED_MODULE_0_vue___default.a.use(__WEBPACK_IMPORTED_MODULE_1_vuex__["default"]);

var store = new __WEBPACK_IMPORTED_MODULE_1_vuex__["default"].Store({
    state: {
        user: {},
        access_token: null,
        isAuthenticated: false
    }
});

/* harmony default export */ __webpack_exports__["a"] = (store);

/***/ })
],[18]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXIuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy92dWUtc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXNDbGllbnQuanMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL3Z1ZS9sYXlvdXRzL3BhZ2VzLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvdnVlL2xheW91dHMvYWRtaW4udnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy9hcHAuanMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL2Jvb3RzdHJhcC5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvY29yZS9Gb3JtLmpzIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy9jb3JlL0Vycm9ycy5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvcm91dGVzLmpzIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy92dWUvbGF5b3V0cy9wYWdlcy52dWU/MWZiYyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvdnVlL2xheW91dHMvcGFnZXMudnVlP2I1MGIiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2xpc3RUb1N0eWxlcy5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvdnVlL2xheW91dHMvcGFnZXMudnVlPzU3ZWYiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL3Z1ZS9wYWdlcy9ob21lLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvdnVlL3BhZ2VzL2hvbWUudnVlP2RlNWQiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL3Z1ZS9wYWdlcy9hYm91dC52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL3Z1ZS9wYWdlcy9hYm91dC52dWU/NWFiNCIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvdnVlL3BhZ2VzL2NvbnRhY3QudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy92dWUvcGFnZXMvY29udGFjdC52dWU/ZGY1MiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvdnVlL2xheW91dHMvcG9ydGFsLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvdnVlL2xheW91dHMvcG9ydGFsLnZ1ZT83YjYyIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy92dWUvYXV0aC9sb2dpbi52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL3Z1ZS9hdXRoL2xvZ2luLnZ1ZT9lMzJmIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy92dWUvYXV0aC9sb2dpbi52dWU/OTM3ZSIsIndlYnBhY2s6Ly8vcmVzb3VyY2VzL2pzL3Z1ZS9hdXRoL2xvZ2luLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvdnVlL2F1dGgvbG9naW4udnVlPzU1YmUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL3Z1ZS9sYXlvdXRzL2FkbWluLnZ1ZT9jMmZiIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy92dWUvbGF5b3V0cy9hZG1pbi52dWU/ZWFkNCIsIndlYnBhY2s6Ly8vcmVzb3VyY2VzL2pzL3Z1ZS9sYXlvdXRzL2FkbWluLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvdnVlL2xheW91dHMvYWRtaW4udnVlPzRiYzMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL3Z1ZS9kYXNoYm9hcmQvaW5kZXgudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy92dWUvZGFzaGJvYXJkL2luZGV4LnZ1ZT9mNjBmIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy92dWUvZGFzaGJvYXJkL2luZGV4LnZ1ZT9lMDFhIiwid2VicGFjazovLy9yZXNvdXJjZXMvanMvdnVlL2Rhc2hib2FyZC9pbmRleC52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL3Z1ZS9kYXNoYm9hcmQvaW5kZXgudnVlP2ZiNzUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL3Z1ZS91c2Vycy9pbmRleC52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL3Z1ZS91c2Vycy9pbmRleC52dWU/ZTk3YSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvdnVlL3VzZXJzL2luZGV4LnZ1ZT84NDkwIiwid2VicGFjazovLy9yZXNvdXJjZXMvanMvdnVlL3VzZXJzL2luZGV4LnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvdnVlL3VzZXJzL2luZGV4LnZ1ZT83MjM1Iiwid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy92dWUvbGF5b3V0cy9lcnJvcnMudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy92dWUvbGF5b3V0cy9lcnJvcnMudnVlPzUzM2UiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL3Z1ZS9sYXlvdXRzL2Vycm9ycy52dWU/MjlhMSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvdnVlL2xheW91dHMvZXJyb3JzLnZ1ZT9mOTM1Iiwid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy92dWUvZXJyb3JzLzQwNC52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL3Z1ZS9lcnJvcnMvNDA0LnZ1ZT9hYmRlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy9zdG9yZS5qcyJdLCJuYW1lcyI6WyJWdWUiLCJlbCIsInJvdXRlciIsInN0b3JlIiwiZGF0YSIsInNoYXJlZCIsIm1vdW50ZWQiLCJ0aGF0IiwiJG9uIiwibXNnIiwiJHN3YWwiLCJjcmVhdGVkIiwiYWNjZXNzVG9rZW4iLCJsb2NhbFN0b3JhZ2UiLCJnZXRJdGVtIiwiYXhpb3MiLCJnZXQiLCJ0aGVuIiwicmVzcG9uc2UiLCJzZXRMb2dpbiIsImNhdGNoIiwiZXJyb3IiLCJkZXN0cm95TG9naW4iLCJtZXRob2RzIiwidXNlciIsIiRzdG9yZSIsInN0YXRlIiwiaXNBdXRoZW50aWNhdGVkIiwiYWNjZXNzX3Rva2VuIiwicmVtb3ZlSXRlbSIsIiRyb3V0ZXIiLCJwdXNoIiwid2luZG93IiwiRm9ybSIsInVzZSIsImRlZmF1bHRzIiwiaGVhZGVycyIsImNvbW1vbiIsImJhc2VVUkwiLCJpbnRlcmNlcHRvcnMiLCJyZXF1ZXN0IiwiY29uZmlnIiwiUHJvbWlzZSIsInJlamVjdCIsIm9yaWdpbmFsUmVxdWVzdCIsInN0YXR1cyIsIl9yZXRyeSIsInJlZnJlc2hUb2tlbiIsInBvc3QiLCJzZXRJdGVtIiwicmVmcmVzaF90b2tlbiIsIm9yaWdpbmFsRGF0YSIsImZpZWxkIiwiZXJyb3JzIiwicHJvcGVydHkiLCJjbGVhciIsInVybCIsInN1Ym1pdCIsInJlcXVlc3RUeXBlIiwicmVzb2x2ZSIsIm9uU3VjY2VzcyIsIm9uRmFpbCIsInJlc2V0IiwicmVjb3JkIiwiRXJyb3JzIiwiaGFzT3duUHJvcGVydHkiLCJPYmplY3QiLCJrZXlzIiwibGVuZ3RoIiwicm91dGVzIiwicGF0aCIsImNvbXBvbmVudCIsInJlcXVpcmUiLCJtZXRhIiwicmVxdWlyZXNBdXRoIiwiY2hpbGRyZW4iLCJsaW5rQWN0aXZlQ2xhc3MiLCJtb2RlIiwiYmVmb3JlRWFjaCIsInRvIiwiZnJvbSIsIm5leHQiLCJxdWVyeSIsInJlZGlyZWN0IiwiZnVsbFBhdGgiLCJWdWV4IiwiU3RvcmUiXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSx5QkFBeUI7QUFDekI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FDdEdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUMsZ0JBQWdCO0FBQ25ELElBQUk7QUFDSjtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQkFBZ0IsaUJBQWlCO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWSxvQkFBb0I7QUFDaEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0RBQW9ELGNBQWM7O0FBRWxFO0FBQ0E7Ozs7Ozs7QUMzRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFVBQVUsaUJBQWlCO0FBQzNCO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG1CQUFtQixtQkFBbUI7QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsbUJBQW1CLHNCQUFzQjtBQUN6QztBQUNBO0FBQ0EsdUJBQXVCLDJCQUEyQjtBQUNsRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGlCQUFpQixtQkFBbUI7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIsMkJBQTJCO0FBQ2hEO0FBQ0E7QUFDQSxZQUFZLHVCQUF1QjtBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EscUJBQXFCLHVCQUF1QjtBQUM1QztBQUNBO0FBQ0EsOEJBQThCO0FBQzlCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlEQUF5RDtBQUN6RDs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7OztBQ3ROQTtBQUNBO0FBQ0E7QUFDQSx3QkFBbU07QUFDbk07QUFDQTtBQUNBO0FBQ0EsMkNBQXVOO0FBQ3ZOO0FBQ0EsNkNBQWdMO0FBQ2hMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0Usd0RBQXdELElBQUk7O0FBRTNJO0FBQ0EsWUFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7O0FBRUQ7Ozs7Ozs7Ozs7Ozs7OztBQzdDQTtBQUNBO0FBQ0E7QUFDQSx3QkFBbU07QUFDbk07QUFDQTtBQUNBO0FBQ0EsMkNBQXVOO0FBQ3ZOO0FBQ0EsNkNBQWdMO0FBQ2hMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0Usd0RBQXdELElBQUk7O0FBRTNJO0FBQ0EsWUFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7O0FBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0NBOztBQUVBO0FBQ0E7O0FBRUEsSUFBSUEsR0FBSixDQUFRO0FBQ0pDLFFBQUksT0FEQTtBQUVKQyxZQUFTLDJEQUZMO0FBR0pDLFdBQU0sMERBSEY7QUFJSkMsVUFBTTtBQUNGQyxnQkFBUSwwREFBQUY7QUFETixLQUpGO0FBT0pHLFdBUEkscUJBT0s7QUFDTCxZQUFJQyxPQUFPLElBQVg7QUFDQUEsYUFBS0MsR0FBTCxDQUFTLGVBQVQsRUFBMEIsVUFBVUMsR0FBVixFQUFlO0FBQ3JDRixpQkFBS0csS0FBTCxDQUFXLFNBQVgsRUFBcUJELEdBQXJCLEVBQTBCLFNBQTFCO0FBQ0gsU0FGRDtBQUdBRixhQUFLQyxHQUFMLENBQVMsY0FBVCxFQUF5QixVQUFVQyxHQUFWLEVBQWU7QUFDcENGLGlCQUFLRyxLQUFMLENBQVcsT0FBWCxFQUFtQkQsR0FBbkIsRUFBd0IsT0FBeEI7QUFDSCxTQUZEO0FBR0gsS0FmRztBQWdCSkUsV0FoQkkscUJBZ0JLO0FBQ0wsWUFBSUMsY0FBY0MsYUFBYUMsT0FBYixDQUFxQixjQUFyQixDQUFsQjtBQUNBLFlBQUlGLGdCQUFnQixJQUFoQixJQUF3QkEsZ0JBQWdCLFdBQTVDLEVBQXlEO0FBQ3JELGdCQUFJTCxPQUFPLElBQVg7QUFDQVEsa0JBQU1DLEdBQU4sQ0FBVSxXQUFWLEVBQ0tDLElBREwsQ0FDVSxVQUFTQyxRQUFULEVBQWtCO0FBQ3BCWCxxQkFBS1ksUUFBTCxDQUFjRCxTQUFTZCxJQUFULENBQWNBLElBQTVCO0FBQ0gsYUFITCxFQUlLZ0IsS0FKTCxDQUlXLFVBQVNDLEtBQVQsRUFBZTtBQUNsQmQscUJBQUtlLFlBQUw7QUFDSCxhQU5MO0FBT0g7QUFFSixLQTdCRzs7QUE4QkpDLGFBQVE7QUFDSkosZ0JBREksb0JBQ0tLLElBREwsRUFDVztBQUNYLGdCQUFJakIsT0FBTyxJQUFYO0FBQ0E7QUFDQSxpQkFBS2tCLE1BQUwsQ0FBWUMsS0FBWixDQUFrQkYsSUFBbEIsR0FBeUJBLElBQXpCO0FBQ0EsaUJBQUtDLE1BQUwsQ0FBWUMsS0FBWixDQUFrQkMsZUFBbEIsR0FBb0MsSUFBcEM7QUFDQSxpQkFBS0YsTUFBTCxDQUFZQyxLQUFaLENBQWtCRSxZQUFsQixHQUFpQ2YsYUFBYUMsT0FBYixDQUFxQixjQUFyQixDQUFqQztBQUNILFNBUEc7QUFTSlEsb0JBVEksMEJBU1c7QUFDWCxnQkFBSWYsT0FBTyxJQUFYO0FBQ0E7QUFDQSxpQkFBS2tCLE1BQUwsQ0FBWUMsS0FBWixDQUFrQkYsSUFBbEIsR0FBeUIsRUFBekI7QUFDQSxpQkFBS0MsTUFBTCxDQUFZQyxLQUFaLENBQWtCQyxlQUFsQixHQUFvQyxLQUFwQztBQUNBLGlCQUFLRixNQUFMLENBQVlDLEtBQVosQ0FBa0JFLFlBQWxCLEdBQWlDLElBQWpDO0FBQ0FmLHlCQUFhZ0IsVUFBYixDQUF3QixjQUF4QjtBQUNBaEIseUJBQWFnQixVQUFiLENBQXdCLGVBQXhCO0FBQ0F0QixpQkFBS3VCLE9BQUwsQ0FBYUMsSUFBYixDQUFrQixZQUFsQjtBQUVIO0FBbkJHO0FBOUJKLENBQVIsRTs7Ozs7Ozs7Ozs7OztBQ0xBO0FBQ0E7QUFDQTtBQUNBOztBQUdBQyxPQUFPaEMsR0FBUCxHQUFhLDJDQUFiO0FBQ0FnQyxPQUFPQyxJQUFQLEdBQWMsMkRBQWQ7QUFDQSwyQ0FBQWpDLENBQUlrQyxHQUFKLENBQVEsbURBQVI7QUFDQUYsT0FBT2pCLEtBQVAsR0FBZSw2Q0FBZjs7QUFFQWlCLE9BQU9qQixLQUFQLENBQWFvQixRQUFiLENBQXNCQyxPQUF0QixDQUE4QkMsTUFBOUIsR0FBdUM7QUFDbkMsd0JBQW9CO0FBRXhCO0FBSHVDLENBQXZDLENBSUFMLE9BQU9qQixLQUFQLENBQWFvQixRQUFiLENBQXNCRyxPQUF0QixHQUFnQyx3QkFBaEM7O0FBRUEsNkNBQUF2QixDQUFNd0IsWUFBTixDQUFtQkMsT0FBbkIsQ0FBMkJOLEdBQTNCLENBQStCLFVBQUNPLE1BQUQsRUFBWTs7QUFFdkMsUUFBSTdCLG9CQUFKO0FBQ0FBLGtCQUFjQyxhQUFhQyxPQUFiLENBQXFCLGNBQXJCLENBQWQ7O0FBR0EsUUFBSUYsZ0JBQWdCLElBQWhCLElBQXdCQSxnQkFBZ0IsV0FBNUMsRUFBeUQ7QUFDckQ2QixlQUFPTCxPQUFQLENBQWVDLE1BQWYsQ0FBc0IsZUFBdEIsSUFBeUMsWUFBWXpCLFdBQXJEO0FBQ0g7QUFDRCxXQUFPNkIsTUFBUDtBQUNILENBVkQsRUFVRyxVQUFVcEIsS0FBVixFQUFpQjtBQUNoQjtBQUNBLFdBQU9xQixRQUFRQyxNQUFSLENBQWV0QixLQUFmLENBQVA7QUFDSCxDQWJEOztBQWVBLDZDQUFBTixDQUFNd0IsWUFBTixDQUFtQnJCLFFBQW5CLENBQTRCZ0IsR0FBNUIsQ0FBZ0MsVUFBVWhCLFFBQVYsRUFBb0I7QUFDaEQsV0FBT0EsUUFBUDtBQUNILENBRkQsRUFFRyxVQUFVRyxLQUFWLEVBQWlCOztBQUVoQixRQUFNdUIsa0JBQWtCdkIsTUFBTW9CLE1BQTlCOztBQUVBLFFBQUlwQixNQUFNSCxRQUFOLENBQWUyQixNQUFmLEtBQTBCLEdBQTFCLElBQWlDLENBQUNELGdCQUFnQkUsTUFBdEQsRUFBOEQ7O0FBRTFERix3QkFBZ0JFLE1BQWhCLEdBQXlCLElBQXpCOztBQUVBLFlBQU1DLGVBQWVmLE9BQU9uQixZQUFQLENBQW9CQyxPQUFwQixDQUE0QixlQUE1QixDQUFyQjtBQUNBLGVBQU8sNkNBQUFDLENBQU1pQyxJQUFOLENBQVcsbUJBQVgsRUFBZ0MsRUFBRUQsMEJBQUYsRUFBaEMsRUFDRjlCLElBREUsQ0FDRyxnQkFBWTtBQUFBLGdCQUFWYixJQUFVLFFBQVZBLElBQVU7O0FBQ2Q0QixtQkFBT25CLFlBQVAsQ0FBb0JvQyxPQUFwQixDQUE0QixjQUE1QixFQUE0QzdDLEtBQUt3QixZQUFqRDtBQUNBSSxtQkFBT25CLFlBQVAsQ0FBb0JvQyxPQUFwQixDQUE0QixlQUE1QixFQUE2QzdDLEtBQUs4QyxhQUFsRDtBQUNBbkMsWUFBQSw2Q0FBQUEsQ0FBTW9CLFFBQU4sQ0FBZUMsT0FBZixDQUF1QkMsTUFBdkIsQ0FBOEIsZUFBOUIsSUFBaUQsWUFBWWpDLEtBQUt3QixZQUFsRTtBQUNBZ0IsNEJBQWdCUixPQUFoQixDQUF3QixlQUF4QixJQUEyQyxZQUFZaEMsS0FBS3dCLFlBQTVEO0FBQ0EsbUJBQU8sNkNBQUFiLENBQU02QixlQUFOLENBQVA7QUFDSCxTQVBFLENBQVA7QUFRSDs7QUFFRCxXQUFPRixRQUFRQyxNQUFSLENBQWV0QixLQUFmLENBQVA7QUFDSCxDQXRCRCxFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaENBOztJQUVNWSxJO0FBQ0Y7Ozs7O0FBS0Esa0JBQVk3QixJQUFaLEVBQWtCO0FBQUE7O0FBQ2QsYUFBSytDLFlBQUwsR0FBb0IvQyxJQUFwQjs7QUFFQSxhQUFLLElBQUlnRCxLQUFULElBQWtCaEQsSUFBbEIsRUFBd0I7QUFDcEIsaUJBQUtnRCxLQUFMLElBQWNoRCxLQUFLZ0QsS0FBTCxDQUFkO0FBQ0g7O0FBRUQsYUFBS0MsTUFBTCxHQUFjLElBQUksd0RBQUosRUFBZDtBQUNIOztBQUdEOzs7Ozs7OytCQUdPO0FBQ0gsZ0JBQUlqRCxPQUFPLEVBQVg7O0FBRUEsaUJBQUssSUFBSWtELFFBQVQsSUFBcUIsS0FBS0gsWUFBMUIsRUFBd0M7QUFDcEMvQyxxQkFBS2tELFFBQUwsSUFBaUIsS0FBS0EsUUFBTCxDQUFqQjtBQUNIOztBQUVELG1CQUFPbEQsSUFBUDtBQUNIOztBQUdEOzs7Ozs7Z0NBR1E7QUFDSixpQkFBSyxJQUFJZ0QsS0FBVCxJQUFrQixLQUFLRCxZQUF2QixFQUFxQztBQUNqQyxxQkFBS0MsS0FBTCxJQUFjLEVBQWQ7QUFDSDs7QUFFRCxpQkFBS0MsTUFBTCxDQUFZRSxLQUFaO0FBQ0g7O0FBR0Q7Ozs7Ozs7OzZCQUtLQyxHLEVBQUs7QUFDTixtQkFBTyxLQUFLQyxNQUFMLENBQVksTUFBWixFQUFvQkQsR0FBcEIsQ0FBUDtBQUNIOztBQUdEOzs7Ozs7Ozs0QkFLSUEsRyxFQUFLO0FBQ0wsbUJBQU8sS0FBS0MsTUFBTCxDQUFZLEtBQVosRUFBbUJELEdBQW5CLENBQVA7QUFDSDs7QUFHRDs7Ozs7Ozs7OEJBS01BLEcsRUFBSztBQUNQLG1CQUFPLEtBQUtDLE1BQUwsQ0FBWSxPQUFaLEVBQXFCRCxHQUFyQixDQUFQO0FBQ0g7O0FBR0Q7Ozs7Ozs7O2dDQUtPQSxHLEVBQUs7QUFDUixtQkFBTyxLQUFLQyxNQUFMLENBQVksUUFBWixFQUFzQkQsR0FBdEIsQ0FBUDtBQUNIOztBQUdEOzs7Ozs7Ozs7K0JBTU9FLFcsRUFBYUYsRyxFQUFLO0FBQUE7O0FBQ3JCLG1CQUFPLElBQUlkLE9BQUosQ0FBWSxVQUFDaUIsT0FBRCxFQUFVaEIsTUFBVixFQUFxQjtBQUNwQzVCLHNCQUFNMkMsV0FBTixFQUFtQkYsR0FBbkIsRUFBd0IsTUFBS3BELElBQUwsRUFBeEIsRUFDS2EsSUFETCxDQUNVLG9CQUFZO0FBQ2QsMEJBQUsyQyxTQUFMLENBQWUxQyxTQUFTZCxJQUF4Qjs7QUFFQXVELDRCQUFRekMsU0FBU2QsSUFBakI7QUFDSCxpQkFMTCxFQU1LZ0IsS0FOTCxDQU1XLGlCQUFTO0FBQ1osMEJBQUt5QyxNQUFMLENBQVl4QyxNQUFNSCxRQUFOLENBQWVkLElBQTNCOztBQUVBdUMsMkJBQU90QixNQUFNSCxRQUFOLENBQWVkLElBQXRCO0FBQ0gsaUJBVkw7QUFXSCxhQVpNLENBQVA7QUFhSDs7QUFHRDs7Ozs7Ozs7a0NBS1VBLEksRUFBTTtBQUNaOztBQUVBLGlCQUFLMEQsS0FBTDtBQUNIOztBQUdEOzs7Ozs7OzsrQkFLT1QsTSxFQUFRO0FBQ1gsaUJBQUtBLE1BQUwsQ0FBWVUsTUFBWixDQUFtQlYsTUFBbkI7QUFDSDs7Ozs7O0FBR0wseURBQWVwQixJQUFmLEU7Ozs7Ozs7Ozs7O0lDbElNK0IsTTtBQUNGOzs7QUFHQSxzQkFBYztBQUFBOztBQUNWLGFBQUtYLE1BQUwsR0FBYyxFQUFkO0FBQ0g7O0FBR0Q7Ozs7Ozs7Ozs0QkFLSUQsSyxFQUFPO0FBQ1AsbUJBQU8sS0FBS0MsTUFBTCxDQUFZWSxjQUFaLENBQTJCYixLQUEzQixDQUFQO0FBQ0g7O0FBR0Q7Ozs7Ozs4QkFHTTtBQUNGLG1CQUFPYyxPQUFPQyxJQUFQLENBQVksS0FBS2QsTUFBakIsRUFBeUJlLE1BQXpCLEdBQWtDLENBQXpDO0FBQ0g7O0FBR0Q7Ozs7Ozs7OzRCQUtJaEIsSyxFQUFPO0FBQ1AsZ0JBQUksS0FBS0MsTUFBTCxDQUFZRCxLQUFaLENBQUosRUFBd0I7QUFDcEIsdUJBQU8sS0FBS0MsTUFBTCxDQUFZRCxLQUFaLEVBQW1CLENBQW5CLENBQVA7QUFDSDtBQUNKOztBQUdEOzs7Ozs7OzsrQkFLT0MsTSxFQUFRO0FBQ1gsaUJBQUtBLE1BQUwsR0FBY0EsTUFBZDtBQUNIOztBQUdEOzs7Ozs7Ozs4QkFLTUQsSyxFQUFPO0FBQ1QsZ0JBQUlBLEtBQUosRUFBVztBQUNQLHVCQUFPLEtBQUtDLE1BQUwsQ0FBWUQsS0FBWixDQUFQOztBQUVBO0FBQ0g7O0FBRUQsaUJBQUtDLE1BQUwsR0FBYyxFQUFkO0FBQ0g7Ozs7OztBQUdMLHlEQUFlVyxNQUFmLEU7Ozs7Ozs7O0FDakVBOztBQUVBLElBQU1LLFNBQVMsQ0FDWDtBQUNJQyxVQUFNLEdBRFY7QUFFSUMsZUFBVyxtQkFBQUMsQ0FBUSxDQUFSLENBRmY7QUFHSUMsVUFBTSxFQUFDQyxjQUFjLEtBQWYsRUFIVjtBQUlJQyxjQUFVLENBQ047QUFDSUwsY0FBTSxFQURWO0FBRUlDLG1CQUFXLG1CQUFBQyxDQUFRLEVBQVIsQ0FGZjtBQUdJQyxjQUFNLEVBQUNDLGNBQWMsS0FBZjtBQUhWLEtBRE07QUFKZCxDQURXLEVBYVY7QUFDR0osVUFBTSxRQURUO0FBRUdDLGVBQVcsbUJBQUFDLENBQVEsQ0FBUixDQUZkO0FBR0dDLFVBQU0sRUFBQ0MsY0FBYyxLQUFmLEVBSFQ7QUFJR0MsY0FBVSxDQUNOO0FBQ0lMLGNBQU0sRUFEVjtBQUVJQyxtQkFBVyxtQkFBQUMsQ0FBUSxFQUFSLENBRmY7QUFHSUMsY0FBTSxFQUFDQyxjQUFjLEtBQWY7QUFIVixLQURNO0FBSmIsQ0FiVSxFQXlCVjtBQUNHSixVQUFNLFVBRFQ7QUFFR0MsZUFBVyxtQkFBQUMsQ0FBUSxDQUFSLENBRmQ7QUFHR0MsVUFBTSxFQUFDQyxjQUFjLEtBQWYsRUFIVDtBQUlHQyxjQUFVLENBQ047QUFDSUwsY0FBTSxFQURWO0FBRUlDLG1CQUFXLG1CQUFBQyxDQUFRLEVBQVIsQ0FGZjtBQUdJQyxjQUFNLEVBQUNDLGNBQWMsS0FBZjtBQUhWLEtBRE07QUFKYixDQXpCVSxFQXFDWDtBQUNJSixVQUFNLFFBRFY7QUFFSUMsZUFBVyxtQkFBQUMsQ0FBUSxFQUFSLENBRmY7QUFHSUMsVUFBTSxFQUFDQyxjQUFjLEtBQWYsRUFIVjtBQUlJQyxjQUFVLENBQ047QUFDSUwsY0FBTSxFQURWO0FBRUlDLG1CQUFXLG1CQUFBQyxDQUFRLEVBQVIsQ0FGZjtBQUdJQyxjQUFNLEVBQUNDLGNBQWMsS0FBZjtBQUhWLEtBRE07QUFKZCxDQXJDVyxFQWlEVjtBQUNHSixVQUFNLFlBRFQ7QUFFR0MsZUFBVyxtQkFBQUMsQ0FBUSxFQUFSLENBRmQ7QUFHR0MsVUFBTSxFQUFDQyxjQUFjLElBQWYsRUFIVDtBQUlHQyxjQUFVLENBQ047QUFDSUwsY0FBTSxFQURWO0FBRUlDLG1CQUFXLG1CQUFBQyxDQUFRLEVBQVIsQ0FGZjtBQUdJQyxjQUFNLEVBQUNDLGNBQWMsSUFBZjtBQUhWLEtBRE07QUFKYixDQWpEVSxFQTZEWDtBQUNJSixVQUFNLFFBRFY7QUFFSUMsZUFBVyxtQkFBQUMsQ0FBUSxFQUFSLENBRmY7QUFHSUMsVUFBTSxFQUFDQyxjQUFjLElBQWYsRUFIVjtBQUlJQyxjQUFVLENBQ047QUFDSUwsY0FBTSxFQURWO0FBRUlDLG1CQUFXLG1CQUFBQyxDQUFRLEVBQVIsQ0FGZjtBQUdJQyxjQUFNLEVBQUNDLGNBQWMsSUFBZjtBQUhWLEtBRE07QUFKZCxDQTdEVyxFQXlFWDtBQUNJSixVQUFNLEdBRFY7QUFFSUMsZUFBVyxtQkFBQUMsQ0FBUSxFQUFSLENBRmY7QUFHSUMsVUFBTSxFQUFDQyxjQUFjLEtBQWYsRUFIVjtBQUlJQyxjQUFVLENBQ047QUFDSUwsY0FBTSxFQURWO0FBRUlDLG1CQUFXLG1CQUFBQyxDQUFRLEVBQVIsQ0FGZjtBQUdJQyxjQUFNLEVBQUNDLGNBQWMsS0FBZjtBQUhWLEtBRE07QUFKZCxDQXpFVyxDQUFmOztBQXVGQSxJQUFNeEUsU0FBUyxJQUFJLG1EQUFKLENBQWM7QUFDekIwRSxxQkFBaUIsUUFEUTtBQUV6QkMsVUFBTSxTQUZtQjtBQUd6QlI7QUFIeUIsQ0FBZCxDQUFmOztBQU1BbkUsT0FBTzRFLFVBQVAsQ0FBa0IsVUFBQ0MsRUFBRCxFQUFLQyxJQUFMLEVBQVdDLElBQVgsRUFBb0I7QUFDbEMsUUFBSXJFLGNBQWNDLGFBQWFDLE9BQWIsQ0FBcUIsY0FBckIsQ0FBbEI7QUFDQSxRQUFJaUUsR0FBR04sSUFBSCxDQUFRQyxZQUFaLEVBQTBCO0FBQ3RCLFlBQUksQ0FBQzlELFdBQUQsSUFBZ0JBLGdCQUFnQixJQUFwQyxFQUEwQztBQUN0Q3FFLGlCQUFLO0FBQ0RYLHNCQUFNLFFBREw7QUFFRFksdUJBQU8sRUFBQ0MsVUFBVUosR0FBR0ssUUFBZDtBQUZOLGFBQUw7QUFJSDtBQUNKO0FBQ0RIO0FBR0gsQ0FiRDs7QUFlQSx5REFBZS9FLE1BQWYsRTs7Ozs7O0FDOUdBOztBQUVBO0FBQ0Esb0NBQW1PO0FBQ25PO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnSkFBZ0osa0ZBQWtGO0FBQ2xPLHlKQUF5SixrRkFBa0Y7QUFDM087QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsZ0NBQWdDLFVBQVUsRUFBRTtBQUM1QyxDOzs7Ozs7QUNwQkE7QUFDQTs7O0FBR0E7QUFDQSx3ZUFBeWUsbUJBQW1CLEdBQUcsUUFBUSx3QkFBd0IsNkhBQTZILGtCQUFrQiwwQkFBMEIsR0FBRyxvQkFBb0IsbUJBQW1CLEdBQUcsK0NBQStDLGtCQUFrQixHQUFHLDBGQUEwRixtQkFBbUIsR0FBRyx5QkFBeUIsa0JBQWtCLG1CQUFtQixrQ0FBa0MsNkJBQTZCLEdBQUcsdUNBQXVDLHVCQUF1QixHQUFHLHVDQUF1QyxxQkFBcUIsc0JBQXNCLHdCQUF3QixHQUFHLGNBQWMsaUJBQWlCLGdCQUFnQixzQkFBc0IsR0FBRyxXQUFXLHFCQUFxQix5QkFBeUIsR0FBRyxpQ0FBaUMscUJBQXFCLEdBQUcsc0RBQXNELG9DQUFvQyw4QkFBOEIsR0FBRyxHQUFHLDJCQUEyQixrQkFBa0IsOEJBQThCLEdBQUcsNkJBQTZCLHNCQUFzQixHQUFHLEdBQUcsZ0JBQWdCLDBSQUEwUixTQUFTLE1BQU0sV0FBVyxNQUFNLFVBQVUsS0FBSyxLQUFLLHNCQUFzQixLQUFLLEtBQUssV0FBVyxLQUFLLEtBQUssVUFBVSxNQUFNLFdBQVcsS0FBSyxVQUFVLEtBQUssT0FBTyxVQUFVLEtBQUssS0FBSyxVQUFVLFVBQVUsV0FBVyxXQUFXLE1BQU0sV0FBVyxLQUFLLFdBQVcsTUFBTSxXQUFXLEtBQUssVUFBVSxXQUFXLFdBQVcsS0FBSyxLQUFLLFVBQVUsVUFBVSxXQUFXLEtBQUssS0FBSyxVQUFVLFdBQVcsTUFBTSxXQUFXLEtBQUssVUFBVSxNQUFNLFdBQVcsS0FBSyxNQUFNLFdBQVcsS0FBSyxLQUFLLEtBQUssS0FBSyxXQUFXLEtBQUssS0FBSyxVQUFVLEtBQUssbXNFQUFtc0UsaUJBQWlCLDhSQUE4UixpQkFBaUIsdVJBQXVSLG1CQUFtQiwwdUNBQTB1QyxtQkFBbUIsR0FBRyxVQUFVLHdCQUF3Qiw2SEFBNkgsb0JBQW9CLDBCQUEwQixHQUFHLHNCQUFzQixtQkFBbUIsR0FBRyxpREFBaUQsa0JBQWtCLEdBQUcsNEZBQTRGLG1CQUFtQixHQUFHLDJCQUEyQixrQkFBa0IsbUJBQW1CLGtDQUFrQyw2QkFBNkIsR0FBRyx5Q0FBeUMsdUJBQXVCLEdBQUcseUNBQXlDLHFCQUFxQixzQkFBc0Isd0JBQXdCLEdBQUcsZ0JBQWdCLGlCQUFpQixnQkFBZ0Isc0JBQXNCLEdBQUcsYUFBYSxxQkFBcUIseUJBQXlCLEdBQUcsbUNBQW1DLHFCQUFxQixHQUFHLHdEQUF3RCx3Q0FBd0MsOEJBQThCLE9BQU8sR0FBRyw2QkFBNkIsc0JBQXNCLDhCQUE4QixPQUFPLG1DQUFtQyxzQkFBc0IsT0FBTyxHQUFHLHVDQUF1Qzs7QUFFaHVQOzs7Ozs7O0FDUEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsaUJBQWlCO0FBQ2xDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQyx3QkFBd0I7QUFDM0QsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzFCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9CQUFvQixTQUFTLHNCQUFzQixFQUFFO0FBQ3JEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCO0FBQ2hCLE9BQU87QUFDUDtBQUNBLG1CQUFtQiwyQkFBMkI7QUFDOUM7QUFDQTtBQUNBLGFBQWEsK0JBQStCO0FBQzVDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsc0NBQXNDLFVBQVUsRUFBRTtBQUNuRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQjtBQUN0QixhQUFhO0FBQ2I7QUFDQSx3QkFBd0IsNkNBQTZDO0FBQ3JFO0FBQ0E7QUFDQTtBQUNBLHVDQUF1QyxTQUFTLGNBQWMsRUFBRTtBQUNoRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1Q0FBdUMsU0FBUyxnQkFBZ0IsRUFBRTtBQUNsRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1Q0FBdUMsU0FBUyxjQUFjLEVBQUU7QUFDaEU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPLDJCQUEyQjtBQUNsQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBLG9CQUFvQix5QkFBeUI7QUFDN0M7QUFDQSxvQkFBb0IsMEJBQTBCO0FBQzlDO0FBQ0Esb0JBQW9CLDBCQUEwQjtBQUM5QztBQUNBLG9CQUFvQiwwQkFBMEI7QUFDOUM7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPLHdDQUF3QyxtQkFBbUIsRUFBRTtBQUNwRTtBQUNBLGtCQUFrQixxQ0FBcUM7QUFDdkQ7QUFDQSxvQkFBb0I7QUFDcEIsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLG9CQUFvQjtBQUNwQixXQUFXO0FBQ1g7QUFDQTtBQUNBLG9CQUFvQjtBQUNwQixXQUFXO0FBQ1g7QUFDQTtBQUNBLG1CQUFtQixnQ0FBZ0M7QUFDbkQscUJBQXFCLHNCQUFzQjtBQUMzQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQSx1QkFBdUIsa0NBQWtDO0FBQ3pEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLDZCQUE2QjtBQUNsRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQSx1QkFBdUIsa0NBQWtDO0FBQ3pEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLHNCQUFzQjtBQUMzQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQSx1QkFBdUIsa0NBQWtDO0FBQ3pEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9CQUFvQjtBQUNwQixXQUFXO0FBQ1gsdUJBQXVCLDJCQUEyQjtBQUNsRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQkFBb0I7QUFDcEIsV0FBVztBQUNYLHVCQUF1QiwyQkFBMkI7QUFDbEQ7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIscUJBQXFCO0FBQ3RDLG1CQUFtQiwyQkFBMkI7QUFDOUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7OztBQ3ZOQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkNBQWdMO0FBQ2hMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0Usd0RBQXdELElBQUk7O0FBRTNJO0FBQ0EsWUFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7O0FBRUQ7Ozs7Ozs7QUN6Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQixTQUFTLGFBQWEsRUFBRTtBQUM5QyxpQkFBaUIscUJBQXFCO0FBQ3RDLG1CQUFtQiwyQkFBMkI7QUFDOUMsb0JBQW9CLDZCQUE2QjtBQUNqRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsMEJBQTBCO0FBQzdDLHFCQUFxQixxQ0FBcUM7QUFDMUQsdUJBQXVCLCtCQUErQjtBQUN0RDtBQUNBLHlCQUF5QixtQ0FBbUM7QUFDNUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUIsNEJBQTRCO0FBQ25EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIseUNBQXlDLFlBQVksRUFBRTtBQUN4RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsMEJBQTBCO0FBQzdDLHFCQUFxQixxQ0FBcUM7QUFDMUQsdUJBQXVCLCtCQUErQjtBQUN0RDtBQUNBLHlCQUF5QixrQ0FBa0M7QUFDM0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUIsNEJBQTRCO0FBQ25EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIseUNBQXlDLFlBQVksRUFBRTtBQUN4RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsMEJBQTBCO0FBQzdDLHFCQUFxQixxQ0FBcUM7QUFDMUQsdUJBQXVCLCtCQUErQjtBQUN0RDtBQUNBLHlCQUF5QixxQ0FBcUM7QUFDOUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUIsNEJBQTRCO0FBQ25EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIseUNBQXlDLFlBQVksRUFBRTtBQUN4RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixxQkFBcUI7QUFDdEMsbUJBQW1CLDJCQUEyQjtBQUM5QyxvQkFBb0IsNkJBQTZCO0FBQ2pEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLG1DQUFtQztBQUN0RCxtQkFBbUIsU0FBUyw4QkFBOEIsRUFBRTtBQUM1RDtBQUNBO0FBQ0Esc0JBQXNCO0FBQ3RCLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsbUNBQW1DO0FBQ3RELG1CQUFtQixTQUFTLDhCQUE4QixFQUFFO0FBQzVEO0FBQ0E7QUFDQSxzQkFBc0I7QUFDdEIsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQixtQ0FBbUM7QUFDdEQsbUJBQW1CLFNBQVMsOEJBQThCLEVBQUU7QUFDNUQ7QUFDQTtBQUNBLHNCQUFzQjtBQUN0QixhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLG1DQUFtQztBQUN0RCxtQkFBbUIsU0FBUyw4QkFBOEIsRUFBRTtBQUM1RDtBQUNBO0FBQ0Esc0JBQXNCO0FBQ3RCLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsbUNBQW1DO0FBQ3RELG1CQUFtQixTQUFTLDhCQUE4QixFQUFFO0FBQzVEO0FBQ0E7QUFDQSxzQkFBc0I7QUFDdEIsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQixtQ0FBbUM7QUFDdEQsbUJBQW1CLFNBQVMsOEJBQThCLEVBQUU7QUFDNUQ7QUFDQTtBQUNBLHNCQUFzQjtBQUN0QixhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIscUJBQXFCO0FBQ3RDLG1CQUFtQiwyQkFBMkI7QUFDOUMsb0JBQW9CLDZCQUE2QjtBQUNqRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQiwwQkFBMEI7QUFDN0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsMEJBQTBCO0FBQzdDO0FBQ0E7QUFDQSxvQkFBb0I7QUFDcEIsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsc0JBQXNCO0FBQ3ZDLG1CQUFtQixxQkFBcUI7QUFDeEMscUJBQXFCLDBCQUEwQjtBQUMvQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQiwwQkFBMEI7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3QkFBd0I7QUFDeEIsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7O0FDMU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2Q0FBZ0w7QUFDaEw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSx3REFBd0QsSUFBSTs7QUFFM0k7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQzs7QUFFRDs7Ozs7OztBQ3pDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCLFNBQVMsOEJBQThCLEVBQUU7QUFDL0QsaUJBQWlCLDJCQUEyQjtBQUM1QyxtQkFBbUIscUJBQXFCO0FBQ3hDLHFCQUFxQiwyQkFBMkI7QUFDaEQsc0JBQXNCLDZCQUE2QjtBQUNuRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQiw0QkFBNEI7QUFDbEQ7QUFDQSx5QkFBeUIsU0FBUyxxQkFBcUIsRUFBRTtBQUN6RDtBQUNBO0FBQ0Esd0JBQXdCLHdCQUF3QjtBQUNoRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQixxQkFBcUI7QUFDeEMscUJBQXFCLDBCQUEwQjtBQUMvQztBQUNBO0FBQ0Esc0JBQXNCO0FBQ3RCLGFBQWE7QUFDYjtBQUNBO0FBQ0EscUJBQXFCLDBCQUEwQjtBQUMvQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQixxQkFBcUI7QUFDeEMscUJBQXFCLDJCQUEyQjtBQUNoRCxzQkFBc0IsNkJBQTZCO0FBQ25EO0FBQ0E7QUFDQSxxQkFBcUIsc0NBQXNDO0FBQzNELHVCQUF1QiwyQkFBMkI7QUFDbEQ7QUFDQTtBQUNBLHdCQUF3QjtBQUN4QixlQUFlO0FBQ2Y7QUFDQSx5QkFBeUIseUJBQXlCO0FBQ2xEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMEJBQTBCLDZCQUE2QjtBQUN2RDtBQUNBLDZCQUE2QixTQUFTLFlBQVksRUFBRTtBQUNwRCwrQkFBK0IsNkNBQTZDO0FBQzVFO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCLFNBQVMsWUFBWSxFQUFFO0FBQ3BELCtCQUErQiw2Q0FBNkM7QUFDNUU7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkIsU0FBUyxZQUFZLEVBQUU7QUFDcEQsK0JBQStCLDRDQUE0QztBQUMzRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQixzQ0FBc0M7QUFDM0QsdUJBQXVCLDJCQUEyQjtBQUNsRDtBQUNBO0FBQ0Esd0JBQXdCO0FBQ3hCLGVBQWU7QUFDZjtBQUNBLHlCQUF5Qix5QkFBeUI7QUFDbEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwQkFBMEIsNkJBQTZCO0FBQ3ZEO0FBQ0EsNkJBQTZCLFNBQVMsWUFBWSxFQUFFO0FBQ3BELCtCQUErQiw2Q0FBNkM7QUFDNUU7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkIsU0FBUyxZQUFZLEVBQUU7QUFDcEQsK0JBQStCLDZDQUE2QztBQUM1RTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QixTQUFTLFlBQVksRUFBRTtBQUNwRCwrQkFBK0IsNENBQTRDO0FBQzNFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLHNDQUFzQztBQUMzRCx1QkFBdUIsMkJBQTJCO0FBQ2xEO0FBQ0E7QUFDQSx3QkFBd0I7QUFDeEIsZUFBZTtBQUNmO0FBQ0EseUJBQXlCLHlCQUF5QjtBQUNsRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDBCQUEwQiw2QkFBNkI7QUFDdkQ7QUFDQSw2QkFBNkIsU0FBUyxZQUFZLEVBQUU7QUFDcEQsK0JBQStCLDZDQUE2QztBQUM1RTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QixTQUFTLFlBQVksRUFBRTtBQUNwRCwrQkFBK0IsNkNBQTZDO0FBQzVFO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCLFNBQVMsWUFBWSxFQUFFO0FBQ3BELCtCQUErQiw0Q0FBNEM7QUFDM0U7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIsc0NBQXNDO0FBQzNELHVCQUF1QiwyQkFBMkI7QUFDbEQ7QUFDQTtBQUNBLHdCQUF3QjtBQUN4QixlQUFlO0FBQ2Y7QUFDQSx5QkFBeUIseUJBQXlCO0FBQ2xEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMEJBQTBCLDZCQUE2QjtBQUN2RDtBQUNBLDZCQUE2QixTQUFTLFlBQVksRUFBRTtBQUNwRCwrQkFBK0IsNkNBQTZDO0FBQzVFO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCLFNBQVMsWUFBWSxFQUFFO0FBQ3BELCtCQUErQiw2Q0FBNkM7QUFDNUU7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkIsU0FBUyxZQUFZLEVBQUU7QUFDcEQsK0JBQStCLDRDQUE0QztBQUMzRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQixzQ0FBc0M7QUFDM0QsdUJBQXVCLDJCQUEyQjtBQUNsRDtBQUNBO0FBQ0Esd0JBQXdCO0FBQ3hCLGVBQWU7QUFDZjtBQUNBLHlCQUF5Qix5QkFBeUI7QUFDbEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwQkFBMEIsNkJBQTZCO0FBQ3ZEO0FBQ0EsNkJBQTZCLFNBQVMsWUFBWSxFQUFFO0FBQ3BELCtCQUErQiw2Q0FBNkM7QUFDNUU7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkIsU0FBUyxZQUFZLEVBQUU7QUFDcEQsK0JBQStCLDZDQUE2QztBQUM1RTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QixTQUFTLFlBQVksRUFBRTtBQUNwRCwrQkFBK0IsNENBQTRDO0FBQzNFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLHNDQUFzQztBQUMzRCx1QkFBdUIsMkJBQTJCO0FBQ2xEO0FBQ0E7QUFDQSx3QkFBd0I7QUFDeEIsZUFBZTtBQUNmO0FBQ0EseUJBQXlCLHlCQUF5QjtBQUNsRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDBCQUEwQiw2QkFBNkI7QUFDdkQ7QUFDQSw2QkFBNkIsU0FBUyxZQUFZLEVBQUU7QUFDcEQsK0JBQStCLDZDQUE2QztBQUM1RTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QixTQUFTLFlBQVksRUFBRTtBQUNwRCwrQkFBK0IsNkNBQTZDO0FBQzVFO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCLFNBQVMsWUFBWSxFQUFFO0FBQ3BELCtCQUErQiw0Q0FBNEM7QUFDM0U7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQixxQkFBcUI7QUFDeEMscUJBQXFCLDJCQUEyQjtBQUNoRCxzQkFBc0IsNkJBQTZCO0FBQ25EO0FBQ0E7QUFDQSxxQkFBcUIsNENBQTRDO0FBQ2pFO0FBQ0E7QUFDQSxzQkFBc0I7QUFDdEIsYUFBYTtBQUNiO0FBQ0E7QUFDQSxxQkFBcUIsNENBQTRDO0FBQ2pFO0FBQ0E7QUFDQSxzQkFBc0I7QUFDdEIsYUFBYTtBQUNiO0FBQ0E7QUFDQSxxQkFBcUIsNENBQTRDO0FBQ2pFO0FBQ0E7QUFDQSxzQkFBc0I7QUFDdEIsYUFBYTtBQUNiO0FBQ0E7QUFDQSxxQkFBcUIsNENBQTRDO0FBQ2pFO0FBQ0E7QUFDQSxzQkFBc0I7QUFDdEIsYUFBYTtBQUNiO0FBQ0E7QUFDQSxxQkFBcUIsNENBQTRDO0FBQ2pFO0FBQ0E7QUFDQSxzQkFBc0I7QUFDdEIsYUFBYTtBQUNiO0FBQ0E7QUFDQSxxQkFBcUIsNENBQTRDO0FBQ2pFO0FBQ0E7QUFDQSxzQkFBc0I7QUFDdEIsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7QUNyWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZDQUFnTDtBQUNoTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLHdEQUF3RCxJQUFJOztBQUUzSTtBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDOztBQUVEOzs7Ozs7O0FDekNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQkFBc0IsU0FBUyxnQ0FBZ0MsRUFBRTtBQUNqRSxpQkFBaUIsMkJBQTJCO0FBQzVDLGtCQUFrQiwyQkFBMkI7QUFDN0M7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0IsNEJBQTRCO0FBQzlDLG9CQUFvQixpQ0FBaUM7QUFDckQscUJBQXFCLFNBQVMscUJBQXFCLEVBQUU7QUFDckQ7QUFDQTtBQUNBLG9CQUFvQix3Q0FBd0M7QUFDNUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIscUJBQXFCO0FBQ3hDLHFCQUFxQiwrQkFBK0I7QUFDcEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EscUJBQXFCLCtCQUErQjtBQUNwRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDBCQUEwQixTQUFTLGlCQUFpQixFQUFFO0FBQ3REO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMEJBQTBCLFNBQVMsaUJBQWlCLEVBQUU7QUFDdEQ7QUFDQSx1QkFBdUIsU0FBUyxrQ0FBa0MsRUFBRTtBQUNwRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMEJBQTBCLFNBQVMsaUJBQWlCLEVBQUU7QUFDdEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQixxQkFBcUI7QUFDeEMscUJBQXFCLCtCQUErQjtBQUNwRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBLDJCQUEyQiwwQ0FBMEM7QUFDckUsNkJBQTZCLDBCQUEwQjtBQUN2RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0EsNkJBQTZCLDRCQUE0QjtBQUN6RDtBQUNBO0FBQ0E7QUFDQSwyQkFBMkIsMENBQTBDO0FBQ3JFLDZCQUE2QiwwQkFBMEI7QUFDdkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBLCtCQUErQiw0QkFBNEI7QUFDM0Q7QUFDQTtBQUNBO0FBQ0EsMkJBQTJCLDBDQUEwQztBQUNyRSw2QkFBNkIsMEJBQTBCO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQSwrQkFBK0IsNEJBQTRCO0FBQzNEO0FBQ0E7QUFDQTtBQUNBLDJCQUEyQiwwQ0FBMEM7QUFDckUsNkJBQTZCLDBCQUEwQjtBQUN2RDtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9DQUFvQyxpQkFBaUI7QUFDckQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0EsK0JBQStCLDRCQUE0QjtBQUMzRDtBQUNBO0FBQ0E7QUFDQSwyQkFBMkIsU0FBUyxnQkFBZ0IsRUFBRTtBQUN0RDtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIseUNBQXlDLGlCQUFpQixFQUFFO0FBQy9FO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7OztBQzdMQTtBQUNBO0FBQ0E7QUFDQSwyQ0FBdU47QUFDdk47QUFDQSw2Q0FBZ0w7QUFDaEw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSx3REFBd0QsSUFBSTs7QUFFM0k7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQzs7QUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDekNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CLFNBQVMsdUJBQXVCLEVBQUU7QUFDdEQ7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7QUNkQTtBQUNBO0FBQ0E7QUFDQSx3QkFBbU07QUFDbk07QUFDQTtBQUNBO0FBQ0EsMkNBQXVOO0FBQ3ZOO0FBQ0EsNkNBQWdMO0FBQ2hMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0Usd0RBQXdELElBQUk7O0FBRTNJO0FBQ0EsWUFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7O0FBRUQ7Ozs7Ozs7QUM3Q0E7O0FBRUE7QUFDQSxvQ0FBbU87QUFDbk87QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdKQUFnSixrRkFBa0Y7QUFDbE8seUpBQXlKLGtGQUFrRjtBQUMzTztBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxnQ0FBZ0MsVUFBVSxFQUFFO0FBQzVDLEM7Ozs7OztBQ3BCQTtBQUNBOzs7QUFHQTtBQUNBLHllQUEwZSwrQkFBK0IsR0FBRyxZQUFZLGlCQUFpQixHQUFHLGlCQUFpQixxQkFBcUIsdUJBQXVCLDZCQUE2QixHQUFHLDZCQUE2QixpQkFBaUIseUJBQXlCLDJCQUEyQix1QkFBdUIsc0NBQXNDLEdBQUcsR0FBRyxxQkFBcUIscUJBQXFCLEdBQUcsd0JBQXdCLDJCQUEyQixHQUFHLG1DQUFtQyx3QkFBd0IsR0FBRywwQkFBMEIsbUJBQW1CLHNCQUFzQixHQUFHLHVDQUF1QyxvQkFBb0IsR0FBRyxrREFBa0QscUJBQXFCLEdBQUcseUNBQXlDLHVCQUF1QixtQkFBbUIsR0FBRyw2Q0FBNkMseUJBQXlCLEdBQUcsbUhBQW1ILGtCQUFrQixrQkFBa0IsR0FBRyx3Q0FBd0Msc0JBQXNCLEdBQUcscUNBQXFDLHdCQUF3QixHQUFHLHNDQUFzQyx5QkFBeUIsR0FBRyxvQ0FBb0MsY0FBYyxnQkFBZ0IsR0FBRyx5Q0FBeUMscUJBQXFCLHNCQUFzQixHQUFHLDRCQUE0QixtQkFBbUIsR0FBRyxrQkFBa0Isc0NBQXNDLEdBQUcsMkJBQTJCLCtCQUErQixHQUFHLG1CQUFtQixrQkFBa0IsR0FBRyw2QkFBNkIseUJBQXlCLEdBQUcsMkNBQTJDLHlCQUF5QixHQUFHLGlFQUFpRSxvQ0FBb0MsR0FBRyxtQ0FBbUMsd0JBQXdCLEdBQUcsa0NBQWtDLHdCQUF3QixHQUFHLDZCQUE2QixZQUFZLGtCQUFrQiwwQkFBMEIsb0JBQW9CLHdCQUF3QixHQUFHLHVIQUF1SCx5QkFBeUIsR0FBRyxHQUFHLGdCQUFnQixvQkFBb0IsbUNBQW1DLHlCQUF5QixHQUFHLDRCQUE0QixvQkFBb0IsR0FBRyw0QkFBNEIsb0JBQW9CLEdBQUcseUJBQXlCLG9CQUFvQixHQUFHLDRCQUE0QixvQkFBb0IsR0FBRywyQkFBMkIsb0JBQW9CLEdBQUcsc0tBQXNLLGtCQUFrQixHQUFHLFNBQVMsZUFBZSxnQkFBZ0Isc0JBQXNCLEdBQUcsWUFBWSx5QkFBeUIseUJBQXlCLHVDQUF1QyxHQUFHLDRCQUE0Qix1QkFBdUIsR0FBRyw2QkFBNkIsd0JBQXdCLEdBQUcseUJBQXlCLGVBQWUsR0FBRyxvREFBb0QsdUJBQXVCLEdBQUcsMkJBQTJCLG1CQUFtQix3QkFBd0IsR0FBRyxnQkFBZ0IscUJBQXFCLEdBQUcsZUFBZSxvQkFBb0IsbUJBQW1CLEdBQUcsdUJBQXVCLGlCQUFpQixrQkFBa0IsR0FBRyw4TUFBOE0sNkJBQTZCLEdBQUcsNENBQTRDLHlCQUF5QixrQkFBa0IsOEJBQThCLEdBQUcsNkNBQTZDLHlCQUF5QixrQkFBa0IsOEJBQThCLEdBQUcsd0NBQXdDLHlCQUF5QixrQkFBa0IsOEJBQThCLGtDQUFrQyxHQUFHLGVBQWUsaUJBQWlCLGtCQUFrQixvQkFBb0IseUJBQXlCLHdCQUF3QixxQkFBcUIsOEJBQThCLEdBQUcsc0JBQXNCLGlCQUFpQixrQkFBa0Isd0JBQXdCLHlCQUF5QixxQkFBcUIsdUJBQXVCLEdBQUcsc0JBQXNCLGlCQUFpQixrQkFBa0Isd0JBQXdCLHlCQUF5QixxQkFBcUIsdUJBQXVCLEdBQUcsZ0NBQWdDLHVCQUF1QiwwQkFBMEIsNEJBQTRCLHVDQUF1QyxHQUFHLGNBQWMsb0JBQW9CLEdBQUcsU0FBUyxxQkFBcUIsR0FBRyxnQkFBZ0IsMkJBQTJCLEdBQUcsaUNBQWlDLDJCQUEyQixrQkFBa0IsK0JBQStCLEdBQUcsb0JBQW9CLG9CQUFvQixHQUFHLDBCQUEwQixvQkFBb0IsR0FBRyxjQUFjLDJCQUEyQixHQUFHLCtCQUErQiwyQkFBMkIsa0JBQWtCLCtCQUErQixHQUFHLGtCQUFrQixvQkFBb0IsR0FBRyx3QkFBd0Isb0JBQW9CLEdBQUcsaUJBQWlCLDJCQUEyQixHQUFHLGtDQUFrQywyQkFBMkIsa0JBQWtCLCtCQUErQixHQUFHLHFCQUFxQixvQkFBb0IsR0FBRywyQkFBMkIsb0JBQW9CLEdBQUcsYUFBYSx3QkFBd0IsMEJBQTBCLHNCQUFzQixHQUFHLG9CQUFvQixvQkFBb0Isd0JBQXdCLFlBQVksZUFBZSxlQUFlLGdCQUFnQix5QkFBeUIsK0JBQStCLEdBQUcsa0JBQWtCLHdCQUF3Qix5QkFBeUIsR0FBRyxpREFBaUQsb0JBQW9CLG9CQUFvQixHQUFHLHdCQUF3QixpQkFBaUIsR0FBRyxpREFBaUQsb0JBQW9CLG9CQUFvQixHQUFHLHdCQUF3QixpQkFBaUIsR0FBRyxvQ0FBb0MsaUJBQWlCLHdCQUF3QixnQkFBZ0IsbUJBQW1CLCtCQUErQix3QkFBd0Isd0RBQXdELGdEQUFnRCxHQUFHLDJDQUEyQyxvQkFBb0IsMkJBQTJCLHdCQUF3QixlQUFlLGtCQUFrQix3Q0FBd0MsZ0NBQWdDLDJDQUEyQyxrQ0FBa0MsR0FBRywwQ0FBMEMsb0JBQW9CLDJCQUEyQix3QkFBd0IsZUFBZSxrQkFBa0Isd0NBQXdDLGdDQUFnQywyQ0FBMkMsa0NBQWtDLEdBQUcsb0NBQW9DLGtCQUFrQix3QkFBd0IsZUFBZSxlQUFlLGlCQUFpQixrQkFBa0Isd0JBQXdCLG9DQUFvQyx3QkFBd0Isc0JBQXNCLHVCQUF1QixpQkFBaUIsK0JBQStCLEdBQUcsc0RBQXNELGtCQUFrQixHQUFHLDZEQUE2RCxpQkFBaUIsaUJBQWlCLDhCQUE4QiwwQkFBMEIsR0FBRyw0REFBNEQsaUJBQWlCLGlCQUFpQiw4QkFBOEIsMEJBQTBCLEdBQUcsMkJBQTJCLDBDQUEwQyxHQUFHLDJCQUEyQiwwQ0FBMEMsR0FBRywyQkFBMkIsMENBQTBDLEdBQUcsMEJBQTBCLDBDQUEwQyxHQUFHLHdCQUF3QiwwQ0FBMEMsR0FBRyxtQkFBbUIsbUJBQW1CLG9CQUFvQixHQUFHLDZDQUE2QyxzQkFBc0IsR0FBRywwQkFBMEIscUJBQXFCLEdBQUcsNkJBQTZCLHNCQUFzQixrQkFBa0IsR0FBRyxzQ0FBc0Msd0JBQXdCLDZCQUE2QixnQ0FBZ0MsR0FBRyxzQ0FBc0MsaUJBQWlCLGtCQUFrQixzQkFBc0IsR0FBRyxzQ0FBc0Msb0JBQW9CLEdBQUcsNkNBQTZDLG1CQUFtQixtQkFBbUIsZ0NBQWdDLDRCQUE0QixHQUFHLDRDQUE0QyxtQkFBbUIsbUJBQW1CLGdDQUFnQyw0QkFBNEIsR0FBRyxHQUFHLGdCQUFnQixtUkFBbVIsU0FBUyxLQUFLLEtBQUssV0FBVyxLQUFLLEtBQUssVUFBVSxLQUFLLEtBQUssV0FBVyxXQUFXLFdBQVcsS0FBSyxLQUFLLEtBQUssV0FBVyxXQUFXLFdBQVcsV0FBVyxLQUFLLEtBQUssS0FBSyxXQUFXLEtBQUssS0FBSyxXQUFXLEtBQUssS0FBSyxXQUFXLEtBQUssS0FBSyxVQUFVLFdBQVcsS0FBSyxLQUFLLFVBQVUsS0FBSyxLQUFLLFdBQVcsS0FBSyxLQUFLLFdBQVcsVUFBVSxLQUFLLEtBQUssV0FBVyxLQUFLLE9BQU8sVUFBVSxVQUFVLEtBQUssS0FBSyxXQUFXLEtBQUssS0FBSyxXQUFXLEtBQUssS0FBSyxXQUFXLEtBQUssS0FBSyxVQUFVLFVBQVUsS0FBSyxLQUFLLFdBQVcsV0FBVyxLQUFLLEtBQUssVUFBVSxLQUFLLEtBQUssV0FBVyxLQUFLLEtBQUssV0FBVyxLQUFLLEtBQUssVUFBVSxLQUFLLEtBQUssV0FBVyxLQUFLLEtBQUssV0FBVyxLQUFLLE1BQU0sV0FBVyxLQUFLLEtBQUssV0FBVyxLQUFLLEtBQUssV0FBVyxLQUFLLEtBQUssS0FBSyxVQUFVLFdBQVcsVUFBVSxXQUFXLEtBQUssT0FBTyxXQUFXLEtBQUssS0FBSyxLQUFLLFVBQVUsV0FBVyxXQUFXLEtBQUssS0FBSyxVQUFVLEtBQUssS0FBSyxVQUFVLEtBQUssS0FBSyxVQUFVLEtBQUssS0FBSyxVQUFVLEtBQUssS0FBSyxVQUFVLEtBQUssU0FBUyxVQUFVLEtBQUssS0FBSyxVQUFVLFVBQVUsV0FBVyxLQUFLLEtBQUssV0FBVyxXQUFXLFdBQVcsS0FBSyxLQUFLLFdBQVcsS0FBSyxLQUFLLFdBQVcsS0FBSyxLQUFLLFVBQVUsS0FBSyxNQUFNLFdBQVcsS0FBSyxLQUFLLFVBQVUsV0FBVyxLQUFLLEtBQUssV0FBVyxLQUFLLEtBQUssVUFBVSxVQUFVLEtBQUssS0FBSyxVQUFVLFVBQVUsS0FBSyxTQUFTLFdBQVcsS0FBSyxLQUFLLFdBQVcsVUFBVSxXQUFXLEtBQUssS0FBSyxXQUFXLFVBQVUsV0FBVyxLQUFLLEtBQUssV0FBVyxVQUFVLFdBQVcsV0FBVyxLQUFLLEtBQUssVUFBVSxVQUFVLFVBQVUsV0FBVyxXQUFXLFdBQVcsV0FBVyxLQUFLLEtBQUssVUFBVSxVQUFVLFdBQVcsV0FBVyxXQUFXLFdBQVcsS0FBSyxLQUFLLFVBQVUsVUFBVSxXQUFXLFdBQVcsV0FBVyxXQUFXLEtBQUssS0FBSyxXQUFXLFdBQVcsV0FBVyxXQUFXLEtBQUssS0FBSyxVQUFVLEtBQUssS0FBSyxXQUFXLEtBQUssS0FBSyxXQUFXLEtBQUssS0FBSyxXQUFXLFVBQVUsV0FBVyxLQUFLLEtBQUssVUFBVSxLQUFLLEtBQUssVUFBVSxLQUFLLEtBQUssV0FBVyxLQUFLLEtBQUssV0FBVyxVQUFVLFdBQVcsS0FBSyxLQUFLLFVBQVUsS0FBSyxLQUFLLFVBQVUsS0FBSyxLQUFLLFdBQVcsS0FBSyxLQUFLLFdBQVcsVUFBVSxXQUFXLEtBQUssS0FBSyxVQUFVLEtBQUssS0FBSyxVQUFVLEtBQUssS0FBSyxXQUFXLFdBQVcsV0FBVyxLQUFLLEtBQUssVUFBVSxXQUFXLFVBQVUsVUFBVSxVQUFVLFVBQVUsV0FBVyxXQUFXLEtBQUssS0FBSyxXQUFXLFdBQVcsS0FBSyxNQUFNLFVBQVUsVUFBVSxLQUFLLEtBQUssVUFBVSxLQUFLLE1BQU0sVUFBVSxVQUFVLEtBQUssS0FBSyxVQUFVLEtBQUssS0FBSyxVQUFVLFdBQVcsVUFBVSxVQUFVLFdBQVcsV0FBVyxXQUFXLFdBQVcsS0FBSyxLQUFLLFVBQVUsV0FBVyxXQUFXLFVBQVUsVUFBVSxXQUFXLFdBQVcsV0FBVyxXQUFXLEtBQUssS0FBSyxVQUFVLFdBQVcsV0FBVyxVQUFVLFVBQVUsV0FBVyxXQUFXLFdBQVcsV0FBVyxLQUFLLEtBQUssVUFBVSxXQUFXLFVBQVUsVUFBVSxVQUFVLFVBQVUsV0FBVyxXQUFXLFdBQVcsV0FBVyxXQUFXLFVBQVUsV0FBVyxLQUFLLEtBQUssVUFBVSxLQUFLLEtBQUssVUFBVSxVQUFVLFdBQVcsV0FBVyxLQUFLLEtBQUssVUFBVSxVQUFVLFdBQVcsV0FBVyxLQUFLLEtBQUssV0FBVyxLQUFLLEtBQUssV0FBVyxLQUFLLEtBQUssV0FBVyxLQUFLLEtBQUssV0FBVyxLQUFLLEtBQUssV0FBVyxLQUFLLEtBQUssVUFBVSxVQUFVLEtBQUssTUFBTSxXQUFXLEtBQUssS0FBSyxXQUFXLEtBQUssS0FBSyxLQUFLLFVBQVUsS0FBSyxLQUFLLFdBQVcsV0FBVyxXQUFXLEtBQUssS0FBSyxVQUFVLFVBQVUsVUFBVSxLQUFLLEtBQUssVUFBVSxLQUFLLEtBQUssVUFBVSxVQUFVLFdBQVcsV0FBVyxLQUFLLEtBQUssVUFBVSxVQUFVLFdBQVcsV0FBVyxLQUFLLCtnREFBK2dELGFBQWEsY0FBYywrQkFBK0IsNEVBQTRFLDRCQUE0QixXQUFXLE9BQU8sbUJBQW1CLG1CQUFtQixxRUFBcUUsZ0RBQWdELDRDQUE0QyxhQUFhLE9BQU8saUJBQWlCLGdCQUFnQiwwQkFBMEIsK0VBQStFLHVFQUF1RSx5RUFBeUUsa0NBQWtDLFdBQVcsbUNBQW1DLDhCQUE4QixtRUFBbUUsYUFBYSxFQUFFLFNBQVMsc0NBQXNDLDBCQUEwQixxRUFBcUUsd0RBQXdELHFEQUFxRCxrRkFBa0YsNENBQTRDLFdBQVcsd0JBQXdCLCtCQUErQixhQUFhLEVBQUUsV0FBVyxPQUFPLFFBQVEsNFRBQTRULCtCQUErQixJQUFJLGFBQWEsaUJBQWlCLElBQUksa0JBQWtCLHFCQUFxQix1QkFBdUIsNkJBQTZCLElBQUksOEJBQThCLG9CQUFvQix5QkFBeUIsMkJBQTJCLHVCQUF1QixzQ0FBc0MsTUFBTSxJQUFJLHNCQUFzQixxQkFBcUIsSUFBSSx5QkFBeUIsMkJBQTJCLElBQUksb0NBQW9DLHdCQUF3QixJQUFJLDJCQUEyQixtQkFBbUIsc0JBQXNCLElBQUksd0NBQXdDLG9CQUFvQixJQUFJLG1EQUFtRCxxQkFBcUIsSUFBSSwwQ0FBMEMsdUJBQXVCLG1CQUFtQixJQUFJLDhDQUE4Qyx5QkFBeUIsSUFBSSxvSEFBb0gsa0JBQWtCLGtCQUFrQixJQUFJLHlDQUF5QyxzQkFBc0IsSUFBSSxzQ0FBc0Msd0JBQXdCLElBQUksdUNBQXVDLHlCQUF5QixJQUFJLHFDQUFxQyxjQUFjLGdCQUFnQixJQUFJLDBDQUEwQyxxQkFBcUIsc0JBQXNCLElBQUksNkJBQTZCLG1CQUFtQixJQUFJLG1CQUFtQixzQ0FBc0MsSUFBSSw0QkFBNEIsK0JBQStCLElBQUksb0JBQW9CLGtCQUFrQixJQUFJLDhCQUE4Qix5QkFBeUIsSUFBSSw0Q0FBNEMseUJBQXlCLElBQUksa0VBQWtFLG9DQUFvQyxJQUFJLG9DQUFvQyx3QkFBd0IsSUFBSSxtQ0FBbUMsd0JBQXdCLElBQUksOEJBQThCLGVBQWUsa0JBQWtCLDBCQUEwQixvQkFBb0Isd0JBQXdCLE1BQU0sMEhBQTBILHlCQUF5QixNQUFNLElBQUksaUJBQWlCLG9CQUFvQixtQ0FBbUMseUJBQXlCLElBQUksNkJBQTZCLG9CQUFvQixJQUFJLDZCQUE2QixvQkFBb0IsSUFBSSwwQkFBMEIsb0JBQW9CLElBQUksNkJBQTZCLG9CQUFvQixJQUFJLDRCQUE0QixvQkFBb0IsSUFBSSx1S0FBdUssa0JBQWtCLElBQUksVUFBVSxlQUFlLGdCQUFnQixzQkFBc0IsSUFBSSxhQUFhLHlCQUF5Qix5QkFBeUIsdUNBQXVDLElBQUksNkJBQTZCLHVCQUF1QixJQUFJLDhCQUE4Qix3QkFBd0IsSUFBSSwwQkFBMEIsZUFBZSxJQUFJLHFEQUFxRCx1QkFBdUIsSUFBSSw0QkFBNEIsbUJBQW1CLHdCQUF3QixJQUFJLGlCQUFpQixxQkFBcUIsSUFBSSxnQkFBZ0Isb0JBQW9CLG1CQUFtQixJQUFJLHdCQUF3QixpQkFBaUIsa0JBQWtCLElBQUksK01BQStNLDZCQUE2QixJQUFJLDZDQUE2Qyx5QkFBeUIsa0JBQWtCLDhCQUE4QixJQUFJLDhDQUE4Qyx5QkFBeUIsa0JBQWtCLDhCQUE4QixJQUFJLHlDQUF5Qyx5QkFBeUIsa0JBQWtCLDhCQUE4QixrQ0FBa0MsSUFBSSxnQkFBZ0IsaUJBQWlCLGtCQUFrQixvQkFBb0IseUJBQXlCLHdCQUF3QixxQkFBcUIsOEJBQThCLElBQUksdUJBQXVCLGlCQUFpQixrQkFBa0Isd0JBQXdCLHlCQUF5QixxQkFBcUIsdUJBQXVCLElBQUksdUJBQXVCLGlCQUFpQixrQkFBa0Isd0JBQXdCLHlCQUF5QixxQkFBcUIsdUJBQXVCLElBQUksaUNBQWlDLHVCQUF1QiwwQkFBMEIsNEJBQTRCLHVDQUF1QyxJQUFJLGVBQWUsb0JBQW9CLElBQUksVUFBVSxxQkFBcUIsSUFBSSxpQkFBaUIsMkJBQTJCLElBQUksa0NBQWtDLDJCQUEyQixrQkFBa0IsK0JBQStCLElBQUkscUJBQXFCLG9CQUFvQixJQUFJLDJCQUEyQixvQkFBb0IsSUFBSSxlQUFlLDJCQUEyQixJQUFJLGdDQUFnQywyQkFBMkIsa0JBQWtCLCtCQUErQixJQUFJLG1CQUFtQixvQkFBb0IsSUFBSSx5QkFBeUIsb0JBQW9CLElBQUksa0JBQWtCLDJCQUEyQixJQUFJLG1DQUFtQywyQkFBMkIsa0JBQWtCLCtCQUErQixJQUFJLHNCQUFzQixvQkFBb0IsSUFBSSw0QkFBNEIsb0JBQW9CLElBQUksY0FBYyx3QkFBd0IsMEJBQTBCLHNCQUFzQixJQUFJLHFCQUFxQixvQkFBb0Isd0JBQXdCLFlBQVksZUFBZSxlQUFlLGdCQUFnQix5QkFBeUIsK0JBQStCLElBQUksbUJBQW1CLHdCQUF3Qix5QkFBeUIsSUFBSSxrREFBa0Qsb0JBQW9CLG9CQUFvQixJQUFJLHlCQUF5QixpQkFBaUIsSUFBSSxrREFBa0Qsb0JBQW9CLG9CQUFvQixJQUFJLHlCQUF5QixpQkFBaUIsSUFBSSxxQ0FBcUMsaUJBQWlCLHdCQUF3QixnQkFBZ0IsbUJBQW1CLCtCQUErQix3QkFBd0Isd0RBQXdELGdEQUFnRCxJQUFJLDRDQUE0QyxvQkFBb0IsMkJBQTJCLHdCQUF3QixlQUFlLGtCQUFrQix3Q0FBd0MsZ0NBQWdDLDJDQUEyQyxrQ0FBa0MsSUFBSSwyQ0FBMkMsb0JBQW9CLDJCQUEyQix3QkFBd0IsZUFBZSxrQkFBa0Isd0NBQXdDLGdDQUFnQywyQ0FBMkMsa0NBQWtDLElBQUkscUNBQXFDLGtCQUFrQix3QkFBd0IsZUFBZSxlQUFlLGlCQUFpQixrQkFBa0Isd0JBQXdCLG9DQUFvQyx3QkFBd0Isc0JBQXNCLHVCQUF1QixpQkFBaUIsK0JBQStCLElBQUksdURBQXVELGtCQUFrQixJQUFJLDhEQUE4RCxpQkFBaUIsaUJBQWlCLDhCQUE4QiwwQkFBMEIsSUFBSSw2REFBNkQsaUJBQWlCLGlCQUFpQiw4QkFBOEIsMEJBQTBCLElBQUksNEJBQTRCLDBDQUEwQyxJQUFJLDRCQUE0QiwwQ0FBMEMsSUFBSSw0QkFBNEIsMENBQTBDLElBQUksMkJBQTJCLDBDQUEwQyxJQUFJLHlCQUF5QiwwQ0FBMEMsSUFBSSxvQkFBb0IsbUJBQW1CLG9CQUFvQixJQUFJLDhDQUE4QyxzQkFBc0IsSUFBSSwyQkFBMkIscUJBQXFCLElBQUksOEJBQThCLHlCQUF5QixrQkFBa0IsTUFBTSx5Q0FBeUMsd0JBQXdCLDZCQUE2QixnQ0FBZ0MsTUFBTSx5Q0FBeUMsaUJBQWlCLGtCQUFrQixzQkFBc0IsTUFBTSx5Q0FBeUMsb0JBQW9CLE1BQU0sZ0RBQWdELG1CQUFtQixtQkFBbUIsZ0NBQWdDLDRCQUE0QixNQUFNLCtDQUErQyxtQkFBbUIsbUJBQW1CLGdDQUFnQyw0QkFBNEIsTUFBTSxJQUFJLHFDQUFxQzs7QUFFNW14Qjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3VDQTswQkFHQTs7OzBCQUdBOzBCQUVBO0FBSEE7c0JBS0E7QUFOQTtBQU9BOztjQUVBOztnQ0FDQTtBQUNBO21CQUNBOytDQUNBOzhCQUVBO0FBQ0E7QUFDQTs7O2dDQUVBO3VCQUNBOzJCQUNBLDRDQUNBOzhEQUNBOytEQUNBO3FCQUNBO0FBQ0Esc0NBQ0E7MEJBQ0E7dURBRUE7QUFDQTtBQUNBOztrREFDQTt1QkFFQTs7c0JBQ0Esc0NBQ0E7dURBQ0E7b0RBQ0E7c0VBQ0E7a0NBQ0E7c0NBQ0E7NEJBRUE7QUFFQTtBQUlBO0FBaENBOztBQXBCQSxHOzs7Ozs7QUNoREE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQkFBb0IsU0FBUyw2QkFBNkIsRUFBRTtBQUM1RCxlQUFlLDJCQUEyQjtBQUMxQyxpQkFBaUIscUJBQXFCO0FBQ3RDLG1CQUFtQiwwQ0FBMEM7QUFDN0QscUJBQXFCLGlEQUFpRDtBQUN0RTtBQUNBO0FBQ0EsdUJBQXVCLDRCQUE0QjtBQUNuRCwwQkFBMEIsU0FBUyxlQUFlLEVBQUU7QUFDcEQ7QUFDQSw2QkFBNkIsNEJBQTRCO0FBQ3pEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QjtBQUN2QixpQ0FBaUMsMkJBQTJCO0FBQzVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBLDZCQUE2Qiw0QkFBNEI7QUFDekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCO0FBQ3ZCLGlDQUFpQywyQkFBMkI7QUFDNUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkJBQTJCO0FBQzNCLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQiwrQkFBK0I7QUFDckQsZ0JBQWdCLDZCQUE2QjtBQUM3QztBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQiwwQkFBMEI7QUFDaEQ7QUFDQTtBQUNBLGtCQUFrQjtBQUNsQixTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7QUN6SEE7O0FBRUE7QUFDQSxvQ0FBbU87QUFDbk87QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdKQUFnSixrRkFBa0Y7QUFDbE8seUpBQXlKLGtGQUFrRjtBQUMzTztBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxnQ0FBZ0MsVUFBVSxFQUFFO0FBQzVDLEM7Ozs7OztBQ3BCQTtBQUNBOzs7QUFHQTtBQUNBLHlXQUEwVyxxRkFBcUY7O0FBRS9iOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNtSUE7d0JBR0E7V0FDQTtBQUNBOzhCQUNBLENBQ0E7Ozs4QkFFQTtpQkFDQTs4QkFDQTs4QkFDQTsrQkFDQTswQ0FDQTt1Q0FDQTt3QkFDQTtBQUlBO0FBWkE7O0FBTkEsRzs7Ozs7O0FDNUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CLCtCQUErQjtBQUNuRCxlQUFlLHlEQUF5RDtBQUN4RSxpQkFBaUIsaUNBQWlDO0FBQ2xELG1CQUFtQiwrQkFBK0I7QUFDbEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQkFBc0I7QUFDdEIsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsc0NBQXNDLFlBQVksRUFBRTtBQUN2RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQkFBb0I7QUFDcEIsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQiw2Q0FBNkM7QUFDbkU7QUFDQTtBQUNBLG9DQUFvQyxTQUFTLFVBQVUsRUFBRTtBQUN6RDtBQUNBO0FBQ0E7QUFDQSx3QkFBd0IsMkJBQTJCO0FBQ25EO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLHVDQUF1QyxlQUFlLEVBQUU7QUFDM0U7QUFDQSw4QkFBOEIsaUNBQWlDO0FBQy9EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3QkFBd0I7QUFDeEI7QUFDQSwyQ0FBMkMsU0FBUyxnQkFBZ0IsRUFBRTtBQUN0RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw4QkFBOEIseUJBQXlCO0FBQ3ZEO0FBQ0E7QUFDQSwrQkFBK0IsTUFBTSxvQkFBb0IsRUFBRTtBQUMzRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsZ0RBQWdEO0FBQy9ELGlCQUFpQixrQ0FBa0M7QUFDbkQsbUJBQW1CLHFCQUFxQjtBQUN4QyxxQkFBcUIsa0NBQWtDO0FBQ3ZEO0FBQ0EscUJBQXFCLDJCQUEyQjtBQUNoRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdCQUF3QjtBQUN4QixlQUFlO0FBQ2Y7QUFDQSwyQkFBMkIscUNBQXFDO0FBQ2hFLDRCQUE0QixnQ0FBZ0M7QUFDNUQ7QUFDQTtBQUNBO0FBQ0EsMkNBQTJDLFNBQVMsa0JBQWtCLEVBQUU7QUFDeEU7QUFDQTtBQUNBLDJCQUEyQjtBQUMzQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyQ0FBMkMsU0FBUyxjQUFjLEVBQUU7QUFDcEU7QUFDQTtBQUNBLDJCQUEyQjtBQUMzQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixtQ0FBbUM7QUFDcEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQSxvQkFBb0IseUJBQXlCO0FBQzdDO0FBQ0Esb0JBQW9CLDBCQUEwQjtBQUM5QztBQUNBLG9CQUFvQiwwQkFBMEI7QUFDOUM7QUFDQSxvQkFBb0IsMEJBQTBCO0FBQzlDO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQjtBQUNoQixPQUFPO0FBQ1A7QUFDQSxtQkFBbUIsNEJBQTRCO0FBQy9DO0FBQ0E7QUFDQSxvQkFBb0I7QUFDcEIsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyx5Q0FBeUMsaUJBQWlCLEVBQUU7QUFDdkUsb0JBQW9CLDRDQUE0QztBQUNoRTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0Esb0JBQW9CLHVCQUF1QjtBQUMzQztBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLFNBQVMsWUFBWSxFQUFFO0FBQ3RDLG9CQUFvQiwyQ0FBMkM7QUFDL0Q7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU8sNkNBQTZDLGlCQUFpQixFQUFFO0FBQ3ZFO0FBQ0E7QUFDQTtBQUNBLFdBQVcsU0FBUyxvREFBb0QsRUFBRTtBQUMxRTtBQUNBLHdCQUF3QiwwQ0FBMEM7QUFDbEU7QUFDQSx3QkFBd0IsdUJBQXVCO0FBQy9DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CO0FBQ3BCLFdBQVc7QUFDWDtBQUNBLHVCQUF1Qiw0QkFBNEI7QUFDbkQsd0JBQXdCLGdDQUFnQztBQUN4RCxtQ0FBbUMsU0FBUyxZQUFZLEVBQUU7QUFDMUQ7QUFDQSxtQ0FBbUMsU0FBUyxZQUFZLEVBQUU7QUFDMUQ7QUFDQSxtQ0FBbUMsU0FBUyxZQUFZLEVBQUU7QUFDMUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDRCQUE0QjtBQUM1QixtQkFBbUI7QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QjtBQUN2QjtBQUNBLG9DQUFvQyx5Q0FBeUM7QUFDN0U7QUFDQSxvQ0FBb0MsdUJBQXVCO0FBQzNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0NBQWdDO0FBQ2hDLHVCQUF1QjtBQUN2QjtBQUNBLG1DQUFtQyw0QkFBNEI7QUFDL0Qsb0NBQW9DLGdDQUFnQztBQUNwRTtBQUNBLHVDQUF1QyxTQUFTLFlBQVksRUFBRTtBQUM5RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUNBQXVDLFNBQVMsWUFBWSxFQUFFO0FBQzlEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1Q0FBdUMsU0FBUyxZQUFZLEVBQUU7QUFDOUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLFNBQVMsWUFBWSxFQUFFO0FBQ3RDLG9CQUFvQiw0Q0FBNEM7QUFDaEU7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCLGtDQUFrQztBQUMzRCxlQUFlLDJCQUEyQjtBQUMxQyxnQkFBZ0IseUJBQXlCO0FBQ3pDO0FBQ0EsZUFBZSxTQUFTLHVDQUF1QyxFQUFFO0FBQ2pFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7OztBQ2hXQTtBQUNBO0FBQ0E7QUFDQSx3QkFBbU07QUFDbk07QUFDQTtBQUNBO0FBQ0EsMkNBQXVOO0FBQ3ZOO0FBQ0EsNkNBQWdMO0FBQ2hMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0Usd0RBQXdELElBQUk7O0FBRTNJO0FBQ0EsWUFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7O0FBRUQ7Ozs7Ozs7QUM3Q0E7O0FBRUE7QUFDQSxvQ0FBbU87QUFDbk87QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdKQUFnSixrRkFBa0Y7QUFDbE8seUpBQXlKLGtGQUFrRjtBQUMzTztBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxnQ0FBZ0MsVUFBVSxFQUFFO0FBQzVDLEM7Ozs7OztBQ3BCQTtBQUNBOzs7QUFHQTtBQUNBLHFJQUFzSSxxRkFBcUY7O0FBRTNOOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2FBOzBCQUdBOzttQkFHQTtBQUZBO0FBR0E7O2NBRUE7O2dDQUNBO0FBQ0E7bUJBQ0E7a0JBQ0EsMENBQ0E7dUNBQ0E7QUFDQSxrQ0FDQTt3QkFDQTtBQUNBO0FBQ0E7OztzQ0FFQTt1QkFDQTtBQUlBO0FBTkE7O0FBbkJBLEc7Ozs7OztBQ3RCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCLFNBQVMsOEJBQThCLEVBQUU7QUFDL0QsaUJBQWlCLHFDQUFxQztBQUN0RCxtQkFBbUIsK0JBQStCO0FBQ2xEO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQiw0QkFBNEI7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7O0FDbkNBO0FBQ0E7QUFDQTtBQUNBLHdCQUFtTTtBQUNuTTtBQUNBO0FBQ0E7QUFDQSwyQ0FBdU47QUFDdk47QUFDQSw2Q0FBZ0w7QUFDaEw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSx3REFBd0QsSUFBSTs7QUFFM0k7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQzs7QUFFRDs7Ozs7OztBQzdDQTs7QUFFQTtBQUNBLG9DQUFtTztBQUNuTztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0pBQWdKLGtGQUFrRjtBQUNsTyx5SkFBeUosa0ZBQWtGO0FBQzNPO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBLGdDQUFnQyxVQUFVLEVBQUU7QUFDNUMsQzs7Ozs7O0FDcEJBO0FBQ0E7OztBQUdBO0FBQ0EsK0pBQWdLLHFGQUFxRjs7QUFFclA7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzBCQTswQkFHQTs7bUJBR0E7QUFGQTtBQUdBOztjQUVBOztnQ0FDQTtBQUNBO21CQUNBO2tCQUNBLDBDQUNBO3VDQUNBO0FBQ0Esa0NBQ0E7d0JBQ0E7QUFDQTtBQUNBOzs7c0NBRUE7dUJBQ0E7QUFJQTtBQU5BOztBQW5CQSxHOzs7Ozs7QUNuQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQkFBb0IsU0FBUyw4QkFBOEIsRUFBRTtBQUM3RCxlQUFlLHFDQUFxQztBQUNwRCxpQkFBaUIsK0JBQStCO0FBQ2hEO0FBQ0EsaUJBQWlCLDRCQUE0QjtBQUM3QztBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7QUN0REE7QUFDQTtBQUNBO0FBQ0Esd0JBQW1NO0FBQ25NO0FBQ0E7QUFDQTtBQUNBLDJDQUF1TjtBQUN2TjtBQUNBLDZDQUFnTDtBQUNoTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLHdEQUF3RCxJQUFJOztBQUUzSTtBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDOztBQUVEOzs7Ozs7O0FDN0NBOztBQUVBO0FBQ0Esb0NBQW1PO0FBQ25PO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnSkFBZ0osa0ZBQWtGO0FBQ2xPLHlKQUF5SixrRkFBa0Y7QUFDM087QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsZ0NBQWdDLFVBQVUsRUFBRTtBQUM1QyxDOzs7Ozs7QUNwQkE7QUFDQTs7O0FBR0E7QUFDQSxpQ0FBa0Msc0NBQXNDLHltQkFBeW1CLEdBQUcsbUJBQW1CLG1CQUFtQixtQkFBbUIsR0FBRyxrQkFBa0IsZ0JBQWdCLG1CQUFtQixHQUFHLHVCQUF1QixtQkFBbUIsR0FBRyxjQUFjLGtMQUFrTCxpQkFBaUIsS0FBSyx1QkFBdUIsS0FBSyx1QkFBdUIsS0FBSyxpQkFBaUIsMk5BQTJOLHNDQUFzQywwbUJBQTBtQixtQkFBbUIsbUJBQW1CLG9CQUFvQixrQkFBa0IsZ0JBQWdCLG9CQUFvQix1QkFBdUIsbUJBQW1CLEVBQUUsaUNBQWlDOztBQUU1cEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNQQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9CQUFvQixTQUFTLHVCQUF1QixFQUFFO0FBQ3REO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7O0FDZEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZDQUFnTDtBQUNoTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLHdEQUF3RCxJQUFJOztBQUUzSTtBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDOztBQUVEOzs7Ozs7O0FDekNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CLHNDQUFzQztBQUMxRCxlQUFlLDJCQUEyQjtBQUMxQyxpQkFBaUIscUJBQXFCO0FBQ3RDLG1CQUFtQiwyQkFBMkI7QUFDOUMscUJBQXFCLGdDQUFnQztBQUNyRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QiwrQkFBK0I7QUFDdEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLCtCQUErQjtBQUM5QztBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsZ0RBQWdELFVBQVUsRUFBRTtBQUMvRTtBQUNBLGdDQUFnQywwQ0FBMEM7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDRCQUE0QjtBQUM1QixtQkFBbUI7QUFDbkI7QUFDQSxnQ0FBZ0MsOENBQThDO0FBQzlFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7Ozs7O0FDNURBO0FBQ0E7O0FBRUEsMkNBQUFGLENBQUlrQyxHQUFKLENBQVEsNkNBQVI7O0FBRU8sSUFBTS9CLFFBQVEsSUFBSSw2Q0FBQWtGLENBQUtDLEtBQVQsQ0FBZTtBQUNoQzVELFdBQU87QUFDSEYsY0FBTSxFQURIO0FBRUhJLHNCQUFjLElBRlg7QUFHSEQseUJBQWlCO0FBSGQ7QUFEeUIsQ0FBZixDQUFkOztBQVFQLHlEQUFleEIsS0FBZixFIiwiZmlsZSI6Ii9wdWJsaWMvanMvYXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogZ2xvYmFscyBfX1ZVRV9TU1JfQ09OVEVYVF9fICovXG5cbi8vIElNUE9SVEFOVDogRG8gTk9UIHVzZSBFUzIwMTUgZmVhdHVyZXMgaW4gdGhpcyBmaWxlLlxuLy8gVGhpcyBtb2R1bGUgaXMgYSBydW50aW1lIHV0aWxpdHkgZm9yIGNsZWFuZXIgY29tcG9uZW50IG1vZHVsZSBvdXRwdXQgYW5kIHdpbGxcbi8vIGJlIGluY2x1ZGVkIGluIHRoZSBmaW5hbCB3ZWJwYWNrIHVzZXIgYnVuZGxlLlxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIG5vcm1hbGl6ZUNvbXBvbmVudCAoXG4gIHJhd1NjcmlwdEV4cG9ydHMsXG4gIGNvbXBpbGVkVGVtcGxhdGUsXG4gIGZ1bmN0aW9uYWxUZW1wbGF0ZSxcbiAgaW5qZWN0U3R5bGVzLFxuICBzY29wZUlkLFxuICBtb2R1bGVJZGVudGlmaWVyIC8qIHNlcnZlciBvbmx5ICovXG4pIHtcbiAgdmFyIGVzTW9kdWxlXG4gIHZhciBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgfHwge31cblxuICAvLyBFUzYgbW9kdWxlcyBpbnRlcm9wXG4gIHZhciB0eXBlID0gdHlwZW9mIHJhd1NjcmlwdEV4cG9ydHMuZGVmYXVsdFxuICBpZiAodHlwZSA9PT0gJ29iamVjdCcgfHwgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGVzTW9kdWxlID0gcmF3U2NyaXB0RXhwb3J0c1xuICAgIHNjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgfVxuXG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAoY29tcGlsZWRUZW1wbGF0ZSkge1xuICAgIG9wdGlvbnMucmVuZGVyID0gY29tcGlsZWRUZW1wbGF0ZS5yZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IGNvbXBpbGVkVGVtcGxhdGUuc3RhdGljUmVuZGVyRm5zXG4gICAgb3B0aW9ucy5fY29tcGlsZWQgPSB0cnVlXG4gIH1cblxuICAvLyBmdW5jdGlvbmFsIHRlbXBsYXRlXG4gIGlmIChmdW5jdGlvbmFsVGVtcGxhdGUpIHtcbiAgICBvcHRpb25zLmZ1bmN0aW9uYWwgPSB0cnVlXG4gIH1cblxuICAvLyBzY29wZWRJZFxuICBpZiAoc2NvcGVJZCkge1xuICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkXG4gIH1cblxuICB2YXIgaG9va1xuICBpZiAobW9kdWxlSWRlbnRpZmllcikgeyAvLyBzZXJ2ZXIgYnVpbGRcbiAgICBob29rID0gZnVuY3Rpb24gKGNvbnRleHQpIHtcbiAgICAgIC8vIDIuMyBpbmplY3Rpb25cbiAgICAgIGNvbnRleHQgPVxuICAgICAgICBjb250ZXh0IHx8IC8vIGNhY2hlZCBjYWxsXG4gICAgICAgICh0aGlzLiR2bm9kZSAmJiB0aGlzLiR2bm9kZS5zc3JDb250ZXh0KSB8fCAvLyBzdGF0ZWZ1bFxuICAgICAgICAodGhpcy5wYXJlbnQgJiYgdGhpcy5wYXJlbnQuJHZub2RlICYmIHRoaXMucGFyZW50LiR2bm9kZS5zc3JDb250ZXh0KSAvLyBmdW5jdGlvbmFsXG4gICAgICAvLyAyLjIgd2l0aCBydW5Jbk5ld0NvbnRleHQ6IHRydWVcbiAgICAgIGlmICghY29udGV4dCAmJiB0eXBlb2YgX19WVUVfU1NSX0NPTlRFWFRfXyAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgY29udGV4dCA9IF9fVlVFX1NTUl9DT05URVhUX19cbiAgICAgIH1cbiAgICAgIC8vIGluamVjdCBjb21wb25lbnQgc3R5bGVzXG4gICAgICBpZiAoaW5qZWN0U3R5bGVzKSB7XG4gICAgICAgIGluamVjdFN0eWxlcy5jYWxsKHRoaXMsIGNvbnRleHQpXG4gICAgICB9XG4gICAgICAvLyByZWdpc3RlciBjb21wb25lbnQgbW9kdWxlIGlkZW50aWZpZXIgZm9yIGFzeW5jIGNodW5rIGluZmVycmVuY2VcbiAgICAgIGlmIChjb250ZXh0ICYmIGNvbnRleHQuX3JlZ2lzdGVyZWRDb21wb25lbnRzKSB7XG4gICAgICAgIGNvbnRleHQuX3JlZ2lzdGVyZWRDb21wb25lbnRzLmFkZChtb2R1bGVJZGVudGlmaWVyKVxuICAgICAgfVxuICAgIH1cbiAgICAvLyB1c2VkIGJ5IHNzciBpbiBjYXNlIGNvbXBvbmVudCBpcyBjYWNoZWQgYW5kIGJlZm9yZUNyZWF0ZVxuICAgIC8vIG5ldmVyIGdldHMgY2FsbGVkXG4gICAgb3B0aW9ucy5fc3NyUmVnaXN0ZXIgPSBob29rXG4gIH0gZWxzZSBpZiAoaW5qZWN0U3R5bGVzKSB7XG4gICAgaG9vayA9IGluamVjdFN0eWxlc1xuICB9XG5cbiAgaWYgKGhvb2spIHtcbiAgICB2YXIgZnVuY3Rpb25hbCA9IG9wdGlvbnMuZnVuY3Rpb25hbFxuICAgIHZhciBleGlzdGluZyA9IGZ1bmN0aW9uYWxcbiAgICAgID8gb3B0aW9ucy5yZW5kZXJcbiAgICAgIDogb3B0aW9ucy5iZWZvcmVDcmVhdGVcblxuICAgIGlmICghZnVuY3Rpb25hbCkge1xuICAgICAgLy8gaW5qZWN0IGNvbXBvbmVudCByZWdpc3RyYXRpb24gYXMgYmVmb3JlQ3JlYXRlIGhvb2tcbiAgICAgIG9wdGlvbnMuYmVmb3JlQ3JlYXRlID0gZXhpc3RpbmdcbiAgICAgICAgPyBbXS5jb25jYXQoZXhpc3RpbmcsIGhvb2spXG4gICAgICAgIDogW2hvb2tdXG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIGZvciB0ZW1wbGF0ZS1vbmx5IGhvdC1yZWxvYWQgYmVjYXVzZSBpbiB0aGF0IGNhc2UgdGhlIHJlbmRlciBmbiBkb2Vzbid0XG4gICAgICAvLyBnbyB0aHJvdWdoIHRoZSBub3JtYWxpemVyXG4gICAgICBvcHRpb25zLl9pbmplY3RTdHlsZXMgPSBob29rXG4gICAgICAvLyByZWdpc3RlciBmb3IgZnVuY3Rpb2FsIGNvbXBvbmVudCBpbiB2dWUgZmlsZVxuICAgICAgb3B0aW9ucy5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXJXaXRoU3R5bGVJbmplY3Rpb24gKGgsIGNvbnRleHQpIHtcbiAgICAgICAgaG9vay5jYWxsKGNvbnRleHQpXG4gICAgICAgIHJldHVybiBleGlzdGluZyhoLCBjb250ZXh0KVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZXNNb2R1bGU6IGVzTW9kdWxlLFxuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qc1xuLy8gbW9kdWxlIGlkID0gMVxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIvKlxuXHRNSVQgTGljZW5zZSBodHRwOi8vd3d3Lm9wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL21pdC1saWNlbnNlLnBocFxuXHRBdXRob3IgVG9iaWFzIEtvcHBlcnMgQHNva3JhXG4qL1xuLy8gY3NzIGJhc2UgY29kZSwgaW5qZWN0ZWQgYnkgdGhlIGNzcy1sb2FkZXJcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24odXNlU291cmNlTWFwKSB7XG5cdHZhciBsaXN0ID0gW107XG5cblx0Ly8gcmV0dXJuIHRoZSBsaXN0IG9mIG1vZHVsZXMgYXMgY3NzIHN0cmluZ1xuXHRsaXN0LnRvU3RyaW5nID0gZnVuY3Rpb24gdG9TdHJpbmcoKSB7XG5cdFx0cmV0dXJuIHRoaXMubWFwKGZ1bmN0aW9uIChpdGVtKSB7XG5cdFx0XHR2YXIgY29udGVudCA9IGNzc1dpdGhNYXBwaW5nVG9TdHJpbmcoaXRlbSwgdXNlU291cmNlTWFwKTtcblx0XHRcdGlmKGl0ZW1bMl0pIHtcblx0XHRcdFx0cmV0dXJuIFwiQG1lZGlhIFwiICsgaXRlbVsyXSArIFwie1wiICsgY29udGVudCArIFwifVwiO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0cmV0dXJuIGNvbnRlbnQ7XG5cdFx0XHR9XG5cdFx0fSkuam9pbihcIlwiKTtcblx0fTtcblxuXHQvLyBpbXBvcnQgYSBsaXN0IG9mIG1vZHVsZXMgaW50byB0aGUgbGlzdFxuXHRsaXN0LmkgPSBmdW5jdGlvbihtb2R1bGVzLCBtZWRpYVF1ZXJ5KSB7XG5cdFx0aWYodHlwZW9mIG1vZHVsZXMgPT09IFwic3RyaW5nXCIpXG5cdFx0XHRtb2R1bGVzID0gW1tudWxsLCBtb2R1bGVzLCBcIlwiXV07XG5cdFx0dmFyIGFscmVhZHlJbXBvcnRlZE1vZHVsZXMgPSB7fTtcblx0XHRmb3IodmFyIGkgPSAwOyBpIDwgdGhpcy5sZW5ndGg7IGkrKykge1xuXHRcdFx0dmFyIGlkID0gdGhpc1tpXVswXTtcblx0XHRcdGlmKHR5cGVvZiBpZCA9PT0gXCJudW1iZXJcIilcblx0XHRcdFx0YWxyZWFkeUltcG9ydGVkTW9kdWxlc1tpZF0gPSB0cnVlO1xuXHRcdH1cblx0XHRmb3IoaSA9IDA7IGkgPCBtb2R1bGVzLmxlbmd0aDsgaSsrKSB7XG5cdFx0XHR2YXIgaXRlbSA9IG1vZHVsZXNbaV07XG5cdFx0XHQvLyBza2lwIGFscmVhZHkgaW1wb3J0ZWQgbW9kdWxlXG5cdFx0XHQvLyB0aGlzIGltcGxlbWVudGF0aW9uIGlzIG5vdCAxMDAlIHBlcmZlY3QgZm9yIHdlaXJkIG1lZGlhIHF1ZXJ5IGNvbWJpbmF0aW9uc1xuXHRcdFx0Ly8gIHdoZW4gYSBtb2R1bGUgaXMgaW1wb3J0ZWQgbXVsdGlwbGUgdGltZXMgd2l0aCBkaWZmZXJlbnQgbWVkaWEgcXVlcmllcy5cblx0XHRcdC8vICBJIGhvcGUgdGhpcyB3aWxsIG5ldmVyIG9jY3VyIChIZXkgdGhpcyB3YXkgd2UgaGF2ZSBzbWFsbGVyIGJ1bmRsZXMpXG5cdFx0XHRpZih0eXBlb2YgaXRlbVswXSAhPT0gXCJudW1iZXJcIiB8fCAhYWxyZWFkeUltcG9ydGVkTW9kdWxlc1tpdGVtWzBdXSkge1xuXHRcdFx0XHRpZihtZWRpYVF1ZXJ5ICYmICFpdGVtWzJdKSB7XG5cdFx0XHRcdFx0aXRlbVsyXSA9IG1lZGlhUXVlcnk7XG5cdFx0XHRcdH0gZWxzZSBpZihtZWRpYVF1ZXJ5KSB7XG5cdFx0XHRcdFx0aXRlbVsyXSA9IFwiKFwiICsgaXRlbVsyXSArIFwiKSBhbmQgKFwiICsgbWVkaWFRdWVyeSArIFwiKVwiO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGxpc3QucHVzaChpdGVtKTtcblx0XHRcdH1cblx0XHR9XG5cdH07XG5cdHJldHVybiBsaXN0O1xufTtcblxuZnVuY3Rpb24gY3NzV2l0aE1hcHBpbmdUb1N0cmluZyhpdGVtLCB1c2VTb3VyY2VNYXApIHtcblx0dmFyIGNvbnRlbnQgPSBpdGVtWzFdIHx8ICcnO1xuXHR2YXIgY3NzTWFwcGluZyA9IGl0ZW1bM107XG5cdGlmICghY3NzTWFwcGluZykge1xuXHRcdHJldHVybiBjb250ZW50O1xuXHR9XG5cblx0aWYgKHVzZVNvdXJjZU1hcCAmJiB0eXBlb2YgYnRvYSA9PT0gJ2Z1bmN0aW9uJykge1xuXHRcdHZhciBzb3VyY2VNYXBwaW5nID0gdG9Db21tZW50KGNzc01hcHBpbmcpO1xuXHRcdHZhciBzb3VyY2VVUkxzID0gY3NzTWFwcGluZy5zb3VyY2VzLm1hcChmdW5jdGlvbiAoc291cmNlKSB7XG5cdFx0XHRyZXR1cm4gJy8qIyBzb3VyY2VVUkw9JyArIGNzc01hcHBpbmcuc291cmNlUm9vdCArIHNvdXJjZSArICcgKi8nXG5cdFx0fSk7XG5cblx0XHRyZXR1cm4gW2NvbnRlbnRdLmNvbmNhdChzb3VyY2VVUkxzKS5jb25jYXQoW3NvdXJjZU1hcHBpbmddKS5qb2luKCdcXG4nKTtcblx0fVxuXG5cdHJldHVybiBbY29udGVudF0uam9pbignXFxuJyk7XG59XG5cbi8vIEFkYXB0ZWQgZnJvbSBjb252ZXJ0LXNvdXJjZS1tYXAgKE1JVClcbmZ1bmN0aW9uIHRvQ29tbWVudChzb3VyY2VNYXApIHtcblx0Ly8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXVuZGVmXG5cdHZhciBiYXNlNjQgPSBidG9hKHVuZXNjYXBlKGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShzb3VyY2VNYXApKSkpO1xuXHR2YXIgZGF0YSA9ICdzb3VyY2VNYXBwaW5nVVJMPWRhdGE6YXBwbGljYXRpb24vanNvbjtjaGFyc2V0PXV0Zi04O2Jhc2U2NCwnICsgYmFzZTY0O1xuXG5cdHJldHVybiAnLyojICcgKyBkYXRhICsgJyAqLyc7XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1xuLy8gbW9kdWxlIGlkID0gMlxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIvKlxuICBNSVQgTGljZW5zZSBodHRwOi8vd3d3Lm9wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL21pdC1saWNlbnNlLnBocFxuICBBdXRob3IgVG9iaWFzIEtvcHBlcnMgQHNva3JhXG4gIE1vZGlmaWVkIGJ5IEV2YW4gWW91IEB5eXg5OTA4MDNcbiovXG5cbnZhciBoYXNEb2N1bWVudCA9IHR5cGVvZiBkb2N1bWVudCAhPT0gJ3VuZGVmaW5lZCdcblxuaWYgKHR5cGVvZiBERUJVRyAhPT0gJ3VuZGVmaW5lZCcgJiYgREVCVUcpIHtcbiAgaWYgKCFoYXNEb2N1bWVudCkge1xuICAgIHRocm93IG5ldyBFcnJvcihcbiAgICAndnVlLXN0eWxlLWxvYWRlciBjYW5ub3QgYmUgdXNlZCBpbiBhIG5vbi1icm93c2VyIGVudmlyb25tZW50LiAnICtcbiAgICBcIlVzZSB7IHRhcmdldDogJ25vZGUnIH0gaW4geW91ciBXZWJwYWNrIGNvbmZpZyB0byBpbmRpY2F0ZSBhIHNlcnZlci1yZW5kZXJpbmcgZW52aXJvbm1lbnQuXCJcbiAgKSB9XG59XG5cbnZhciBsaXN0VG9TdHlsZXMgPSByZXF1aXJlKCcuL2xpc3RUb1N0eWxlcycpXG5cbi8qXG50eXBlIFN0eWxlT2JqZWN0ID0ge1xuICBpZDogbnVtYmVyO1xuICBwYXJ0czogQXJyYXk8U3R5bGVPYmplY3RQYXJ0PlxufVxuXG50eXBlIFN0eWxlT2JqZWN0UGFydCA9IHtcbiAgY3NzOiBzdHJpbmc7XG4gIG1lZGlhOiBzdHJpbmc7XG4gIHNvdXJjZU1hcDogP3N0cmluZ1xufVxuKi9cblxudmFyIHN0eWxlc0luRG9tID0gey8qXG4gIFtpZDogbnVtYmVyXToge1xuICAgIGlkOiBudW1iZXIsXG4gICAgcmVmczogbnVtYmVyLFxuICAgIHBhcnRzOiBBcnJheTwob2JqPzogU3R5bGVPYmplY3RQYXJ0KSA9PiB2b2lkPlxuICB9XG4qL31cblxudmFyIGhlYWQgPSBoYXNEb2N1bWVudCAmJiAoZG9jdW1lbnQuaGVhZCB8fCBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnaGVhZCcpWzBdKVxudmFyIHNpbmdsZXRvbkVsZW1lbnQgPSBudWxsXG52YXIgc2luZ2xldG9uQ291bnRlciA9IDBcbnZhciBpc1Byb2R1Y3Rpb24gPSBmYWxzZVxudmFyIG5vb3AgPSBmdW5jdGlvbiAoKSB7fVxuXG4vLyBGb3JjZSBzaW5nbGUtdGFnIHNvbHV0aW9uIG9uIElFNi05LCB3aGljaCBoYXMgYSBoYXJkIGxpbWl0IG9uIHRoZSAjIG9mIDxzdHlsZT5cbi8vIHRhZ3MgaXQgd2lsbCBhbGxvdyBvbiBhIHBhZ2VcbnZhciBpc09sZElFID0gdHlwZW9mIG5hdmlnYXRvciAhPT0gJ3VuZGVmaW5lZCcgJiYgL21zaWUgWzYtOV1cXGIvLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudC50b0xvd2VyQ2FzZSgpKVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChwYXJlbnRJZCwgbGlzdCwgX2lzUHJvZHVjdGlvbikge1xuICBpc1Byb2R1Y3Rpb24gPSBfaXNQcm9kdWN0aW9uXG5cbiAgdmFyIHN0eWxlcyA9IGxpc3RUb1N0eWxlcyhwYXJlbnRJZCwgbGlzdClcbiAgYWRkU3R5bGVzVG9Eb20oc3R5bGVzKVxuXG4gIHJldHVybiBmdW5jdGlvbiB1cGRhdGUgKG5ld0xpc3QpIHtcbiAgICB2YXIgbWF5UmVtb3ZlID0gW11cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHN0eWxlcy5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIGl0ZW0gPSBzdHlsZXNbaV1cbiAgICAgIHZhciBkb21TdHlsZSA9IHN0eWxlc0luRG9tW2l0ZW0uaWRdXG4gICAgICBkb21TdHlsZS5yZWZzLS1cbiAgICAgIG1heVJlbW92ZS5wdXNoKGRvbVN0eWxlKVxuICAgIH1cbiAgICBpZiAobmV3TGlzdCkge1xuICAgICAgc3R5bGVzID0gbGlzdFRvU3R5bGVzKHBhcmVudElkLCBuZXdMaXN0KVxuICAgICAgYWRkU3R5bGVzVG9Eb20oc3R5bGVzKVxuICAgIH0gZWxzZSB7XG4gICAgICBzdHlsZXMgPSBbXVxuICAgIH1cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IG1heVJlbW92ZS5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIGRvbVN0eWxlID0gbWF5UmVtb3ZlW2ldXG4gICAgICBpZiAoZG9tU3R5bGUucmVmcyA9PT0gMCkge1xuICAgICAgICBmb3IgKHZhciBqID0gMDsgaiA8IGRvbVN0eWxlLnBhcnRzLmxlbmd0aDsgaisrKSB7XG4gICAgICAgICAgZG9tU3R5bGUucGFydHNbal0oKVxuICAgICAgICB9XG4gICAgICAgIGRlbGV0ZSBzdHlsZXNJbkRvbVtkb21TdHlsZS5pZF1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuZnVuY3Rpb24gYWRkU3R5bGVzVG9Eb20gKHN0eWxlcyAvKiBBcnJheTxTdHlsZU9iamVjdD4gKi8pIHtcbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBzdHlsZXMubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgaXRlbSA9IHN0eWxlc1tpXVxuICAgIHZhciBkb21TdHlsZSA9IHN0eWxlc0luRG9tW2l0ZW0uaWRdXG4gICAgaWYgKGRvbVN0eWxlKSB7XG4gICAgICBkb21TdHlsZS5yZWZzKytcbiAgICAgIGZvciAodmFyIGogPSAwOyBqIDwgZG9tU3R5bGUucGFydHMubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgZG9tU3R5bGUucGFydHNbal0oaXRlbS5wYXJ0c1tqXSlcbiAgICAgIH1cbiAgICAgIGZvciAoOyBqIDwgaXRlbS5wYXJ0cy5sZW5ndGg7IGorKykge1xuICAgICAgICBkb21TdHlsZS5wYXJ0cy5wdXNoKGFkZFN0eWxlKGl0ZW0ucGFydHNbal0pKVxuICAgICAgfVxuICAgICAgaWYgKGRvbVN0eWxlLnBhcnRzLmxlbmd0aCA+IGl0ZW0ucGFydHMubGVuZ3RoKSB7XG4gICAgICAgIGRvbVN0eWxlLnBhcnRzLmxlbmd0aCA9IGl0ZW0ucGFydHMubGVuZ3RoXG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHZhciBwYXJ0cyA9IFtdXG4gICAgICBmb3IgKHZhciBqID0gMDsgaiA8IGl0ZW0ucGFydHMubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgcGFydHMucHVzaChhZGRTdHlsZShpdGVtLnBhcnRzW2pdKSlcbiAgICAgIH1cbiAgICAgIHN0eWxlc0luRG9tW2l0ZW0uaWRdID0geyBpZDogaXRlbS5pZCwgcmVmczogMSwgcGFydHM6IHBhcnRzIH1cbiAgICB9XG4gIH1cbn1cblxuZnVuY3Rpb24gY3JlYXRlU3R5bGVFbGVtZW50ICgpIHtcbiAgdmFyIHN0eWxlRWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3N0eWxlJylcbiAgc3R5bGVFbGVtZW50LnR5cGUgPSAndGV4dC9jc3MnXG4gIGhlYWQuYXBwZW5kQ2hpbGQoc3R5bGVFbGVtZW50KVxuICByZXR1cm4gc3R5bGVFbGVtZW50XG59XG5cbmZ1bmN0aW9uIGFkZFN0eWxlIChvYmogLyogU3R5bGVPYmplY3RQYXJ0ICovKSB7XG4gIHZhciB1cGRhdGUsIHJlbW92ZVxuICB2YXIgc3R5bGVFbGVtZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcignc3R5bGVbZGF0YS12dWUtc3NyLWlkfj1cIicgKyBvYmouaWQgKyAnXCJdJylcblxuICBpZiAoc3R5bGVFbGVtZW50KSB7XG4gICAgaWYgKGlzUHJvZHVjdGlvbikge1xuICAgICAgLy8gaGFzIFNTUiBzdHlsZXMgYW5kIGluIHByb2R1Y3Rpb24gbW9kZS5cbiAgICAgIC8vIHNpbXBseSBkbyBub3RoaW5nLlxuICAgICAgcmV0dXJuIG5vb3BcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gaGFzIFNTUiBzdHlsZXMgYnV0IGluIGRldiBtb2RlLlxuICAgICAgLy8gZm9yIHNvbWUgcmVhc29uIENocm9tZSBjYW4ndCBoYW5kbGUgc291cmNlIG1hcCBpbiBzZXJ2ZXItcmVuZGVyZWRcbiAgICAgIC8vIHN0eWxlIHRhZ3MgLSBzb3VyY2UgbWFwcyBpbiA8c3R5bGU+IG9ubHkgd29ya3MgaWYgdGhlIHN0eWxlIHRhZyBpc1xuICAgICAgLy8gY3JlYXRlZCBhbmQgaW5zZXJ0ZWQgZHluYW1pY2FsbHkuIFNvIHdlIHJlbW92ZSB0aGUgc2VydmVyIHJlbmRlcmVkXG4gICAgICAvLyBzdHlsZXMgYW5kIGluamVjdCBuZXcgb25lcy5cbiAgICAgIHN0eWxlRWxlbWVudC5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHN0eWxlRWxlbWVudClcbiAgICB9XG4gIH1cblxuICBpZiAoaXNPbGRJRSkge1xuICAgIC8vIHVzZSBzaW5nbGV0b24gbW9kZSBmb3IgSUU5LlxuICAgIHZhciBzdHlsZUluZGV4ID0gc2luZ2xldG9uQ291bnRlcisrXG4gICAgc3R5bGVFbGVtZW50ID0gc2luZ2xldG9uRWxlbWVudCB8fCAoc2luZ2xldG9uRWxlbWVudCA9IGNyZWF0ZVN0eWxlRWxlbWVudCgpKVxuICAgIHVwZGF0ZSA9IGFwcGx5VG9TaW5nbGV0b25UYWcuYmluZChudWxsLCBzdHlsZUVsZW1lbnQsIHN0eWxlSW5kZXgsIGZhbHNlKVxuICAgIHJlbW92ZSA9IGFwcGx5VG9TaW5nbGV0b25UYWcuYmluZChudWxsLCBzdHlsZUVsZW1lbnQsIHN0eWxlSW5kZXgsIHRydWUpXG4gIH0gZWxzZSB7XG4gICAgLy8gdXNlIG11bHRpLXN0eWxlLXRhZyBtb2RlIGluIGFsbCBvdGhlciBjYXNlc1xuICAgIHN0eWxlRWxlbWVudCA9IGNyZWF0ZVN0eWxlRWxlbWVudCgpXG4gICAgdXBkYXRlID0gYXBwbHlUb1RhZy5iaW5kKG51bGwsIHN0eWxlRWxlbWVudClcbiAgICByZW1vdmUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICBzdHlsZUVsZW1lbnQucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChzdHlsZUVsZW1lbnQpXG4gICAgfVxuICB9XG5cbiAgdXBkYXRlKG9iailcblxuICByZXR1cm4gZnVuY3Rpb24gdXBkYXRlU3R5bGUgKG5ld09iaiAvKiBTdHlsZU9iamVjdFBhcnQgKi8pIHtcbiAgICBpZiAobmV3T2JqKSB7XG4gICAgICBpZiAobmV3T2JqLmNzcyA9PT0gb2JqLmNzcyAmJlxuICAgICAgICAgIG5ld09iai5tZWRpYSA9PT0gb2JqLm1lZGlhICYmXG4gICAgICAgICAgbmV3T2JqLnNvdXJjZU1hcCA9PT0gb2JqLnNvdXJjZU1hcCkge1xuICAgICAgICByZXR1cm5cbiAgICAgIH1cbiAgICAgIHVwZGF0ZShvYmogPSBuZXdPYmopXG4gICAgfSBlbHNlIHtcbiAgICAgIHJlbW92ZSgpXG4gICAgfVxuICB9XG59XG5cbnZhciByZXBsYWNlVGV4dCA9IChmdW5jdGlvbiAoKSB7XG4gIHZhciB0ZXh0U3RvcmUgPSBbXVxuXG4gIHJldHVybiBmdW5jdGlvbiAoaW5kZXgsIHJlcGxhY2VtZW50KSB7XG4gICAgdGV4dFN0b3JlW2luZGV4XSA9IHJlcGxhY2VtZW50XG4gICAgcmV0dXJuIHRleHRTdG9yZS5maWx0ZXIoQm9vbGVhbikuam9pbignXFxuJylcbiAgfVxufSkoKVxuXG5mdW5jdGlvbiBhcHBseVRvU2luZ2xldG9uVGFnIChzdHlsZUVsZW1lbnQsIGluZGV4LCByZW1vdmUsIG9iaikge1xuICB2YXIgY3NzID0gcmVtb3ZlID8gJycgOiBvYmouY3NzXG5cbiAgaWYgKHN0eWxlRWxlbWVudC5zdHlsZVNoZWV0KSB7XG4gICAgc3R5bGVFbGVtZW50LnN0eWxlU2hlZXQuY3NzVGV4dCA9IHJlcGxhY2VUZXh0KGluZGV4LCBjc3MpXG4gIH0gZWxzZSB7XG4gICAgdmFyIGNzc05vZGUgPSBkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShjc3MpXG4gICAgdmFyIGNoaWxkTm9kZXMgPSBzdHlsZUVsZW1lbnQuY2hpbGROb2Rlc1xuICAgIGlmIChjaGlsZE5vZGVzW2luZGV4XSkgc3R5bGVFbGVtZW50LnJlbW92ZUNoaWxkKGNoaWxkTm9kZXNbaW5kZXhdKVxuICAgIGlmIChjaGlsZE5vZGVzLmxlbmd0aCkge1xuICAgICAgc3R5bGVFbGVtZW50Lmluc2VydEJlZm9yZShjc3NOb2RlLCBjaGlsZE5vZGVzW2luZGV4XSlcbiAgICB9IGVsc2Uge1xuICAgICAgc3R5bGVFbGVtZW50LmFwcGVuZENoaWxkKGNzc05vZGUpXG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGFwcGx5VG9UYWcgKHN0eWxlRWxlbWVudCwgb2JqKSB7XG4gIHZhciBjc3MgPSBvYmouY3NzXG4gIHZhciBtZWRpYSA9IG9iai5tZWRpYVxuICB2YXIgc291cmNlTWFwID0gb2JqLnNvdXJjZU1hcFxuXG4gIGlmIChtZWRpYSkge1xuICAgIHN0eWxlRWxlbWVudC5zZXRBdHRyaWJ1dGUoJ21lZGlhJywgbWVkaWEpXG4gIH1cblxuICBpZiAoc291cmNlTWFwKSB7XG4gICAgLy8gaHR0cHM6Ly9kZXZlbG9wZXIuY2hyb21lLmNvbS9kZXZ0b29scy9kb2NzL2phdmFzY3JpcHQtZGVidWdnaW5nXG4gICAgLy8gdGhpcyBtYWtlcyBzb3VyY2UgbWFwcyBpbnNpZGUgc3R5bGUgdGFncyB3b3JrIHByb3Blcmx5IGluIENocm9tZVxuICAgIGNzcyArPSAnXFxuLyojIHNvdXJjZVVSTD0nICsgc291cmNlTWFwLnNvdXJjZXNbMF0gKyAnICovJ1xuICAgIC8vIGh0dHA6Ly9zdGFja292ZXJmbG93LmNvbS9hLzI2NjAzODc1XG4gICAgY3NzICs9ICdcXG4vKiMgc291cmNlTWFwcGluZ1VSTD1kYXRhOmFwcGxpY2F0aW9uL2pzb247YmFzZTY0LCcgKyBidG9hKHVuZXNjYXBlKGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShzb3VyY2VNYXApKSkpICsgJyAqLydcbiAgfVxuXG4gIGlmIChzdHlsZUVsZW1lbnQuc3R5bGVTaGVldCkge1xuICAgIHN0eWxlRWxlbWVudC5zdHlsZVNoZWV0LmNzc1RleHQgPSBjc3NcbiAgfSBlbHNlIHtcbiAgICB3aGlsZSAoc3R5bGVFbGVtZW50LmZpcnN0Q2hpbGQpIHtcbiAgICAgIHN0eWxlRWxlbWVudC5yZW1vdmVDaGlsZChzdHlsZUVsZW1lbnQuZmlyc3RDaGlsZClcbiAgICB9XG4gICAgc3R5bGVFbGVtZW50LmFwcGVuZENoaWxkKGRvY3VtZW50LmNyZWF0ZVRleHROb2RlKGNzcykpXG4gIH1cbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlc0NsaWVudC5qc1xuLy8gbW9kdWxlIGlkID0gM1xuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJ2YXIgZGlzcG9zZWQgPSBmYWxzZVxuZnVuY3Rpb24gaW5qZWN0U3R5bGUgKHNzckNvbnRleHQpIHtcbiAgaWYgKGRpc3Bvc2VkKSByZXR1cm5cbiAgcmVxdWlyZShcIiEhdnVlLXN0eWxlLWxvYWRlciFjc3MtbG9hZGVyP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXg/e1xcXCJ2dWVcXFwiOnRydWUsXFxcImlkXFxcIjpcXFwiZGF0YS12LTIzYTAwODgyXFxcIixcXFwic2NvcGVkXFxcIjpmYWxzZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zdHlsZXMmaW5kZXg9MCZidXN0Q2FjaGUhLi9wYWdlcy52dWVcIilcbn1cbnZhciBub3JtYWxpemVDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKVxuLyogc2NyaXB0ICovXG52YXIgX192dWVfc2NyaXB0X18gPSByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXSxcXFwicGx1Z2luc1xcXCI6W1xcXCJ0cmFuc2Zvcm0tb2JqZWN0LXJlc3Qtc3ByZWFkXFxcIl19IS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wJmJ1c3RDYWNoZSEuL3BhZ2VzLnZ1ZVwiKVxuLyogdGVtcGxhdGUgKi9cbnZhciBfX3Z1ZV90ZW1wbGF0ZV9fID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMjNhMDA4ODJcXFwiLFxcXCJoYXNTY29wZWRcXFwiOmZhbHNlLFxcXCJidWJsZVxcXCI6e1xcXCJ0cmFuc2Zvcm1zXFxcIjp7fX19IS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAmYnVzdENhY2hlIS4vcGFnZXMudnVlXCIpXG4vKiB0ZW1wbGF0ZSBmdW5jdGlvbmFsICovXG4gIHZhciBfX3Z1ZV90ZW1wbGF0ZV9mdW5jdGlvbmFsX18gPSBmYWxzZVxuLyogc3R5bGVzICovXG52YXIgX192dWVfc3R5bGVzX18gPSBpbmplY3RTdHlsZVxuLyogc2NvcGVJZCAqL1xudmFyIF9fdnVlX3Njb3BlSWRfXyA9IG51bGxcbi8qIG1vZHVsZUlkZW50aWZpZXIgKHNlcnZlciBvbmx5KSAqL1xudmFyIF9fdnVlX21vZHVsZV9pZGVudGlmaWVyX18gPSBudWxsXG52YXIgQ29tcG9uZW50ID0gbm9ybWFsaXplQ29tcG9uZW50KFxuICBfX3Z1ZV9zY3JpcHRfXyxcbiAgX192dWVfdGVtcGxhdGVfXyxcbiAgX192dWVfdGVtcGxhdGVfZnVuY3Rpb25hbF9fLFxuICBfX3Z1ZV9zdHlsZXNfXyxcbiAgX192dWVfc2NvcGVJZF9fLFxuICBfX3Z1ZV9tb2R1bGVfaWRlbnRpZmllcl9fXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcInJlc291cmNlcy9qcy92dWUvbGF5b3V0cy9wYWdlcy52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkgeyAgcmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5LnN1YnN0cigwLCAyKSAhPT0gXCJfX1wifSkpIHsgIGNvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTIzYTAwODgyXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtMjNhMDA4ODJcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4nICsgJyAgfVxuICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICBkaXNwb3NlZCA9IHRydWVcbiAgfSlcbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2pzL3Z1ZS9sYXlvdXRzL3BhZ2VzLnZ1ZVxuLy8gbW9kdWxlIGlkID0gN1xuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJ2YXIgZGlzcG9zZWQgPSBmYWxzZVxuZnVuY3Rpb24gaW5qZWN0U3R5bGUgKHNzckNvbnRleHQpIHtcbiAgaWYgKGRpc3Bvc2VkKSByZXR1cm5cbiAgcmVxdWlyZShcIiEhdnVlLXN0eWxlLWxvYWRlciFjc3MtbG9hZGVyP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXg/e1xcXCJ2dWVcXFwiOnRydWUsXFxcImlkXFxcIjpcXFwiZGF0YS12LTIxOTk0MmU2XFxcIixcXFwic2NvcGVkXFxcIjpmYWxzZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zdHlsZXMmaW5kZXg9MCZidXN0Q2FjaGUhLi9hZG1pbi52dWVcIilcbn1cbnZhciBub3JtYWxpemVDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKVxuLyogc2NyaXB0ICovXG52YXIgX192dWVfc2NyaXB0X18gPSByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXSxcXFwicGx1Z2luc1xcXCI6W1xcXCJ0cmFuc2Zvcm0tb2JqZWN0LXJlc3Qtc3ByZWFkXFxcIl19IS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wJmJ1c3RDYWNoZSEuL2FkbWluLnZ1ZVwiKVxuLyogdGVtcGxhdGUgKi9cbnZhciBfX3Z1ZV90ZW1wbGF0ZV9fID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMjE5OTQyZTZcXFwiLFxcXCJoYXNTY29wZWRcXFwiOmZhbHNlLFxcXCJidWJsZVxcXCI6e1xcXCJ0cmFuc2Zvcm1zXFxcIjp7fX19IS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAmYnVzdENhY2hlIS4vYWRtaW4udnVlXCIpXG4vKiB0ZW1wbGF0ZSBmdW5jdGlvbmFsICovXG4gIHZhciBfX3Z1ZV90ZW1wbGF0ZV9mdW5jdGlvbmFsX18gPSBmYWxzZVxuLyogc3R5bGVzICovXG52YXIgX192dWVfc3R5bGVzX18gPSBpbmplY3RTdHlsZVxuLyogc2NvcGVJZCAqL1xudmFyIF9fdnVlX3Njb3BlSWRfXyA9IG51bGxcbi8qIG1vZHVsZUlkZW50aWZpZXIgKHNlcnZlciBvbmx5KSAqL1xudmFyIF9fdnVlX21vZHVsZV9pZGVudGlmaWVyX18gPSBudWxsXG52YXIgQ29tcG9uZW50ID0gbm9ybWFsaXplQ29tcG9uZW50KFxuICBfX3Z1ZV9zY3JpcHRfXyxcbiAgX192dWVfdGVtcGxhdGVfXyxcbiAgX192dWVfdGVtcGxhdGVfZnVuY3Rpb25hbF9fLFxuICBfX3Z1ZV9zdHlsZXNfXyxcbiAgX192dWVfc2NvcGVJZF9fLFxuICBfX3Z1ZV9tb2R1bGVfaWRlbnRpZmllcl9fXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcInJlc291cmNlcy9qcy92dWUvbGF5b3V0cy9hZG1pbi52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkgeyAgcmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5LnN1YnN0cigwLCAyKSAhPT0gXCJfX1wifSkpIHsgIGNvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTIxOTk0MmU2XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtMjE5OTQyZTZcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4nICsgJyAgfVxuICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICBkaXNwb3NlZCA9IHRydWVcbiAgfSlcbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2pzL3Z1ZS9sYXlvdXRzL2FkbWluLnZ1ZVxuLy8gbW9kdWxlIGlkID0gMTZcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiaW1wb3J0ICcuL2Jvb3RzdHJhcC5qcyc7XG5cbmltcG9ydCByb3V0ZXIgZnJvbSAnLi9yb3V0ZXMuanMnO1xuaW1wb3J0IHN0b3JlIGZyb20gJy4vc3RvcmUuanMnO1xuXG5uZXcgVnVlKHtcbiAgICBlbDogJyNyb290JyxcbiAgICByb3V0ZXIgOiByb3V0ZXIsXG4gICAgc3RvcmU6c3RvcmUsXG4gICAgZGF0YToge1xuICAgICAgICBzaGFyZWQ6IHN0b3JlXG4gICAgfSxcbiAgICBtb3VudGVkKCl7XG4gICAgICAgIGxldCB0aGF0ID0gdGhpcztcbiAgICAgICAgdGhhdC4kb24oJ25vdGlmeVN1Y2Nlc3MnLCBmdW5jdGlvbiAobXNnKSB7XG4gICAgICAgICAgICB0aGF0LiRzd2FsKCdTdWNjZXNzJyxtc2csIFwic3VjY2Vzc1wiKTtcbiAgICAgICAgfSk7XG4gICAgICAgIHRoYXQuJG9uKCdub3RpZnlGYWlsZWQnLCBmdW5jdGlvbiAobXNnKSB7XG4gICAgICAgICAgICB0aGF0LiRzd2FsKCdFcnJvcicsbXNnLCBcImVycm9yXCIpO1xuICAgICAgICB9KTtcbiAgICB9LFxuICAgIGNyZWF0ZWQoKXtcbiAgICAgICAgdmFyIGFjY2Vzc1Rva2VuID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2FjY2Vzc190b2tlbicpO1xuICAgICAgICBpZiAoYWNjZXNzVG9rZW4gIT09IG51bGwgJiYgYWNjZXNzVG9rZW4gIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICB2YXIgdGhhdCA9IHRoaXM7XG4gICAgICAgICAgICBheGlvcy5nZXQoJy9hcGkvdXNlcicpXG4gICAgICAgICAgICAgICAgLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2Upe1xuICAgICAgICAgICAgICAgICAgICB0aGF0LnNldExvZ2luKHJlc3BvbnNlLmRhdGEuZGF0YSk7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuY2F0Y2goZnVuY3Rpb24oZXJyb3Ipe1xuICAgICAgICAgICAgICAgICAgICB0aGF0LmRlc3Ryb3lMb2dpbigpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICB9LFxuICAgIG1ldGhvZHM6e1xuICAgICAgICBzZXRMb2dpbih1c2VyKSB7XG4gICAgICAgICAgICBsZXQgdGhhdCA9IHRoaXM7XG4gICAgICAgICAgICAvLyBTYXZlIGxvZ2luIGluZm8gaW4gb3VyIGRhdGEgYW5kIHNldCBoZWFkZXIgaW4gY2FzZSBpdCdzIG5vdCBzZXQgYWxyZWFkeVxuICAgICAgICAgICAgdGhpcy4kc3RvcmUuc3RhdGUudXNlciA9IHVzZXI7XG4gICAgICAgICAgICB0aGlzLiRzdG9yZS5zdGF0ZS5pc0F1dGhlbnRpY2F0ZWQgPSB0cnVlO1xuICAgICAgICAgICAgdGhpcy4kc3RvcmUuc3RhdGUuYWNjZXNzX3Rva2VuID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2FjY2Vzc190b2tlbicpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGRlc3Ryb3lMb2dpbigpIHtcbiAgICAgICAgICAgIGxldCB0aGF0ID0gdGhpcztcbiAgICAgICAgICAgIC8vIENsZWFudXAgd2hlbiB0b2tlbiB3YXMgaW52YWxpZCBvdXIgdXNlciBoYXMgbG9nZ2VkIG91dFxuICAgICAgICAgICAgdGhpcy4kc3RvcmUuc3RhdGUudXNlciA9IHt9O1xuICAgICAgICAgICAgdGhpcy4kc3RvcmUuc3RhdGUuaXNBdXRoZW50aWNhdGVkID0gZmFsc2U7XG4gICAgICAgICAgICB0aGlzLiRzdG9yZS5zdGF0ZS5hY2Nlc3NfdG9rZW4gPSBudWxsO1xuICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oJ2FjY2Vzc190b2tlbicpO1xuICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oJ3JlZnJlc2hfdG9rZW4nKTtcbiAgICAgICAgICAgIHRoYXQuJHJvdXRlci5wdXNoKCcvZGFzaGJvYXJkJyk7XG5cbiAgICAgICAgfSxcbiAgICB9XG59KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9qcy9hcHAuanMiLCJpbXBvcnQgVnVlIGZyb20gJ3Z1ZSc7XG5pbXBvcnQgVnVlUm91dGVyIGZyb20gJ3Z1ZS1yb3V0ZXInO1xuaW1wb3J0IGF4aW9zIGZyb20gJ2F4aW9zJztcbmltcG9ydCBGb3JtIGZyb20gJy4vY29yZS9Gb3JtJztcblxuXG53aW5kb3cuVnVlID0gVnVlO1xud2luZG93LkZvcm0gPSBGb3JtO1xuVnVlLnVzZShWdWVSb3V0ZXIpO1xud2luZG93LmF4aW9zID0gYXhpb3M7XG5cbndpbmRvdy5heGlvcy5kZWZhdWx0cy5oZWFkZXJzLmNvbW1vbiA9IHtcbiAgICAnWC1SZXF1ZXN0ZWQtV2l0aCc6ICdYTUxIdHRwUmVxdWVzdCdcbn1cbi8vIHdpbmRvdy5heGlvcy5kZWZhdWx0cy5iYXNlVVJMID0gJ2h0dHBzOi8vYXBpLW1hdGNodXAuaGVyb2t1YXBwLmNvbSc7XG53aW5kb3cuYXhpb3MuZGVmYXVsdHMuYmFzZVVSTCA9ICdodHRwOi8vYXBpLm1hdGNodXAuY29tJztcblxuYXhpb3MuaW50ZXJjZXB0b3JzLnJlcXVlc3QudXNlKChjb25maWcpID0+IHtcblxuICAgIGxldCBhY2Nlc3NUb2tlbjtcbiAgICBhY2Nlc3NUb2tlbiA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdhY2Nlc3NfdG9rZW4nKTtcblxuXG4gICAgaWYgKGFjY2Vzc1Rva2VuICE9PSBudWxsICYmIGFjY2Vzc1Rva2VuICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICBjb25maWcuaGVhZGVycy5jb21tb25bJ0F1dGhvcml6YXRpb24nXSA9ICdCZWFyZXIgJyArIGFjY2Vzc1Rva2VuO1xuICAgIH1cbiAgICByZXR1cm4gY29uZmlnXG59LCBmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAvLyBEbyBzb21ldGhpbmcgd2l0aCByZXF1ZXN0IGVycm9yXG4gICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKVxufSk7XG5cbmF4aW9zLmludGVyY2VwdG9ycy5yZXNwb25zZS51c2UoZnVuY3Rpb24gKHJlc3BvbnNlKSB7XG4gICAgcmV0dXJuIHJlc3BvbnNlO1xufSwgZnVuY3Rpb24gKGVycm9yKSB7XG5cbiAgICBjb25zdCBvcmlnaW5hbFJlcXVlc3QgPSBlcnJvci5jb25maWc7XG5cbiAgICBpZiAoZXJyb3IucmVzcG9uc2Uuc3RhdHVzID09PSA0MDEgJiYgIW9yaWdpbmFsUmVxdWVzdC5fcmV0cnkpIHtcblxuICAgICAgICBvcmlnaW5hbFJlcXVlc3QuX3JldHJ5ID0gdHJ1ZTtcblxuICAgICAgICBjb25zdCByZWZyZXNoVG9rZW4gPSB3aW5kb3cubG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3JlZnJlc2hfdG9rZW4nKTtcbiAgICAgICAgcmV0dXJuIGF4aW9zLnBvc3QoJy9hcGkvYXV0aC9yZWZyZXNoJywgeyByZWZyZXNoVG9rZW4gfSlcbiAgICAgICAgICAgIC50aGVuKCh7ZGF0YX0pID0+IHtcbiAgICAgICAgICAgICAgICB3aW5kb3cubG9jYWxTdG9yYWdlLnNldEl0ZW0oJ2FjY2Vzc190b2tlbicsIGRhdGEuYWNjZXNzX3Rva2VuKTtcbiAgICAgICAgICAgICAgICB3aW5kb3cubG9jYWxTdG9yYWdlLnNldEl0ZW0oJ3JlZnJlc2hfdG9rZW4nLCBkYXRhLnJlZnJlc2hfdG9rZW4pO1xuICAgICAgICAgICAgICAgIGF4aW9zLmRlZmF1bHRzLmhlYWRlcnMuY29tbW9uWydBdXRob3JpemF0aW9uJ10gPSAnQmVhcmVyICcgKyBkYXRhLmFjY2Vzc190b2tlbjtcbiAgICAgICAgICAgICAgICBvcmlnaW5hbFJlcXVlc3QuaGVhZGVyc1snQXV0aG9yaXphdGlvbiddID0gJ0JlYXJlciAnICsgZGF0YS5hY2Nlc3NfdG9rZW47XG4gICAgICAgICAgICAgICAgcmV0dXJuIGF4aW9zKG9yaWdpbmFsUmVxdWVzdCk7XG4gICAgICAgICAgICB9KTtcbiAgICB9XG5cbiAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xufSk7XG5cblxuXG5cblxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2pzL2Jvb3RzdHJhcC5qcyIsImltcG9ydCBFcnJvcnMgZnJvbSAnLi9FcnJvcnMnO1xuXG5jbGFzcyBGb3JtIHtcbiAgICAvKipcbiAgICAgKiBDcmVhdGUgYSBuZXcgRm9ybSBpbnN0YW5jZS5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7b2JqZWN0fSBkYXRhXG4gICAgICovXG4gICAgY29uc3RydWN0b3IoZGF0YSkge1xuICAgICAgICB0aGlzLm9yaWdpbmFsRGF0YSA9IGRhdGE7XG5cbiAgICAgICAgZm9yIChsZXQgZmllbGQgaW4gZGF0YSkge1xuICAgICAgICAgICAgdGhpc1tmaWVsZF0gPSBkYXRhW2ZpZWxkXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuZXJyb3JzID0gbmV3IEVycm9ycygpO1xuICAgIH1cblxuXG4gICAgLyoqXG4gICAgICogRmV0Y2ggYWxsIHJlbGV2YW50IGRhdGEgZm9yIHRoZSBmb3JtLlxuICAgICAqL1xuICAgIGRhdGEoKSB7XG4gICAgICAgIGxldCBkYXRhID0ge307XG5cbiAgICAgICAgZm9yIChsZXQgcHJvcGVydHkgaW4gdGhpcy5vcmlnaW5hbERhdGEpIHtcbiAgICAgICAgICAgIGRhdGFbcHJvcGVydHldID0gdGhpc1twcm9wZXJ0eV07XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gZGF0YTtcbiAgICB9XG5cblxuICAgIC8qKlxuICAgICAqIFJlc2V0IHRoZSBmb3JtIGZpZWxkcy5cbiAgICAgKi9cbiAgICByZXNldCgpIHtcbiAgICAgICAgZm9yIChsZXQgZmllbGQgaW4gdGhpcy5vcmlnaW5hbERhdGEpIHtcbiAgICAgICAgICAgIHRoaXNbZmllbGRdID0gJyc7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmVycm9ycy5jbGVhcigpO1xuICAgIH1cblxuXG4gICAgLyoqXG4gICAgICogU2VuZCBhIFBPU1QgcmVxdWVzdCB0byB0aGUgZ2l2ZW4gVVJMLlxuICAgICAqIC5cbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gdXJsXG4gICAgICovXG4gICAgcG9zdCh1cmwpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3VibWl0KCdwb3N0JywgdXJsKTtcbiAgICB9XG5cblxuICAgIC8qKlxuICAgICAqIFNlbmQgYSBQVVQgcmVxdWVzdCB0byB0aGUgZ2l2ZW4gVVJMLlxuICAgICAqIC5cbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gdXJsXG4gICAgICovXG4gICAgcHV0KHVybCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zdWJtaXQoJ3B1dCcsIHVybCk7XG4gICAgfVxuXG5cbiAgICAvKipcbiAgICAgKiBTZW5kIGEgUEFUQ0ggcmVxdWVzdCB0byB0aGUgZ2l2ZW4gVVJMLlxuICAgICAqIC5cbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gdXJsXG4gICAgICovXG4gICAgcGF0Y2godXJsKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnN1Ym1pdCgncGF0Y2gnLCB1cmwpO1xuICAgIH1cblxuXG4gICAgLyoqXG4gICAgICogU2VuZCBhIERFTEVURSByZXF1ZXN0IHRvIHRoZSBnaXZlbiBVUkwuXG4gICAgICogLlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSB1cmxcbiAgICAgKi9cbiAgICBkZWxldGUodXJsKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnN1Ym1pdCgnZGVsZXRlJywgdXJsKTtcbiAgICB9XG5cblxuICAgIC8qKlxuICAgICAqIFN1Ym1pdCB0aGUgZm9ybS5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSByZXF1ZXN0VHlwZVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSB1cmxcbiAgICAgKi9cbiAgICBzdWJtaXQocmVxdWVzdFR5cGUsIHVybCkge1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgICAgYXhpb3NbcmVxdWVzdFR5cGVdKHVybCwgdGhpcy5kYXRhKCkpXG4gICAgICAgICAgICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm9uU3VjY2VzcyhyZXNwb25zZS5kYXRhKTtcblxuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHJlc3BvbnNlLmRhdGEpO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vbkZhaWwoZXJyb3IucmVzcG9uc2UuZGF0YSk7XG5cbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KGVycm9yLnJlc3BvbnNlLmRhdGEpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cblxuICAgIC8qKlxuICAgICAqIEhhbmRsZSBhIHN1Y2Nlc3NmdWwgZm9ybSBzdWJtaXNzaW9uLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtvYmplY3R9IGRhdGFcbiAgICAgKi9cbiAgICBvblN1Y2Nlc3MoZGF0YSkge1xuICAgICAgICAvL2FsZXJ0KGRhdGEubWVzc2FnZSk7IC8vIHRlbXBvcmFyeVxuXG4gICAgICAgIHRoaXMucmVzZXQoKTtcbiAgICB9XG5cblxuICAgIC8qKlxuICAgICAqIEhhbmRsZSBhIGZhaWxlZCBmb3JtIHN1Ym1pc3Npb24uXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge29iamVjdH0gZXJyb3JzXG4gICAgICovXG4gICAgb25GYWlsKGVycm9ycykge1xuICAgICAgICB0aGlzLmVycm9ycy5yZWNvcmQoZXJyb3JzKTtcbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IEZvcm07XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvanMvY29yZS9Gb3JtLmpzIiwiY2xhc3MgRXJyb3JzIHtcbiAgICAvKipcbiAgICAgKiBDcmVhdGUgYSBuZXcgRXJyb3JzIGluc3RhbmNlLlxuICAgICAqL1xuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICB0aGlzLmVycm9ycyA9IHt9O1xuICAgIH1cblxuXG4gICAgLyoqXG4gICAgICogRGV0ZXJtaW5lIGlmIGFuIGVycm9ycyBleGlzdHMgZm9yIHRoZSBnaXZlbiBmaWVsZC5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBmaWVsZFxuICAgICAqL1xuICAgIGhhcyhmaWVsZCkge1xuICAgICAgICByZXR1cm4gdGhpcy5lcnJvcnMuaGFzT3duUHJvcGVydHkoZmllbGQpO1xuICAgIH1cblxuXG4gICAgLyoqXG4gICAgICogRGV0ZXJtaW5lIGlmIHdlIGhhdmUgYW55IGVycm9ycy5cbiAgICAgKi9cbiAgICBhbnkoKSB7XG4gICAgICAgIHJldHVybiBPYmplY3Qua2V5cyh0aGlzLmVycm9ycykubGVuZ3RoID4gMDtcbiAgICB9XG5cblxuICAgIC8qKlxuICAgICAqIFJldHJpZXZlIHRoZSBlcnJvciBtZXNzYWdlIGZvciBhIGZpZWxkLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGZpZWxkXG4gICAgICovXG4gICAgZ2V0KGZpZWxkKSB7XG4gICAgICAgIGlmICh0aGlzLmVycm9yc1tmaWVsZF0pIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmVycm9yc1tmaWVsZF1bMF07XG4gICAgICAgIH1cbiAgICB9XG5cblxuICAgIC8qKlxuICAgICAqIFJlY29yZCB0aGUgbmV3IGVycm9ycy5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7b2JqZWN0fSBlcnJvcnNcbiAgICAgKi9cbiAgICByZWNvcmQoZXJyb3JzKSB7XG4gICAgICAgIHRoaXMuZXJyb3JzID0gZXJyb3JzO1xuICAgIH1cblxuXG4gICAgLyoqXG4gICAgICogQ2xlYXIgb25lIG9yIGFsbCBlcnJvciBmaWVsZHMuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge3N0cmluZ3xudWxsfSBmaWVsZFxuICAgICAqL1xuICAgIGNsZWFyKGZpZWxkKSB7XG4gICAgICAgIGlmIChmaWVsZCkge1xuICAgICAgICAgICAgZGVsZXRlIHRoaXMuZXJyb3JzW2ZpZWxkXTtcblxuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5lcnJvcnMgPSB7fTtcbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IEVycm9ycztcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvanMvY29yZS9FcnJvcnMuanMiLCJpbXBvcnQgVnVlUm91dGVyIGZyb20gJ3Z1ZS1yb3V0ZXInO1xuXG5jb25zdCByb3V0ZXMgPSBbXG4gICAge1xuICAgICAgICBwYXRoOiAnLycsXG4gICAgICAgIGNvbXBvbmVudDogcmVxdWlyZSgnLi92dWUvbGF5b3V0cy9wYWdlcycpLFxuICAgICAgICBtZXRhOiB7cmVxdWlyZXNBdXRoOiBmYWxzZX0sXG4gICAgICAgIGNoaWxkcmVuOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgcGF0aDogJycsXG4gICAgICAgICAgICAgICAgY29tcG9uZW50OiByZXF1aXJlKCcuL3Z1ZS9wYWdlcy9ob21lJyksXG4gICAgICAgICAgICAgICAgbWV0YToge3JlcXVpcmVzQXV0aDogZmFsc2V9XG4gICAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICB9LFxuICAgICB7XG4gICAgICAgIHBhdGg6ICcvYWJvdXQnLFxuICAgICAgICBjb21wb25lbnQ6IHJlcXVpcmUoJy4vdnVlL2xheW91dHMvcGFnZXMnKSxcbiAgICAgICAgbWV0YToge3JlcXVpcmVzQXV0aDogZmFsc2V9LFxuICAgICAgICBjaGlsZHJlbjogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHBhdGg6ICcnLFxuICAgICAgICAgICAgICAgIGNvbXBvbmVudDogcmVxdWlyZSgnLi92dWUvcGFnZXMvYWJvdXQnKSxcbiAgICAgICAgICAgICAgICBtZXRhOiB7cmVxdWlyZXNBdXRoOiBmYWxzZX1cbiAgICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgIH0sXG4gICAgIHtcbiAgICAgICAgcGF0aDogJy9jb250YWN0JyxcbiAgICAgICAgY29tcG9uZW50OiByZXF1aXJlKCcuL3Z1ZS9sYXlvdXRzL3BhZ2VzJyksXG4gICAgICAgIG1ldGE6IHtyZXF1aXJlc0F1dGg6IGZhbHNlfSxcbiAgICAgICAgY2hpbGRyZW46IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBwYXRoOiAnJyxcbiAgICAgICAgICAgICAgICBjb21wb25lbnQ6IHJlcXVpcmUoJy4vdnVlL3BhZ2VzL2NvbnRhY3QnKSxcbiAgICAgICAgICAgICAgICBtZXRhOiB7cmVxdWlyZXNBdXRoOiBmYWxzZX1cbiAgICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgIH0sXG4gICAge1xuICAgICAgICBwYXRoOiAnL2xvZ2luJyxcbiAgICAgICAgY29tcG9uZW50OiByZXF1aXJlKCcuL3Z1ZS9sYXlvdXRzL3BvcnRhbCcpLFxuICAgICAgICBtZXRhOiB7cmVxdWlyZXNBdXRoOiBmYWxzZX0sXG4gICAgICAgIGNoaWxkcmVuOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgcGF0aDogJycsXG4gICAgICAgICAgICAgICAgY29tcG9uZW50OiByZXF1aXJlKCcuL3Z1ZS9hdXRoL2xvZ2luJyksXG4gICAgICAgICAgICAgICAgbWV0YToge3JlcXVpcmVzQXV0aDogZmFsc2V9XG4gICAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICB9LFxuICAgICB7XG4gICAgICAgIHBhdGg6ICcvZGFzaGJvYXJkJyxcbiAgICAgICAgY29tcG9uZW50OiByZXF1aXJlKCcuL3Z1ZS9sYXlvdXRzL2FkbWluJyksXG4gICAgICAgIG1ldGE6IHtyZXF1aXJlc0F1dGg6IHRydWV9LFxuICAgICAgICBjaGlsZHJlbjogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHBhdGg6ICcnLFxuICAgICAgICAgICAgICAgIGNvbXBvbmVudDogcmVxdWlyZSgnLi92dWUvZGFzaGJvYXJkL2luZGV4JyksXG4gICAgICAgICAgICAgICAgbWV0YToge3JlcXVpcmVzQXV0aDogdHJ1ZX1cbiAgICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgIH0sXG4gICAge1xuICAgICAgICBwYXRoOiAnL3VzZXJzJyxcbiAgICAgICAgY29tcG9uZW50OiByZXF1aXJlKCcuL3Z1ZS9sYXlvdXRzL2FkbWluJyksXG4gICAgICAgIG1ldGE6IHtyZXF1aXJlc0F1dGg6IHRydWV9LFxuICAgICAgICBjaGlsZHJlbjogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHBhdGg6ICcnLFxuICAgICAgICAgICAgICAgIGNvbXBvbmVudDogcmVxdWlyZSgnLi92dWUvdXNlcnMvaW5kZXgnKSxcbiAgICAgICAgICAgICAgICBtZXRhOiB7cmVxdWlyZXNBdXRoOiB0cnVlfVxuICAgICAgICAgICAgfVxuICAgICAgICBdXG4gICAgfSxcbiAgICB7XG4gICAgICAgIHBhdGg6ICcqJyxcbiAgICAgICAgY29tcG9uZW50OiByZXF1aXJlKCcuL3Z1ZS9sYXlvdXRzL2Vycm9ycycpLFxuICAgICAgICBtZXRhOiB7cmVxdWlyZXNBdXRoOiBmYWxzZX0sXG4gICAgICAgIGNoaWxkcmVuOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgcGF0aDogJycsXG4gICAgICAgICAgICAgICAgY29tcG9uZW50OiByZXF1aXJlKCcuL3Z1ZS9lcnJvcnMvNDA0JyksXG4gICAgICAgICAgICAgICAgbWV0YToge3JlcXVpcmVzQXV0aDogZmFsc2V9XG4gICAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICB9XG5dO1xuXG5jb25zdCByb3V0ZXIgPSBuZXcgVnVlUm91dGVyKHtcbiAgICBsaW5rQWN0aXZlQ2xhc3M6IFwiYWN0aXZlXCIsXG4gICAgbW9kZTogJ2hpc3RvcnknLFxuICAgIHJvdXRlc1xufSk7XG5cbnJvdXRlci5iZWZvcmVFYWNoKCh0bywgZnJvbSwgbmV4dCkgPT4ge1xuICAgIGxldCBhY2Nlc3NUb2tlbiA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdhY2Nlc3NfdG9rZW4nKTtcbiAgICBpZiAodG8ubWV0YS5yZXF1aXJlc0F1dGgpIHtcbiAgICAgICAgaWYgKCFhY2Nlc3NUb2tlbiB8fCBhY2Nlc3NUb2tlbiA9PT0gbnVsbCkge1xuICAgICAgICAgICAgbmV4dCh7XG4gICAgICAgICAgICAgICAgcGF0aDogJy9sb2dpbicsXG4gICAgICAgICAgICAgICAgcXVlcnk6IHtyZWRpcmVjdDogdG8uZnVsbFBhdGh9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBuZXh0KCk7XG5cblxufSk7XG5cbmV4cG9ydCBkZWZhdWx0IHJvdXRlcjtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvanMvcm91dGVzLmpzIiwiLy8gc3R5bGUtbG9hZGVyOiBBZGRzIHNvbWUgY3NzIHRvIHRoZSBET00gYnkgYWRkaW5nIGEgPHN0eWxlPiB0YWdcblxuLy8gbG9hZCB0aGUgc3R5bGVzXG52YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJ2dWVcXFwiOnRydWUsXFxcImlkXFxcIjpcXFwiZGF0YS12LTIzYTAwODgyXFxcIixcXFwic2NvcGVkXFxcIjpmYWxzZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCZidXN0Q2FjaGUhLi9wYWdlcy52dWVcIik7XG5pZih0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbmlmKGNvbnRlbnQubG9jYWxzKSBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzO1xuLy8gYWRkIHRoZSBzdHlsZXMgdG8gdGhlIERPTVxudmFyIHVwZGF0ZSA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlc0NsaWVudC5qc1wiKShcIjA0NTJlYmI4XCIsIGNvbnRlbnQsIGZhbHNlKTtcbi8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbmlmKG1vZHVsZS5ob3QpIHtcbiAvLyBXaGVuIHRoZSBzdHlsZXMgY2hhbmdlLCB1cGRhdGUgdGhlIDxzdHlsZT4gdGFnc1xuIGlmKCFjb250ZW50LmxvY2Fscykge1xuICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJ2dWVcXFwiOnRydWUsXFxcImlkXFxcIjpcXFwiZGF0YS12LTIzYTAwODgyXFxcIixcXFwic2NvcGVkXFxcIjpmYWxzZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCZidXN0Q2FjaGUhLi9wYWdlcy52dWVcIiwgZnVuY3Rpb24oKSB7XG4gICAgIHZhciBuZXdDb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcInZ1ZVxcXCI6dHJ1ZSxcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMjNhMDA4ODJcXFwiLFxcXCJzY29wZWRcXFwiOmZhbHNlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wJmJ1c3RDYWNoZSEuL3BhZ2VzLnZ1ZVwiKTtcbiAgICAgaWYodHlwZW9mIG5ld0NvbnRlbnQgPT09ICdzdHJpbmcnKSBuZXdDb250ZW50ID0gW1ttb2R1bGUuaWQsIG5ld0NvbnRlbnQsICcnXV07XG4gICAgIHVwZGF0ZShuZXdDb250ZW50KTtcbiAgIH0pO1xuIH1cbiAvLyBXaGVuIHRoZSBtb2R1bGUgaXMgZGlzcG9zZWQsIHJlbW92ZSB0aGUgPHN0eWxlPiB0YWdzXG4gbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyB1cGRhdGUoKTsgfSk7XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvdnVlLXN0eWxlLWxvYWRlciEuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyP3NvdXJjZU1hcCEuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJ2dWVcIjp0cnVlLFwiaWRcIjpcImRhdGEtdi0yM2EwMDg4MlwiLFwic2NvcGVkXCI6ZmFsc2UsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wJmJ1c3RDYWNoZSEuL3Jlc291cmNlcy9qcy92dWUvbGF5b3V0cy9wYWdlcy52dWVcbi8vIG1vZHVsZSBpZCA9IDQzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJcXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG4vKiFcXG4gKiBTdGFydCBCb290c3RyYXAgLSBNb2Rlcm4gQnVzaW5lc3MgKGh0dHA6Ly9zdGFydGJvb3RzdHJhcC5jb20vKVxcbiAqIENvcHlyaWdodCAyMDEzLTIwMTYgU3RhcnQgQm9vdHN0cmFwXFxuICogTGljZW5zZWQgdW5kZXIgTUlUIChodHRwczovL2dpdGh1Yi5jb20vQmxhY2tyb2NrRGlnaXRhbC9zdGFydGJvb3RzdHJhcC9ibG9iL2doLXBhZ2VzL0xJQ0VOU0UpXFxuICovXFxuXFxuLyogR2xvYmFsIFN0eWxlcyAqL1xcbmh0bWwsXFxuYm9keSB7XFxuICAgIGhlaWdodDogMTAwJTtcXG59XFxuYm9keSB7XFxuICAgIHBhZGRpbmctdG9wOiA1MHB4OyAvKiBSZXF1aXJlZCBwYWRkaW5nIGZvciAubmF2YmFyLWZpeGVkLXRvcC4gUmVtb3ZlIGlmIHVzaW5nIC5uYXZiYXItc3RhdGljLXRvcC4gQ2hhbmdlIGlmIGhlaWdodCBvZiBuYXZpZ2F0aW9uIGNoYW5nZXMuICovXFxufVxcbi5pbWctcG9ydGZvbGlvIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcXG59XFxuLmltZy1ob3Zlcjpob3ZlciB7XFxuICAgIG9wYWNpdHk6IDAuODtcXG59XFxuXFxuLyogSG9tZSBQYWdlIENhcm91c2VsICovXFxuaGVhZGVyLmNhcm91c2VsIHtcXG4gICAgaGVpZ2h0OiA1MCU7XFxufVxcbmhlYWRlci5jYXJvdXNlbCAuaXRlbSxcXG5oZWFkZXIuY2Fyb3VzZWwgLml0ZW0uYWN0aXZlLFxcbmhlYWRlci5jYXJvdXNlbCAuY2Fyb3VzZWwtaW5uZXIge1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxufVxcbmhlYWRlci5jYXJvdXNlbCAuZmlsbCB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcXG59XFxuXFxuLyogNDA0IFBhZ2UgU3R5bGVzICovXFxuLmVycm9yLTQwNCB7XFxuICAgIGZvbnQtc2l6ZTogMTAwcHg7XFxufVxcblxcbi8qIFByaWNpbmcgUGFnZSBTdHlsZXMgKi9cXG4ucHJpY2Uge1xcbiAgICBkaXNwbGF5OiBibG9jaztcXG4gICAgZm9udC1zaXplOiA1MHB4O1xcbiAgICBsaW5lLWhlaWdodDogNTBweDtcXG59XFxuLnByaWNlIHN1cCB7XFxuICAgIHRvcDogLTIwcHg7XFxuICAgIGxlZnQ6IDJweDtcXG4gICAgZm9udC1zaXplOiAyMHB4O1xcbn1cXG4ucGVyaW9kIHtcXG4gICAgZGlzcGxheTogYmxvY2s7XFxuICAgIGZvbnQtc3R5bGU6IGl0YWxpYztcXG59XFxuXFxuLyogRm9vdGVyIFN0eWxlcyAqL1xcbmZvb3RlciB7XFxuICAgIG1hcmdpbjogNTBweCAwO1xcbn1cXG5cXG4vKiBSZXNwb25zaXZlIFN0eWxlcyAqL1xcbkBtZWRpYShtYXgtd2lkdGg6OTkxcHgpIHtcXG4uY3VzdG9tZXItaW1nLFxcbiAgICAuaW1nLXJlbGF0ZWQge1xcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMzBweDtcXG59XFxufVxcbkBtZWRpYShtYXgtd2lkdGg6NzY3cHgpIHtcXG4uaW1nLXBvcnRmb2xpbyB7XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xcbn1cXG5oZWFkZXIuY2Fyb3VzZWwgLmNhcm91c2VsIHtcXG4gICAgICAgIGhlaWdodDogNzAlO1xcbn1cXG59XFxuXFxuXFx0XFxuXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9Vc2Vycy92aWxsYW1vcm5hbnRvbmlvanIvRGV2ZWxvcG1lbnQvZ3JleWZveC9tYXRjaHVwL3dlYi9yZXNvdXJjZXMvanMvdnVlL2xheW91dHMvcmVzb3VyY2VzL2pzL3Z1ZS9sYXlvdXRzL3BhZ2VzLnZ1ZT8yYzNiZGQ5N1wiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBMEdBOzs7O0dBSUE7O0FBRUEsbUJBQUE7QUFFQTs7SUFFQSxhQUFBO0NBQ0E7QUFFQTtJQUNBLGtCQUFBLENBQUEseUhBQUE7Q0FDQTtBQUVBO0lBQ0Esb0JBQUE7Q0FDQTtBQUVBO0lBQ0EsYUFBQTtDQUNBOztBQUVBLHdCQUFBO0FBRUE7SUFDQSxZQUFBO0NBQ0E7QUFFQTs7O0lBR0EsYUFBQTtDQUNBO0FBRUE7SUFDQSxZQUFBO0lBQ0EsYUFBQTtJQUNBLDRCQUFBO0lBQ0EsdUJBQUE7Q0FDQTs7QUFFQSxxQkFBQTtBQUVBO0lBQ0EsaUJBQUE7Q0FDQTs7QUFFQSx5QkFBQTtBQUVBO0lBQ0EsZUFBQTtJQUNBLGdCQUFBO0lBQ0Esa0JBQUE7Q0FDQTtBQUVBO0lBQ0EsV0FBQTtJQUNBLFVBQUE7SUFDQSxnQkFBQTtDQUNBO0FBRUE7SUFDQSxlQUFBO0lBQ0EsbUJBQUE7Q0FDQTs7QUFFQSxtQkFBQTtBQUVBO0lBQ0EsZUFBQTtDQUNBOztBQUVBLHVCQUFBO0FBRUE7QUFDQTs7UUFFQSxvQkFBQTtDQUNBO0NBQ0E7QUFFQTtBQUNBO1FBQ0Esb0JBQUE7Q0FDQTtBQUVBO1FBQ0EsWUFBQTtDQUNBO0NBQ0FcIixcImZpbGVcIjpcInBhZ2VzLnZ1ZVwiLFwic291cmNlc0NvbnRlbnRcIjpbXCI8dGVtcGxhdGU+XFxuPGRpdiBpZD1cXFwibGF5b3V0cy1wYWdlc1xcXCI+XFxuXFxuICAgIDwhLS0gTmF2aWdhdGlvbiAtLT5cXG4gICAgPG5hdiBjbGFzcz1cXFwibmF2YmFyIG5hdmJhci1pbnZlcnNlIG5hdmJhci1maXhlZC10b3BcXFwiIHJvbGU9XFxcIm5hdmlnYXRpb25cXFwiPlxcbiAgICAgICAgPGRpdiBjbGFzcz1cXFwiY29udGFpbmVyXFxcIj5cXG4gICAgICAgICAgICA8IS0tIEJyYW5kIGFuZCB0b2dnbGUgZ2V0IGdyb3VwZWQgZm9yIGJldHRlciBtb2JpbGUgZGlzcGxheSAtLT5cXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJuYXZiYXItaGVhZGVyXFxcIj5cXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVxcXCJidXR0b25cXFwiIGNsYXNzPVxcXCJuYXZiYXItdG9nZ2xlXFxcIiBkYXRhLXRvZ2dsZT1cXFwiY29sbGFwc2VcXFwiIGRhdGEtdGFyZ2V0PVxcXCIjYnMtZXhhbXBsZS1uYXZiYXItY29sbGFwc2UtMVxcXCI+XFxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cXFwic3Itb25seVxcXCI+VG9nZ2xlIG5hdmlnYXRpb248L3NwYW4+XFxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cXFwiaWNvbi1iYXJcXFwiPjwvc3Bhbj5cXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVxcXCJpY29uLWJhclxcXCI+PC9zcGFuPlxcbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XFxcImljb24tYmFyXFxcIj48L3NwYW4+XFxuICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxcbiAgICAgICAgICAgICAgICA8cm91dGVyLWxpbmsgdG89Jy8nIGNsYXNzPVxcXCJuYXZiYXItYnJhbmRcXFwiPlZ1ZUpTIFN0YXJ0ZXI8L3JvdXRlci1saW5rPlxcbiAgICAgICAgICAgIDwvZGl2PlxcbiAgICAgICAgICAgIDwhLS0gQ29sbGVjdCB0aGUgbmF2IGxpbmtzLCBmb3JtcywgYW5kIG90aGVyIGNvbnRlbnQgZm9yIHRvZ2dsaW5nIC0tPlxcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcImNvbGxhcHNlIG5hdmJhci1jb2xsYXBzZVxcXCIgaWQ9XFxcImJzLWV4YW1wbGUtbmF2YmFyLWNvbGxhcHNlLTFcXFwiPlxcbiAgICAgICAgICAgICAgICA8dWwgY2xhc3M9XFxcIm5hdiBuYXZiYXItbmF2IG5hdmJhci1yaWdodFxcXCI+XFxuICAgICAgICAgICAgICAgICAgICA8bGk+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPHJvdXRlci1saW5rIHRvPSdhYm91dCc+QWJvdXQ8L3JvdXRlci1saW5rPlxcbiAgICAgICAgICAgICAgICAgICAgPC9saT5cXG4gICAgICAgICAgICAgICAgICAgIDxsaT5cXG4gICAgICAgICAgICAgICAgICAgIFxcdDxyb3V0ZXItbGluayB0bz0nY29udGFjdCc+Q29udGFjdDwvcm91dGVyLWxpbms+XFxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxcbiAgICAgICAgICAgICAgICAgICAgPGxpPlxcbiAgICAgICAgICAgICAgICAgICAgXFx0PHJvdXRlci1saW5rIHRvPSdsb2dpbic+TG9naW48L3JvdXRlci1saW5rPlxcbiAgICAgICAgICAgICAgICAgICAgPC9saT5cXG4gICAgICAgICAgICAgICAgICAgIFxcbiAgICAgICAgICAgICAgICA8L3VsPlxcbiAgICAgICAgICAgIDwvZGl2PlxcbiAgICAgICAgICAgIDwhLS0gLy5uYXZiYXItY29sbGFwc2UgLS0+XFxuICAgICAgICA8L2Rpdj5cXG4gICAgICAgIDwhLS0gLy5jb250YWluZXIgLS0+XFxuICAgIDwvbmF2PlxcblxcbiAgICA8IS0tIEhlYWRlciBDYXJvdXNlbCAtLT5cXG4gICAgPGhlYWRlciBpZD1cXFwibXlDYXJvdXNlbFxcXCIgY2xhc3M9XFxcImNhcm91c2VsIHNsaWRlXFxcIj5cXG4gICAgICAgIDwhLS0gSW5kaWNhdG9ycyAtLT5cXG4gICAgICAgIDxvbCBjbGFzcz1cXFwiY2Fyb3VzZWwtaW5kaWNhdG9yc1xcXCI+XFxuICAgICAgICAgICAgPGxpIGRhdGEtdGFyZ2V0PVxcXCIjbXlDYXJvdXNlbFxcXCIgZGF0YS1zbGlkZS10bz1cXFwiMFxcXCIgY2xhc3M9XFxcIlxcXCI+PC9saT5cXG4gICAgICAgICAgICA8bGkgZGF0YS10YXJnZXQ9XFxcIiNteUNhcm91c2VsXFxcIiBkYXRhLXNsaWRlLXRvPVxcXCIxXFxcIiBjbGFzcz1cXFwiYWN0aXZlXFxcIj48L2xpPlxcbiAgICAgICAgICAgIDxsaSBkYXRhLXRhcmdldD1cXFwiI215Q2Fyb3VzZWxcXFwiIGRhdGEtc2xpZGUtdG89XFxcIjJcXFwiPjwvbGk+XFxuICAgICAgICA8L29sPlxcblxcbiAgICAgICAgPCEtLSBXcmFwcGVyIGZvciBzbGlkZXMgLS0+XFxuICAgICAgICA8ZGl2IGNsYXNzPVxcXCJjYXJvdXNlbC1pbm5lclxcXCI+XFxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiaXRlbVxcXCI+XFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcImZpbGxcXFwiIHN0eWxlPVxcXCJiYWNrZ3JvdW5kLWltYWdlOnVybCgnaHR0cDovL3BsYWNlaG9sZC5pdC8xOTAweDEwODAmYW1wO3RleHQ9U2xpZGUgT25lJyk7XFxcIj48L2Rpdj5cXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiY2Fyb3VzZWwtY2FwdGlvblxcXCI+XFxuICAgICAgICAgICAgICAgICAgICA8aDI+Q2FwdGlvbiAxPC9oMj5cXG4gICAgICAgICAgICAgICAgPC9kaXY+XFxuICAgICAgICAgICAgPC9kaXY+XFxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiaXRlbSBhY3RpdmVcXFwiPlxcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJmaWxsXFxcIiBzdHlsZT1cXFwiYmFja2dyb3VuZC1pbWFnZTp1cmwoJ2h0dHA6Ly9wbGFjZWhvbGQuaXQvMTkwMHgxMDgwJmFtcDt0ZXh0PVNsaWRlIFR3bycpO1xcXCI+PC9kaXY+XFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcImNhcm91c2VsLWNhcHRpb25cXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgPGgyPkNhcHRpb24gMjwvaDI+XFxuICAgICAgICAgICAgICAgIDwvZGl2PlxcbiAgICAgICAgICAgIDwvZGl2PlxcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcIml0ZW1cXFwiPlxcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJmaWxsXFxcIiBzdHlsZT1cXFwiYmFja2dyb3VuZC1pbWFnZTp1cmwoJ2h0dHA6Ly9wbGFjZWhvbGQuaXQvMTkwMHgxMDgwJmFtcDt0ZXh0PVNsaWRlIFRocmVlJyk7XFxcIj48L2Rpdj5cXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiY2Fyb3VzZWwtY2FwdGlvblxcXCI+XFxuICAgICAgICAgICAgICAgICAgICA8aDI+Q2FwdGlvbiAzPC9oMj5cXG4gICAgICAgICAgICAgICAgPC9kaXY+XFxuICAgICAgICAgICAgPC9kaXY+XFxuICAgICAgICA8L2Rpdj5cXG5cXG4gICAgICAgIDwhLS0gQ29udHJvbHMgLS0+XFxuICAgICAgICA8YSBjbGFzcz1cXFwibGVmdCBjYXJvdXNlbC1jb250cm9sXFxcIiBocmVmPVxcXCIjbXlDYXJvdXNlbFxcXCIgZGF0YS1zbGlkZT1cXFwicHJldlxcXCI+XFxuICAgICAgICAgICAgPHNwYW4gY2xhc3M9XFxcImljb24tcHJldlxcXCI+PC9zcGFuPlxcbiAgICAgICAgPC9hPlxcbiAgICAgICAgPGEgY2xhc3M9XFxcInJpZ2h0IGNhcm91c2VsLWNvbnRyb2xcXFwiIGhyZWY9XFxcIiNteUNhcm91c2VsXFxcIiBkYXRhLXNsaWRlPVxcXCJuZXh0XFxcIj5cXG4gICAgICAgICAgICA8c3BhbiBjbGFzcz1cXFwiaWNvbi1uZXh0XFxcIj48L3NwYW4+XFxuICAgICAgICA8L2E+XFxuICAgIDwvaGVhZGVyPlxcblxcbiAgICA8IS0tIFBhZ2UgQ29udGVudCAtLT5cXG4gICAgPGRpdiBjbGFzcz1cXFwiY29udGFpbmVyXFxcIj5cXG5cXG4gICAgICAgIFxcblxcbiAgICAgICAgPHJvdXRlci12aWV3Pjwvcm91dGVyLXZpZXc+XFxuXFxuICAgICAgICA8aHI+XFxuXFxuICAgICAgICA8IS0tIEZvb3RlciAtLT5cXG4gICAgICAgIDxmb290ZXI+XFxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwicm93XFxcIj5cXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiY29sLWxnLTEyXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgIDxwPkNvcHlyaWdodCDCqSBZb3VyIFdlYnNpdGUgMjAxNDwvcD5cXG4gICAgICAgICAgICAgICAgPC9kaXY+XFxuICAgICAgICAgICAgPC9kaXY+XFxuICAgICAgICA8L2Zvb3Rlcj5cXG5cXG4gICAgPC9kaXY+XFxuICAgIDwhLS0gLy5jb250YWluZXIgLS0+XFxuXFxuXFxuXFxuPC9kaXY+XFxuPC90ZW1wbGF0ZT5cXG5cXG48c2NyaXB0Plxcbjwvc2NyaXB0PlxcblxcbjxzdHlsZSB0eXBlPVxcXCJ0ZXh0L2Nzc1xcXCIgbWVkaWE9XFxcInNjcmVlblxcXCI+XFxuLyohXFxuICogU3RhcnQgQm9vdHN0cmFwIC0gTW9kZXJuIEJ1c2luZXNzIChodHRwOi8vc3RhcnRib290c3RyYXAuY29tLylcXG4gKiBDb3B5cmlnaHQgMjAxMy0yMDE2IFN0YXJ0IEJvb3RzdHJhcFxcbiAqIExpY2Vuc2VkIHVuZGVyIE1JVCAoaHR0cHM6Ly9naXRodWIuY29tL0JsYWNrcm9ja0RpZ2l0YWwvc3RhcnRib290c3RyYXAvYmxvYi9naC1wYWdlcy9MSUNFTlNFKVxcbiAqL1xcblxcbi8qIEdsb2JhbCBTdHlsZXMgKi9cXG5cXG5odG1sLFxcbmJvZHkge1xcbiAgICBoZWlnaHQ6IDEwMCU7XFxufVxcblxcbmJvZHkge1xcbiAgICBwYWRkaW5nLXRvcDogNTBweDsgLyogUmVxdWlyZWQgcGFkZGluZyBmb3IgLm5hdmJhci1maXhlZC10b3AuIFJlbW92ZSBpZiB1c2luZyAubmF2YmFyLXN0YXRpYy10b3AuIENoYW5nZSBpZiBoZWlnaHQgb2YgbmF2aWdhdGlvbiBjaGFuZ2VzLiAqL1xcbn1cXG5cXG4uaW1nLXBvcnRmb2xpbyB7XFxuICAgIG1hcmdpbi1ib3R0b206IDMwcHg7XFxufVxcblxcbi5pbWctaG92ZXI6aG92ZXIge1xcbiAgICBvcGFjaXR5OiAwLjg7XFxufVxcblxcbi8qIEhvbWUgUGFnZSBDYXJvdXNlbCAqL1xcblxcbmhlYWRlci5jYXJvdXNlbCB7XFxuICAgIGhlaWdodDogNTAlO1xcbn1cXG5cXG5oZWFkZXIuY2Fyb3VzZWwgLml0ZW0sXFxuaGVhZGVyLmNhcm91c2VsIC5pdGVtLmFjdGl2ZSxcXG5oZWFkZXIuY2Fyb3VzZWwgLmNhcm91c2VsLWlubmVyIHtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbn1cXG5cXG5oZWFkZXIuY2Fyb3VzZWwgLmZpbGwge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgaGVpZ2h0OiAxMDAlO1xcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XFxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XFxufVxcblxcbi8qIDQwNCBQYWdlIFN0eWxlcyAqL1xcblxcbi5lcnJvci00MDQge1xcbiAgICBmb250LXNpemU6IDEwMHB4O1xcbn1cXG5cXG4vKiBQcmljaW5nIFBhZ2UgU3R5bGVzICovXFxuXFxuLnByaWNlIHtcXG4gICAgZGlzcGxheTogYmxvY2s7XFxuICAgIGZvbnQtc2l6ZTogNTBweDtcXG4gICAgbGluZS1oZWlnaHQ6IDUwcHg7XFxufVxcblxcbi5wcmljZSBzdXAge1xcbiAgICB0b3A6IC0yMHB4O1xcbiAgICBsZWZ0OiAycHg7XFxuICAgIGZvbnQtc2l6ZTogMjBweDtcXG59XFxuXFxuLnBlcmlvZCB7XFxuICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgICBmb250LXN0eWxlOiBpdGFsaWM7XFxufVxcblxcbi8qIEZvb3RlciBTdHlsZXMgKi9cXG5cXG5mb290ZXIge1xcbiAgICBtYXJnaW46IDUwcHggMDtcXG59XFxuXFxuLyogUmVzcG9uc2l2ZSBTdHlsZXMgKi9cXG5cXG5AbWVkaWEobWF4LXdpZHRoOjk5MXB4KSB7XFxuICAgIC5jdXN0b21lci1pbWcsXFxuICAgIC5pbWctcmVsYXRlZCB7XFxuICAgICAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xcbiAgICB9XFxufVxcblxcbkBtZWRpYShtYXgtd2lkdGg6NzY3cHgpIHtcXG4gICAgLmltZy1wb3J0Zm9saW8ge1xcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcXG4gICAgfVxcblxcbiAgICBoZWFkZXIuY2Fyb3VzZWwgLmNhcm91c2VsIHtcXG4gICAgICAgIGhlaWdodDogNzAlO1xcbiAgICB9XFxufVxcblxcblxcdFxcbjwvc3R5bGU+XFxuXFxuXCJdLFwic291cmNlUm9vdFwiOlwiXCJ9XSk7XG5cbi8vIGV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXI/c291cmNlTWFwIS4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcInZ1ZVwiOnRydWUsXCJpZFwiOlwiZGF0YS12LTIzYTAwODgyXCIsXCJzY29wZWRcIjpmYWxzZSxcImhhc0lubGluZUNvbmZpZ1wiOnRydWV9IS4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAmYnVzdENhY2hlIS4vcmVzb3VyY2VzL2pzL3Z1ZS9sYXlvdXRzL3BhZ2VzLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNDRcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiLyoqXG4gKiBUcmFuc2xhdGVzIHRoZSBsaXN0IGZvcm1hdCBwcm9kdWNlZCBieSBjc3MtbG9hZGVyIGludG8gc29tZXRoaW5nXG4gKiBlYXNpZXIgdG8gbWFuaXB1bGF0ZS5cbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBsaXN0VG9TdHlsZXMgKHBhcmVudElkLCBsaXN0KSB7XG4gIHZhciBzdHlsZXMgPSBbXVxuICB2YXIgbmV3U3R5bGVzID0ge31cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsaXN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGl0ZW0gPSBsaXN0W2ldXG4gICAgdmFyIGlkID0gaXRlbVswXVxuICAgIHZhciBjc3MgPSBpdGVtWzFdXG4gICAgdmFyIG1lZGlhID0gaXRlbVsyXVxuICAgIHZhciBzb3VyY2VNYXAgPSBpdGVtWzNdXG4gICAgdmFyIHBhcnQgPSB7XG4gICAgICBpZDogcGFyZW50SWQgKyAnOicgKyBpLFxuICAgICAgY3NzOiBjc3MsXG4gICAgICBtZWRpYTogbWVkaWEsXG4gICAgICBzb3VyY2VNYXA6IHNvdXJjZU1hcFxuICAgIH1cbiAgICBpZiAoIW5ld1N0eWxlc1tpZF0pIHtcbiAgICAgIHN0eWxlcy5wdXNoKG5ld1N0eWxlc1tpZF0gPSB7IGlkOiBpZCwgcGFydHM6IFtwYXJ0XSB9KVxuICAgIH0gZWxzZSB7XG4gICAgICBuZXdTdHlsZXNbaWRdLnBhcnRzLnB1c2gocGFydClcbiAgICB9XG4gIH1cbiAgcmV0dXJuIHN0eWxlc1xufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvdnVlLXN0eWxlLWxvYWRlci9saWIvbGlzdFRvU3R5bGVzLmpzXG4vLyBtb2R1bGUgaWQgPSA0NVxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJ2YXIgcmVuZGVyID0gZnVuY3Rpb24oKSB7XG4gIHZhciBfdm0gPSB0aGlzXG4gIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgcmV0dXJuIF9jKFwiZGl2XCIsIHsgYXR0cnM6IHsgaWQ6IFwibGF5b3V0cy1wYWdlc1wiIH0gfSwgW1xuICAgIF9jKFxuICAgICAgXCJuYXZcIixcbiAgICAgIHtcbiAgICAgICAgc3RhdGljQ2xhc3M6IFwibmF2YmFyIG5hdmJhci1pbnZlcnNlIG5hdmJhci1maXhlZC10b3BcIixcbiAgICAgICAgYXR0cnM6IHsgcm9sZTogXCJuYXZpZ2F0aW9uXCIgfVxuICAgICAgfSxcbiAgICAgIFtcbiAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb250YWluZXJcIiB9LCBbXG4gICAgICAgICAgX2MoXG4gICAgICAgICAgICBcImRpdlwiLFxuICAgICAgICAgICAgeyBzdGF0aWNDbGFzczogXCJuYXZiYXItaGVhZGVyXCIgfSxcbiAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgX3ZtLl9tKDApLFxuICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICBcInJvdXRlci1saW5rXCIsXG4gICAgICAgICAgICAgICAgeyBzdGF0aWNDbGFzczogXCJuYXZiYXItYnJhbmRcIiwgYXR0cnM6IHsgdG86IFwiL1wiIH0gfSxcbiAgICAgICAgICAgICAgICBbX3ZtLl92KFwiVnVlSlMgU3RhcnRlclwiKV1cbiAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIDFcbiAgICAgICAgICApLFxuICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgX2MoXG4gICAgICAgICAgICBcImRpdlwiLFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJjb2xsYXBzZSBuYXZiYXItY29sbGFwc2VcIixcbiAgICAgICAgICAgICAgYXR0cnM6IHsgaWQ6IFwiYnMtZXhhbXBsZS1uYXZiYXItY29sbGFwc2UtMVwiIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBbXG4gICAgICAgICAgICAgIF9jKFwidWxcIiwgeyBzdGF0aWNDbGFzczogXCJuYXYgbmF2YmFyLW5hdiBuYXZiYXItcmlnaHRcIiB9LCBbXG4gICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICBcImxpXCIsXG4gICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgIF9jKFwicm91dGVyLWxpbmtcIiwgeyBhdHRyczogeyB0bzogXCJhYm91dFwiIH0gfSwgW1xuICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIkFib3V0XCIpXG4gICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICAgMVxuICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgIFwibGlcIixcbiAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJyb3V0ZXItbGlua1wiLCB7IGF0dHJzOiB7IHRvOiBcImNvbnRhY3RcIiB9IH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCJDb250YWN0XCIpXG4gICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICAgMVxuICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgIFwibGlcIixcbiAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJyb3V0ZXItbGlua1wiLCB7IGF0dHJzOiB7IHRvOiBcImxvZ2luXCIgfSB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiTG9naW5cIilcbiAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAxXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgXVxuICAgICAgICAgIClcbiAgICAgICAgXSlcbiAgICAgIF1cbiAgICApLFxuICAgIF92bS5fdihcIiBcIiksXG4gICAgX3ZtLl9tKDEpLFxuICAgIF92bS5fdihcIiBcIiksXG4gICAgX2MoXG4gICAgICBcImRpdlwiLFxuICAgICAgeyBzdGF0aWNDbGFzczogXCJjb250YWluZXJcIiB9LFxuICAgICAgW19jKFwicm91dGVyLXZpZXdcIiksIF92bS5fdihcIiBcIiksIF9jKFwiaHJcIiksIF92bS5fdihcIiBcIiksIF92bS5fbSgyKV0sXG4gICAgICAxXG4gICAgKVxuICBdKVxufVxudmFyIHN0YXRpY1JlbmRlckZucyA9IFtcbiAgZnVuY3Rpb24oKSB7XG4gICAgdmFyIF92bSA9IHRoaXNcbiAgICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgICByZXR1cm4gX2MoXG4gICAgICBcImJ1dHRvblwiLFxuICAgICAge1xuICAgICAgICBzdGF0aWNDbGFzczogXCJuYXZiYXItdG9nZ2xlXCIsXG4gICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgdHlwZTogXCJidXR0b25cIixcbiAgICAgICAgICBcImRhdGEtdG9nZ2xlXCI6IFwiY29sbGFwc2VcIixcbiAgICAgICAgICBcImRhdGEtdGFyZ2V0XCI6IFwiI2JzLWV4YW1wbGUtbmF2YmFyLWNvbGxhcHNlLTFcIlxuICAgICAgICB9XG4gICAgICB9LFxuICAgICAgW1xuICAgICAgICBfYyhcInNwYW5cIiwgeyBzdGF0aWNDbGFzczogXCJzci1vbmx5XCIgfSwgW192bS5fdihcIlRvZ2dsZSBuYXZpZ2F0aW9uXCIpXSksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFwic3BhblwiLCB7IHN0YXRpY0NsYXNzOiBcImljb24tYmFyXCIgfSksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFwic3BhblwiLCB7IHN0YXRpY0NsYXNzOiBcImljb24tYmFyXCIgfSksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFwic3BhblwiLCB7IHN0YXRpY0NsYXNzOiBcImljb24tYmFyXCIgfSlcbiAgICAgIF1cbiAgICApXG4gIH0sXG4gIGZ1bmN0aW9uKCkge1xuICAgIHZhciBfdm0gPSB0aGlzXG4gICAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gICAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gICAgcmV0dXJuIF9jKFxuICAgICAgXCJoZWFkZXJcIixcbiAgICAgIHsgc3RhdGljQ2xhc3M6IFwiY2Fyb3VzZWwgc2xpZGVcIiwgYXR0cnM6IHsgaWQ6IFwibXlDYXJvdXNlbFwiIH0gfSxcbiAgICAgIFtcbiAgICAgICAgX2MoXCJvbFwiLCB7IHN0YXRpY0NsYXNzOiBcImNhcm91c2VsLWluZGljYXRvcnNcIiB9LCBbXG4gICAgICAgICAgX2MoXCJsaVwiLCB7XG4gICAgICAgICAgICBhdHRyczogeyBcImRhdGEtdGFyZ2V0XCI6IFwiI215Q2Fyb3VzZWxcIiwgXCJkYXRhLXNsaWRlLXRvXCI6IFwiMFwiIH1cbiAgICAgICAgICB9KSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF9jKFwibGlcIiwge1xuICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiYWN0aXZlXCIsXG4gICAgICAgICAgICBhdHRyczogeyBcImRhdGEtdGFyZ2V0XCI6IFwiI215Q2Fyb3VzZWxcIiwgXCJkYXRhLXNsaWRlLXRvXCI6IFwiMVwiIH1cbiAgICAgICAgICB9KSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF9jKFwibGlcIiwge1xuICAgICAgICAgICAgYXR0cnM6IHsgXCJkYXRhLXRhcmdldFwiOiBcIiNteUNhcm91c2VsXCIsIFwiZGF0YS1zbGlkZS10b1wiOiBcIjJcIiB9XG4gICAgICAgICAgfSlcbiAgICAgICAgXSksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY2Fyb3VzZWwtaW5uZXJcIiB9LCBbXG4gICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJpdGVtXCIgfSwgW1xuICAgICAgICAgICAgX2MoXCJkaXZcIiwge1xuICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJmaWxsXCIsXG4gICAgICAgICAgICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICAgICAgICAgICAgXCJiYWNrZ3JvdW5kLWltYWdlXCI6XG4gICAgICAgICAgICAgICAgICBcInVybCgnaHR0cDovL3BsYWNlaG9sZC5pdC8xOTAweDEwODAmdGV4dD1TbGlkZSBPbmUnKVwiXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY2Fyb3VzZWwtY2FwdGlvblwiIH0sIFtcbiAgICAgICAgICAgICAgX2MoXCJoMlwiLCBbX3ZtLl92KFwiQ2FwdGlvbiAxXCIpXSlcbiAgICAgICAgICAgIF0pXG4gICAgICAgICAgXSksXG4gICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcIml0ZW0gYWN0aXZlXCIgfSwgW1xuICAgICAgICAgICAgX2MoXCJkaXZcIiwge1xuICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJmaWxsXCIsXG4gICAgICAgICAgICAgIHN0YXRpY1N0eWxlOiB7XG4gICAgICAgICAgICAgICAgXCJiYWNrZ3JvdW5kLWltYWdlXCI6XG4gICAgICAgICAgICAgICAgICBcInVybCgnaHR0cDovL3BsYWNlaG9sZC5pdC8xOTAweDEwODAmdGV4dD1TbGlkZSBUd28nKVwiXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY2Fyb3VzZWwtY2FwdGlvblwiIH0sIFtcbiAgICAgICAgICAgICAgX2MoXCJoMlwiLCBbX3ZtLl92KFwiQ2FwdGlvbiAyXCIpXSlcbiAgICAgICAgICAgIF0pXG4gICAgICAgICAgXSksXG4gICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcIml0ZW1cIiB9LCBbXG4gICAgICAgICAgICBfYyhcImRpdlwiLCB7XG4gICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImZpbGxcIixcbiAgICAgICAgICAgICAgc3RhdGljU3R5bGU6IHtcbiAgICAgICAgICAgICAgICBcImJhY2tncm91bmQtaW1hZ2VcIjpcbiAgICAgICAgICAgICAgICAgIFwidXJsKCdodHRwOi8vcGxhY2Vob2xkLml0LzE5MDB4MTA4MCZ0ZXh0PVNsaWRlIFRocmVlJylcIlxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNhcm91c2VsLWNhcHRpb25cIiB9LCBbXG4gICAgICAgICAgICAgIF9jKFwiaDJcIiwgW192bS5fdihcIkNhcHRpb24gM1wiKV0pXG4gICAgICAgICAgICBdKVxuICAgICAgICAgIF0pXG4gICAgICAgIF0pLFxuICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICBfYyhcbiAgICAgICAgICBcImFcIixcbiAgICAgICAgICB7XG4gICAgICAgICAgICBzdGF0aWNDbGFzczogXCJsZWZ0IGNhcm91c2VsLWNvbnRyb2xcIixcbiAgICAgICAgICAgIGF0dHJzOiB7IGhyZWY6IFwiI215Q2Fyb3VzZWxcIiwgXCJkYXRhLXNsaWRlXCI6IFwicHJldlwiIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIFtfYyhcInNwYW5cIiwgeyBzdGF0aWNDbGFzczogXCJpY29uLXByZXZcIiB9KV1cbiAgICAgICAgKSxcbiAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgX2MoXG4gICAgICAgICAgXCJhXCIsXG4gICAgICAgICAge1xuICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwicmlnaHQgY2Fyb3VzZWwtY29udHJvbFwiLFxuICAgICAgICAgICAgYXR0cnM6IHsgaHJlZjogXCIjbXlDYXJvdXNlbFwiLCBcImRhdGEtc2xpZGVcIjogXCJuZXh0XCIgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgW19jKFwic3BhblwiLCB7IHN0YXRpY0NsYXNzOiBcImljb24tbmV4dFwiIH0pXVxuICAgICAgICApXG4gICAgICBdXG4gICAgKVxuICB9LFxuICBmdW5jdGlvbigpIHtcbiAgICB2YXIgX3ZtID0gdGhpc1xuICAgIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICAgIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICAgIHJldHVybiBfYyhcImZvb3RlclwiLCBbXG4gICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcInJvd1wiIH0sIFtcbiAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtbGctMTJcIiB9LCBbXG4gICAgICAgICAgX2MoXCJwXCIsIFtfdm0uX3YoXCJDb3B5cmlnaHQgwqkgWW91ciBXZWJzaXRlIDIwMTRcIildKVxuICAgICAgICBdKVxuICAgICAgXSlcbiAgICBdKVxuICB9XG5dXG5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbm1vZHVsZS5leHBvcnRzID0geyByZW5kZXI6IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnMgfVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKSAgICAgIC5yZXJlbmRlcihcImRhdGEtdi0yM2EwMDg4MlwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtMjNhMDA4ODJcIixcImhhc1Njb3BlZFwiOmZhbHNlLFwiYnVibGVcIjp7XCJ0cmFuc2Zvcm1zXCI6e319fSEuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAmYnVzdENhY2hlIS4vcmVzb3VyY2VzL2pzL3Z1ZS9sYXlvdXRzL3BhZ2VzLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNDdcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwidmFyIGRpc3Bvc2VkID0gZmFsc2VcbnZhciBub3JtYWxpemVDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKVxuLyogc2NyaXB0ICovXG52YXIgX192dWVfc2NyaXB0X18gPSBudWxsXG4vKiB0ZW1wbGF0ZSAqL1xudmFyIF9fdnVlX3RlbXBsYXRlX18gPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0xNmRlNGQzNlxcXCIsXFxcImhhc1Njb3BlZFxcXCI6ZmFsc2UsXFxcImJ1YmxlXFxcIjp7XFxcInRyYW5zZm9ybXNcXFwiOnt9fX0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCZidXN0Q2FjaGUhLi9ob21lLnZ1ZVwiKVxuLyogdGVtcGxhdGUgZnVuY3Rpb25hbCAqL1xuICB2YXIgX192dWVfdGVtcGxhdGVfZnVuY3Rpb25hbF9fID0gZmFsc2Vcbi8qIHN0eWxlcyAqL1xudmFyIF9fdnVlX3N0eWxlc19fID0gbnVsbFxuLyogc2NvcGVJZCAqL1xudmFyIF9fdnVlX3Njb3BlSWRfXyA9IG51bGxcbi8qIG1vZHVsZUlkZW50aWZpZXIgKHNlcnZlciBvbmx5KSAqL1xudmFyIF9fdnVlX21vZHVsZV9pZGVudGlmaWVyX18gPSBudWxsXG52YXIgQ29tcG9uZW50ID0gbm9ybWFsaXplQ29tcG9uZW50KFxuICBfX3Z1ZV9zY3JpcHRfXyxcbiAgX192dWVfdGVtcGxhdGVfXyxcbiAgX192dWVfdGVtcGxhdGVfZnVuY3Rpb25hbF9fLFxuICBfX3Z1ZV9zdHlsZXNfXyxcbiAgX192dWVfc2NvcGVJZF9fLFxuICBfX3Z1ZV9tb2R1bGVfaWRlbnRpZmllcl9fXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcInJlc291cmNlcy9qcy92dWUvcGFnZXMvaG9tZS52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkgeyAgcmV0dXJuIGtleSAhPT0gXCJkZWZhdWx0XCIgJiYga2V5LnN1YnN0cigwLCAyKSAhPT0gXCJfX1wifSkpIHsgIGNvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LTE2ZGU0ZDM2XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtMTZkZTRkMzZcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4nICsgJyAgfVxuICBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICBkaXNwb3NlZCA9IHRydWVcbiAgfSlcbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2pzL3Z1ZS9wYWdlcy9ob21lLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNDhcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwidmFyIHJlbmRlciA9IGZ1bmN0aW9uKCkge1xuICB2YXIgX3ZtID0gdGhpc1xuICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gIHJldHVybiBfdm0uX20oMClcbn1cbnZhciBzdGF0aWNSZW5kZXJGbnMgPSBbXG4gIGZ1bmN0aW9uKCkge1xuICAgIHZhciBfdm0gPSB0aGlzXG4gICAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gICAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gICAgcmV0dXJuIF9jKFwiZGl2XCIsIHsgYXR0cnM6IHsgaWQ6IFwiaG9tZVwiIH0gfSwgW1xuICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJyb3dcIiB9LCBbXG4gICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLWxnLTEyXCIgfSwgW1xuICAgICAgICAgIF9jKFwiaDFcIiwgeyBzdGF0aWNDbGFzczogXCJwYWdlLWhlYWRlclwiIH0sIFtcbiAgICAgICAgICAgIF92bS5fdihcbiAgICAgICAgICAgICAgXCJcXG4gICAgICAgICAgICAgICAgICAgIFdlbGNvbWUgdG8gTW9kZXJuIEJ1c2luZXNzXFxuICAgICAgICAgICAgICAgIFwiXG4gICAgICAgICAgICApXG4gICAgICAgICAgXSlcbiAgICAgICAgXSksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTRcIiB9LCBbXG4gICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJwYW5lbCBwYW5lbC1kZWZhdWx0XCIgfSwgW1xuICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJwYW5lbC1oZWFkaW5nXCIgfSwgW1xuICAgICAgICAgICAgICBfYyhcImg0XCIsIFtcbiAgICAgICAgICAgICAgICBfYyhcImlcIiwgeyBzdGF0aWNDbGFzczogXCJmYSBmYS1mdyBmYS1jaGVja1wiIH0pLFxuICAgICAgICAgICAgICAgIF92bS5fdihcIiBCb290c3RyYXAgdjMuMi4wXCIpXG4gICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICBdKSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcInBhbmVsLWJvZHlcIiB9LCBbXG4gICAgICAgICAgICAgIF9jKFwicFwiLCBbXG4gICAgICAgICAgICAgICAgX3ZtLl92KFxuICAgICAgICAgICAgICAgICAgXCJMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzaWNpbmcgZWxpdC4gSXRhcXVlLCBvcHRpbyBjb3Jwb3JpcyBxdWFlIG51bGxhIGFzcGVybmF0dXIgaW4gYWxpYXMgYXQgbnVtcXVhbSByZXJ1bSBlYSBleGNlcHR1cmkgZXhwZWRpdGEgdGVuZXR1ciBhc3N1bWVuZGEgdm9sdXB0YXRpYnVzIGV2ZW5pZXQgaW5jaWR1bnQgZGljdGEgbm9zdHJ1bSBxdW9kP1wiXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgXCJhXCIsXG4gICAgICAgICAgICAgICAgeyBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRlZmF1bHRcIiwgYXR0cnM6IHsgaHJlZjogXCIjXCIgfSB9LFxuICAgICAgICAgICAgICAgIFtfdm0uX3YoXCJMZWFybiBNb3JlXCIpXVxuICAgICAgICAgICAgICApXG4gICAgICAgICAgICBdKVxuICAgICAgICAgIF0pXG4gICAgICAgIF0pLFxuICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbC1tZC00XCIgfSwgW1xuICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwicGFuZWwgcGFuZWwtZGVmYXVsdFwiIH0sIFtcbiAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwicGFuZWwtaGVhZGluZ1wiIH0sIFtcbiAgICAgICAgICAgICAgX2MoXCJoNFwiLCBbXG4gICAgICAgICAgICAgICAgX2MoXCJpXCIsIHsgc3RhdGljQ2xhc3M6IFwiZmEgZmEtZncgZmEtZ2lmdFwiIH0pLFxuICAgICAgICAgICAgICAgIF92bS5fdihcIiBGcmVlICYgT3BlbiBTb3VyY2VcIilcbiAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwicGFuZWwtYm9keVwiIH0sIFtcbiAgICAgICAgICAgICAgX2MoXCJwXCIsIFtcbiAgICAgICAgICAgICAgICBfdm0uX3YoXG4gICAgICAgICAgICAgICAgICBcIkxvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNpY2luZyBlbGl0LiBJdGFxdWUsIG9wdGlvIGNvcnBvcmlzIHF1YWUgbnVsbGEgYXNwZXJuYXR1ciBpbiBhbGlhcyBhdCBudW1xdWFtIHJlcnVtIGVhIGV4Y2VwdHVyaSBleHBlZGl0YSB0ZW5ldHVyIGFzc3VtZW5kYSB2b2x1cHRhdGlidXMgZXZlbmlldCBpbmNpZHVudCBkaWN0YSBub3N0cnVtIHF1b2Q/XCJcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICBcImFcIixcbiAgICAgICAgICAgICAgICB7IHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZGVmYXVsdFwiLCBhdHRyczogeyBocmVmOiBcIiNcIiB9IH0sXG4gICAgICAgICAgICAgICAgW192bS5fdihcIkxlYXJuIE1vcmVcIildXG4gICAgICAgICAgICAgIClcbiAgICAgICAgICAgIF0pXG4gICAgICAgICAgXSlcbiAgICAgICAgXSksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTRcIiB9LCBbXG4gICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJwYW5lbCBwYW5lbC1kZWZhdWx0XCIgfSwgW1xuICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJwYW5lbC1oZWFkaW5nXCIgfSwgW1xuICAgICAgICAgICAgICBfYyhcImg0XCIsIFtcbiAgICAgICAgICAgICAgICBfYyhcImlcIiwgeyBzdGF0aWNDbGFzczogXCJmYSBmYS1mdyBmYS1jb21wYXNzXCIgfSksXG4gICAgICAgICAgICAgICAgX3ZtLl92KFwiIEVhc3kgdG8gVXNlXCIpXG4gICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICBdKSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcInBhbmVsLWJvZHlcIiB9LCBbXG4gICAgICAgICAgICAgIF9jKFwicFwiLCBbXG4gICAgICAgICAgICAgICAgX3ZtLl92KFxuICAgICAgICAgICAgICAgICAgXCJMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzaWNpbmcgZWxpdC4gSXRhcXVlLCBvcHRpbyBjb3Jwb3JpcyBxdWFlIG51bGxhIGFzcGVybmF0dXIgaW4gYWxpYXMgYXQgbnVtcXVhbSByZXJ1bSBlYSBleGNlcHR1cmkgZXhwZWRpdGEgdGVuZXR1ciBhc3N1bWVuZGEgdm9sdXB0YXRpYnVzIGV2ZW5pZXQgaW5jaWR1bnQgZGljdGEgbm9zdHJ1bSBxdW9kP1wiXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgXCJhXCIsXG4gICAgICAgICAgICAgICAgeyBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRlZmF1bHRcIiwgYXR0cnM6IHsgaHJlZjogXCIjXCIgfSB9LFxuICAgICAgICAgICAgICAgIFtfdm0uX3YoXCJMZWFybiBNb3JlXCIpXVxuICAgICAgICAgICAgICApXG4gICAgICAgICAgICBdKVxuICAgICAgICAgIF0pXG4gICAgICAgIF0pXG4gICAgICBdKSxcbiAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcInJvd1wiIH0sIFtcbiAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtbGctMTJcIiB9LCBbXG4gICAgICAgICAgX2MoXCJoMlwiLCB7IHN0YXRpY0NsYXNzOiBcInBhZ2UtaGVhZGVyXCIgfSwgW1xuICAgICAgICAgICAgX3ZtLl92KFwiUG9ydGZvbGlvIEhlYWRpbmdcIilcbiAgICAgICAgICBdKVxuICAgICAgICBdKSxcbiAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtbWQtNCBjb2wtc20tNlwiIH0sIFtcbiAgICAgICAgICBfYyhcImFcIiwgeyBhdHRyczogeyBocmVmOiBcInBvcnRmb2xpby1pdGVtLmh0bWxcIiB9IH0sIFtcbiAgICAgICAgICAgIF9jKFwiaW1nXCIsIHtcbiAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiaW1nLXJlc3BvbnNpdmUgaW1nLXBvcnRmb2xpbyBpbWctaG92ZXJcIixcbiAgICAgICAgICAgICAgYXR0cnM6IHsgc3JjOiBcImh0dHA6Ly9wbGFjZWhvbGQuaXQvNzAweDQ1MFwiLCBhbHQ6IFwiXCIgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICBdKVxuICAgICAgICBdKSxcbiAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtbWQtNCBjb2wtc20tNlwiIH0sIFtcbiAgICAgICAgICBfYyhcImFcIiwgeyBhdHRyczogeyBocmVmOiBcInBvcnRmb2xpby1pdGVtLmh0bWxcIiB9IH0sIFtcbiAgICAgICAgICAgIF9jKFwiaW1nXCIsIHtcbiAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiaW1nLXJlc3BvbnNpdmUgaW1nLXBvcnRmb2xpbyBpbWctaG92ZXJcIixcbiAgICAgICAgICAgICAgYXR0cnM6IHsgc3JjOiBcImh0dHA6Ly9wbGFjZWhvbGQuaXQvNzAweDQ1MFwiLCBhbHQ6IFwiXCIgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICBdKVxuICAgICAgICBdKSxcbiAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtbWQtNCBjb2wtc20tNlwiIH0sIFtcbiAgICAgICAgICBfYyhcImFcIiwgeyBhdHRyczogeyBocmVmOiBcInBvcnRmb2xpby1pdGVtLmh0bWxcIiB9IH0sIFtcbiAgICAgICAgICAgIF9jKFwiaW1nXCIsIHtcbiAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiaW1nLXJlc3BvbnNpdmUgaW1nLXBvcnRmb2xpbyBpbWctaG92ZXJcIixcbiAgICAgICAgICAgICAgYXR0cnM6IHsgc3JjOiBcImh0dHA6Ly9wbGFjZWhvbGQuaXQvNzAweDQ1MFwiLCBhbHQ6IFwiXCIgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICBdKVxuICAgICAgICBdKSxcbiAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtbWQtNCBjb2wtc20tNlwiIH0sIFtcbiAgICAgICAgICBfYyhcImFcIiwgeyBhdHRyczogeyBocmVmOiBcInBvcnRmb2xpby1pdGVtLmh0bWxcIiB9IH0sIFtcbiAgICAgICAgICAgIF9jKFwiaW1nXCIsIHtcbiAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiaW1nLXJlc3BvbnNpdmUgaW1nLXBvcnRmb2xpbyBpbWctaG92ZXJcIixcbiAgICAgICAgICAgICAgYXR0cnM6IHsgc3JjOiBcImh0dHA6Ly9wbGFjZWhvbGQuaXQvNzAweDQ1MFwiLCBhbHQ6IFwiXCIgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICBdKVxuICAgICAgICBdKSxcbiAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtbWQtNCBjb2wtc20tNlwiIH0sIFtcbiAgICAgICAgICBfYyhcImFcIiwgeyBhdHRyczogeyBocmVmOiBcInBvcnRmb2xpby1pdGVtLmh0bWxcIiB9IH0sIFtcbiAgICAgICAgICAgIF9jKFwiaW1nXCIsIHtcbiAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiaW1nLXJlc3BvbnNpdmUgaW1nLXBvcnRmb2xpbyBpbWctaG92ZXJcIixcbiAgICAgICAgICAgICAgYXR0cnM6IHsgc3JjOiBcImh0dHA6Ly9wbGFjZWhvbGQuaXQvNzAweDQ1MFwiLCBhbHQ6IFwiXCIgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICBdKVxuICAgICAgICBdKSxcbiAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtbWQtNCBjb2wtc20tNlwiIH0sIFtcbiAgICAgICAgICBfYyhcImFcIiwgeyBhdHRyczogeyBocmVmOiBcInBvcnRmb2xpby1pdGVtLmh0bWxcIiB9IH0sIFtcbiAgICAgICAgICAgIF9jKFwiaW1nXCIsIHtcbiAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiaW1nLXJlc3BvbnNpdmUgaW1nLXBvcnRmb2xpbyBpbWctaG92ZXJcIixcbiAgICAgICAgICAgICAgYXR0cnM6IHsgc3JjOiBcImh0dHA6Ly9wbGFjZWhvbGQuaXQvNzAweDQ1MFwiLCBhbHQ6IFwiXCIgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICBdKVxuICAgICAgICBdKVxuICAgICAgXSksXG4gICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJyb3dcIiB9LCBbXG4gICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLWxnLTEyXCIgfSwgW1xuICAgICAgICAgIF9jKFwiaDJcIiwgeyBzdGF0aWNDbGFzczogXCJwYWdlLWhlYWRlclwiIH0sIFtcbiAgICAgICAgICAgIF92bS5fdihcIk1vZGVybiBCdXNpbmVzcyBGZWF0dXJlc1wiKVxuICAgICAgICAgIF0pXG4gICAgICAgIF0pLFxuICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbC1tZC02XCIgfSwgW1xuICAgICAgICAgIF9jKFwicFwiLCBbXG4gICAgICAgICAgICBfdm0uX3YoXCJUaGUgTW9kZXJuIEJ1c2luZXNzIHRlbXBsYXRlIGJ5IFN0YXJ0IEJvb3RzdHJhcCBpbmNsdWRlczpcIilcbiAgICAgICAgICBdKSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF9jKFwidWxcIiwgW1xuICAgICAgICAgICAgX2MoXCJsaVwiLCBbX2MoXCJzdHJvbmdcIiwgW192bS5fdihcIkJvb3RzdHJhcCB2My4yLjBcIildKV0pLFxuICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgIF9jKFwibGlcIiwgW192bS5fdihcImpRdWVyeSB2MS4xMS4wXCIpXSksXG4gICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgX2MoXCJsaVwiLCBbX3ZtLl92KFwiRm9udCBBd2Vzb21lIHY0LjEuMFwiKV0pLFxuICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgIF9jKFwibGlcIiwgW192bS5fdihcIldvcmtpbmcgUEhQIGNvbnRhY3QgZm9ybSB3aXRoIHZhbGlkYXRpb25cIildKSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcImxpXCIsIFtfdm0uX3YoXCJVbnN0eWxlZCBwYWdlIGVsZW1lbnRzIGZvciBlYXN5IGN1c3RvbWl6YXRpb25cIildKSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcImxpXCIsIFtfdm0uX3YoXCIxNyBIVE1MIHBhZ2VzXCIpXSlcbiAgICAgICAgICBdKSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF9jKFwicFwiLCBbXG4gICAgICAgICAgICBfdm0uX3YoXG4gICAgICAgICAgICAgIFwiTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2ljaW5nIGVsaXQuIENvcnBvcmlzLCBvbW5pcyBkb2xvcmVtcXVlIG5vbiBjdW0gaWQgcmVwcmVoZW5kZXJpdCwgcXVpc3F1YW0gdG90YW0gYXNwZXJuYXR1ciB0ZW1wb3JhIG1pbmltYSB1bmRlIGFsaXF1aWQgZWEgY3VscGEgc3VudC4gUmVpY2llbmRpcyBxdWlhIGRvbG9ydW0gZHVjaW11cyB1bmRlLlwiXG4gICAgICAgICAgICApXG4gICAgICAgICAgXSlcbiAgICAgICAgXSksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTZcIiB9LCBbXG4gICAgICAgICAgX2MoXCJpbWdcIiwge1xuICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiaW1nLXJlc3BvbnNpdmVcIixcbiAgICAgICAgICAgIGF0dHJzOiB7IHNyYzogXCJodHRwOi8vcGxhY2Vob2xkLml0LzcwMHg0NTBcIiwgYWx0OiBcIlwiIH1cbiAgICAgICAgICB9KVxuICAgICAgICBdKVxuICAgICAgXSksXG4gICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgX2MoXCJoclwiKSxcbiAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcIndlbGxcIiB9LCBbXG4gICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwicm93XCIgfSwgW1xuICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLW1kLThcIiB9LCBbXG4gICAgICAgICAgICBfYyhcInBcIiwgW1xuICAgICAgICAgICAgICBfdm0uX3YoXG4gICAgICAgICAgICAgICAgXCJMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzaWNpbmcgZWxpdC4gTW9sZXN0aWFzLCBleHBlZGl0YSwgc2FlcGUsIHZlcm8gcmVydW0gZGVsZW5pdGkgYmVhdGFlIHZlbmlhbSBoYXJ1bSBuZXF1ZSBuZW1vIHByYWVzZW50aXVtIGN1bSBhbGlhcyBhc3BlcmlvcmVzIGNvbW1vZGkuXCJcbiAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgXSlcbiAgICAgICAgICBdKSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTRcIiB9LCBbXG4gICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgXCJhXCIsXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWxnIGJ0bi1kZWZhdWx0IGJ0bi1ibG9ja1wiLFxuICAgICAgICAgICAgICAgIGF0dHJzOiB7IGhyZWY6IFwiI1wiIH1cbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgW192bS5fdihcIkNhbGwgdG8gQWN0aW9uXCIpXVxuICAgICAgICAgICAgKVxuICAgICAgICAgIF0pXG4gICAgICAgIF0pXG4gICAgICBdKVxuICAgIF0pXG4gIH1cbl1cbnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxubW9kdWxlLmV4cG9ydHMgPSB7IHJlbmRlcjogcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnM6IHN0YXRpY1JlbmRlckZucyB9XG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpICAgICAgLnJlcmVuZGVyKFwiZGF0YS12LTE2ZGU0ZDM2XCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0xNmRlNGQzNlwiLFwiaGFzU2NvcGVkXCI6ZmFsc2UsXCJidWJsZVwiOntcInRyYW5zZm9ybXNcIjp7fX19IS4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCZidXN0Q2FjaGUhLi9yZXNvdXJjZXMvanMvdnVlL3BhZ2VzL2hvbWUudnVlXG4vLyBtb2R1bGUgaWQgPSA0OVxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJ2YXIgZGlzcG9zZWQgPSBmYWxzZVxudmFyIG5vcm1hbGl6ZUNvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpXG4vKiBzY3JpcHQgKi9cbnZhciBfX3Z1ZV9zY3JpcHRfXyA9IG51bGxcbi8qIHRlbXBsYXRlICovXG52YXIgX192dWVfdGVtcGxhdGVfXyA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTBkMWU3Yjc0XFxcIixcXFwiaGFzU2NvcGVkXFxcIjpmYWxzZSxcXFwiYnVibGVcXFwiOntcXFwidHJhbnNmb3Jtc1xcXCI6e319fSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wJmJ1c3RDYWNoZSEuL2Fib3V0LnZ1ZVwiKVxuLyogdGVtcGxhdGUgZnVuY3Rpb25hbCAqL1xuICB2YXIgX192dWVfdGVtcGxhdGVfZnVuY3Rpb25hbF9fID0gZmFsc2Vcbi8qIHN0eWxlcyAqL1xudmFyIF9fdnVlX3N0eWxlc19fID0gbnVsbFxuLyogc2NvcGVJZCAqL1xudmFyIF9fdnVlX3Njb3BlSWRfXyA9IG51bGxcbi8qIG1vZHVsZUlkZW50aWZpZXIgKHNlcnZlciBvbmx5KSAqL1xudmFyIF9fdnVlX21vZHVsZV9pZGVudGlmaWVyX18gPSBudWxsXG52YXIgQ29tcG9uZW50ID0gbm9ybWFsaXplQ29tcG9uZW50KFxuICBfX3Z1ZV9zY3JpcHRfXyxcbiAgX192dWVfdGVtcGxhdGVfXyxcbiAgX192dWVfdGVtcGxhdGVfZnVuY3Rpb25hbF9fLFxuICBfX3Z1ZV9zdHlsZXNfXyxcbiAgX192dWVfc2NvcGVJZF9fLFxuICBfX3Z1ZV9tb2R1bGVfaWRlbnRpZmllcl9fXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcInJlc291cmNlcy9qcy92dWUvcGFnZXMvYWJvdXQudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHsgIHJldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleS5zdWJzdHIoMCwgMikgIT09IFwiX19cIn0pKSB7ICBjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi0wZDFlN2I3NFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTBkMWU3Yjc0XCIsIENvbXBvbmVudC5vcHRpb25zKVxuJyArICcgIH1cbiAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgZGlzcG9zZWQgPSB0cnVlXG4gIH0pXG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9qcy92dWUvcGFnZXMvYWJvdXQudnVlXG4vLyBtb2R1bGUgaWQgPSA1MFxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJ2YXIgcmVuZGVyID0gZnVuY3Rpb24oKSB7XG4gIHZhciBfdm0gPSB0aGlzXG4gIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgcmV0dXJuIF92bS5fbSgwKVxufVxudmFyIHN0YXRpY1JlbmRlckZucyA9IFtcbiAgZnVuY3Rpb24oKSB7XG4gICAgdmFyIF92bSA9IHRoaXNcbiAgICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgICByZXR1cm4gX2MoXCJkaXZcIiwgeyBhdHRyczogeyBpZDogXCJwYWdlcy1hYm91dC1jb21wb25lbnRcIiB9IH0sIFtcbiAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29udGFpbmVyXCIgfSwgW1xuICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcInJvd1wiIH0sIFtcbiAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbC1sZy0xMlwiIH0sIFtcbiAgICAgICAgICAgIF9jKFwiaDFcIiwgeyBzdGF0aWNDbGFzczogXCJwYWdlLWhlYWRlclwiIH0sIFtcbiAgICAgICAgICAgICAgX3ZtLl92KFwiQWJvdXRcXG4gICAgICAgICAgICAgICAgICAgIFwiKSxcbiAgICAgICAgICAgICAgX2MoXCJzbWFsbFwiLCBbX3ZtLl92KFwiU3ViaGVhZGluZ1wiKV0pXG4gICAgICAgICAgICBdKSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcIm9sXCIsIHsgc3RhdGljQ2xhc3M6IFwiYnJlYWRjcnVtYlwiIH0sIFtcbiAgICAgICAgICAgICAgX2MoXCJsaVwiLCBbXG4gICAgICAgICAgICAgICAgX2MoXCJhXCIsIHsgYXR0cnM6IHsgaHJlZjogXCJpbmRleC5odG1sXCIgfSB9LCBbX3ZtLl92KFwiSG9tZVwiKV0pXG4gICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICBfYyhcImxpXCIsIHsgc3RhdGljQ2xhc3M6IFwiYWN0aXZlXCIgfSwgW192bS5fdihcIkFib3V0XCIpXSlcbiAgICAgICAgICAgIF0pXG4gICAgICAgICAgXSlcbiAgICAgICAgXSksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwicm93XCIgfSwgW1xuICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTZcIiB9LCBbXG4gICAgICAgICAgICBfYyhcImltZ1wiLCB7XG4gICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImltZy1yZXNwb25zaXZlXCIsXG4gICAgICAgICAgICAgIGF0dHJzOiB7IHNyYzogXCJodHRwOi8vcGxhY2Vob2xkLml0Lzc1MHg0NTBcIiwgYWx0OiBcIlwiIH1cbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgXSksXG4gICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbC1tZC02XCIgfSwgW1xuICAgICAgICAgICAgX2MoXCJoMlwiLCBbX3ZtLl92KFwiQWJvdXQgTW9kZXJuIEJ1c2luZXNzXCIpXSksXG4gICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgX2MoXCJwXCIsIFtcbiAgICAgICAgICAgICAgX3ZtLl92KFxuICAgICAgICAgICAgICAgIFwiTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2ljaW5nIGVsaXQuIFNlZCB2b2x1cHRhdGUgbmloaWwgZXVtIGNvbnNlY3RldHVyIHNpbWlsaXF1ZT8gQ29uc2VjdGV0dXIsIHF1b2QsIGluY2lkdW50LCBoYXJ1bSBuaXNpIGRvbG9yZXMgZGVsZWN0dXMgcmVwcmVoZW5kZXJpdCB2b2x1cHRhdGVtIHBlcmZlcmVuZGlzIGRpY3RhIGRvbG9yZW0gbm9uIGJsYW5kaXRpaXMgZXggZnVnaWF0LlwiXG4gICAgICAgICAgICAgIClcbiAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgIF9jKFwicFwiLCBbXG4gICAgICAgICAgICAgIF92bS5fdihcbiAgICAgICAgICAgICAgICBcIkxvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNpY2luZyBlbGl0LiBTYWVwZSwgbWFnbmksIGFwZXJpYW0gdml0YWUgaWxsdW0gdm9sdXB0YXR1bSBhdXQgc2VxdWkgaW1wZWRpdCBub24gdmVsaXQgYWIgZWEgcGFyaWF0dXIgc2ludCBxdWlkZW0gY29ycG9yaXMgZXZlbmlldC4gT2RpdCwgdGVtcG9yaWJ1cyByZXByZWhlbmRlcml0IGRvbG9ydW0hXCJcbiAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgXSksXG4gICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgX2MoXCJwXCIsIFtcbiAgICAgICAgICAgICAgX3ZtLl92KFxuICAgICAgICAgICAgICAgIFwiTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2ljaW5nIGVsaXQuIEV0LCBjb25zZXF1dW50dXIsIG1vZGkgbW9sbGl0aWEgY29ycG9yaXMgaXBzYSB2b2x1cHRhdGUgY29ycnVwdGkgZXVtIHJhdGlvbmUgZXggZWEgcHJhZXNlbnRpdW0gcXVpYnVzZGFtPyBBdXQsIGluIGV1bSBmYWNlcmUgY29ycnVwdGkgbmVjZXNzaXRhdGlidXMgcGVyc3BpY2lhdGlzIHF1aXM/XCJcbiAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgXSlcbiAgICAgICAgICBdKVxuICAgICAgICBdKSxcbiAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJyb3dcIiB9LCBbXG4gICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtbGctMTJcIiB9LCBbXG4gICAgICAgICAgICBfYyhcImgyXCIsIHsgc3RhdGljQ2xhc3M6IFwicGFnZS1oZWFkZXJcIiB9LCBbX3ZtLl92KFwiT3VyIFRlYW1cIildKVxuICAgICAgICAgIF0pLFxuICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtbWQtNCB0ZXh0LWNlbnRlclwiIH0sIFtcbiAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwidGh1bWJuYWlsXCIgfSwgW1xuICAgICAgICAgICAgICBfYyhcImltZ1wiLCB7XG4gICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiaW1nLXJlc3BvbnNpdmVcIixcbiAgICAgICAgICAgICAgICBhdHRyczogeyBzcmM6IFwiaHR0cDovL3BsYWNlaG9sZC5pdC83NTB4NDUwXCIsIGFsdDogXCJcIiB9XG4gICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNhcHRpb25cIiB9LCBbXG4gICAgICAgICAgICAgICAgX2MoXCJoM1wiLCBbXG4gICAgICAgICAgICAgICAgICBfdm0uX3YoXCJKb2huIFNtaXRoXCIpLFxuICAgICAgICAgICAgICAgICAgX2MoXCJiclwiKSxcbiAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICBfYyhcInNtYWxsXCIsIFtfdm0uX3YoXCJKb2IgVGl0bGVcIildKVxuICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgX2MoXCJwXCIsIFtcbiAgICAgICAgICAgICAgICAgIF92bS5fdihcbiAgICAgICAgICAgICAgICAgICAgXCJMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzaWNpbmcgZWxpdC4gSXN0ZSBzYWVwZSBldCBxdWlzcXVhbSBuZXNjaXVudCBtYXhpbWUuXCJcbiAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgIF9jKFwidWxcIiwgeyBzdGF0aWNDbGFzczogXCJsaXN0LWlubGluZVwiIH0sIFtcbiAgICAgICAgICAgICAgICAgIF9jKFwibGlcIiwgW1xuICAgICAgICAgICAgICAgICAgICBfYyhcImFcIiwgeyBhdHRyczogeyBocmVmOiBcIiNcIiB9IH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICBfYyhcImlcIiwgeyBzdGF0aWNDbGFzczogXCJmYSBmYS0yeCBmYS1mYWNlYm9vay1zcXVhcmVcIiB9KVxuICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgX2MoXCJsaVwiLCBbXG4gICAgICAgICAgICAgICAgICAgIF9jKFwiYVwiLCB7IGF0dHJzOiB7IGhyZWY6IFwiI1wiIH0gfSwgW1xuICAgICAgICAgICAgICAgICAgICAgIF9jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcImZhIGZhLTJ4IGZhLWxpbmtlZGluLXNxdWFyZVwiIH0pXG4gICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICBfYyhcImxpXCIsIFtcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJhXCIsIHsgYXR0cnM6IHsgaHJlZjogXCIjXCIgfSB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgX2MoXCJpXCIsIHsgc3RhdGljQ2xhc3M6IFwiZmEgZmEtMnggZmEtdHdpdHRlci1zcXVhcmVcIiB9KVxuICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgXSlcbiAgICAgICAgICBdKSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTQgdGV4dC1jZW50ZXJcIiB9LCBbXG4gICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcInRodW1ibmFpbFwiIH0sIFtcbiAgICAgICAgICAgICAgX2MoXCJpbWdcIiwge1xuICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImltZy1yZXNwb25zaXZlXCIsXG4gICAgICAgICAgICAgICAgYXR0cnM6IHsgc3JjOiBcImh0dHA6Ly9wbGFjZWhvbGQuaXQvNzUweDQ1MFwiLCBhbHQ6IFwiXCIgfVxuICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjYXB0aW9uXCIgfSwgW1xuICAgICAgICAgICAgICAgIF9jKFwiaDNcIiwgW1xuICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiSm9obiBTbWl0aFwiKSxcbiAgICAgICAgICAgICAgICAgIF9jKFwiYnJcIiksXG4gICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgX2MoXCJzbWFsbFwiLCBbX3ZtLl92KFwiSm9iIFRpdGxlXCIpXSlcbiAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgIF9jKFwicFwiLCBbXG4gICAgICAgICAgICAgICAgICBfdm0uX3YoXG4gICAgICAgICAgICAgICAgICAgIFwiTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2ljaW5nIGVsaXQuIElzdGUgc2FlcGUgZXQgcXVpc3F1YW0gbmVzY2l1bnQgbWF4aW1lLlwiXG4gICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICBfYyhcInVsXCIsIHsgc3RhdGljQ2xhc3M6IFwibGlzdC1pbmxpbmVcIiB9LCBbXG4gICAgICAgICAgICAgICAgICBfYyhcImxpXCIsIFtcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJhXCIsIHsgYXR0cnM6IHsgaHJlZjogXCIjXCIgfSB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgX2MoXCJpXCIsIHsgc3RhdGljQ2xhc3M6IFwiZmEgZmEtMnggZmEtZmFjZWJvb2stc3F1YXJlXCIgfSlcbiAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgIF9jKFwibGlcIiwgW1xuICAgICAgICAgICAgICAgICAgICBfYyhcImFcIiwgeyBhdHRyczogeyBocmVmOiBcIiNcIiB9IH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICBfYyhcImlcIiwgeyBzdGF0aWNDbGFzczogXCJmYSBmYS0yeCBmYS1saW5rZWRpbi1zcXVhcmVcIiB9KVxuICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgX2MoXCJsaVwiLCBbXG4gICAgICAgICAgICAgICAgICAgIF9jKFwiYVwiLCB7IGF0dHJzOiB7IGhyZWY6IFwiI1wiIH0gfSwgW1xuICAgICAgICAgICAgICAgICAgICAgIF9jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcImZhIGZhLTJ4IGZhLXR3aXR0ZXItc3F1YXJlXCIgfSlcbiAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgIF0pXG4gICAgICAgICAgXSksXG4gICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbC1tZC00IHRleHQtY2VudGVyXCIgfSwgW1xuICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJ0aHVtYm5haWxcIiB9LCBbXG4gICAgICAgICAgICAgIF9jKFwiaW1nXCIsIHtcbiAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJpbWctcmVzcG9uc2l2ZVwiLFxuICAgICAgICAgICAgICAgIGF0dHJzOiB7IHNyYzogXCJodHRwOi8vcGxhY2Vob2xkLml0Lzc1MHg0NTBcIiwgYWx0OiBcIlwiIH1cbiAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY2FwdGlvblwiIH0sIFtcbiAgICAgICAgICAgICAgICBfYyhcImgzXCIsIFtcbiAgICAgICAgICAgICAgICAgIF92bS5fdihcIkpvaG4gU21pdGhcIiksXG4gICAgICAgICAgICAgICAgICBfYyhcImJyXCIpLFxuICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgIF9jKFwic21hbGxcIiwgW192bS5fdihcIkpvYiBUaXRsZVwiKV0pXG4gICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICBfYyhcInBcIiwgW1xuICAgICAgICAgICAgICAgICAgX3ZtLl92KFxuICAgICAgICAgICAgICAgICAgICBcIkxvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNpY2luZyBlbGl0LiBJc3RlIHNhZXBlIGV0IHF1aXNxdWFtIG5lc2NpdW50IG1heGltZS5cIlxuICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgX2MoXCJ1bFwiLCB7IHN0YXRpY0NsYXNzOiBcImxpc3QtaW5saW5lXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgX2MoXCJsaVwiLCBbXG4gICAgICAgICAgICAgICAgICAgIF9jKFwiYVwiLCB7IGF0dHJzOiB7IGhyZWY6IFwiI1wiIH0gfSwgW1xuICAgICAgICAgICAgICAgICAgICAgIF9jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcImZhIGZhLTJ4IGZhLWZhY2Vib29rLXNxdWFyZVwiIH0pXG4gICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICBfYyhcImxpXCIsIFtcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJhXCIsIHsgYXR0cnM6IHsgaHJlZjogXCIjXCIgfSB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgX2MoXCJpXCIsIHsgc3RhdGljQ2xhc3M6IFwiZmEgZmEtMnggZmEtbGlua2VkaW4tc3F1YXJlXCIgfSlcbiAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgIF9jKFwibGlcIiwgW1xuICAgICAgICAgICAgICAgICAgICBfYyhcImFcIiwgeyBhdHRyczogeyBocmVmOiBcIiNcIiB9IH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICBfYyhcImlcIiwgeyBzdGF0aWNDbGFzczogXCJmYSBmYS0yeCBmYS10d2l0dGVyLXNxdWFyZVwiIH0pXG4gICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICBdKVxuICAgICAgICAgIF0pLFxuICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtbWQtNCB0ZXh0LWNlbnRlclwiIH0sIFtcbiAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwidGh1bWJuYWlsXCIgfSwgW1xuICAgICAgICAgICAgICBfYyhcImltZ1wiLCB7XG4gICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiaW1nLXJlc3BvbnNpdmVcIixcbiAgICAgICAgICAgICAgICBhdHRyczogeyBzcmM6IFwiaHR0cDovL3BsYWNlaG9sZC5pdC83NTB4NDUwXCIsIGFsdDogXCJcIiB9XG4gICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNhcHRpb25cIiB9LCBbXG4gICAgICAgICAgICAgICAgX2MoXCJoM1wiLCBbXG4gICAgICAgICAgICAgICAgICBfdm0uX3YoXCJKb2huIFNtaXRoXCIpLFxuICAgICAgICAgICAgICAgICAgX2MoXCJiclwiKSxcbiAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICBfYyhcInNtYWxsXCIsIFtfdm0uX3YoXCJKb2IgVGl0bGVcIildKVxuICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgX2MoXCJwXCIsIFtcbiAgICAgICAgICAgICAgICAgIF92bS5fdihcbiAgICAgICAgICAgICAgICAgICAgXCJMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzaWNpbmcgZWxpdC4gSXN0ZSBzYWVwZSBldCBxdWlzcXVhbSBuZXNjaXVudCBtYXhpbWUuXCJcbiAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgIF9jKFwidWxcIiwgeyBzdGF0aWNDbGFzczogXCJsaXN0LWlubGluZVwiIH0sIFtcbiAgICAgICAgICAgICAgICAgIF9jKFwibGlcIiwgW1xuICAgICAgICAgICAgICAgICAgICBfYyhcImFcIiwgeyBhdHRyczogeyBocmVmOiBcIiNcIiB9IH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICBfYyhcImlcIiwgeyBzdGF0aWNDbGFzczogXCJmYSBmYS0yeCBmYS1mYWNlYm9vay1zcXVhcmVcIiB9KVxuICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgX2MoXCJsaVwiLCBbXG4gICAgICAgICAgICAgICAgICAgIF9jKFwiYVwiLCB7IGF0dHJzOiB7IGhyZWY6IFwiI1wiIH0gfSwgW1xuICAgICAgICAgICAgICAgICAgICAgIF9jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcImZhIGZhLTJ4IGZhLWxpbmtlZGluLXNxdWFyZVwiIH0pXG4gICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICBfYyhcImxpXCIsIFtcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJhXCIsIHsgYXR0cnM6IHsgaHJlZjogXCIjXCIgfSB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgX2MoXCJpXCIsIHsgc3RhdGljQ2xhc3M6IFwiZmEgZmEtMnggZmEtdHdpdHRlci1zcXVhcmVcIiB9KVxuICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgXSlcbiAgICAgICAgICBdKSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTQgdGV4dC1jZW50ZXJcIiB9LCBbXG4gICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcInRodW1ibmFpbFwiIH0sIFtcbiAgICAgICAgICAgICAgX2MoXCJpbWdcIiwge1xuICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImltZy1yZXNwb25zaXZlXCIsXG4gICAgICAgICAgICAgICAgYXR0cnM6IHsgc3JjOiBcImh0dHA6Ly9wbGFjZWhvbGQuaXQvNzUweDQ1MFwiLCBhbHQ6IFwiXCIgfVxuICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjYXB0aW9uXCIgfSwgW1xuICAgICAgICAgICAgICAgIF9jKFwiaDNcIiwgW1xuICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiSm9obiBTbWl0aFwiKSxcbiAgICAgICAgICAgICAgICAgIF9jKFwiYnJcIiksXG4gICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgX2MoXCJzbWFsbFwiLCBbX3ZtLl92KFwiSm9iIFRpdGxlXCIpXSlcbiAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgIF9jKFwicFwiLCBbXG4gICAgICAgICAgICAgICAgICBfdm0uX3YoXG4gICAgICAgICAgICAgICAgICAgIFwiTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2ljaW5nIGVsaXQuIElzdGUgc2FlcGUgZXQgcXVpc3F1YW0gbmVzY2l1bnQgbWF4aW1lLlwiXG4gICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICBfYyhcInVsXCIsIHsgc3RhdGljQ2xhc3M6IFwibGlzdC1pbmxpbmVcIiB9LCBbXG4gICAgICAgICAgICAgICAgICBfYyhcImxpXCIsIFtcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJhXCIsIHsgYXR0cnM6IHsgaHJlZjogXCIjXCIgfSB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgX2MoXCJpXCIsIHsgc3RhdGljQ2xhc3M6IFwiZmEgZmEtMnggZmEtZmFjZWJvb2stc3F1YXJlXCIgfSlcbiAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgIF9jKFwibGlcIiwgW1xuICAgICAgICAgICAgICAgICAgICBfYyhcImFcIiwgeyBhdHRyczogeyBocmVmOiBcIiNcIiB9IH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICBfYyhcImlcIiwgeyBzdGF0aWNDbGFzczogXCJmYSBmYS0yeCBmYS1saW5rZWRpbi1zcXVhcmVcIiB9KVxuICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgX2MoXCJsaVwiLCBbXG4gICAgICAgICAgICAgICAgICAgIF9jKFwiYVwiLCB7IGF0dHJzOiB7IGhyZWY6IFwiI1wiIH0gfSwgW1xuICAgICAgICAgICAgICAgICAgICAgIF9jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcImZhIGZhLTJ4IGZhLXR3aXR0ZXItc3F1YXJlXCIgfSlcbiAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgIF0pXG4gICAgICAgICAgXSksXG4gICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbC1tZC00IHRleHQtY2VudGVyXCIgfSwgW1xuICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJ0aHVtYm5haWxcIiB9LCBbXG4gICAgICAgICAgICAgIF9jKFwiaW1nXCIsIHtcbiAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJpbWctcmVzcG9uc2l2ZVwiLFxuICAgICAgICAgICAgICAgIGF0dHJzOiB7IHNyYzogXCJodHRwOi8vcGxhY2Vob2xkLml0Lzc1MHg0NTBcIiwgYWx0OiBcIlwiIH1cbiAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY2FwdGlvblwiIH0sIFtcbiAgICAgICAgICAgICAgICBfYyhcImgzXCIsIFtcbiAgICAgICAgICAgICAgICAgIF92bS5fdihcIkpvaG4gU21pdGhcIiksXG4gICAgICAgICAgICAgICAgICBfYyhcImJyXCIpLFxuICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgIF9jKFwic21hbGxcIiwgW192bS5fdihcIkpvYiBUaXRsZVwiKV0pXG4gICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICBfYyhcInBcIiwgW1xuICAgICAgICAgICAgICAgICAgX3ZtLl92KFxuICAgICAgICAgICAgICAgICAgICBcIkxvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNpY2luZyBlbGl0LiBJc3RlIHNhZXBlIGV0IHF1aXNxdWFtIG5lc2NpdW50IG1heGltZS5cIlxuICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgX2MoXCJ1bFwiLCB7IHN0YXRpY0NsYXNzOiBcImxpc3QtaW5saW5lXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgX2MoXCJsaVwiLCBbXG4gICAgICAgICAgICAgICAgICAgIF9jKFwiYVwiLCB7IGF0dHJzOiB7IGhyZWY6IFwiI1wiIH0gfSwgW1xuICAgICAgICAgICAgICAgICAgICAgIF9jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcImZhIGZhLTJ4IGZhLWZhY2Vib29rLXNxdWFyZVwiIH0pXG4gICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICBfYyhcImxpXCIsIFtcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJhXCIsIHsgYXR0cnM6IHsgaHJlZjogXCIjXCIgfSB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgX2MoXCJpXCIsIHsgc3RhdGljQ2xhc3M6IFwiZmEgZmEtMnggZmEtbGlua2VkaW4tc3F1YXJlXCIgfSlcbiAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgIF9jKFwibGlcIiwgW1xuICAgICAgICAgICAgICAgICAgICBfYyhcImFcIiwgeyBhdHRyczogeyBocmVmOiBcIiNcIiB9IH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICBfYyhcImlcIiwgeyBzdGF0aWNDbGFzczogXCJmYSBmYS0yeCBmYS10d2l0dGVyLXNxdWFyZVwiIH0pXG4gICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICBdKVxuICAgICAgICAgIF0pXG4gICAgICAgIF0pLFxuICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcInJvd1wiIH0sIFtcbiAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbC1sZy0xMlwiIH0sIFtcbiAgICAgICAgICAgIF9jKFwiaDJcIiwgeyBzdGF0aWNDbGFzczogXCJwYWdlLWhlYWRlclwiIH0sIFtfdm0uX3YoXCJPdXIgQ3VzdG9tZXJzXCIpXSlcbiAgICAgICAgICBdKSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29sLXNtLTQgY29sLXhzLTZcIiB9LCBbXG4gICAgICAgICAgICBfYyhcImltZ1wiLCB7XG4gICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImltZy1yZXNwb25zaXZlIGN1c3RvbWVyLWltZ1wiLFxuICAgICAgICAgICAgICBhdHRyczogeyBzcmM6IFwiaHR0cDovL3BsYWNlaG9sZC5pdC81MDB4MzAwXCIsIGFsdDogXCJcIiB9XG4gICAgICAgICAgICB9KVxuICAgICAgICAgIF0pLFxuICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb2wtc20tNCBjb2wteHMtNlwiIH0sIFtcbiAgICAgICAgICAgIF9jKFwiaW1nXCIsIHtcbiAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiaW1nLXJlc3BvbnNpdmUgY3VzdG9tZXItaW1nXCIsXG4gICAgICAgICAgICAgIGF0dHJzOiB7IHNyYzogXCJodHRwOi8vcGxhY2Vob2xkLml0LzUwMHgzMDBcIiwgYWx0OiBcIlwiIH1cbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgXSksXG4gICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbC1zbS00IGNvbC14cy02XCIgfSwgW1xuICAgICAgICAgICAgX2MoXCJpbWdcIiwge1xuICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJpbWctcmVzcG9uc2l2ZSBjdXN0b21lci1pbWdcIixcbiAgICAgICAgICAgICAgYXR0cnM6IHsgc3JjOiBcImh0dHA6Ly9wbGFjZWhvbGQuaXQvNTAweDMwMFwiLCBhbHQ6IFwiXCIgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICBdKSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTIgY29sLXNtLTQgY29sLXhzLTZcIiB9LCBbXG4gICAgICAgICAgICBfYyhcImltZ1wiLCB7XG4gICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImltZy1yZXNwb25zaXZlIGN1c3RvbWVyLWltZ1wiLFxuICAgICAgICAgICAgICBhdHRyczogeyBzcmM6IFwiaHR0cDovL3BsYWNlaG9sZC5pdC81MDB4MzAwXCIsIGFsdDogXCJcIiB9XG4gICAgICAgICAgICB9KVxuICAgICAgICAgIF0pLFxuICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBjb2wtc20tNCBjb2wteHMtNlwiIH0sIFtcbiAgICAgICAgICAgIF9jKFwiaW1nXCIsIHtcbiAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiaW1nLXJlc3BvbnNpdmUgY3VzdG9tZXItaW1nXCIsXG4gICAgICAgICAgICAgIGF0dHJzOiB7IHNyYzogXCJodHRwOi8vcGxhY2Vob2xkLml0LzUwMHgzMDBcIiwgYWx0OiBcIlwiIH1cbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgXSksXG4gICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yIGNvbC1zbS00IGNvbC14cy02XCIgfSwgW1xuICAgICAgICAgICAgX2MoXCJpbWdcIiwge1xuICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJpbWctcmVzcG9uc2l2ZSBjdXN0b21lci1pbWdcIixcbiAgICAgICAgICAgICAgYXR0cnM6IHsgc3JjOiBcImh0dHA6Ly9wbGFjZWhvbGQuaXQvNTAweDMwMFwiLCBhbHQ6IFwiXCIgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICBdKVxuICAgICAgICBdKVxuICAgICAgXSlcbiAgICBdKVxuICB9XG5dXG5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbm1vZHVsZS5leHBvcnRzID0geyByZW5kZXI6IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnMgfVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKSAgICAgIC5yZXJlbmRlcihcImRhdGEtdi0wZDFlN2I3NFwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtMGQxZTdiNzRcIixcImhhc1Njb3BlZFwiOmZhbHNlLFwiYnVibGVcIjp7XCJ0cmFuc2Zvcm1zXCI6e319fSEuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAmYnVzdENhY2hlIS4vcmVzb3VyY2VzL2pzL3Z1ZS9wYWdlcy9hYm91dC52dWVcbi8vIG1vZHVsZSBpZCA9IDUxXG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsInZhciBkaXNwb3NlZCA9IGZhbHNlXG52YXIgbm9ybWFsaXplQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIilcbi8qIHNjcmlwdCAqL1xudmFyIF9fdnVlX3NjcmlwdF9fID0gbnVsbFxuLyogdGVtcGxhdGUgKi9cbnZhciBfX3Z1ZV90ZW1wbGF0ZV9fID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNjJjNjFkMTlcXFwiLFxcXCJoYXNTY29wZWRcXFwiOmZhbHNlLFxcXCJidWJsZVxcXCI6e1xcXCJ0cmFuc2Zvcm1zXFxcIjp7fX19IS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAmYnVzdENhY2hlIS4vY29udGFjdC52dWVcIilcbi8qIHRlbXBsYXRlIGZ1bmN0aW9uYWwgKi9cbiAgdmFyIF9fdnVlX3RlbXBsYXRlX2Z1bmN0aW9uYWxfXyA9IGZhbHNlXG4vKiBzdHlsZXMgKi9cbnZhciBfX3Z1ZV9zdHlsZXNfXyA9IG51bGxcbi8qIHNjb3BlSWQgKi9cbnZhciBfX3Z1ZV9zY29wZUlkX18gPSBudWxsXG4vKiBtb2R1bGVJZGVudGlmaWVyIChzZXJ2ZXIgb25seSkgKi9cbnZhciBfX3Z1ZV9tb2R1bGVfaWRlbnRpZmllcl9fID0gbnVsbFxudmFyIENvbXBvbmVudCA9IG5vcm1hbGl6ZUNvbXBvbmVudChcbiAgX192dWVfc2NyaXB0X18sXG4gIF9fdnVlX3RlbXBsYXRlX18sXG4gIF9fdnVlX3RlbXBsYXRlX2Z1bmN0aW9uYWxfXyxcbiAgX192dWVfc3R5bGVzX18sXG4gIF9fdnVlX3Njb3BlSWRfXyxcbiAgX192dWVfbW9kdWxlX2lkZW50aWZpZXJfX1xuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJyZXNvdXJjZXMvanMvdnVlL3BhZ2VzL2NvbnRhY3QudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHsgIHJldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleS5zdWJzdHIoMCwgMikgIT09IFwiX19cIn0pKSB7ICBjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi02MmM2MWQxOVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTYyYzYxZDE5XCIsIENvbXBvbmVudC5vcHRpb25zKVxuJyArICcgIH1cbiAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgZGlzcG9zZWQgPSB0cnVlXG4gIH0pXG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9qcy92dWUvcGFnZXMvY29udGFjdC52dWVcbi8vIG1vZHVsZSBpZCA9IDUyXG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsInZhciByZW5kZXIgPSBmdW5jdGlvbigpIHtcbiAgdmFyIF92bSA9IHRoaXNcbiAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICByZXR1cm4gX3ZtLl9tKDApXG59XG52YXIgc3RhdGljUmVuZGVyRm5zID0gW1xuICBmdW5jdGlvbigpIHtcbiAgICB2YXIgX3ZtID0gdGhpc1xuICAgIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICAgIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICAgIHJldHVybiBfYyhcImRpdlwiLCB7IGF0dHJzOiB7IGlkOiBcInBhZ2VzLWNvbnRhY3QtY29tcG9uZW50XCIgfSB9LCBbXG4gICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbnRhaW5lclwiIH0sIFtcbiAgICAgICAgX2MoXCJoMVwiLCB7IHN0YXRpY0NsYXNzOiBcIm10LTQgbWItM1wiIH0sIFtcbiAgICAgICAgICBfdm0uX3YoXCJDb250YWN0XFxuICAgICAgICBcIiksXG4gICAgICAgICAgX2MoXCJzbWFsbFwiLCBbX3ZtLl92KFwiU3ViaGVhZGluZ1wiKV0pXG4gICAgICAgIF0pLFxuICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICBfYyhcIm9sXCIsIHsgc3RhdGljQ2xhc3M6IFwiYnJlYWRjcnVtYlwiIH0sIFtcbiAgICAgICAgICBfYyhcImxpXCIsIHsgc3RhdGljQ2xhc3M6IFwiYnJlYWRjcnVtYi1pdGVtXCIgfSwgW1xuICAgICAgICAgICAgX2MoXCJhXCIsIHsgYXR0cnM6IHsgaHJlZjogXCJpbmRleC5odG1sXCIgfSB9LCBbX3ZtLl92KFwiSG9tZVwiKV0pXG4gICAgICAgICAgXSksXG4gICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICBfYyhcImxpXCIsIHsgc3RhdGljQ2xhc3M6IFwiYnJlYWRjcnVtYi1pdGVtIGFjdGl2ZVwiIH0sIFtcbiAgICAgICAgICAgIF92bS5fdihcIkNvbnRhY3RcIilcbiAgICAgICAgICBdKVxuICAgICAgICBdKSxcbiAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJyb3dcIiB9LCBbXG4gICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtbGctOCBtYi00XCIgfSwgW1xuICAgICAgICAgICAgX2MoXCJpZnJhbWVcIiwge1xuICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgIHdpZHRoOiBcIjEwMCVcIixcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IFwiNDAwcHhcIixcbiAgICAgICAgICAgICAgICBmcmFtZWJvcmRlcjogXCIwXCIsXG4gICAgICAgICAgICAgICAgc2Nyb2xsaW5nOiBcIm5vXCIsXG4gICAgICAgICAgICAgICAgbWFyZ2luaGVpZ2h0OiBcIjBcIixcbiAgICAgICAgICAgICAgICBtYXJnaW53aWR0aDogXCIwXCIsXG4gICAgICAgICAgICAgICAgc3JjOlxuICAgICAgICAgICAgICAgICAgXCJodHRwOi8vbWFwcy5nb29nbGUuY29tL21hcHM/aGw9ZW4maWU9VVRGOCZsbD0zNy4wNjI1LC05NS42NzcwNjgmc3BuPTU2LjUwNjE3NCw3OS4wMTM2NzImdD1tJno9NCZvdXRwdXQ9ZW1iZWRcIlxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuICAgICAgICAgIF0pLFxuICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtbGctNCBtYi00XCIgfSwgW1xuICAgICAgICAgICAgX2MoXCJoM1wiLCBbX3ZtLl92KFwiQ29udGFjdCBEZXRhaWxzXCIpXSksXG4gICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgX2MoXCJwXCIsIFtcbiAgICAgICAgICAgICAgX3ZtLl92KFwiXFxuICAgICAgICAgICAgMzQ4MSBNZWxyb3NlIFBsYWNlXFxuICAgICAgICAgICAgXCIpLFxuICAgICAgICAgICAgICBfYyhcImJyXCIpLFxuICAgICAgICAgICAgICBfdm0uX3YoXCJCZXZlcmx5IEhpbGxzLCBDQSA5MDIxMFxcbiAgICAgICAgICAgIFwiKSxcbiAgICAgICAgICAgICAgX2MoXCJiclwiKVxuICAgICAgICAgICAgXSksXG4gICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgX2MoXCJwXCIsIFtcbiAgICAgICAgICAgICAgX2MoXCJhYmJyXCIsIHsgYXR0cnM6IHsgdGl0bGU6IFwiUGhvbmVcIiB9IH0sIFtfdm0uX3YoXCJQXCIpXSksXG4gICAgICAgICAgICAgIF92bS5fdihcIjogKDEyMykgNDU2LTc4OTBcXG4gICAgICAgICAgXCIpXG4gICAgICAgICAgICBdKSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcInBcIiwgW1xuICAgICAgICAgICAgICBfYyhcImFiYnJcIiwgeyBhdHRyczogeyB0aXRsZTogXCJFbWFpbFwiIH0gfSwgW192bS5fdihcIkVcIildKSxcbiAgICAgICAgICAgICAgX3ZtLl92KFwiOlxcbiAgICAgICAgICAgIFwiKSxcbiAgICAgICAgICAgICAgX2MoXCJhXCIsIHsgYXR0cnM6IHsgaHJlZjogXCJtYWlsdG86bmFtZUBleGFtcGxlLmNvbVwiIH0gfSwgW1xuICAgICAgICAgICAgICAgIF92bS5fdihcIm5hbWVAZXhhbXBsZS5jb21cXG4gICAgICAgICAgICBcIilcbiAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgIF9jKFwicFwiLCBbXG4gICAgICAgICAgICAgIF9jKFwiYWJiclwiLCB7IGF0dHJzOiB7IHRpdGxlOiBcIkhvdXJzXCIgfSB9LCBbX3ZtLl92KFwiSFwiKV0pLFxuICAgICAgICAgICAgICBfdm0uX3YoXCI6IE1vbmRheSAtIEZyaWRheTogOTowMCBBTSB0byA1OjAwIFBNXFxuICAgICAgICAgIFwiKVxuICAgICAgICAgICAgXSlcbiAgICAgICAgICBdKVxuICAgICAgICBdKSxcbiAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJyb3dcIiB9LCBbXG4gICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtbGctOCBtYi00XCIgfSwgW1xuICAgICAgICAgICAgX2MoXCJoM1wiLCBbX3ZtLl92KFwiU2VuZCB1cyBhIE1lc3NhZ2VcIildKSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgXCJmb3JtXCIsXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgICAgbmFtZTogXCJzZW50TWVzc2FnZVwiLFxuICAgICAgICAgICAgICAgICAgaWQ6IFwiY29udGFjdEZvcm1cIixcbiAgICAgICAgICAgICAgICAgIG5vdmFsaWRhdGU6IFwiXCJcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbnRyb2wtZ3JvdXAgZm9ybS1ncm91cFwiIH0sIFtcbiAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29udHJvbHNcIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgIF9jKFwibGFiZWxcIiwgW192bS5fdihcIkZ1bGwgTmFtZTpcIildKSxcbiAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJpbnB1dFwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IFwidGV4dFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IFwibmFtZVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IFwiXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBcImRhdGEtdmFsaWRhdGlvbi1yZXF1aXJlZC1tZXNzYWdlXCI6XG4gICAgICAgICAgICAgICAgICAgICAgICAgIFwiUGxlYXNlIGVudGVyIHlvdXIgbmFtZS5cIlxuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgIF9jKFwicFwiLCB7IHN0YXRpY0NsYXNzOiBcImhlbHAtYmxvY2tcIiB9KVxuICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29udHJvbC1ncm91cCBmb3JtLWdyb3VwXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb250cm9sc1wiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJsYWJlbFwiLCBbX3ZtLl92KFwiUGhvbmUgTnVtYmVyOlwiKV0pLFxuICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICBfYyhcImlucHV0XCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICAgICAgICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJ0ZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkOiBcInBob25lXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogXCJcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiZGF0YS12YWxpZGF0aW9uLXJlcXVpcmVkLW1lc3NhZ2VcIjpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgXCJQbGVhc2UgZW50ZXIgeW91ciBwaG9uZSBudW1iZXIuXCJcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImhlbHAtYmxvY2tcIiB9KVxuICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29udHJvbC1ncm91cCBmb3JtLWdyb3VwXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb250cm9sc1wiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJsYWJlbFwiLCBbX3ZtLl92KFwiRW1haWwgQWRkcmVzczpcIildKSxcbiAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJpbnB1dFwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IFwiZW1haWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkOiBcImVtYWlsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogXCJcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiZGF0YS12YWxpZGF0aW9uLXJlcXVpcmVkLW1lc3NhZ2VcIjpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgXCJQbGVhc2UgZW50ZXIgeW91ciBlbWFpbCBhZGRyZXNzLlwiXG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJoZWxwLWJsb2NrXCIgfSlcbiAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbnRyb2wtZ3JvdXAgZm9ybS1ncm91cFwiIH0sIFtcbiAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29udHJvbHNcIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgIF9jKFwibGFiZWxcIiwgW192bS5fdihcIk1lc3NhZ2U6XCIpXSksXG4gICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgIF9jKFwidGV4dGFyZWFcIiwge1xuICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY1N0eWxlOiB7IHJlc2l6ZTogXCJub25lXCIgfSxcbiAgICAgICAgICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgcm93czogXCIxMFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgY29sczogXCIxMDBcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkOiBcIm1lc3NhZ2VcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiBcIlwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgXCJkYXRhLXZhbGlkYXRpb24tcmVxdWlyZWQtbWVzc2FnZVwiOlxuICAgICAgICAgICAgICAgICAgICAgICAgICBcIlBsZWFzZSBlbnRlciB5b3VyIG1lc3NhZ2VcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIG1heGxlbmd0aDogXCI5OTlcIlxuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiaGVscC1ibG9ja1wiIH0pXG4gICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBhdHRyczogeyBpZDogXCJzdWNjZXNzXCIgfSB9KSxcbiAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgXCJidXR0b25cIixcbiAgICAgICAgICAgICAgICAgIHsgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5XCIsIGF0dHJzOiB7IHR5cGU6IFwic3VibWl0XCIgfSB9LFxuICAgICAgICAgICAgICAgICAgW192bS5fdihcIlNlbmQgTWVzc2FnZVwiKV1cbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgIF1cbiAgICAgICAgICAgIClcbiAgICAgICAgICBdKVxuICAgICAgICBdKVxuICAgICAgXSlcbiAgICBdKVxuICB9XG5dXG5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbm1vZHVsZS5leHBvcnRzID0geyByZW5kZXI6IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnMgfVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKSAgICAgIC5yZXJlbmRlcihcImRhdGEtdi02MmM2MWQxOVwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNjJjNjFkMTlcIixcImhhc1Njb3BlZFwiOmZhbHNlLFwiYnVibGVcIjp7XCJ0cmFuc2Zvcm1zXCI6e319fSEuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAmYnVzdENhY2hlIS4vcmVzb3VyY2VzL2pzL3Z1ZS9wYWdlcy9jb250YWN0LnZ1ZVxuLy8gbW9kdWxlIGlkID0gNTNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwidmFyIGRpc3Bvc2VkID0gZmFsc2VcbnZhciBub3JtYWxpemVDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKVxuLyogc2NyaXB0ICovXG52YXIgX192dWVfc2NyaXB0X18gPSByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXSxcXFwicGx1Z2luc1xcXCI6W1xcXCJ0cmFuc2Zvcm0tb2JqZWN0LXJlc3Qtc3ByZWFkXFxcIl19IS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wJmJ1c3RDYWNoZSEuL3BvcnRhbC52dWVcIilcbi8qIHRlbXBsYXRlICovXG52YXIgX192dWVfdGVtcGxhdGVfXyA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTNlNDk3Y2M0XFxcIixcXFwiaGFzU2NvcGVkXFxcIjpmYWxzZSxcXFwiYnVibGVcXFwiOntcXFwidHJhbnNmb3Jtc1xcXCI6e319fSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wJmJ1c3RDYWNoZSEuL3BvcnRhbC52dWVcIilcbi8qIHRlbXBsYXRlIGZ1bmN0aW9uYWwgKi9cbiAgdmFyIF9fdnVlX3RlbXBsYXRlX2Z1bmN0aW9uYWxfXyA9IGZhbHNlXG4vKiBzdHlsZXMgKi9cbnZhciBfX3Z1ZV9zdHlsZXNfXyA9IG51bGxcbi8qIHNjb3BlSWQgKi9cbnZhciBfX3Z1ZV9zY29wZUlkX18gPSBudWxsXG4vKiBtb2R1bGVJZGVudGlmaWVyIChzZXJ2ZXIgb25seSkgKi9cbnZhciBfX3Z1ZV9tb2R1bGVfaWRlbnRpZmllcl9fID0gbnVsbFxudmFyIENvbXBvbmVudCA9IG5vcm1hbGl6ZUNvbXBvbmVudChcbiAgX192dWVfc2NyaXB0X18sXG4gIF9fdnVlX3RlbXBsYXRlX18sXG4gIF9fdnVlX3RlbXBsYXRlX2Z1bmN0aW9uYWxfXyxcbiAgX192dWVfc3R5bGVzX18sXG4gIF9fdnVlX3Njb3BlSWRfXyxcbiAgX192dWVfbW9kdWxlX2lkZW50aWZpZXJfX1xuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJyZXNvdXJjZXMvanMvdnVlL2xheW91dHMvcG9ydGFsLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7ICByZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkuc3Vic3RyKDAsIDIpICE9PSBcIl9fXCJ9KSkgeyAgY29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtM2U0OTdjYzRcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi0zZTQ5N2NjNFwiLCBDb21wb25lbnQub3B0aW9ucylcbicgKyAnICB9XG4gIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbiAoZGF0YSkge1xuICAgIGRpc3Bvc2VkID0gdHJ1ZVxuICB9KVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvanMvdnVlL2xheW91dHMvcG9ydGFsLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNTRcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwidmFyIHJlbmRlciA9IGZ1bmN0aW9uKCkge1xuICB2YXIgX3ZtID0gdGhpc1xuICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gIHJldHVybiBfYyhcImRpdlwiLCB7IGF0dHJzOiB7IGlkOiBcImxheW91dHMtcG9ydGFsXCIgfSB9LCBbX2MoXCJyb3V0ZXItdmlld1wiKV0sIDEpXG59XG52YXIgc3RhdGljUmVuZGVyRm5zID0gW11cbnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxubW9kdWxlLmV4cG9ydHMgPSB7IHJlbmRlcjogcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnM6IHN0YXRpY1JlbmRlckZucyB9XG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpICAgICAgLnJlcmVuZGVyKFwiZGF0YS12LTNlNDk3Y2M0XCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0zZTQ5N2NjNFwiLFwiaGFzU2NvcGVkXCI6ZmFsc2UsXCJidWJsZVwiOntcInRyYW5zZm9ybXNcIjp7fX19IS4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCZidXN0Q2FjaGUhLi9yZXNvdXJjZXMvanMvdnVlL2xheW91dHMvcG9ydGFsLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNTZcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwidmFyIGRpc3Bvc2VkID0gZmFsc2VcbmZ1bmN0aW9uIGluamVjdFN0eWxlIChzc3JDb250ZXh0KSB7XG4gIGlmIChkaXNwb3NlZCkgcmV0dXJuXG4gIHJlcXVpcmUoXCIhIXZ1ZS1zdHlsZS1sb2FkZXIhY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4P3tcXFwidnVlXFxcIjp0cnVlLFxcXCJpZFxcXCI6XFxcImRhdGEtdi0zYTNkYjUyZVxcXCIsXFxcInNjb3BlZFxcXCI6ZmFsc2UsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c3R5bGVzJmluZGV4PTAmYnVzdENhY2hlIS4vbG9naW4udnVlXCIpXG59XG52YXIgbm9ybWFsaXplQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIilcbi8qIHNjcmlwdCAqL1xudmFyIF9fdnVlX3NjcmlwdF9fID0gcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV0sXFxcInBsdWdpbnNcXFwiOltcXFwidHJhbnNmb3JtLW9iamVjdC1yZXN0LXNwcmVhZFxcXCJdfSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCZidXN0Q2FjaGUhLi9sb2dpbi52dWVcIilcbi8qIHRlbXBsYXRlICovXG52YXIgX192dWVfdGVtcGxhdGVfXyA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTNhM2RiNTJlXFxcIixcXFwiaGFzU2NvcGVkXFxcIjpmYWxzZSxcXFwiYnVibGVcXFwiOntcXFwidHJhbnNmb3Jtc1xcXCI6e319fSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wJmJ1c3RDYWNoZSEuL2xvZ2luLnZ1ZVwiKVxuLyogdGVtcGxhdGUgZnVuY3Rpb25hbCAqL1xuICB2YXIgX192dWVfdGVtcGxhdGVfZnVuY3Rpb25hbF9fID0gZmFsc2Vcbi8qIHN0eWxlcyAqL1xudmFyIF9fdnVlX3N0eWxlc19fID0gaW5qZWN0U3R5bGVcbi8qIHNjb3BlSWQgKi9cbnZhciBfX3Z1ZV9zY29wZUlkX18gPSBudWxsXG4vKiBtb2R1bGVJZGVudGlmaWVyIChzZXJ2ZXIgb25seSkgKi9cbnZhciBfX3Z1ZV9tb2R1bGVfaWRlbnRpZmllcl9fID0gbnVsbFxudmFyIENvbXBvbmVudCA9IG5vcm1hbGl6ZUNvbXBvbmVudChcbiAgX192dWVfc2NyaXB0X18sXG4gIF9fdnVlX3RlbXBsYXRlX18sXG4gIF9fdnVlX3RlbXBsYXRlX2Z1bmN0aW9uYWxfXyxcbiAgX192dWVfc3R5bGVzX18sXG4gIF9fdnVlX3Njb3BlSWRfXyxcbiAgX192dWVfbW9kdWxlX2lkZW50aWZpZXJfX1xuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJyZXNvdXJjZXMvanMvdnVlL2F1dGgvbG9naW4udnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHsgIHJldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleS5zdWJzdHIoMCwgMikgIT09IFwiX19cIn0pKSB7ICBjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi0zYTNkYjUyZVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTNhM2RiNTJlXCIsIENvbXBvbmVudC5vcHRpb25zKVxuJyArICcgIH1cbiAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgZGlzcG9zZWQgPSB0cnVlXG4gIH0pXG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9qcy92dWUvYXV0aC9sb2dpbi52dWVcbi8vIG1vZHVsZSBpZCA9IDU3XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIi8vIHN0eWxlLWxvYWRlcjogQWRkcyBzb21lIGNzcyB0byB0aGUgRE9NIGJ5IGFkZGluZyBhIDxzdHlsZT4gdGFnXG5cbi8vIGxvYWQgdGhlIHN0eWxlc1xudmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwidnVlXFxcIjp0cnVlLFxcXCJpZFxcXCI6XFxcImRhdGEtdi0zYTNkYjUyZVxcXCIsXFxcInNjb3BlZFxcXCI6ZmFsc2UsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAmYnVzdENhY2hlIS4vbG9naW4udnVlXCIpO1xuaWYodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG5pZihjb250ZW50LmxvY2FscykgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2Fscztcbi8vIGFkZCB0aGUgc3R5bGVzIHRvIHRoZSBET01cbnZhciB1cGRhdGUgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXNDbGllbnQuanNcIikoXCI3ZTgzN2NiNFwiLCBjb250ZW50LCBmYWxzZSk7XG4vLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG5pZihtb2R1bGUuaG90KSB7XG4gLy8gV2hlbiB0aGUgc3R5bGVzIGNoYW5nZSwgdXBkYXRlIHRoZSA8c3R5bGU+IHRhZ3NcbiBpZighY29udGVudC5sb2NhbHMpIHtcbiAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwidnVlXFxcIjp0cnVlLFxcXCJpZFxcXCI6XFxcImRhdGEtdi0zYTNkYjUyZVxcXCIsXFxcInNjb3BlZFxcXCI6ZmFsc2UsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAmYnVzdENhY2hlIS4vbG9naW4udnVlXCIsIGZ1bmN0aW9uKCkge1xuICAgICB2YXIgbmV3Q29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJ2dWVcXFwiOnRydWUsXFxcImlkXFxcIjpcXFwiZGF0YS12LTNhM2RiNTJlXFxcIixcXFwic2NvcGVkXFxcIjpmYWxzZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCZidXN0Q2FjaGUhLi9sb2dpbi52dWVcIik7XG4gICAgIGlmKHR5cGVvZiBuZXdDb250ZW50ID09PSAnc3RyaW5nJykgbmV3Q29udGVudCA9IFtbbW9kdWxlLmlkLCBuZXdDb250ZW50LCAnJ11dO1xuICAgICB1cGRhdGUobmV3Q29udGVudCk7XG4gICB9KTtcbiB9XG4gLy8gV2hlbiB0aGUgbW9kdWxlIGlzIGRpc3Bvc2VkLCByZW1vdmUgdGhlIDxzdHlsZT4gdGFnc1xuIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgdXBkYXRlKCk7IH0pO1xufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3Z1ZS1zdHlsZS1sb2FkZXIhLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXI/e1widnVlXCI6dHJ1ZSxcImlkXCI6XCJkYXRhLXYtM2EzZGI1MmVcIixcInNjb3BlZFwiOmZhbHNlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCZidXN0Q2FjaGUhLi9yZXNvdXJjZXMvanMvdnVlL2F1dGgvbG9naW4udnVlXG4vLyBtb2R1bGUgaWQgPSA1OFxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuXFxuLyohXFxuICogU3RhcnQgQm9vdHN0cmFwIC0gU0IgQWRtaW4gMiB2My4zLjcrMSAoaHR0cDovL3N0YXJ0Ym9vdHN0cmFwLmNvbS90ZW1wbGF0ZS1vdmVydmlld3Mvc2ItYWRtaW4tMilcXG4gKiBDb3B5cmlnaHQgMjAxMy0yMDE2IFN0YXJ0IEJvb3RzdHJhcFxcbiAqIExpY2Vuc2VkIHVuZGVyIE1JVCAoaHR0cHM6Ly9naXRodWIuY29tL0JsYWNrcm9ja0RpZ2l0YWwvc3RhcnRib290c3RyYXAvYmxvYi9naC1wYWdlcy9MSUNFTlNFKVxcbiAqL1xcbmJvZHkge1xcbiBcXHRiYWNrZ3JvdW5kLWNvbG9yOiAjZjhmOGY4O1xcbn1cXG4jd3JhcHBlciB7XFxuIFxcdHdpZHRoOiAxMDAlO1xcbn1cXG4jcGFnZS13cmFwcGVyIHtcXG4gXFx0cGFkZGluZzogMCAxNXB4O1xcbiBcXHRtaW4taGVpZ2h0OiA1NjhweDtcXG4gXFx0YmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XFxufVxcbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkge1xcbiNwYWdlLXdyYXBwZXIge1xcbiBcXHRcXHRwb3NpdGlvbjogaW5oZXJpdDtcXG4gXFx0XFx0bWFyZ2luOiAwIDAgMCAyNTBweDtcXG4gXFx0XFx0cGFkZGluZzogMCAzMHB4O1xcbiBcXHRcXHRib3JkZXItbGVmdDogMXB4IHNvbGlkICNlN2U3ZTc7XFxufVxcbn1cXG4ubmF2YmFyLXRvcC1saW5rcyB7XFxuIFxcdG1hcmdpbi1yaWdodDogMDtcXG59XFxuLm5hdmJhci10b3AtbGlua3MgbGkge1xcbiBcXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxufVxcbi5uYXZiYXItdG9wLWxpbmtzIGxpOmxhc3QtY2hpbGQge1xcbiBcXHRtYXJnaW4tcmlnaHQ6IDE1cHg7XFxufVxcbi5uYXZiYXItdG9wLWxpbmtzIGxpIGEge1xcbiBcXHRwYWRkaW5nOiAxNXB4O1xcbiBcXHRtaW4taGVpZ2h0OiA1MHB4O1xcbn1cXG4ubmF2YmFyLXRvcC1saW5rcyAuZHJvcGRvd24tbWVudSBsaSB7XFxuIFxcdGRpc3BsYXk6IGJsb2NrO1xcbn1cXG4ubmF2YmFyLXRvcC1saW5rcyAuZHJvcGRvd24tbWVudSBsaTpsYXN0LWNoaWxkIHtcXG4gXFx0bWFyZ2luLXJpZ2h0OiAwO1xcbn1cXG4ubmF2YmFyLXRvcC1saW5rcyAuZHJvcGRvd24tbWVudSBsaSBhIHtcXG4gXFx0cGFkZGluZzogM3B4IDIwcHg7XFxuIFxcdG1pbi1oZWlnaHQ6IDA7XFxufVxcbi5uYXZiYXItdG9wLWxpbmtzIC5kcm9wZG93bi1tZW51IGxpIGEgZGl2IHtcXG4gXFx0d2hpdGUtc3BhY2U6IG5vcm1hbDtcXG59XFxuLm5hdmJhci10b3AtbGlua3MgLmRyb3Bkb3duLW1lc3NhZ2VzLFxcbiAubmF2YmFyLXRvcC1saW5rcyAuZHJvcGRvd24tdGFza3MsXFxuIC5uYXZiYXItdG9wLWxpbmtzIC5kcm9wZG93bi1hbGVydHMge1xcbiBcXHR3aWR0aDogMzEwcHg7XFxuIFxcdG1pbi13aWR0aDogMDtcXG59XFxuLm5hdmJhci10b3AtbGlua3MgLmRyb3Bkb3duLW1lc3NhZ2VzIHtcXG4gXFx0bWFyZ2luLWxlZnQ6IDVweDtcXG59XFxuLm5hdmJhci10b3AtbGlua3MgLmRyb3Bkb3duLXRhc2tzIHtcXG4gXFx0bWFyZ2luLWxlZnQ6IC01OXB4O1xcbn1cXG4ubmF2YmFyLXRvcC1saW5rcyAuZHJvcGRvd24tYWxlcnRzIHtcXG4gXFx0bWFyZ2luLWxlZnQ6IC0xMjNweDtcXG59XFxuLm5hdmJhci10b3AtbGlua3MgLmRyb3Bkb3duLXVzZXIge1xcbiBcXHRyaWdodDogMDtcXG4gXFx0bGVmdDogYXV0bztcXG59XFxuLnNpZGViYXIgLnNpZGViYXItbmF2Lm5hdmJhci1jb2xsYXBzZSB7XFxuIFxcdHBhZGRpbmctbGVmdDogMDtcXG4gXFx0cGFkZGluZy1yaWdodDogMDtcXG59XFxuLnNpZGViYXIgLnNpZGViYXItc2VhcmNoIHtcXG4gXFx0cGFkZGluZzogMTVweDtcXG59XFxuLnNpZGViYXIgdWwgbGkge1xcbiBcXHRib3JkZXItYm90dG9tOiAxcHggc29saWQgI2U3ZTdlNztcXG59XFxuLnNpZGViYXIgdWwgbGkgYS5hY3RpdmUge1xcbiBcXHRiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlZWVlO1xcbn1cXG4uc2lkZWJhciAuYXJyb3cge1xcbiBcXHRmbG9hdDogcmlnaHQ7XFxufVxcbi5zaWRlYmFyIC5mYS5hcnJvdzpiZWZvcmUge1xcbiBcXHRjb250ZW50OiBcXFwiXFxcXEYxMDRcXFwiO1xcbn1cXG4uc2lkZWJhciAuYWN0aXZlID4gYSA+IC5mYS5hcnJvdzpiZWZvcmUge1xcbiBcXHRjb250ZW50OiBcXFwiXFxcXEYxMDdcXFwiO1xcbn1cXG4uc2lkZWJhciAubmF2LXNlY29uZC1sZXZlbCBsaSxcXG4gLnNpZGViYXIgLm5hdi10aGlyZC1sZXZlbCBsaSB7XFxuIFxcdGJvcmRlci1ib3R0b206IG5vbmUgIWltcG9ydGFudDtcXG59XFxuLnNpZGViYXIgLm5hdi1zZWNvbmQtbGV2ZWwgbGkgYSB7XFxuIFxcdHBhZGRpbmctbGVmdDogMzdweDtcXG59XFxuLnNpZGViYXIgLm5hdi10aGlyZC1sZXZlbCBsaSBhIHtcXG4gXFx0cGFkZGluZy1sZWZ0OiA1MnB4O1xcbn1cXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcXG4uc2lkZWJhciB7XFxuIFxcdFxcdHotaW5kZXg6IDE7XFxuIFxcdFxcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gXFx0XFx0d2lkdGg6IDI1MHB4O1xcbiBcXHRcXHRtYXJnaW4tdG9wOiA1MXB4O1xcbn1cXG4ubmF2YmFyLXRvcC1saW5rcyAuZHJvcGRvd24tbWVzc2FnZXMsXFxuIFxcdC5uYXZiYXItdG9wLWxpbmtzIC5kcm9wZG93bi10YXNrcyxcXG4gXFx0Lm5hdmJhci10b3AtbGlua3MgLmRyb3Bkb3duLWFsZXJ0cyB7XFxuIFxcdFxcdG1hcmdpbi1sZWZ0OiBhdXRvO1xcbn1cXG59XFxuLmJ0bi1vdXRsaW5lIHtcXG4gXFx0Y29sb3I6IGluaGVyaXQ7XFxuIFxcdGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xcbiBcXHR0cmFuc2l0aW9uOiBhbGwgLjVzO1xcbn1cXG4uYnRuLXByaW1hcnkuYnRuLW91dGxpbmUge1xcbiBcXHRjb2xvcjogIzQyOGJjYTtcXG59XFxuLmJ0bi1zdWNjZXNzLmJ0bi1vdXRsaW5lIHtcXG4gXFx0Y29sb3I6ICM1Y2I4NWM7XFxufVxcbi5idG4taW5mby5idG4tb3V0bGluZSB7XFxuIFxcdGNvbG9yOiAjNWJjMGRlO1xcbn1cXG4uYnRuLXdhcm5pbmcuYnRuLW91dGxpbmUge1xcbiBcXHRjb2xvcjogI2YwYWQ0ZTtcXG59XFxuLmJ0bi1kYW5nZXIuYnRuLW91dGxpbmUge1xcbiBcXHRjb2xvcjogI2Q5NTM0ZjtcXG59XFxuLmJ0bi1wcmltYXJ5LmJ0bi1vdXRsaW5lOmhvdmVyLFxcbiAuYnRuLXN1Y2Nlc3MuYnRuLW91dGxpbmU6aG92ZXIsXFxuIC5idG4taW5mby5idG4tb3V0bGluZTpob3ZlcixcXG4gLmJ0bi13YXJuaW5nLmJ0bi1vdXRsaW5lOmhvdmVyLFxcbiAuYnRuLWRhbmdlci5idG4tb3V0bGluZTpob3ZlciB7XFxuIFxcdGNvbG9yOiB3aGl0ZTtcXG59XFxuLmNoYXQge1xcbiBcXHRtYXJnaW46IDA7XFxuIFxcdHBhZGRpbmc6IDA7XFxuIFxcdGxpc3Qtc3R5bGU6IG5vbmU7XFxufVxcbi5jaGF0IGxpIHtcXG4gXFx0bWFyZ2luLWJvdHRvbTogMTBweDtcXG4gXFx0cGFkZGluZy1ib3R0b206IDVweDtcXG4gXFx0Ym9yZGVyLWJvdHRvbTogMXB4IGRvdHRlZCAjOTk5OTk5O1xcbn1cXG4uY2hhdCBsaS5sZWZ0IC5jaGF0LWJvZHkge1xcbiBcXHRtYXJnaW4tbGVmdDogNjBweDtcXG59XFxuLmNoYXQgbGkucmlnaHQgLmNoYXQtYm9keSB7XFxuIFxcdG1hcmdpbi1yaWdodDogNjBweDtcXG59XFxuLmNoYXQgbGkgLmNoYXQtYm9keSBwIHtcXG4gXFx0bWFyZ2luOiAwO1xcbn1cXG4ucGFuZWwgLnNsaWRlZG93biAuZ2x5cGhpY29uLFxcbiAuY2hhdCAuZ2x5cGhpY29uIHtcXG4gXFx0bWFyZ2luLXJpZ2h0OiA1cHg7XFxufVxcbi5jaGF0LXBhbmVsIC5wYW5lbC1ib2R5IHtcXG4gXFx0aGVpZ2h0OiAzNTBweDtcXG4gXFx0b3ZlcmZsb3cteTogc2Nyb2xsO1xcbn1cXG4ubG9naW4tcGFuZWwge1xcbiBcXHRtYXJnaW4tdG9wOiAyNSU7XFxufVxcbi5mbG90LWNoYXJ0IHtcXG4gXFx0ZGlzcGxheTogYmxvY2s7XFxuIFxcdGhlaWdodDogNDAwcHg7XFxufVxcbi5mbG90LWNoYXJ0LWNvbnRlbnQge1xcbiBcXHR3aWR0aDogMTAwJTtcXG4gXFx0aGVpZ2h0OiAxMDAlO1xcbn1cXG50YWJsZS5kYXRhVGFibGUgdGhlYWQgLnNvcnRpbmcsXFxuIHRhYmxlLmRhdGFUYWJsZSB0aGVhZCAuc29ydGluZ19hc2MsXFxuIHRhYmxlLmRhdGFUYWJsZSB0aGVhZCAuc29ydGluZ19kZXNjLFxcbiB0YWJsZS5kYXRhVGFibGUgdGhlYWQgLnNvcnRpbmdfYXNjX2Rpc2FibGVkLFxcbiB0YWJsZS5kYXRhVGFibGUgdGhlYWQgLnNvcnRpbmdfZGVzY19kaXNhYmxlZCB7XFxuIFxcdGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xcbn1cXG50YWJsZS5kYXRhVGFibGUgdGhlYWQgLnNvcnRpbmdfYXNjOmFmdGVyIHtcXG4gXFx0Y29udGVudDogXFxcIlxcXFxGMERFXFxcIjtcXG4gXFx0ZmxvYXQ6IHJpZ2h0O1xcbiBcXHRmb250LWZhbWlseTogZm9udGF3ZXNvbWU7XFxufVxcbnRhYmxlLmRhdGFUYWJsZSB0aGVhZCAuc29ydGluZ19kZXNjOmFmdGVyIHtcXG4gXFx0Y29udGVudDogXFxcIlxcXFxGMEREXFxcIjtcXG4gXFx0ZmxvYXQ6IHJpZ2h0O1xcbiBcXHRmb250LWZhbWlseTogZm9udGF3ZXNvbWU7XFxufVxcbnRhYmxlLmRhdGFUYWJsZSB0aGVhZCAuc29ydGluZzphZnRlciB7XFxuIFxcdGNvbnRlbnQ6IFxcXCJcXFxcRjBEQ1xcXCI7XFxuIFxcdGZsb2F0OiByaWdodDtcXG4gXFx0Zm9udC1mYW1pbHk6IGZvbnRhd2Vzb21lO1xcbiBcXHRjb2xvcjogcmdiYSg1MCwgNTAsIDUwLCAwLjUpO1xcbn1cXG4uYnRuLWNpcmNsZSB7XFxuIFxcdHdpZHRoOiAzMHB4O1xcbiBcXHRoZWlnaHQ6IDMwcHg7XFxuIFxcdHBhZGRpbmc6IDZweCAwO1xcbiBcXHRib3JkZXItcmFkaXVzOiAxNXB4O1xcbiBcXHR0ZXh0LWFsaWduOiBjZW50ZXI7XFxuIFxcdGZvbnQtc2l6ZTogMTJweDtcXG4gXFx0bGluZS1oZWlnaHQ6IDEuNDI4NTcxNDI5O1xcbn1cXG4uYnRuLWNpcmNsZS5idG4tbGcge1xcbiBcXHR3aWR0aDogNTBweDtcXG4gXFx0aGVpZ2h0OiA1MHB4O1xcbiBcXHRwYWRkaW5nOiAxMHB4IDE2cHg7XFxuIFxcdGJvcmRlci1yYWRpdXM6IDI1cHg7XFxuIFxcdGZvbnQtc2l6ZTogMThweDtcXG4gXFx0bGluZS1oZWlnaHQ6IDEuMzM7XFxufVxcbi5idG4tY2lyY2xlLmJ0bi14bCB7XFxuIFxcdHdpZHRoOiA3MHB4O1xcbiBcXHRoZWlnaHQ6IDcwcHg7XFxuIFxcdHBhZGRpbmc6IDEwcHggMTZweDtcXG4gXFx0Ym9yZGVyLXJhZGl1czogMzVweDtcXG4gXFx0Zm9udC1zaXplOiAyNHB4O1xcbiBcXHRsaW5lLWhlaWdodDogMS4zMztcXG59XFxuLnNob3ctZ3JpZCBbY2xhc3NePVxcXCJjb2wtXFxcIl0ge1xcbiBcXHRwYWRkaW5nLXRvcDogMTBweDtcXG4gXFx0cGFkZGluZy1ib3R0b206IDEwcHg7XFxuIFxcdGJvcmRlcjogMXB4IHNvbGlkICNkZGQ7XFxuIFxcdGJhY2tncm91bmQtY29sb3I6ICNlZWUgIWltcG9ydGFudDtcXG59XFxuLnNob3ctZ3JpZCB7XFxuIFxcdG1hcmdpbjogMTVweCAwO1xcbn1cXG4uaHVnZSB7XFxuIFxcdGZvbnQtc2l6ZTogNDBweDtcXG59XFxuLnBhbmVsLWdyZWVuIHtcXG4gXFx0Ym9yZGVyLWNvbG9yOiAjNWNiODVjO1xcbn1cXG4ucGFuZWwtZ3JlZW4gPiAucGFuZWwtaGVhZGluZyB7XFxuIFxcdGJvcmRlci1jb2xvcjogIzVjYjg1YztcXG4gXFx0Y29sb3I6IHdoaXRlO1xcbiBcXHRiYWNrZ3JvdW5kLWNvbG9yOiAjNWNiODVjO1xcbn1cXG4ucGFuZWwtZ3JlZW4gPiBhIHtcXG4gXFx0Y29sb3I6ICM1Y2I4NWM7XFxufVxcbi5wYW5lbC1ncmVlbiA+IGE6aG92ZXIge1xcbiBcXHRjb2xvcjogIzNkOGIzZDtcXG59XFxuLnBhbmVsLXJlZCB7XFxuIFxcdGJvcmRlci1jb2xvcjogI2Q5NTM0ZjtcXG59XFxuLnBhbmVsLXJlZCA+IC5wYW5lbC1oZWFkaW5nIHtcXG4gXFx0Ym9yZGVyLWNvbG9yOiAjZDk1MzRmO1xcbiBcXHRjb2xvcjogd2hpdGU7XFxuIFxcdGJhY2tncm91bmQtY29sb3I6ICNkOTUzNGY7XFxufVxcbi5wYW5lbC1yZWQgPiBhIHtcXG4gXFx0Y29sb3I6ICNkOTUzNGY7XFxufVxcbi5wYW5lbC1yZWQgPiBhOmhvdmVyIHtcXG4gXFx0Y29sb3I6ICNiNTJiMjc7XFxufVxcbi5wYW5lbC15ZWxsb3cge1xcbiBcXHRib3JkZXItY29sb3I6ICNmMGFkNGU7XFxufVxcbi5wYW5lbC15ZWxsb3cgPiAucGFuZWwtaGVhZGluZyB7XFxuIFxcdGJvcmRlci1jb2xvcjogI2YwYWQ0ZTtcXG4gXFx0Y29sb3I6IHdoaXRlO1xcbiBcXHRiYWNrZ3JvdW5kLWNvbG9yOiAjZjBhZDRlO1xcbn1cXG4ucGFuZWwteWVsbG93ID4gYSB7XFxuIFxcdGNvbG9yOiAjZjBhZDRlO1xcbn1cXG4ucGFuZWwteWVsbG93ID4gYTpob3ZlciB7XFxuIFxcdGNvbG9yOiAjZGY4YTEzO1xcbn1cXG4udGltZWxpbmUge1xcbiBcXHRwb3NpdGlvbjogcmVsYXRpdmU7XFxuIFxcdHBhZGRpbmc6IDIwcHggMCAyMHB4O1xcbiBcXHRsaXN0LXN0eWxlOiBub25lO1xcbn1cXG4udGltZWxpbmU6YmVmb3JlIHtcXG4gXFx0Y29udGVudDogXFxcIiBcXFwiO1xcbiBcXHRwb3NpdGlvbjogYWJzb2x1dGU7XFxuIFxcdHRvcDogMDtcXG4gXFx0Ym90dG9tOiAwO1xcbiBcXHRsZWZ0OiA1MCU7XFxuIFxcdHdpZHRoOiAzcHg7XFxuIFxcdG1hcmdpbi1sZWZ0OiAtMS41cHg7XFxuIFxcdGJhY2tncm91bmQtY29sb3I6ICNlZWVlZWU7XFxufVxcbi50aW1lbGluZSA+IGxpIHtcXG4gXFx0cG9zaXRpb246IHJlbGF0aXZlO1xcbiBcXHRtYXJnaW4tYm90dG9tOiAyMHB4O1xcbn1cXG4udGltZWxpbmUgPiBsaTpiZWZvcmUsXFxuIC50aW1lbGluZSA+IGxpOmFmdGVyIHtcXG4gXFx0Y29udGVudDogXFxcIiBcXFwiO1xcbiBcXHRkaXNwbGF5OiB0YWJsZTtcXG59XFxuLnRpbWVsaW5lID4gbGk6YWZ0ZXIge1xcbiBcXHRjbGVhcjogYm90aDtcXG59XFxuLnRpbWVsaW5lID4gbGk6YmVmb3JlLFxcbiAudGltZWxpbmUgPiBsaTphZnRlciB7XFxuIFxcdGNvbnRlbnQ6IFxcXCIgXFxcIjtcXG4gXFx0ZGlzcGxheTogdGFibGU7XFxufVxcbi50aW1lbGluZSA+IGxpOmFmdGVyIHtcXG4gXFx0Y2xlYXI6IGJvdGg7XFxufVxcbi50aW1lbGluZSA+IGxpID4gLnRpbWVsaW5lLXBhbmVsIHtcXG4gXFx0ZmxvYXQ6IGxlZnQ7XFxuIFxcdHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gXFx0d2lkdGg6IDQ2JTtcXG4gXFx0cGFkZGluZzogMjBweDtcXG4gXFx0Ym9yZGVyOiAxcHggc29saWQgI2Q0ZDRkNDtcXG4gXFx0Ym9yZGVyLXJhZGl1czogMnB4O1xcbiBcXHQtd2Via2l0LWJveC1zaGFkb3c6IDAgMXB4IDZweCByZ2JhKDAsIDAsIDAsIDAuMTc1KTtcXG4gXFx0Ym94LXNoYWRvdzogMCAxcHggNnB4IHJnYmEoMCwgMCwgMCwgMC4xNzUpO1xcbn1cXG4udGltZWxpbmUgPiBsaSA+IC50aW1lbGluZS1wYW5lbDpiZWZvcmUge1xcbiBcXHRjb250ZW50OiBcXFwiIFxcXCI7XFxuIFxcdGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gXFx0cG9zaXRpb246IGFic29sdXRlO1xcbiBcXHR0b3A6IDI2cHg7XFxuIFxcdHJpZ2h0OiAtMTVweDtcXG4gXFx0Ym9yZGVyLXRvcDogMTVweCBzb2xpZCB0cmFuc3BhcmVudDtcXG4gXFx0Ym9yZGVyLXJpZ2h0OiAwIHNvbGlkICNjY2M7XFxuIFxcdGJvcmRlci1ib3R0b206IDE1cHggc29saWQgdHJhbnNwYXJlbnQ7XFxuIFxcdGJvcmRlci1sZWZ0OiAxNXB4IHNvbGlkICNjY2M7XFxufVxcbi50aW1lbGluZSA+IGxpID4gLnRpbWVsaW5lLXBhbmVsOmFmdGVyIHtcXG4gXFx0Y29udGVudDogXFxcIiBcXFwiO1xcbiBcXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuIFxcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gXFx0dG9wOiAyN3B4O1xcbiBcXHRyaWdodDogLTE0cHg7XFxuIFxcdGJvcmRlci10b3A6IDE0cHggc29saWQgdHJhbnNwYXJlbnQ7XFxuIFxcdGJvcmRlci1yaWdodDogMCBzb2xpZCAjZmZmO1xcbiBcXHRib3JkZXItYm90dG9tOiAxNHB4IHNvbGlkIHRyYW5zcGFyZW50O1xcbiBcXHRib3JkZXItbGVmdDogMTRweCBzb2xpZCAjZmZmO1xcbn1cXG4udGltZWxpbmUgPiBsaSA+IC50aW1lbGluZS1iYWRnZSB7XFxuIFxcdHotaW5kZXg6IDEwMDtcXG4gXFx0cG9zaXRpb246IGFic29sdXRlO1xcbiBcXHR0b3A6IDE2cHg7XFxuIFxcdGxlZnQ6IDUwJTtcXG4gXFx0d2lkdGg6IDUwcHg7XFxuIFxcdGhlaWdodDogNTBweDtcXG4gXFx0bWFyZ2luLWxlZnQ6IC0yNXB4O1xcbiBcXHRib3JkZXItcmFkaXVzOiA1MCUgNTAlIDUwJSA1MCU7XFxuIFxcdHRleHQtYWxpZ246IGNlbnRlcjtcXG4gXFx0Zm9udC1zaXplOiAxLjRlbTtcXG4gXFx0bGluZS1oZWlnaHQ6IDUwcHg7XFxuIFxcdGNvbG9yOiAjZmZmO1xcbiBcXHRiYWNrZ3JvdW5kLWNvbG9yOiAjOTk5OTk5O1xcbn1cXG4udGltZWxpbmUgPiBsaS50aW1lbGluZS1pbnZlcnRlZCA+IC50aW1lbGluZS1wYW5lbCB7XFxuIFxcdGZsb2F0OiByaWdodDtcXG59XFxuLnRpbWVsaW5lID4gbGkudGltZWxpbmUtaW52ZXJ0ZWQgPiAudGltZWxpbmUtcGFuZWw6YmVmb3JlIHtcXG4gXFx0cmlnaHQ6IGF1dG87XFxuIFxcdGxlZnQ6IC0xNXB4O1xcbiBcXHRib3JkZXItcmlnaHQtd2lkdGg6IDE1cHg7XFxuIFxcdGJvcmRlci1sZWZ0LXdpZHRoOiAwO1xcbn1cXG4udGltZWxpbmUgPiBsaS50aW1lbGluZS1pbnZlcnRlZCA+IC50aW1lbGluZS1wYW5lbDphZnRlciB7XFxuIFxcdHJpZ2h0OiBhdXRvO1xcbiBcXHRsZWZ0OiAtMTRweDtcXG4gXFx0Ym9yZGVyLXJpZ2h0LXdpZHRoOiAxNHB4O1xcbiBcXHRib3JkZXItbGVmdC13aWR0aDogMDtcXG59XFxuLnRpbWVsaW5lLWJhZGdlLnByaW1hcnkge1xcbiBcXHRiYWNrZ3JvdW5kLWNvbG9yOiAjMmU2ZGE0ICFpbXBvcnRhbnQ7XFxufVxcbi50aW1lbGluZS1iYWRnZS5zdWNjZXNzIHtcXG4gXFx0YmFja2dyb3VuZC1jb2xvcjogIzNmOTAzZiAhaW1wb3J0YW50O1xcbn1cXG4udGltZWxpbmUtYmFkZ2Uud2FybmluZyB7XFxuIFxcdGJhY2tncm91bmQtY29sb3I6ICNmMGFkNGUgIWltcG9ydGFudDtcXG59XFxuLnRpbWVsaW5lLWJhZGdlLmRhbmdlciB7XFxuIFxcdGJhY2tncm91bmQtY29sb3I6ICNkOTUzNGYgIWltcG9ydGFudDtcXG59XFxuLnRpbWVsaW5lLWJhZGdlLmluZm8ge1xcbiBcXHRiYWNrZ3JvdW5kLWNvbG9yOiAjNWJjMGRlICFpbXBvcnRhbnQ7XFxufVxcbi50aW1lbGluZS10aXRsZSB7XFxuIFxcdG1hcmdpbi10b3A6IDA7XFxuIFxcdGNvbG9yOiBpbmhlcml0O1xcbn1cXG4udGltZWxpbmUtYm9keSA+IHAsXFxuIC50aW1lbGluZS1ib2R5ID4gdWwge1xcbiBcXHRtYXJnaW4tYm90dG9tOiAwO1xcbn1cXG4udGltZWxpbmUtYm9keSA+IHAgKyBwIHtcXG4gXFx0bWFyZ2luLXRvcDogNXB4O1xcbn1cXG5AbWVkaWEgKG1heC13aWR0aDogNzY3cHgpIHtcXG51bC50aW1lbGluZTpiZWZvcmUge1xcbiBcXHRcXHRsZWZ0OiA0MHB4O1xcbn1cXG51bC50aW1lbGluZSA+IGxpID4gLnRpbWVsaW5lLXBhbmVsIHtcXG4gXFx0XFx0d2lkdGg6IGNhbGMoMTAlKTtcXG4gXFx0XFx0d2lkdGg6IC1tb3otY2FsYygxMCUpO1xcbiBcXHRcXHR3aWR0aDogLXdlYmtpdC1jYWxjKDEwJSk7XFxufVxcbnVsLnRpbWVsaW5lID4gbGkgPiAudGltZWxpbmUtYmFkZ2Uge1xcbiBcXHRcXHR0b3A6IDE2cHg7XFxuIFxcdFxcdGxlZnQ6IDE1cHg7XFxuIFxcdFxcdG1hcmdpbi1sZWZ0OiAwO1xcbn1cXG51bC50aW1lbGluZSA+IGxpID4gLnRpbWVsaW5lLXBhbmVsIHtcXG4gXFx0XFx0ZmxvYXQ6IHJpZ2h0O1xcbn1cXG51bC50aW1lbGluZSA+IGxpID4gLnRpbWVsaW5lLXBhbmVsOmJlZm9yZSB7XFxuIFxcdFxcdHJpZ2h0OiBhdXRvO1xcbiBcXHRcXHRsZWZ0OiAtMTVweDtcXG4gXFx0XFx0Ym9yZGVyLXJpZ2h0LXdpZHRoOiAxNXB4O1xcbiBcXHRcXHRib3JkZXItbGVmdC13aWR0aDogMDtcXG59XFxudWwudGltZWxpbmUgPiBsaSA+IC50aW1lbGluZS1wYW5lbDphZnRlciB7XFxuIFxcdFxcdHJpZ2h0OiBhdXRvO1xcbiBcXHRcXHRsZWZ0OiAtMTRweDtcXG4gXFx0XFx0Ym9yZGVyLXJpZ2h0LXdpZHRoOiAxNHB4O1xcbiBcXHRcXHRib3JkZXItbGVmdC13aWR0aDogMDtcXG59XFxufVxcblxcbiBcXG4gXCIsIFwiXCIsIHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIi9Vc2Vycy92aWxsYW1vcm5hbnRvbmlvanIvRGV2ZWxvcG1lbnQvZ3JleWZveC9tYXRjaHVwL3dlYi9yZXNvdXJjZXMvanMvdnVlL2F1dGgvcmVzb3VyY2VzL2pzL3Z1ZS9hdXRoL2xvZ2luLnZ1ZT8zMTUyYWFlOFwiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF5R0E7Ozs7R0FJQTtBQUNBO0VBQ0EsMEJBQUE7Q0FDQTtBQUNBO0VBQ0EsWUFBQTtDQUNBO0FBQ0E7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0Esd0JBQUE7Q0FDQTtBQUNBO0FBQ0E7R0FDQSxrQkFBQTtHQUNBLG9CQUFBO0dBQ0EsZ0JBQUE7R0FDQSwrQkFBQTtDQUNBO0NBQ0E7QUFDQTtFQUNBLGdCQUFBO0NBQ0E7QUFDQTtFQUNBLHNCQUFBO0NBQ0E7QUFDQTtFQUNBLG1CQUFBO0NBQ0E7QUFDQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtDQUNBO0FBQ0E7RUFDQSxlQUFBO0NBQ0E7QUFDQTtFQUNBLGdCQUFBO0NBQ0E7QUFDQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtDQUNBO0FBQ0E7RUFDQSxvQkFBQTtDQUNBO0FBQ0E7OztFQUdBLGFBQUE7RUFDQSxhQUFBO0NBQ0E7QUFDQTtFQUNBLGlCQUFBO0NBQ0E7QUFDQTtFQUNBLG1CQUFBO0NBQ0E7QUFDQTtFQUNBLG9CQUFBO0NBQ0E7QUFDQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0NBQ0E7QUFDQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7Q0FDQTtBQUNBO0VBQ0EsY0FBQTtDQUNBO0FBQ0E7RUFDQSxpQ0FBQTtDQUNBO0FBQ0E7RUFDQSwwQkFBQTtDQUNBO0FBQ0E7RUFDQSxhQUFBO0NBQ0E7QUFDQTtFQUNBLGlCQUFBO0NBQ0E7QUFDQTtFQUNBLGlCQUFBO0NBQ0E7QUFDQTs7RUFFQSwrQkFBQTtDQUNBO0FBQ0E7RUFDQSxtQkFBQTtDQUNBO0FBQ0E7RUFDQSxtQkFBQTtDQUNBO0FBQ0E7QUFDQTtHQUNBLFdBQUE7R0FDQSxtQkFBQTtHQUNBLGFBQUE7R0FDQSxpQkFBQTtDQUNBO0FBQ0E7OztHQUdBLGtCQUFBO0NBQ0E7Q0FDQTtBQUNBO0VBQ0EsZUFBQTtFQUNBLDhCQUFBO0VBQ0Esb0JBQUE7Q0FDQTtBQUNBO0VBQ0EsZUFBQTtDQUNBO0FBQ0E7RUFDQSxlQUFBO0NBQ0E7QUFDQTtFQUNBLGVBQUE7Q0FDQTtBQUNBO0VBQ0EsZUFBQTtDQUNBO0FBQ0E7RUFDQSxlQUFBO0NBQ0E7QUFDQTs7Ozs7RUFLQSxhQUFBO0NBQ0E7QUFDQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7Q0FDQTtBQUNBO0VBQ0Esb0JBQUE7RUFDQSxvQkFBQTtFQUNBLGtDQUFBO0NBQ0E7QUFDQTtFQUNBLGtCQUFBO0NBQ0E7QUFDQTtFQUNBLG1CQUFBO0NBQ0E7QUFDQTtFQUNBLFVBQUE7Q0FDQTtBQUNBOztFQUVBLGtCQUFBO0NBQ0E7QUFDQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtDQUNBO0FBQ0E7RUFDQSxnQkFBQTtDQUNBO0FBQ0E7RUFDQSxlQUFBO0VBQ0EsY0FBQTtDQUNBO0FBQ0E7RUFDQSxZQUFBO0VBQ0EsYUFBQTtDQUNBO0FBQ0E7Ozs7O0VBS0Esd0JBQUE7Q0FDQTtBQUNBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EseUJBQUE7Q0FDQTtBQUNBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EseUJBQUE7Q0FDQTtBQUNBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EseUJBQUE7RUFDQSw2QkFBQTtDQUNBO0FBQ0E7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGVBQUE7RUFDQSxvQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5QkFBQTtDQUNBO0FBQ0E7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0Esb0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0NBQ0E7QUFDQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7Q0FDQTtBQUNBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLHVCQUFBO0VBQ0Esa0NBQUE7Q0FDQTtBQUNBO0VBQ0EsZUFBQTtDQUNBO0FBQ0E7RUFDQSxnQkFBQTtDQUNBO0FBQ0E7RUFDQSxzQkFBQTtDQUNBO0FBQ0E7RUFDQSxzQkFBQTtFQUNBLGFBQUE7RUFDQSwwQkFBQTtDQUNBO0FBQ0E7RUFDQSxlQUFBO0NBQ0E7QUFDQTtFQUNBLGVBQUE7Q0FDQTtBQUNBO0VBQ0Esc0JBQUE7Q0FDQTtBQUNBO0VBQ0Esc0JBQUE7RUFDQSxhQUFBO0VBQ0EsMEJBQUE7Q0FDQTtBQUNBO0VBQ0EsZUFBQTtDQUNBO0FBQ0E7RUFDQSxlQUFBO0NBQ0E7QUFDQTtFQUNBLHNCQUFBO0NBQ0E7QUFDQTtFQUNBLHNCQUFBO0VBQ0EsYUFBQTtFQUNBLDBCQUFBO0NBQ0E7QUFDQTtFQUNBLGVBQUE7Q0FDQTtBQUNBO0VBQ0EsZUFBQTtDQUNBO0FBQ0E7RUFDQSxtQkFBQTtFQUNBLHFCQUFBO0VBQ0EsaUJBQUE7Q0FDQTtBQUNBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsT0FBQTtFQUNBLFVBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLG9CQUFBO0VBQ0EsMEJBQUE7Q0FDQTtBQUNBO0VBQ0EsbUJBQUE7RUFDQSxvQkFBQTtDQUNBO0FBQ0E7O0VBRUEsYUFBQTtFQUNBLGVBQUE7Q0FDQTtBQUNBO0VBQ0EsWUFBQTtDQUNBO0FBQ0E7O0VBRUEsYUFBQTtFQUNBLGVBQUE7Q0FDQTtBQUNBO0VBQ0EsWUFBQTtDQUNBO0FBQ0E7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtFQUNBLDBCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtREFBQTtFQUNBLDJDQUFBO0NBQ0E7QUFDQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7RUFDQSxtQ0FBQTtFQUNBLDJCQUFBO0VBQ0Esc0NBQUE7RUFDQSw2QkFBQTtDQUNBO0FBQ0E7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7RUFDQSxhQUFBO0VBQ0EsbUNBQUE7RUFDQSwyQkFBQTtFQUNBLHNDQUFBO0VBQ0EsNkJBQUE7Q0FDQTtBQUNBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsVUFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsK0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsMEJBQUE7Q0FDQTtBQUNBO0VBQ0EsYUFBQTtDQUNBO0FBQ0E7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0VBQ0EscUJBQUE7Q0FDQTtBQUNBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLHFCQUFBO0NBQ0E7QUFDQTtFQUNBLHFDQUFBO0NBQ0E7QUFDQTtFQUNBLHFDQUFBO0NBQ0E7QUFDQTtFQUNBLHFDQUFBO0NBQ0E7QUFDQTtFQUNBLHFDQUFBO0NBQ0E7QUFDQTtFQUNBLHFDQUFBO0NBQ0E7QUFDQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0NBQ0E7QUFDQTs7RUFFQSxpQkFBQTtDQUNBO0FBQ0E7RUFDQSxnQkFBQTtDQUNBO0FBQ0E7QUFDQTtHQUNBLFdBQUE7Q0FDQTtBQUNBO0dBQ0EsaUJBQUE7R0FDQSxzQkFBQTtHQUNBLHlCQUFBO0NBQ0E7QUFDQTtHQUNBLFVBQUE7R0FDQSxXQUFBO0dBQ0EsZUFBQTtDQUNBO0FBQ0E7R0FDQSxhQUFBO0NBQ0E7QUFDQTtHQUNBLFlBQUE7R0FDQSxZQUFBO0dBQ0EseUJBQUE7R0FDQSxxQkFBQTtDQUNBO0FBQ0E7R0FDQSxZQUFBO0dBQ0EsWUFBQTtHQUNBLHlCQUFBO0dBQ0EscUJBQUE7Q0FDQTtDQUNBXCIsXCJmaWxlXCI6XCJsb2dpbi52dWVcIixcInNvdXJjZXNDb250ZW50XCI6W1wiPHRlbXBsYXRlPlxcblxcdDxkaXYgaWQ9XFxcImF1dGgtbG9naW4tY29tcG9uZW50XFxcIj5cXG5cXHRcXHRcXG5cXG5cXHRcXHQ8ZGl2IGNsYXNzPVxcXCJjb250YWluZXJcXFwiPlxcblxcdFxcdFxcdDxkaXYgY2xhc3M9XFxcInJvd1xcXCI+XFxuXFx0XFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwiY29sLW1kLTQgY29sLW1kLW9mZnNldC00XFxcIj5cXG5cXHRcXHRcXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJsb2dpbi1wYW5lbCBwYW5lbCBwYW5lbC1kZWZhdWx0XFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJwYW5lbC1oZWFkaW5nXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHQ8aDMgY2xhc3M9XFxcInBhbmVsLXRpdGxlXFxcIj5QbGVhc2UgU2lnbiBJbjwvaDM+XFxuXFx0XFx0XFx0XFx0XFx0XFx0PC9kaXY+XFxuXFx0XFx0XFx0XFx0XFx0XFx0PGRpdiBjbGFzcz1cXFwicGFuZWwtYm9keVxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0PGZvcm0gcm9sZT1cXFwiZm9ybVxcXCI+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0PGZpZWxkc2V0PlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdDxpbnB1dCBjbGFzcz1cXFwiZm9ybS1jb250cm9sXFxcIiB2LW1vZGVsPSdmb3JtLnVzZXJuYW1lJyBwbGFjZWhvbGRlcj1cXFwiRS1tYWlsXFxcIiBuYW1lPVxcXCJlbWFpbFxcXCIgdHlwZT1cXFwiZW1haWxcXFwiIGF1dG9mb2N1cz1cXFwiXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQ8L2Rpdj5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQ8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQ8aW5wdXQgY2xhc3M9XFxcImZvcm0tY29udHJvbFxcXCIgdi1tb2RlbD0nZm9ybS5wYXNzd29yZCcgcGxhY2Vob2xkZXI9XFxcIlBhc3N3b3JkXFxcIiBuYW1lPVxcXCJwYXNzd29yZFxcXCIgdHlwZT1cXFwicGFzc3dvcmRcXFwiIHZhbHVlPVxcXCJcXFwiPlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdDwvZGl2PlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdDxkaXYgY2xhc3M9XFxcImNoZWNrYm94XFxcIj5cXG5cXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHRcXHQ8bGFiZWw+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0PGlucHV0IG5hbWU9XFxcInJlbWVtYmVyXFxcIiB0eXBlPVxcXCJjaGVja2JveFxcXCIgdmFsdWU9XFxcIlJlbWVtYmVyIE1lXFxcIj5SZW1lbWJlciBNZVxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdDwvbGFiZWw+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0PC9kaXY+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0XFx0PCEtLSBDaGFuZ2UgdGhpcyB0byBhIGJ1dHRvbiBvciBpbnB1dCB3aGVuIHVzaW5nIHRoaXMgYXMgYSBmb3JtIC0tPlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdDxhIHYtb246Y2xpY2s9XFxcImxvZ2luXFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1sZyBidG4tc3VjY2VzcyBidG4tYmxvY2tcXFwiPkxvZ2luPC9hPlxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdDwvZmllbGRzZXQ+XFxuXFx0XFx0XFx0XFx0XFx0XFx0XFx0PC9mb3JtPlxcblxcdFxcdFxcdFxcdFxcdFxcdDwvZGl2PlxcblxcdFxcdFxcdFxcdFxcdDwvZGl2PlxcblxcdFxcdFxcdFxcdDwvZGl2PlxcblxcdFxcdFxcdDwvZGl2PlxcblxcdFxcdDwvZGl2PlxcblxcblxcblxcblxcblxcdFxcdFxcblxcblxcblxcdFxcdFxcblxcdDwvZGl2PlxcbjwvdGVtcGxhdGU+XFxuXFxuPHNjcmlwdD5cXG5leHBvcnQgZGVmYXVsdCB7XFxuXFxuXFx0ZGF0YSgpe1xcblxcdFxcdHJldHVybiB7XFxuICAgICAgICAgICAgZm9ybSA6IG5ldyBGb3JtKHtcXG4gICAgICAgICAgICAgICAgdXNlcm5hbWU6ICcnLFxcbiAgICAgICAgICAgICAgICBwYXNzd29yZDogJydcXG4gICAgICAgICAgICB9KSxcXG4gICAgICAgICAgICBtZXNzYWdlczoge31cXG4gICAgICAgIH1cXG4gICAgfSxcXG4gICAgY29tcHV0ZWQ6IHt9LFxcblxcbiAgICBjcmVhdGVkKCl7XFxuICAgICAgICAvL0RPIFNPTUVUSElORyBIRVJFIEFUIEZJUlNUIExPQURcXG4gICAgICAgIGxldCB0aGF0ID0gdGhpcztcXG4gICAgICAgIGlmKHRoYXQuJHN0b3JlLnN0YXRlLmlzQXV0aGVudGljYXRlZCl7XFxuICAgICAgICBcXHR0aGF0LiRyb3V0ZXIucHVzaCgnL2Rhc2hib2FyZCcpO1xcblxcbiAgICAgICAgfVxcbiAgICB9LFxcbiAgICBtZXRob2RzOiB7XFxuICAgIFxcdGxvZ2luKCl7XFxuICAgIFxcdFxcdGxldCB0aGF0ID0gdGhpcztcXG4gICAgXFx0XFx0dGhhdC5mb3JtLnBvc3QoJy9hcGkvYXV0aC90b2tlbicpXFxuICAgIFxcdFxcdC50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSl7XFxuICAgIFxcdFxcdFxcdGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdhY2Nlc3NfdG9rZW4nLHJlc3BvbnNlLmFjY2Vzc190b2tlbik7XFxuICAgIFxcdFxcdFxcdGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdyZWZyZXNoX3Rva2VuJyxyZXNwb25zZS5yZWZyZXNoX3Rva2VuKTtcXG4gICAgXFx0XFx0XFx0dGhhdC5nZXRTZXRVc2VyRGF0YSgpO1xcbiAgICBcXHRcXHR9KVxcbiAgICBcXHRcXHQuY2F0Y2goZnVuY3Rpb24gKGVycm9yKXtcXG4gICAgXFx0XFx0XFx0dGhhdC5mb3JtLnJlc2V0KCk7XFxuICAgIFxcdFxcdFxcdHRoYXQuJHJvdXRlci5hcHAuJGVtaXQoJ25vdGlmeUZhaWxlZCcsJ0xvZ2luIEZhaWxlZCEnKTtcXG5cXG4gICAgXFx0XFx0fSk7XFxuICAgIFxcdH0sXFxuICAgIFxcdGdldFNldFVzZXJEYXRhOiBmdW5jdGlvbiAoKSB7XFxuICAgIFxcdFxcdHZhciB0aGF0ID0gdGhpcztcXG5cXG4gICAgXFx0XFx0YXhpb3MuZ2V0KCcvYXBpL3VzZXInKVxcbiAgICBcXHRcXHQudGhlbihmdW5jdGlvbihyZXNwb25zZSl7XFxuICAgIFxcdFxcdFxcdHRoYXQuJHN0b3JlLnN0YXRlLnVzZXIgPSByZXNwb25zZS5kYXRhLmRhdGE7XFxuICAgIFxcdFxcdFxcdHRoYXQuJHN0b3JlLnN0YXRlLmlzQXV0aGVudGljYXRlZCA9IHRydWU7XFxuICAgIFxcdFxcdFxcdHRoYXQuJHN0b3JlLnN0YXRlLmFjY2Vzc190b2tlbiA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdhY2Nlc3NfdG9rZW4nKTtcXG4gICAgXFx0XFx0XFx0dGhhdC4kcm91dGVyLnB1c2goJy9kYXNoYm9hcmQnKTtcXG4gICAgXFx0XFx0fSkuY2F0Y2goZnVuY3Rpb24oZXJyb3Ipe1xcbiAgICBcXHRcXHRcXHRjb25zb2xlLmxvZyhlcnJvcik7XFxuXFxuICAgIFxcdFxcdH0pO1xcblxcbiAgICBcXHR9XFxuICAgIH1cXG5cXG5cXG59Ozwvc2NyaXB0PlxcblxcblxcbjxzdHlsZSB0eXBlPVxcXCJ0ZXh0L2Nzc1xcXCIgbWVkaWE9XFxcInNjcmVlblxcXCI+XFxuXFxuLyohXFxuICogU3RhcnQgQm9vdHN0cmFwIC0gU0IgQWRtaW4gMiB2My4zLjcrMSAoaHR0cDovL3N0YXJ0Ym9vdHN0cmFwLmNvbS90ZW1wbGF0ZS1vdmVydmlld3Mvc2ItYWRtaW4tMilcXG4gKiBDb3B5cmlnaHQgMjAxMy0yMDE2IFN0YXJ0IEJvb3RzdHJhcFxcbiAqIExpY2Vuc2VkIHVuZGVyIE1JVCAoaHR0cHM6Ly9naXRodWIuY29tL0JsYWNrcm9ja0RpZ2l0YWwvc3RhcnRib290c3RyYXAvYmxvYi9naC1wYWdlcy9MSUNFTlNFKVxcbiAqL1xcbiBib2R5IHtcXG4gXFx0YmFja2dyb3VuZC1jb2xvcjogI2Y4ZjhmODtcXG4gfVxcbiAjd3JhcHBlciB7XFxuIFxcdHdpZHRoOiAxMDAlO1xcbiB9XFxuICNwYWdlLXdyYXBwZXIge1xcbiBcXHRwYWRkaW5nOiAwIDE1cHg7XFxuIFxcdG1pbi1oZWlnaHQ6IDU2OHB4O1xcbiBcXHRiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcXG4gfVxcbiBAbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcXG4gXFx0I3BhZ2Utd3JhcHBlciB7XFxuIFxcdFxcdHBvc2l0aW9uOiBpbmhlcml0O1xcbiBcXHRcXHRtYXJnaW46IDAgMCAwIDI1MHB4O1xcbiBcXHRcXHRwYWRkaW5nOiAwIDMwcHg7XFxuIFxcdFxcdGJvcmRlci1sZWZ0OiAxcHggc29saWQgI2U3ZTdlNztcXG4gXFx0fVxcbiB9XFxuIC5uYXZiYXItdG9wLWxpbmtzIHtcXG4gXFx0bWFyZ2luLXJpZ2h0OiAwO1xcbiB9XFxuIC5uYXZiYXItdG9wLWxpbmtzIGxpIHtcXG4gXFx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiB9XFxuIC5uYXZiYXItdG9wLWxpbmtzIGxpOmxhc3QtY2hpbGQge1xcbiBcXHRtYXJnaW4tcmlnaHQ6IDE1cHg7XFxuIH1cXG4gLm5hdmJhci10b3AtbGlua3MgbGkgYSB7XFxuIFxcdHBhZGRpbmc6IDE1cHg7XFxuIFxcdG1pbi1oZWlnaHQ6IDUwcHg7XFxuIH1cXG4gLm5hdmJhci10b3AtbGlua3MgLmRyb3Bkb3duLW1lbnUgbGkge1xcbiBcXHRkaXNwbGF5OiBibG9jaztcXG4gfVxcbiAubmF2YmFyLXRvcC1saW5rcyAuZHJvcGRvd24tbWVudSBsaTpsYXN0LWNoaWxkIHtcXG4gXFx0bWFyZ2luLXJpZ2h0OiAwO1xcbiB9XFxuIC5uYXZiYXItdG9wLWxpbmtzIC5kcm9wZG93bi1tZW51IGxpIGEge1xcbiBcXHRwYWRkaW5nOiAzcHggMjBweDtcXG4gXFx0bWluLWhlaWdodDogMDtcXG4gfVxcbiAubmF2YmFyLXRvcC1saW5rcyAuZHJvcGRvd24tbWVudSBsaSBhIGRpdiB7XFxuIFxcdHdoaXRlLXNwYWNlOiBub3JtYWw7XFxuIH1cXG4gLm5hdmJhci10b3AtbGlua3MgLmRyb3Bkb3duLW1lc3NhZ2VzLFxcbiAubmF2YmFyLXRvcC1saW5rcyAuZHJvcGRvd24tdGFza3MsXFxuIC5uYXZiYXItdG9wLWxpbmtzIC5kcm9wZG93bi1hbGVydHMge1xcbiBcXHR3aWR0aDogMzEwcHg7XFxuIFxcdG1pbi13aWR0aDogMDtcXG4gfVxcbiAubmF2YmFyLXRvcC1saW5rcyAuZHJvcGRvd24tbWVzc2FnZXMge1xcbiBcXHRtYXJnaW4tbGVmdDogNXB4O1xcbiB9XFxuIC5uYXZiYXItdG9wLWxpbmtzIC5kcm9wZG93bi10YXNrcyB7XFxuIFxcdG1hcmdpbi1sZWZ0OiAtNTlweDtcXG4gfVxcbiAubmF2YmFyLXRvcC1saW5rcyAuZHJvcGRvd24tYWxlcnRzIHtcXG4gXFx0bWFyZ2luLWxlZnQ6IC0xMjNweDtcXG4gfVxcbiAubmF2YmFyLXRvcC1saW5rcyAuZHJvcGRvd24tdXNlciB7XFxuIFxcdHJpZ2h0OiAwO1xcbiBcXHRsZWZ0OiBhdXRvO1xcbiB9XFxuIC5zaWRlYmFyIC5zaWRlYmFyLW5hdi5uYXZiYXItY29sbGFwc2Uge1xcbiBcXHRwYWRkaW5nLWxlZnQ6IDA7XFxuIFxcdHBhZGRpbmctcmlnaHQ6IDA7XFxuIH1cXG4gLnNpZGViYXIgLnNpZGViYXItc2VhcmNoIHtcXG4gXFx0cGFkZGluZzogMTVweDtcXG4gfVxcbiAuc2lkZWJhciB1bCBsaSB7XFxuIFxcdGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZTdlN2U3O1xcbiB9XFxuIC5zaWRlYmFyIHVsIGxpIGEuYWN0aXZlIHtcXG4gXFx0YmFja2dyb3VuZC1jb2xvcjogI2VlZWVlZTtcXG4gfVxcbiAuc2lkZWJhciAuYXJyb3cge1xcbiBcXHRmbG9hdDogcmlnaHQ7XFxuIH1cXG4gLnNpZGViYXIgLmZhLmFycm93OmJlZm9yZSB7XFxuIFxcdGNvbnRlbnQ6IFxcXCJcXFxcZjEwNFxcXCI7XFxuIH1cXG4gLnNpZGViYXIgLmFjdGl2ZSA+IGEgPiAuZmEuYXJyb3c6YmVmb3JlIHtcXG4gXFx0Y29udGVudDogXFxcIlxcXFxmMTA3XFxcIjtcXG4gfVxcbiAuc2lkZWJhciAubmF2LXNlY29uZC1sZXZlbCBsaSxcXG4gLnNpZGViYXIgLm5hdi10aGlyZC1sZXZlbCBsaSB7XFxuIFxcdGJvcmRlci1ib3R0b206IG5vbmUgIWltcG9ydGFudDtcXG4gfVxcbiAuc2lkZWJhciAubmF2LXNlY29uZC1sZXZlbCBsaSBhIHtcXG4gXFx0cGFkZGluZy1sZWZ0OiAzN3B4O1xcbiB9XFxuIC5zaWRlYmFyIC5uYXYtdGhpcmQtbGV2ZWwgbGkgYSB7XFxuIFxcdHBhZGRpbmctbGVmdDogNTJweDtcXG4gfVxcbiBAbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcXG4gXFx0LnNpZGViYXIge1xcbiBcXHRcXHR6LWluZGV4OiAxO1xcbiBcXHRcXHRwb3NpdGlvbjogYWJzb2x1dGU7XFxuIFxcdFxcdHdpZHRoOiAyNTBweDtcXG4gXFx0XFx0bWFyZ2luLXRvcDogNTFweDtcXG4gXFx0fVxcbiBcXHQubmF2YmFyLXRvcC1saW5rcyAuZHJvcGRvd24tbWVzc2FnZXMsXFxuIFxcdC5uYXZiYXItdG9wLWxpbmtzIC5kcm9wZG93bi10YXNrcyxcXG4gXFx0Lm5hdmJhci10b3AtbGlua3MgLmRyb3Bkb3duLWFsZXJ0cyB7XFxuIFxcdFxcdG1hcmdpbi1sZWZ0OiBhdXRvO1xcbiBcXHR9XFxuIH1cXG4gLmJ0bi1vdXRsaW5lIHtcXG4gXFx0Y29sb3I6IGluaGVyaXQ7XFxuIFxcdGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xcbiBcXHR0cmFuc2l0aW9uOiBhbGwgLjVzO1xcbiB9XFxuIC5idG4tcHJpbWFyeS5idG4tb3V0bGluZSB7XFxuIFxcdGNvbG9yOiAjNDI4YmNhO1xcbiB9XFxuIC5idG4tc3VjY2Vzcy5idG4tb3V0bGluZSB7XFxuIFxcdGNvbG9yOiAjNWNiODVjO1xcbiB9XFxuIC5idG4taW5mby5idG4tb3V0bGluZSB7XFxuIFxcdGNvbG9yOiAjNWJjMGRlO1xcbiB9XFxuIC5idG4td2FybmluZy5idG4tb3V0bGluZSB7XFxuIFxcdGNvbG9yOiAjZjBhZDRlO1xcbiB9XFxuIC5idG4tZGFuZ2VyLmJ0bi1vdXRsaW5lIHtcXG4gXFx0Y29sb3I6ICNkOTUzNGY7XFxuIH1cXG4gLmJ0bi1wcmltYXJ5LmJ0bi1vdXRsaW5lOmhvdmVyLFxcbiAuYnRuLXN1Y2Nlc3MuYnRuLW91dGxpbmU6aG92ZXIsXFxuIC5idG4taW5mby5idG4tb3V0bGluZTpob3ZlcixcXG4gLmJ0bi13YXJuaW5nLmJ0bi1vdXRsaW5lOmhvdmVyLFxcbiAuYnRuLWRhbmdlci5idG4tb3V0bGluZTpob3ZlciB7XFxuIFxcdGNvbG9yOiB3aGl0ZTtcXG4gfVxcbiAuY2hhdCB7XFxuIFxcdG1hcmdpbjogMDtcXG4gXFx0cGFkZGluZzogMDtcXG4gXFx0bGlzdC1zdHlsZTogbm9uZTtcXG4gfVxcbiAuY2hhdCBsaSB7XFxuIFxcdG1hcmdpbi1ib3R0b206IDEwcHg7XFxuIFxcdHBhZGRpbmctYm90dG9tOiA1cHg7XFxuIFxcdGJvcmRlci1ib3R0b206IDFweCBkb3R0ZWQgIzk5OTk5OTtcXG4gfVxcbiAuY2hhdCBsaS5sZWZ0IC5jaGF0LWJvZHkge1xcbiBcXHRtYXJnaW4tbGVmdDogNjBweDtcXG4gfVxcbiAuY2hhdCBsaS5yaWdodCAuY2hhdC1ib2R5IHtcXG4gXFx0bWFyZ2luLXJpZ2h0OiA2MHB4O1xcbiB9XFxuIC5jaGF0IGxpIC5jaGF0LWJvZHkgcCB7XFxuIFxcdG1hcmdpbjogMDtcXG4gfVxcbiAucGFuZWwgLnNsaWRlZG93biAuZ2x5cGhpY29uLFxcbiAuY2hhdCAuZ2x5cGhpY29uIHtcXG4gXFx0bWFyZ2luLXJpZ2h0OiA1cHg7XFxuIH1cXG4gLmNoYXQtcGFuZWwgLnBhbmVsLWJvZHkge1xcbiBcXHRoZWlnaHQ6IDM1MHB4O1xcbiBcXHRvdmVyZmxvdy15OiBzY3JvbGw7XFxuIH1cXG4gLmxvZ2luLXBhbmVsIHtcXG4gXFx0bWFyZ2luLXRvcDogMjUlO1xcbiB9XFxuIC5mbG90LWNoYXJ0IHtcXG4gXFx0ZGlzcGxheTogYmxvY2s7XFxuIFxcdGhlaWdodDogNDAwcHg7XFxuIH1cXG4gLmZsb3QtY2hhcnQtY29udGVudCB7XFxuIFxcdHdpZHRoOiAxMDAlO1xcbiBcXHRoZWlnaHQ6IDEwMCU7XFxuIH1cXG4gdGFibGUuZGF0YVRhYmxlIHRoZWFkIC5zb3J0aW5nLFxcbiB0YWJsZS5kYXRhVGFibGUgdGhlYWQgLnNvcnRpbmdfYXNjLFxcbiB0YWJsZS5kYXRhVGFibGUgdGhlYWQgLnNvcnRpbmdfZGVzYyxcXG4gdGFibGUuZGF0YVRhYmxlIHRoZWFkIC5zb3J0aW5nX2FzY19kaXNhYmxlZCxcXG4gdGFibGUuZGF0YVRhYmxlIHRoZWFkIC5zb3J0aW5nX2Rlc2NfZGlzYWJsZWQge1xcbiBcXHRiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcXG4gfVxcbiB0YWJsZS5kYXRhVGFibGUgdGhlYWQgLnNvcnRpbmdfYXNjOmFmdGVyIHtcXG4gXFx0Y29udGVudDogXFxcIlxcXFxmMGRlXFxcIjtcXG4gXFx0ZmxvYXQ6IHJpZ2h0O1xcbiBcXHRmb250LWZhbWlseTogZm9udGF3ZXNvbWU7XFxuIH1cXG4gdGFibGUuZGF0YVRhYmxlIHRoZWFkIC5zb3J0aW5nX2Rlc2M6YWZ0ZXIge1xcbiBcXHRjb250ZW50OiBcXFwiXFxcXGYwZGRcXFwiO1xcbiBcXHRmbG9hdDogcmlnaHQ7XFxuIFxcdGZvbnQtZmFtaWx5OiBmb250YXdlc29tZTtcXG4gfVxcbiB0YWJsZS5kYXRhVGFibGUgdGhlYWQgLnNvcnRpbmc6YWZ0ZXIge1xcbiBcXHRjb250ZW50OiBcXFwiXFxcXGYwZGNcXFwiO1xcbiBcXHRmbG9hdDogcmlnaHQ7XFxuIFxcdGZvbnQtZmFtaWx5OiBmb250YXdlc29tZTtcXG4gXFx0Y29sb3I6IHJnYmEoNTAsIDUwLCA1MCwgMC41KTtcXG4gfVxcbiAuYnRuLWNpcmNsZSB7XFxuIFxcdHdpZHRoOiAzMHB4O1xcbiBcXHRoZWlnaHQ6IDMwcHg7XFxuIFxcdHBhZGRpbmc6IDZweCAwO1xcbiBcXHRib3JkZXItcmFkaXVzOiAxNXB4O1xcbiBcXHR0ZXh0LWFsaWduOiBjZW50ZXI7XFxuIFxcdGZvbnQtc2l6ZTogMTJweDtcXG4gXFx0bGluZS1oZWlnaHQ6IDEuNDI4NTcxNDI5O1xcbiB9XFxuIC5idG4tY2lyY2xlLmJ0bi1sZyB7XFxuIFxcdHdpZHRoOiA1MHB4O1xcbiBcXHRoZWlnaHQ6IDUwcHg7XFxuIFxcdHBhZGRpbmc6IDEwcHggMTZweDtcXG4gXFx0Ym9yZGVyLXJhZGl1czogMjVweDtcXG4gXFx0Zm9udC1zaXplOiAxOHB4O1xcbiBcXHRsaW5lLWhlaWdodDogMS4zMztcXG4gfVxcbiAuYnRuLWNpcmNsZS5idG4teGwge1xcbiBcXHR3aWR0aDogNzBweDtcXG4gXFx0aGVpZ2h0OiA3MHB4O1xcbiBcXHRwYWRkaW5nOiAxMHB4IDE2cHg7XFxuIFxcdGJvcmRlci1yYWRpdXM6IDM1cHg7XFxuIFxcdGZvbnQtc2l6ZTogMjRweDtcXG4gXFx0bGluZS1oZWlnaHQ6IDEuMzM7XFxuIH1cXG4gLnNob3ctZ3JpZCBbY2xhc3NePVxcXCJjb2wtXFxcIl0ge1xcbiBcXHRwYWRkaW5nLXRvcDogMTBweDtcXG4gXFx0cGFkZGluZy1ib3R0b206IDEwcHg7XFxuIFxcdGJvcmRlcjogMXB4IHNvbGlkICNkZGQ7XFxuIFxcdGJhY2tncm91bmQtY29sb3I6ICNlZWUgIWltcG9ydGFudDtcXG4gfVxcbiAuc2hvdy1ncmlkIHtcXG4gXFx0bWFyZ2luOiAxNXB4IDA7XFxuIH1cXG4gLmh1Z2Uge1xcbiBcXHRmb250LXNpemU6IDQwcHg7XFxuIH1cXG4gLnBhbmVsLWdyZWVuIHtcXG4gXFx0Ym9yZGVyLWNvbG9yOiAjNWNiODVjO1xcbiB9XFxuIC5wYW5lbC1ncmVlbiA+IC5wYW5lbC1oZWFkaW5nIHtcXG4gXFx0Ym9yZGVyLWNvbG9yOiAjNWNiODVjO1xcbiBcXHRjb2xvcjogd2hpdGU7XFxuIFxcdGJhY2tncm91bmQtY29sb3I6ICM1Y2I4NWM7XFxuIH1cXG4gLnBhbmVsLWdyZWVuID4gYSB7XFxuIFxcdGNvbG9yOiAjNWNiODVjO1xcbiB9XFxuIC5wYW5lbC1ncmVlbiA+IGE6aG92ZXIge1xcbiBcXHRjb2xvcjogIzNkOGIzZDtcXG4gfVxcbiAucGFuZWwtcmVkIHtcXG4gXFx0Ym9yZGVyLWNvbG9yOiAjZDk1MzRmO1xcbiB9XFxuIC5wYW5lbC1yZWQgPiAucGFuZWwtaGVhZGluZyB7XFxuIFxcdGJvcmRlci1jb2xvcjogI2Q5NTM0ZjtcXG4gXFx0Y29sb3I6IHdoaXRlO1xcbiBcXHRiYWNrZ3JvdW5kLWNvbG9yOiAjZDk1MzRmO1xcbiB9XFxuIC5wYW5lbC1yZWQgPiBhIHtcXG4gXFx0Y29sb3I6ICNkOTUzNGY7XFxuIH1cXG4gLnBhbmVsLXJlZCA+IGE6aG92ZXIge1xcbiBcXHRjb2xvcjogI2I1MmIyNztcXG4gfVxcbiAucGFuZWwteWVsbG93IHtcXG4gXFx0Ym9yZGVyLWNvbG9yOiAjZjBhZDRlO1xcbiB9XFxuIC5wYW5lbC15ZWxsb3cgPiAucGFuZWwtaGVhZGluZyB7XFxuIFxcdGJvcmRlci1jb2xvcjogI2YwYWQ0ZTtcXG4gXFx0Y29sb3I6IHdoaXRlO1xcbiBcXHRiYWNrZ3JvdW5kLWNvbG9yOiAjZjBhZDRlO1xcbiB9XFxuIC5wYW5lbC15ZWxsb3cgPiBhIHtcXG4gXFx0Y29sb3I6ICNmMGFkNGU7XFxuIH1cXG4gLnBhbmVsLXllbGxvdyA+IGE6aG92ZXIge1xcbiBcXHRjb2xvcjogI2RmOGExMztcXG4gfVxcbiAudGltZWxpbmUge1xcbiBcXHRwb3NpdGlvbjogcmVsYXRpdmU7XFxuIFxcdHBhZGRpbmc6IDIwcHggMCAyMHB4O1xcbiBcXHRsaXN0LXN0eWxlOiBub25lO1xcbiB9XFxuIC50aW1lbGluZTpiZWZvcmUge1xcbiBcXHRjb250ZW50OiBcXFwiIFxcXCI7XFxuIFxcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gXFx0dG9wOiAwO1xcbiBcXHRib3R0b206IDA7XFxuIFxcdGxlZnQ6IDUwJTtcXG4gXFx0d2lkdGg6IDNweDtcXG4gXFx0bWFyZ2luLWxlZnQ6IC0xLjVweDtcXG4gXFx0YmFja2dyb3VuZC1jb2xvcjogI2VlZWVlZTtcXG4gfVxcbiAudGltZWxpbmUgPiBsaSB7XFxuIFxcdHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gXFx0bWFyZ2luLWJvdHRvbTogMjBweDtcXG4gfVxcbiAudGltZWxpbmUgPiBsaTpiZWZvcmUsXFxuIC50aW1lbGluZSA+IGxpOmFmdGVyIHtcXG4gXFx0Y29udGVudDogXFxcIiBcXFwiO1xcbiBcXHRkaXNwbGF5OiB0YWJsZTtcXG4gfVxcbiAudGltZWxpbmUgPiBsaTphZnRlciB7XFxuIFxcdGNsZWFyOiBib3RoO1xcbiB9XFxuIC50aW1lbGluZSA+IGxpOmJlZm9yZSxcXG4gLnRpbWVsaW5lID4gbGk6YWZ0ZXIge1xcbiBcXHRjb250ZW50OiBcXFwiIFxcXCI7XFxuIFxcdGRpc3BsYXk6IHRhYmxlO1xcbiB9XFxuIC50aW1lbGluZSA+IGxpOmFmdGVyIHtcXG4gXFx0Y2xlYXI6IGJvdGg7XFxuIH1cXG4gLnRpbWVsaW5lID4gbGkgPiAudGltZWxpbmUtcGFuZWwge1xcbiBcXHRmbG9hdDogbGVmdDtcXG4gXFx0cG9zaXRpb246IHJlbGF0aXZlO1xcbiBcXHR3aWR0aDogNDYlO1xcbiBcXHRwYWRkaW5nOiAyMHB4O1xcbiBcXHRib3JkZXI6IDFweCBzb2xpZCAjZDRkNGQ0O1xcbiBcXHRib3JkZXItcmFkaXVzOiAycHg7XFxuIFxcdC13ZWJraXQtYm94LXNoYWRvdzogMCAxcHggNnB4IHJnYmEoMCwgMCwgMCwgMC4xNzUpO1xcbiBcXHRib3gtc2hhZG93OiAwIDFweCA2cHggcmdiYSgwLCAwLCAwLCAwLjE3NSk7XFxuIH1cXG4gLnRpbWVsaW5lID4gbGkgPiAudGltZWxpbmUtcGFuZWw6YmVmb3JlIHtcXG4gXFx0Y29udGVudDogXFxcIiBcXFwiO1xcbiBcXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuIFxcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gXFx0dG9wOiAyNnB4O1xcbiBcXHRyaWdodDogLTE1cHg7XFxuIFxcdGJvcmRlci10b3A6IDE1cHggc29saWQgdHJhbnNwYXJlbnQ7XFxuIFxcdGJvcmRlci1yaWdodDogMCBzb2xpZCAjY2NjO1xcbiBcXHRib3JkZXItYm90dG9tOiAxNXB4IHNvbGlkIHRyYW5zcGFyZW50O1xcbiBcXHRib3JkZXItbGVmdDogMTVweCBzb2xpZCAjY2NjO1xcbiB9XFxuIC50aW1lbGluZSA+IGxpID4gLnRpbWVsaW5lLXBhbmVsOmFmdGVyIHtcXG4gXFx0Y29udGVudDogXFxcIiBcXFwiO1xcbiBcXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuIFxcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gXFx0dG9wOiAyN3B4O1xcbiBcXHRyaWdodDogLTE0cHg7XFxuIFxcdGJvcmRlci10b3A6IDE0cHggc29saWQgdHJhbnNwYXJlbnQ7XFxuIFxcdGJvcmRlci1yaWdodDogMCBzb2xpZCAjZmZmO1xcbiBcXHRib3JkZXItYm90dG9tOiAxNHB4IHNvbGlkIHRyYW5zcGFyZW50O1xcbiBcXHRib3JkZXItbGVmdDogMTRweCBzb2xpZCAjZmZmO1xcbiB9XFxuIC50aW1lbGluZSA+IGxpID4gLnRpbWVsaW5lLWJhZGdlIHtcXG4gXFx0ei1pbmRleDogMTAwO1xcbiBcXHRwb3NpdGlvbjogYWJzb2x1dGU7XFxuIFxcdHRvcDogMTZweDtcXG4gXFx0bGVmdDogNTAlO1xcbiBcXHR3aWR0aDogNTBweDtcXG4gXFx0aGVpZ2h0OiA1MHB4O1xcbiBcXHRtYXJnaW4tbGVmdDogLTI1cHg7XFxuIFxcdGJvcmRlci1yYWRpdXM6IDUwJSA1MCUgNTAlIDUwJTtcXG4gXFx0dGV4dC1hbGlnbjogY2VudGVyO1xcbiBcXHRmb250LXNpemU6IDEuNGVtO1xcbiBcXHRsaW5lLWhlaWdodDogNTBweDtcXG4gXFx0Y29sb3I6ICNmZmY7XFxuIFxcdGJhY2tncm91bmQtY29sb3I6ICM5OTk5OTk7XFxuIH1cXG4gLnRpbWVsaW5lID4gbGkudGltZWxpbmUtaW52ZXJ0ZWQgPiAudGltZWxpbmUtcGFuZWwge1xcbiBcXHRmbG9hdDogcmlnaHQ7XFxuIH1cXG4gLnRpbWVsaW5lID4gbGkudGltZWxpbmUtaW52ZXJ0ZWQgPiAudGltZWxpbmUtcGFuZWw6YmVmb3JlIHtcXG4gXFx0cmlnaHQ6IGF1dG87XFxuIFxcdGxlZnQ6IC0xNXB4O1xcbiBcXHRib3JkZXItcmlnaHQtd2lkdGg6IDE1cHg7XFxuIFxcdGJvcmRlci1sZWZ0LXdpZHRoOiAwO1xcbiB9XFxuIC50aW1lbGluZSA+IGxpLnRpbWVsaW5lLWludmVydGVkID4gLnRpbWVsaW5lLXBhbmVsOmFmdGVyIHtcXG4gXFx0cmlnaHQ6IGF1dG87XFxuIFxcdGxlZnQ6IC0xNHB4O1xcbiBcXHRib3JkZXItcmlnaHQtd2lkdGg6IDE0cHg7XFxuIFxcdGJvcmRlci1sZWZ0LXdpZHRoOiAwO1xcbiB9XFxuIC50aW1lbGluZS1iYWRnZS5wcmltYXJ5IHtcXG4gXFx0YmFja2dyb3VuZC1jb2xvcjogIzJlNmRhNCAhaW1wb3J0YW50O1xcbiB9XFxuIC50aW1lbGluZS1iYWRnZS5zdWNjZXNzIHtcXG4gXFx0YmFja2dyb3VuZC1jb2xvcjogIzNmOTAzZiAhaW1wb3J0YW50O1xcbiB9XFxuIC50aW1lbGluZS1iYWRnZS53YXJuaW5nIHtcXG4gXFx0YmFja2dyb3VuZC1jb2xvcjogI2YwYWQ0ZSAhaW1wb3J0YW50O1xcbiB9XFxuIC50aW1lbGluZS1iYWRnZS5kYW5nZXIge1xcbiBcXHRiYWNrZ3JvdW5kLWNvbG9yOiAjZDk1MzRmICFpbXBvcnRhbnQ7XFxuIH1cXG4gLnRpbWVsaW5lLWJhZGdlLmluZm8ge1xcbiBcXHRiYWNrZ3JvdW5kLWNvbG9yOiAjNWJjMGRlICFpbXBvcnRhbnQ7XFxuIH1cXG4gLnRpbWVsaW5lLXRpdGxlIHtcXG4gXFx0bWFyZ2luLXRvcDogMDtcXG4gXFx0Y29sb3I6IGluaGVyaXQ7XFxuIH1cXG4gLnRpbWVsaW5lLWJvZHkgPiBwLFxcbiAudGltZWxpbmUtYm9keSA+IHVsIHtcXG4gXFx0bWFyZ2luLWJvdHRvbTogMDtcXG4gfVxcbiAudGltZWxpbmUtYm9keSA+IHAgKyBwIHtcXG4gXFx0bWFyZ2luLXRvcDogNXB4O1xcbiB9XFxuIEBtZWRpYSAobWF4LXdpZHRoOiA3NjdweCkge1xcbiBcXHR1bC50aW1lbGluZTpiZWZvcmUge1xcbiBcXHRcXHRsZWZ0OiA0MHB4O1xcbiBcXHR9XFxuIFxcdHVsLnRpbWVsaW5lID4gbGkgPiAudGltZWxpbmUtcGFuZWwge1xcbiBcXHRcXHR3aWR0aDogY2FsYygxMCUpO1xcbiBcXHRcXHR3aWR0aDogLW1vei1jYWxjKDEwJSk7XFxuIFxcdFxcdHdpZHRoOiAtd2Via2l0LWNhbGMoMTAlKTtcXG4gXFx0fVxcbiBcXHR1bC50aW1lbGluZSA+IGxpID4gLnRpbWVsaW5lLWJhZGdlIHtcXG4gXFx0XFx0dG9wOiAxNnB4O1xcbiBcXHRcXHRsZWZ0OiAxNXB4O1xcbiBcXHRcXHRtYXJnaW4tbGVmdDogMDtcXG4gXFx0fVxcbiBcXHR1bC50aW1lbGluZSA+IGxpID4gLnRpbWVsaW5lLXBhbmVsIHtcXG4gXFx0XFx0ZmxvYXQ6IHJpZ2h0O1xcbiBcXHR9XFxuIFxcdHVsLnRpbWVsaW5lID4gbGkgPiAudGltZWxpbmUtcGFuZWw6YmVmb3JlIHtcXG4gXFx0XFx0cmlnaHQ6IGF1dG87XFxuIFxcdFxcdGxlZnQ6IC0xNXB4O1xcbiBcXHRcXHRib3JkZXItcmlnaHQtd2lkdGg6IDE1cHg7XFxuIFxcdFxcdGJvcmRlci1sZWZ0LXdpZHRoOiAwO1xcbiBcXHR9XFxuIFxcdHVsLnRpbWVsaW5lID4gbGkgPiAudGltZWxpbmUtcGFuZWw6YWZ0ZXIge1xcbiBcXHRcXHRyaWdodDogYXV0bztcXG4gXFx0XFx0bGVmdDogLTE0cHg7XFxuIFxcdFxcdGJvcmRlci1yaWdodC13aWR0aDogMTRweDtcXG4gXFx0XFx0Ym9yZGVyLWxlZnQtd2lkdGg6IDA7XFxuIFxcdH1cXG4gfVxcblxcbiBcXG4gPC9zdHlsZT5cXG5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXI/e1widnVlXCI6dHJ1ZSxcImlkXCI6XCJkYXRhLXYtM2EzZGI1MmVcIixcInNjb3BlZFwiOmZhbHNlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCZidXN0Q2FjaGUhLi9yZXNvdXJjZXMvanMvdnVlL2F1dGgvbG9naW4udnVlXG4vLyBtb2R1bGUgaWQgPSA1OVxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCI8dGVtcGxhdGU+XG5cdDxkaXYgaWQ9XCJhdXRoLWxvZ2luLWNvbXBvbmVudFwiPlxuXHRcdFxuXG5cdFx0PGRpdiBjbGFzcz1cImNvbnRhaW5lclwiPlxuXHRcdFx0PGRpdiBjbGFzcz1cInJvd1wiPlxuXHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY29sLW1kLTQgY29sLW1kLW9mZnNldC00XCI+XG5cdFx0XHRcdFx0PGRpdiBjbGFzcz1cImxvZ2luLXBhbmVsIHBhbmVsIHBhbmVsLWRlZmF1bHRcIj5cblx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJwYW5lbC1oZWFkaW5nXCI+XG5cdFx0XHRcdFx0XHRcdDxoMyBjbGFzcz1cInBhbmVsLXRpdGxlXCI+UGxlYXNlIFNpZ24gSW48L2gzPlxuXHRcdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwicGFuZWwtYm9keVwiPlxuXHRcdFx0XHRcdFx0XHQ8Zm9ybSByb2xlPVwiZm9ybVwiPlxuXHRcdFx0XHRcdFx0XHRcdDxmaWVsZHNldD5cblx0XHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxpbnB1dCBjbGFzcz1cImZvcm0tY29udHJvbFwiIHYtbW9kZWw9J2Zvcm0udXNlcm5hbWUnIHBsYWNlaG9sZGVyPVwiRS1tYWlsXCIgbmFtZT1cImVtYWlsXCIgdHlwZT1cImVtYWlsXCIgYXV0b2ZvY3VzPVwiXCI+XG5cdFx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cblx0XHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxpbnB1dCBjbGFzcz1cImZvcm0tY29udHJvbFwiIHYtbW9kZWw9J2Zvcm0ucGFzc3dvcmQnIHBsYWNlaG9sZGVyPVwiUGFzc3dvcmRcIiBuYW1lPVwicGFzc3dvcmRcIiB0eXBlPVwicGFzc3dvcmRcIiB2YWx1ZT1cIlwiPlxuXHRcdFx0XHRcdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiY2hlY2tib3hcIj5cblx0XHRcdFx0XHRcdFx0XHRcdFx0PGxhYmVsPlxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdDxpbnB1dCBuYW1lPVwicmVtZW1iZXJcIiB0eXBlPVwiY2hlY2tib3hcIiB2YWx1ZT1cIlJlbWVtYmVyIE1lXCI+UmVtZW1iZXIgTWVcblx0XHRcdFx0XHRcdFx0XHRcdFx0PC9sYWJlbD5cblx0XHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxuXHRcdFx0XHRcdFx0XHRcdFx0PCEtLSBDaGFuZ2UgdGhpcyB0byBhIGJ1dHRvbiBvciBpbnB1dCB3aGVuIHVzaW5nIHRoaXMgYXMgYSBmb3JtIC0tPlxuXHRcdFx0XHRcdFx0XHRcdFx0PGEgdi1vbjpjbGljaz1cImxvZ2luXCIgY2xhc3M9XCJidG4gYnRuLWxnIGJ0bi1zdWNjZXNzIGJ0bi1ibG9ja1wiPkxvZ2luPC9hPlxuXHRcdFx0XHRcdFx0XHRcdDwvZmllbGRzZXQ+XG5cdFx0XHRcdFx0XHRcdDwvZm9ybT5cblx0XHRcdFx0XHRcdDwvZGl2PlxuXHRcdFx0XHRcdDwvZGl2PlxuXHRcdFx0XHQ8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXHRcdDwvZGl2PlxuXG5cblxuXG5cdFx0XG5cblxuXHRcdFxuXHQ8L2Rpdj5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5leHBvcnQgZGVmYXVsdCB7XG5cblx0ZGF0YSgpe1xuXHRcdHJldHVybiB7XG4gICAgICAgICAgICBmb3JtIDogbmV3IEZvcm0oe1xuICAgICAgICAgICAgICAgIHVzZXJuYW1lOiAnJyxcbiAgICAgICAgICAgICAgICBwYXNzd29yZDogJydcbiAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgbWVzc2FnZXM6IHt9XG4gICAgICAgIH1cbiAgICB9LFxuICAgIGNvbXB1dGVkOiB7fSxcblxuICAgIGNyZWF0ZWQoKXtcbiAgICAgICAgLy9ETyBTT01FVEhJTkcgSEVSRSBBVCBGSVJTVCBMT0FEXG4gICAgICAgIGxldCB0aGF0ID0gdGhpcztcbiAgICAgICAgaWYodGhhdC4kc3RvcmUuc3RhdGUuaXNBdXRoZW50aWNhdGVkKXtcbiAgICAgICAgXHR0aGF0LiRyb3V0ZXIucHVzaCgnL2Rhc2hib2FyZCcpO1xuXG4gICAgICAgIH1cbiAgICB9LFxuICAgIG1ldGhvZHM6IHtcbiAgICBcdGxvZ2luKCl7XG4gICAgXHRcdGxldCB0aGF0ID0gdGhpcztcbiAgICBcdFx0dGhhdC5mb3JtLnBvc3QoJy9hcGkvYXV0aC90b2tlbicpXG4gICAgXHRcdC50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSl7XG4gICAgXHRcdFx0bG9jYWxTdG9yYWdlLnNldEl0ZW0oJ2FjY2Vzc190b2tlbicscmVzcG9uc2UuYWNjZXNzX3Rva2VuKTtcbiAgICBcdFx0XHRsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgncmVmcmVzaF90b2tlbicscmVzcG9uc2UucmVmcmVzaF90b2tlbik7XG4gICAgXHRcdFx0dGhhdC5nZXRTZXRVc2VyRGF0YSgpO1xuICAgIFx0XHR9KVxuICAgIFx0XHQuY2F0Y2goZnVuY3Rpb24gKGVycm9yKXtcbiAgICBcdFx0XHR0aGF0LmZvcm0ucmVzZXQoKTtcbiAgICBcdFx0XHR0aGF0LiRyb3V0ZXIuYXBwLiRlbWl0KCdub3RpZnlGYWlsZWQnLCdMb2dpbiBGYWlsZWQhJyk7XG5cbiAgICBcdFx0fSk7XG4gICAgXHR9LFxuICAgIFx0Z2V0U2V0VXNlckRhdGE6IGZ1bmN0aW9uICgpIHtcbiAgICBcdFx0dmFyIHRoYXQgPSB0aGlzO1xuXG4gICAgXHRcdGF4aW9zLmdldCgnL2FwaS91c2VyJylcbiAgICBcdFx0LnRoZW4oZnVuY3Rpb24ocmVzcG9uc2Upe1xuICAgIFx0XHRcdHRoYXQuJHN0b3JlLnN0YXRlLnVzZXIgPSByZXNwb25zZS5kYXRhLmRhdGE7XG4gICAgXHRcdFx0dGhhdC4kc3RvcmUuc3RhdGUuaXNBdXRoZW50aWNhdGVkID0gdHJ1ZTtcbiAgICBcdFx0XHR0aGF0LiRzdG9yZS5zdGF0ZS5hY2Nlc3NfdG9rZW4gPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnYWNjZXNzX3Rva2VuJyk7XG4gICAgXHRcdFx0dGhhdC4kcm91dGVyLnB1c2goJy9kYXNoYm9hcmQnKTtcbiAgICBcdFx0fSkuY2F0Y2goZnVuY3Rpb24oZXJyb3Ipe1xuICAgIFx0XHRcdGNvbnNvbGUubG9nKGVycm9yKTtcblxuICAgIFx0XHR9KTtcblxuICAgIFx0fVxuICAgIH1cblxuXG59Ozwvc2NyaXB0PlxuXG5cbjxzdHlsZSB0eXBlPVwidGV4dC9jc3NcIiBtZWRpYT1cInNjcmVlblwiPlxuXG4vKiFcbiAqIFN0YXJ0IEJvb3RzdHJhcCAtIFNCIEFkbWluIDIgdjMuMy43KzEgKGh0dHA6Ly9zdGFydGJvb3RzdHJhcC5jb20vdGVtcGxhdGUtb3ZlcnZpZXdzL3NiLWFkbWluLTIpXG4gKiBDb3B5cmlnaHQgMjAxMy0yMDE2IFN0YXJ0IEJvb3RzdHJhcFxuICogTGljZW5zZWQgdW5kZXIgTUlUIChodHRwczovL2dpdGh1Yi5jb20vQmxhY2tyb2NrRGlnaXRhbC9zdGFydGJvb3RzdHJhcC9ibG9iL2doLXBhZ2VzL0xJQ0VOU0UpXG4gKi9cbiBib2R5IHtcbiBcdGJhY2tncm91bmQtY29sb3I6ICNmOGY4Zjg7XG4gfVxuICN3cmFwcGVyIHtcbiBcdHdpZHRoOiAxMDAlO1xuIH1cbiAjcGFnZS13cmFwcGVyIHtcbiBcdHBhZGRpbmc6IDAgMTVweDtcbiBcdG1pbi1oZWlnaHQ6IDU2OHB4O1xuIFx0YmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gfVxuIEBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkge1xuIFx0I3BhZ2Utd3JhcHBlciB7XG4gXHRcdHBvc2l0aW9uOiBpbmhlcml0O1xuIFx0XHRtYXJnaW46IDAgMCAwIDI1MHB4O1xuIFx0XHRwYWRkaW5nOiAwIDMwcHg7XG4gXHRcdGJvcmRlci1sZWZ0OiAxcHggc29saWQgI2U3ZTdlNztcbiBcdH1cbiB9XG4gLm5hdmJhci10b3AtbGlua3Mge1xuIFx0bWFyZ2luLXJpZ2h0OiAwO1xuIH1cbiAubmF2YmFyLXRvcC1saW5rcyBsaSB7XG4gXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gfVxuIC5uYXZiYXItdG9wLWxpbmtzIGxpOmxhc3QtY2hpbGQge1xuIFx0bWFyZ2luLXJpZ2h0OiAxNXB4O1xuIH1cbiAubmF2YmFyLXRvcC1saW5rcyBsaSBhIHtcbiBcdHBhZGRpbmc6IDE1cHg7XG4gXHRtaW4taGVpZ2h0OiA1MHB4O1xuIH1cbiAubmF2YmFyLXRvcC1saW5rcyAuZHJvcGRvd24tbWVudSBsaSB7XG4gXHRkaXNwbGF5OiBibG9jaztcbiB9XG4gLm5hdmJhci10b3AtbGlua3MgLmRyb3Bkb3duLW1lbnUgbGk6bGFzdC1jaGlsZCB7XG4gXHRtYXJnaW4tcmlnaHQ6IDA7XG4gfVxuIC5uYXZiYXItdG9wLWxpbmtzIC5kcm9wZG93bi1tZW51IGxpIGEge1xuIFx0cGFkZGluZzogM3B4IDIwcHg7XG4gXHRtaW4taGVpZ2h0OiAwO1xuIH1cbiAubmF2YmFyLXRvcC1saW5rcyAuZHJvcGRvd24tbWVudSBsaSBhIGRpdiB7XG4gXHR3aGl0ZS1zcGFjZTogbm9ybWFsO1xuIH1cbiAubmF2YmFyLXRvcC1saW5rcyAuZHJvcGRvd24tbWVzc2FnZXMsXG4gLm5hdmJhci10b3AtbGlua3MgLmRyb3Bkb3duLXRhc2tzLFxuIC5uYXZiYXItdG9wLWxpbmtzIC5kcm9wZG93bi1hbGVydHMge1xuIFx0d2lkdGg6IDMxMHB4O1xuIFx0bWluLXdpZHRoOiAwO1xuIH1cbiAubmF2YmFyLXRvcC1saW5rcyAuZHJvcGRvd24tbWVzc2FnZXMge1xuIFx0bWFyZ2luLWxlZnQ6IDVweDtcbiB9XG4gLm5hdmJhci10b3AtbGlua3MgLmRyb3Bkb3duLXRhc2tzIHtcbiBcdG1hcmdpbi1sZWZ0OiAtNTlweDtcbiB9XG4gLm5hdmJhci10b3AtbGlua3MgLmRyb3Bkb3duLWFsZXJ0cyB7XG4gXHRtYXJnaW4tbGVmdDogLTEyM3B4O1xuIH1cbiAubmF2YmFyLXRvcC1saW5rcyAuZHJvcGRvd24tdXNlciB7XG4gXHRyaWdodDogMDtcbiBcdGxlZnQ6IGF1dG87XG4gfVxuIC5zaWRlYmFyIC5zaWRlYmFyLW5hdi5uYXZiYXItY29sbGFwc2Uge1xuIFx0cGFkZGluZy1sZWZ0OiAwO1xuIFx0cGFkZGluZy1yaWdodDogMDtcbiB9XG4gLnNpZGViYXIgLnNpZGViYXItc2VhcmNoIHtcbiBcdHBhZGRpbmc6IDE1cHg7XG4gfVxuIC5zaWRlYmFyIHVsIGxpIHtcbiBcdGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZTdlN2U3O1xuIH1cbiAuc2lkZWJhciB1bCBsaSBhLmFjdGl2ZSB7XG4gXHRiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlZWVlO1xuIH1cbiAuc2lkZWJhciAuYXJyb3cge1xuIFx0ZmxvYXQ6IHJpZ2h0O1xuIH1cbiAuc2lkZWJhciAuZmEuYXJyb3c6YmVmb3JlIHtcbiBcdGNvbnRlbnQ6IFwiXFxmMTA0XCI7XG4gfVxuIC5zaWRlYmFyIC5hY3RpdmUgPiBhID4gLmZhLmFycm93OmJlZm9yZSB7XG4gXHRjb250ZW50OiBcIlxcZjEwN1wiO1xuIH1cbiAuc2lkZWJhciAubmF2LXNlY29uZC1sZXZlbCBsaSxcbiAuc2lkZWJhciAubmF2LXRoaXJkLWxldmVsIGxpIHtcbiBcdGJvcmRlci1ib3R0b206IG5vbmUgIWltcG9ydGFudDtcbiB9XG4gLnNpZGViYXIgLm5hdi1zZWNvbmQtbGV2ZWwgbGkgYSB7XG4gXHRwYWRkaW5nLWxlZnQ6IDM3cHg7XG4gfVxuIC5zaWRlYmFyIC5uYXYtdGhpcmQtbGV2ZWwgbGkgYSB7XG4gXHRwYWRkaW5nLWxlZnQ6IDUycHg7XG4gfVxuIEBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkge1xuIFx0LnNpZGViYXIge1xuIFx0XHR6LWluZGV4OiAxO1xuIFx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XG4gXHRcdHdpZHRoOiAyNTBweDtcbiBcdFx0bWFyZ2luLXRvcDogNTFweDtcbiBcdH1cbiBcdC5uYXZiYXItdG9wLWxpbmtzIC5kcm9wZG93bi1tZXNzYWdlcyxcbiBcdC5uYXZiYXItdG9wLWxpbmtzIC5kcm9wZG93bi10YXNrcyxcbiBcdC5uYXZiYXItdG9wLWxpbmtzIC5kcm9wZG93bi1hbGVydHMge1xuIFx0XHRtYXJnaW4tbGVmdDogYXV0bztcbiBcdH1cbiB9XG4gLmJ0bi1vdXRsaW5lIHtcbiBcdGNvbG9yOiBpbmhlcml0O1xuIFx0YmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gXHR0cmFuc2l0aW9uOiBhbGwgLjVzO1xuIH1cbiAuYnRuLXByaW1hcnkuYnRuLW91dGxpbmUge1xuIFx0Y29sb3I6ICM0MjhiY2E7XG4gfVxuIC5idG4tc3VjY2Vzcy5idG4tb3V0bGluZSB7XG4gXHRjb2xvcjogIzVjYjg1YztcbiB9XG4gLmJ0bi1pbmZvLmJ0bi1vdXRsaW5lIHtcbiBcdGNvbG9yOiAjNWJjMGRlO1xuIH1cbiAuYnRuLXdhcm5pbmcuYnRuLW91dGxpbmUge1xuIFx0Y29sb3I6ICNmMGFkNGU7XG4gfVxuIC5idG4tZGFuZ2VyLmJ0bi1vdXRsaW5lIHtcbiBcdGNvbG9yOiAjZDk1MzRmO1xuIH1cbiAuYnRuLXByaW1hcnkuYnRuLW91dGxpbmU6aG92ZXIsXG4gLmJ0bi1zdWNjZXNzLmJ0bi1vdXRsaW5lOmhvdmVyLFxuIC5idG4taW5mby5idG4tb3V0bGluZTpob3ZlcixcbiAuYnRuLXdhcm5pbmcuYnRuLW91dGxpbmU6aG92ZXIsXG4gLmJ0bi1kYW5nZXIuYnRuLW91dGxpbmU6aG92ZXIge1xuIFx0Y29sb3I6IHdoaXRlO1xuIH1cbiAuY2hhdCB7XG4gXHRtYXJnaW46IDA7XG4gXHRwYWRkaW5nOiAwO1xuIFx0bGlzdC1zdHlsZTogbm9uZTtcbiB9XG4gLmNoYXQgbGkge1xuIFx0bWFyZ2luLWJvdHRvbTogMTBweDtcbiBcdHBhZGRpbmctYm90dG9tOiA1cHg7XG4gXHRib3JkZXItYm90dG9tOiAxcHggZG90dGVkICM5OTk5OTk7XG4gfVxuIC5jaGF0IGxpLmxlZnQgLmNoYXQtYm9keSB7XG4gXHRtYXJnaW4tbGVmdDogNjBweDtcbiB9XG4gLmNoYXQgbGkucmlnaHQgLmNoYXQtYm9keSB7XG4gXHRtYXJnaW4tcmlnaHQ6IDYwcHg7XG4gfVxuIC5jaGF0IGxpIC5jaGF0LWJvZHkgcCB7XG4gXHRtYXJnaW46IDA7XG4gfVxuIC5wYW5lbCAuc2xpZGVkb3duIC5nbHlwaGljb24sXG4gLmNoYXQgLmdseXBoaWNvbiB7XG4gXHRtYXJnaW4tcmlnaHQ6IDVweDtcbiB9XG4gLmNoYXQtcGFuZWwgLnBhbmVsLWJvZHkge1xuIFx0aGVpZ2h0OiAzNTBweDtcbiBcdG92ZXJmbG93LXk6IHNjcm9sbDtcbiB9XG4gLmxvZ2luLXBhbmVsIHtcbiBcdG1hcmdpbi10b3A6IDI1JTtcbiB9XG4gLmZsb3QtY2hhcnQge1xuIFx0ZGlzcGxheTogYmxvY2s7XG4gXHRoZWlnaHQ6IDQwMHB4O1xuIH1cbiAuZmxvdC1jaGFydC1jb250ZW50IHtcbiBcdHdpZHRoOiAxMDAlO1xuIFx0aGVpZ2h0OiAxMDAlO1xuIH1cbiB0YWJsZS5kYXRhVGFibGUgdGhlYWQgLnNvcnRpbmcsXG4gdGFibGUuZGF0YVRhYmxlIHRoZWFkIC5zb3J0aW5nX2FzYyxcbiB0YWJsZS5kYXRhVGFibGUgdGhlYWQgLnNvcnRpbmdfZGVzYyxcbiB0YWJsZS5kYXRhVGFibGUgdGhlYWQgLnNvcnRpbmdfYXNjX2Rpc2FibGVkLFxuIHRhYmxlLmRhdGFUYWJsZSB0aGVhZCAuc29ydGluZ19kZXNjX2Rpc2FibGVkIHtcbiBcdGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuIH1cbiB0YWJsZS5kYXRhVGFibGUgdGhlYWQgLnNvcnRpbmdfYXNjOmFmdGVyIHtcbiBcdGNvbnRlbnQ6IFwiXFxmMGRlXCI7XG4gXHRmbG9hdDogcmlnaHQ7XG4gXHRmb250LWZhbWlseTogZm9udGF3ZXNvbWU7XG4gfVxuIHRhYmxlLmRhdGFUYWJsZSB0aGVhZCAuc29ydGluZ19kZXNjOmFmdGVyIHtcbiBcdGNvbnRlbnQ6IFwiXFxmMGRkXCI7XG4gXHRmbG9hdDogcmlnaHQ7XG4gXHRmb250LWZhbWlseTogZm9udGF3ZXNvbWU7XG4gfVxuIHRhYmxlLmRhdGFUYWJsZSB0aGVhZCAuc29ydGluZzphZnRlciB7XG4gXHRjb250ZW50OiBcIlxcZjBkY1wiO1xuIFx0ZmxvYXQ6IHJpZ2h0O1xuIFx0Zm9udC1mYW1pbHk6IGZvbnRhd2Vzb21lO1xuIFx0Y29sb3I6IHJnYmEoNTAsIDUwLCA1MCwgMC41KTtcbiB9XG4gLmJ0bi1jaXJjbGUge1xuIFx0d2lkdGg6IDMwcHg7XG4gXHRoZWlnaHQ6IDMwcHg7XG4gXHRwYWRkaW5nOiA2cHggMDtcbiBcdGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gXHR0ZXh0LWFsaWduOiBjZW50ZXI7XG4gXHRmb250LXNpemU6IDEycHg7XG4gXHRsaW5lLWhlaWdodDogMS40Mjg1NzE0Mjk7XG4gfVxuIC5idG4tY2lyY2xlLmJ0bi1sZyB7XG4gXHR3aWR0aDogNTBweDtcbiBcdGhlaWdodDogNTBweDtcbiBcdHBhZGRpbmc6IDEwcHggMTZweDtcbiBcdGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gXHRmb250LXNpemU6IDE4cHg7XG4gXHRsaW5lLWhlaWdodDogMS4zMztcbiB9XG4gLmJ0bi1jaXJjbGUuYnRuLXhsIHtcbiBcdHdpZHRoOiA3MHB4O1xuIFx0aGVpZ2h0OiA3MHB4O1xuIFx0cGFkZGluZzogMTBweCAxNnB4O1xuIFx0Ym9yZGVyLXJhZGl1czogMzVweDtcbiBcdGZvbnQtc2l6ZTogMjRweDtcbiBcdGxpbmUtaGVpZ2h0OiAxLjMzO1xuIH1cbiAuc2hvdy1ncmlkIFtjbGFzc149XCJjb2wtXCJdIHtcbiBcdHBhZGRpbmctdG9wOiAxMHB4O1xuIFx0cGFkZGluZy1ib3R0b206IDEwcHg7XG4gXHRib3JkZXI6IDFweCBzb2xpZCAjZGRkO1xuIFx0YmFja2dyb3VuZC1jb2xvcjogI2VlZSAhaW1wb3J0YW50O1xuIH1cbiAuc2hvdy1ncmlkIHtcbiBcdG1hcmdpbjogMTVweCAwO1xuIH1cbiAuaHVnZSB7XG4gXHRmb250LXNpemU6IDQwcHg7XG4gfVxuIC5wYW5lbC1ncmVlbiB7XG4gXHRib3JkZXItY29sb3I6ICM1Y2I4NWM7XG4gfVxuIC5wYW5lbC1ncmVlbiA+IC5wYW5lbC1oZWFkaW5nIHtcbiBcdGJvcmRlci1jb2xvcjogIzVjYjg1YztcbiBcdGNvbG9yOiB3aGl0ZTtcbiBcdGJhY2tncm91bmQtY29sb3I6ICM1Y2I4NWM7XG4gfVxuIC5wYW5lbC1ncmVlbiA+IGEge1xuIFx0Y29sb3I6ICM1Y2I4NWM7XG4gfVxuIC5wYW5lbC1ncmVlbiA+IGE6aG92ZXIge1xuIFx0Y29sb3I6ICMzZDhiM2Q7XG4gfVxuIC5wYW5lbC1yZWQge1xuIFx0Ym9yZGVyLWNvbG9yOiAjZDk1MzRmO1xuIH1cbiAucGFuZWwtcmVkID4gLnBhbmVsLWhlYWRpbmcge1xuIFx0Ym9yZGVyLWNvbG9yOiAjZDk1MzRmO1xuIFx0Y29sb3I6IHdoaXRlO1xuIFx0YmFja2dyb3VuZC1jb2xvcjogI2Q5NTM0ZjtcbiB9XG4gLnBhbmVsLXJlZCA+IGEge1xuIFx0Y29sb3I6ICNkOTUzNGY7XG4gfVxuIC5wYW5lbC1yZWQgPiBhOmhvdmVyIHtcbiBcdGNvbG9yOiAjYjUyYjI3O1xuIH1cbiAucGFuZWwteWVsbG93IHtcbiBcdGJvcmRlci1jb2xvcjogI2YwYWQ0ZTtcbiB9XG4gLnBhbmVsLXllbGxvdyA+IC5wYW5lbC1oZWFkaW5nIHtcbiBcdGJvcmRlci1jb2xvcjogI2YwYWQ0ZTtcbiBcdGNvbG9yOiB3aGl0ZTtcbiBcdGJhY2tncm91bmQtY29sb3I6ICNmMGFkNGU7XG4gfVxuIC5wYW5lbC15ZWxsb3cgPiBhIHtcbiBcdGNvbG9yOiAjZjBhZDRlO1xuIH1cbiAucGFuZWwteWVsbG93ID4gYTpob3ZlciB7XG4gXHRjb2xvcjogI2RmOGExMztcbiB9XG4gLnRpbWVsaW5lIHtcbiBcdHBvc2l0aW9uOiByZWxhdGl2ZTtcbiBcdHBhZGRpbmc6IDIwcHggMCAyMHB4O1xuIFx0bGlzdC1zdHlsZTogbm9uZTtcbiB9XG4gLnRpbWVsaW5lOmJlZm9yZSB7XG4gXHRjb250ZW50OiBcIiBcIjtcbiBcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiBcdHRvcDogMDtcbiBcdGJvdHRvbTogMDtcbiBcdGxlZnQ6IDUwJTtcbiBcdHdpZHRoOiAzcHg7XG4gXHRtYXJnaW4tbGVmdDogLTEuNXB4O1xuIFx0YmFja2dyb3VuZC1jb2xvcjogI2VlZWVlZTtcbiB9XG4gLnRpbWVsaW5lID4gbGkge1xuIFx0cG9zaXRpb246IHJlbGF0aXZlO1xuIFx0bWFyZ2luLWJvdHRvbTogMjBweDtcbiB9XG4gLnRpbWVsaW5lID4gbGk6YmVmb3JlLFxuIC50aW1lbGluZSA+IGxpOmFmdGVyIHtcbiBcdGNvbnRlbnQ6IFwiIFwiO1xuIFx0ZGlzcGxheTogdGFibGU7XG4gfVxuIC50aW1lbGluZSA+IGxpOmFmdGVyIHtcbiBcdGNsZWFyOiBib3RoO1xuIH1cbiAudGltZWxpbmUgPiBsaTpiZWZvcmUsXG4gLnRpbWVsaW5lID4gbGk6YWZ0ZXIge1xuIFx0Y29udGVudDogXCIgXCI7XG4gXHRkaXNwbGF5OiB0YWJsZTtcbiB9XG4gLnRpbWVsaW5lID4gbGk6YWZ0ZXIge1xuIFx0Y2xlYXI6IGJvdGg7XG4gfVxuIC50aW1lbGluZSA+IGxpID4gLnRpbWVsaW5lLXBhbmVsIHtcbiBcdGZsb2F0OiBsZWZ0O1xuIFx0cG9zaXRpb246IHJlbGF0aXZlO1xuIFx0d2lkdGg6IDQ2JTtcbiBcdHBhZGRpbmc6IDIwcHg7XG4gXHRib3JkZXI6IDFweCBzb2xpZCAjZDRkNGQ0O1xuIFx0Ym9yZGVyLXJhZGl1czogMnB4O1xuIFx0LXdlYmtpdC1ib3gtc2hhZG93OiAwIDFweCA2cHggcmdiYSgwLCAwLCAwLCAwLjE3NSk7XG4gXHRib3gtc2hhZG93OiAwIDFweCA2cHggcmdiYSgwLCAwLCAwLCAwLjE3NSk7XG4gfVxuIC50aW1lbGluZSA+IGxpID4gLnRpbWVsaW5lLXBhbmVsOmJlZm9yZSB7XG4gXHRjb250ZW50OiBcIiBcIjtcbiBcdGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiBcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiBcdHRvcDogMjZweDtcbiBcdHJpZ2h0OiAtMTVweDtcbiBcdGJvcmRlci10b3A6IDE1cHggc29saWQgdHJhbnNwYXJlbnQ7XG4gXHRib3JkZXItcmlnaHQ6IDAgc29saWQgI2NjYztcbiBcdGJvcmRlci1ib3R0b206IDE1cHggc29saWQgdHJhbnNwYXJlbnQ7XG4gXHRib3JkZXItbGVmdDogMTVweCBzb2xpZCAjY2NjO1xuIH1cbiAudGltZWxpbmUgPiBsaSA+IC50aW1lbGluZS1wYW5lbDphZnRlciB7XG4gXHRjb250ZW50OiBcIiBcIjtcbiBcdGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiBcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiBcdHRvcDogMjdweDtcbiBcdHJpZ2h0OiAtMTRweDtcbiBcdGJvcmRlci10b3A6IDE0cHggc29saWQgdHJhbnNwYXJlbnQ7XG4gXHRib3JkZXItcmlnaHQ6IDAgc29saWQgI2ZmZjtcbiBcdGJvcmRlci1ib3R0b206IDE0cHggc29saWQgdHJhbnNwYXJlbnQ7XG4gXHRib3JkZXItbGVmdDogMTRweCBzb2xpZCAjZmZmO1xuIH1cbiAudGltZWxpbmUgPiBsaSA+IC50aW1lbGluZS1iYWRnZSB7XG4gXHR6LWluZGV4OiAxMDA7XG4gXHRwb3NpdGlvbjogYWJzb2x1dGU7XG4gXHR0b3A6IDE2cHg7XG4gXHRsZWZ0OiA1MCU7XG4gXHR3aWR0aDogNTBweDtcbiBcdGhlaWdodDogNTBweDtcbiBcdG1hcmdpbi1sZWZ0OiAtMjVweDtcbiBcdGJvcmRlci1yYWRpdXM6IDUwJSA1MCUgNTAlIDUwJTtcbiBcdHRleHQtYWxpZ246IGNlbnRlcjtcbiBcdGZvbnQtc2l6ZTogMS40ZW07XG4gXHRsaW5lLWhlaWdodDogNTBweDtcbiBcdGNvbG9yOiAjZmZmO1xuIFx0YmFja2dyb3VuZC1jb2xvcjogIzk5OTk5OTtcbiB9XG4gLnRpbWVsaW5lID4gbGkudGltZWxpbmUtaW52ZXJ0ZWQgPiAudGltZWxpbmUtcGFuZWwge1xuIFx0ZmxvYXQ6IHJpZ2h0O1xuIH1cbiAudGltZWxpbmUgPiBsaS50aW1lbGluZS1pbnZlcnRlZCA+IC50aW1lbGluZS1wYW5lbDpiZWZvcmUge1xuIFx0cmlnaHQ6IGF1dG87XG4gXHRsZWZ0OiAtMTVweDtcbiBcdGJvcmRlci1yaWdodC13aWR0aDogMTVweDtcbiBcdGJvcmRlci1sZWZ0LXdpZHRoOiAwO1xuIH1cbiAudGltZWxpbmUgPiBsaS50aW1lbGluZS1pbnZlcnRlZCA+IC50aW1lbGluZS1wYW5lbDphZnRlciB7XG4gXHRyaWdodDogYXV0bztcbiBcdGxlZnQ6IC0xNHB4O1xuIFx0Ym9yZGVyLXJpZ2h0LXdpZHRoOiAxNHB4O1xuIFx0Ym9yZGVyLWxlZnQtd2lkdGg6IDA7XG4gfVxuIC50aW1lbGluZS1iYWRnZS5wcmltYXJ5IHtcbiBcdGJhY2tncm91bmQtY29sb3I6ICMyZTZkYTQgIWltcG9ydGFudDtcbiB9XG4gLnRpbWVsaW5lLWJhZGdlLnN1Y2Nlc3Mge1xuIFx0YmFja2dyb3VuZC1jb2xvcjogIzNmOTAzZiAhaW1wb3J0YW50O1xuIH1cbiAudGltZWxpbmUtYmFkZ2Uud2FybmluZyB7XG4gXHRiYWNrZ3JvdW5kLWNvbG9yOiAjZjBhZDRlICFpbXBvcnRhbnQ7XG4gfVxuIC50aW1lbGluZS1iYWRnZS5kYW5nZXIge1xuIFx0YmFja2dyb3VuZC1jb2xvcjogI2Q5NTM0ZiAhaW1wb3J0YW50O1xuIH1cbiAudGltZWxpbmUtYmFkZ2UuaW5mbyB7XG4gXHRiYWNrZ3JvdW5kLWNvbG9yOiAjNWJjMGRlICFpbXBvcnRhbnQ7XG4gfVxuIC50aW1lbGluZS10aXRsZSB7XG4gXHRtYXJnaW4tdG9wOiAwO1xuIFx0Y29sb3I6IGluaGVyaXQ7XG4gfVxuIC50aW1lbGluZS1ib2R5ID4gcCxcbiAudGltZWxpbmUtYm9keSA+IHVsIHtcbiBcdG1hcmdpbi1ib3R0b206IDA7XG4gfVxuIC50aW1lbGluZS1ib2R5ID4gcCArIHAge1xuIFx0bWFyZ2luLXRvcDogNXB4O1xuIH1cbiBAbWVkaWEgKG1heC13aWR0aDogNzY3cHgpIHtcbiBcdHVsLnRpbWVsaW5lOmJlZm9yZSB7XG4gXHRcdGxlZnQ6IDQwcHg7XG4gXHR9XG4gXHR1bC50aW1lbGluZSA+IGxpID4gLnRpbWVsaW5lLXBhbmVsIHtcbiBcdFx0d2lkdGg6IGNhbGMoMTAlKTtcbiBcdFx0d2lkdGg6IC1tb3otY2FsYygxMCUpO1xuIFx0XHR3aWR0aDogLXdlYmtpdC1jYWxjKDEwJSk7XG4gXHR9XG4gXHR1bC50aW1lbGluZSA+IGxpID4gLnRpbWVsaW5lLWJhZGdlIHtcbiBcdFx0dG9wOiAxNnB4O1xuIFx0XHRsZWZ0OiAxNXB4O1xuIFx0XHRtYXJnaW4tbGVmdDogMDtcbiBcdH1cbiBcdHVsLnRpbWVsaW5lID4gbGkgPiAudGltZWxpbmUtcGFuZWwge1xuIFx0XHRmbG9hdDogcmlnaHQ7XG4gXHR9XG4gXHR1bC50aW1lbGluZSA+IGxpID4gLnRpbWVsaW5lLXBhbmVsOmJlZm9yZSB7XG4gXHRcdHJpZ2h0OiBhdXRvO1xuIFx0XHRsZWZ0OiAtMTVweDtcbiBcdFx0Ym9yZGVyLXJpZ2h0LXdpZHRoOiAxNXB4O1xuIFx0XHRib3JkZXItbGVmdC13aWR0aDogMDtcbiBcdH1cbiBcdHVsLnRpbWVsaW5lID4gbGkgPiAudGltZWxpbmUtcGFuZWw6YWZ0ZXIge1xuIFx0XHRyaWdodDogYXV0bztcbiBcdFx0bGVmdDogLTE0cHg7XG4gXHRcdGJvcmRlci1yaWdodC13aWR0aDogMTRweDtcbiBcdFx0Ym9yZGVyLWxlZnQtd2lkdGg6IDA7XG4gXHR9XG4gfVxuXG4gXG4gPC9zdHlsZT5cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyByZXNvdXJjZXMvanMvdnVlL2F1dGgvbG9naW4udnVlPzMxNTJhYWU4IiwidmFyIHJlbmRlciA9IGZ1bmN0aW9uKCkge1xuICB2YXIgX3ZtID0gdGhpc1xuICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gIHJldHVybiBfYyhcImRpdlwiLCB7IGF0dHJzOiB7IGlkOiBcImF1dGgtbG9naW4tY29tcG9uZW50XCIgfSB9LCBbXG4gICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb250YWluZXJcIiB9LCBbXG4gICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcInJvd1wiIH0sIFtcbiAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtbWQtNCBjb2wtbWQtb2Zmc2V0LTRcIiB9LCBbXG4gICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJsb2dpbi1wYW5lbCBwYW5lbCBwYW5lbC1kZWZhdWx0XCIgfSwgW1xuICAgICAgICAgICAgX3ZtLl9tKDApLFxuICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwicGFuZWwtYm9keVwiIH0sIFtcbiAgICAgICAgICAgICAgX2MoXCJmb3JtXCIsIHsgYXR0cnM6IHsgcm9sZTogXCJmb3JtXCIgfSB9LCBbXG4gICAgICAgICAgICAgICAgX2MoXCJmaWVsZHNldFwiLCBbXG4gICAgICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgIF9jKFwiaW5wdXRcIiwge1xuICAgICAgICAgICAgICAgICAgICAgIGRpcmVjdGl2ZXM6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IF92bS5mb3JtLnVzZXJuYW1lLFxuICAgICAgICAgICAgICAgICAgICAgICAgICBleHByZXNzaW9uOiBcImZvcm0udXNlcm5hbWVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBcIkUtbWFpbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogXCJlbWFpbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJlbWFpbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgYXV0b2ZvY3VzOiBcIlwiXG4gICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICBkb21Qcm9wczogeyB2YWx1ZTogX3ZtLmZvcm0udXNlcm5hbWUgfSxcbiAgICAgICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgaW5wdXQ6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uJHNldChfdm0uZm9ybSwgXCJ1c2VybmFtZVwiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJpbnB1dFwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgZGlyZWN0aXZlczogW1xuICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogX3ZtLmZvcm0ucGFzc3dvcmQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgIGV4cHJlc3Npb246IFwiZm9ybS5wYXNzd29yZFwiXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICAgICAgICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6IFwiUGFzc3dvcmRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IFwicGFzc3dvcmRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IFwicGFzc3dvcmRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBcIlwiXG4gICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICBkb21Qcm9wczogeyB2YWx1ZTogX3ZtLmZvcm0ucGFzc3dvcmQgfSxcbiAgICAgICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgaW5wdXQ6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uJHNldChfdm0uZm9ybSwgXCJwYXNzd29yZFwiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgIF92bS5fbSgxKSxcbiAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgXCJhXCIsXG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWxnIGJ0bi1zdWNjZXNzIGJ0bi1ibG9ja1wiLFxuICAgICAgICAgICAgICAgICAgICAgIG9uOiB7IGNsaWNrOiBfdm0ubG9naW4gfVxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBbX3ZtLl92KFwiTG9naW5cIildXG4gICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgIF0pXG4gICAgICAgICAgXSlcbiAgICAgICAgXSlcbiAgICAgIF0pXG4gICAgXSlcbiAgXSlcbn1cbnZhciBzdGF0aWNSZW5kZXJGbnMgPSBbXG4gIGZ1bmN0aW9uKCkge1xuICAgIHZhciBfdm0gPSB0aGlzXG4gICAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gICAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gICAgcmV0dXJuIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwicGFuZWwtaGVhZGluZ1wiIH0sIFtcbiAgICAgIF9jKFwiaDNcIiwgeyBzdGF0aWNDbGFzczogXCJwYW5lbC10aXRsZVwiIH0sIFtfdm0uX3YoXCJQbGVhc2UgU2lnbiBJblwiKV0pXG4gICAgXSlcbiAgfSxcbiAgZnVuY3Rpb24oKSB7XG4gICAgdmFyIF92bSA9IHRoaXNcbiAgICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgICByZXR1cm4gX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjaGVja2JveFwiIH0sIFtcbiAgICAgIF9jKFwibGFiZWxcIiwgW1xuICAgICAgICBfYyhcImlucHV0XCIsIHtcbiAgICAgICAgICBhdHRyczogeyBuYW1lOiBcInJlbWVtYmVyXCIsIHR5cGU6IFwiY2hlY2tib3hcIiwgdmFsdWU6IFwiUmVtZW1iZXIgTWVcIiB9XG4gICAgICAgIH0pLFxuICAgICAgICBfdm0uX3YoXCJSZW1lbWJlciBNZVxcblxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdFxcdFwiKVxuICAgICAgXSlcbiAgICBdKVxuICB9XG5dXG5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbm1vZHVsZS5leHBvcnRzID0geyByZW5kZXI6IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnMgfVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKSAgICAgIC5yZXJlbmRlcihcImRhdGEtdi0zYTNkYjUyZVwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtM2EzZGI1MmVcIixcImhhc1Njb3BlZFwiOmZhbHNlLFwiYnVibGVcIjp7XCJ0cmFuc2Zvcm1zXCI6e319fSEuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAmYnVzdENhY2hlIS4vcmVzb3VyY2VzL2pzL3Z1ZS9hdXRoL2xvZ2luLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNjFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiLy8gc3R5bGUtbG9hZGVyOiBBZGRzIHNvbWUgY3NzIHRvIHRoZSBET00gYnkgYWRkaW5nIGEgPHN0eWxlPiB0YWdcblxuLy8gbG9hZCB0aGUgc3R5bGVzXG52YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJ2dWVcXFwiOnRydWUsXFxcImlkXFxcIjpcXFwiZGF0YS12LTIxOTk0MmU2XFxcIixcXFwic2NvcGVkXFxcIjpmYWxzZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCZidXN0Q2FjaGUhLi9hZG1pbi52dWVcIik7XG5pZih0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbmlmKGNvbnRlbnQubG9jYWxzKSBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzO1xuLy8gYWRkIHRoZSBzdHlsZXMgdG8gdGhlIERPTVxudmFyIHVwZGF0ZSA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlc0NsaWVudC5qc1wiKShcIjVhMmU1Y2FlXCIsIGNvbnRlbnQsIGZhbHNlKTtcbi8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbmlmKG1vZHVsZS5ob3QpIHtcbiAvLyBXaGVuIHRoZSBzdHlsZXMgY2hhbmdlLCB1cGRhdGUgdGhlIDxzdHlsZT4gdGFnc1xuIGlmKCFjb250ZW50LmxvY2Fscykge1xuICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJ2dWVcXFwiOnRydWUsXFxcImlkXFxcIjpcXFwiZGF0YS12LTIxOTk0MmU2XFxcIixcXFwic2NvcGVkXFxcIjpmYWxzZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCZidXN0Q2FjaGUhLi9hZG1pbi52dWVcIiwgZnVuY3Rpb24oKSB7XG4gICAgIHZhciBuZXdDb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcInZ1ZVxcXCI6dHJ1ZSxcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMjE5OTQyZTZcXFwiLFxcXCJzY29wZWRcXFwiOmZhbHNlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wJmJ1c3RDYWNoZSEuL2FkbWluLnZ1ZVwiKTtcbiAgICAgaWYodHlwZW9mIG5ld0NvbnRlbnQgPT09ICdzdHJpbmcnKSBuZXdDb250ZW50ID0gW1ttb2R1bGUuaWQsIG5ld0NvbnRlbnQsICcnXV07XG4gICAgIHVwZGF0ZShuZXdDb250ZW50KTtcbiAgIH0pO1xuIH1cbiAvLyBXaGVuIHRoZSBtb2R1bGUgaXMgZGlzcG9zZWQsIHJlbW92ZSB0aGUgPHN0eWxlPiB0YWdzXG4gbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyB1cGRhdGUoKTsgfSk7XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvdnVlLXN0eWxlLWxvYWRlciEuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyP3NvdXJjZU1hcCEuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJ2dWVcIjp0cnVlLFwiaWRcIjpcImRhdGEtdi0yMTk5NDJlNlwiLFwic2NvcGVkXCI6ZmFsc2UsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wJmJ1c3RDYWNoZSEuL3Jlc291cmNlcy9qcy92dWUvbGF5b3V0cy9hZG1pbi52dWVcbi8vIG1vZHVsZSBpZCA9IDYyXG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJcXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W10sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIlwiLFwiZmlsZVwiOlwiYWRtaW4udnVlXCIsXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXI/e1widnVlXCI6dHJ1ZSxcImlkXCI6XCJkYXRhLXYtMjE5OTQyZTZcIixcInNjb3BlZFwiOmZhbHNlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCZidXN0Q2FjaGUhLi9yZXNvdXJjZXMvanMvdnVlL2xheW91dHMvYWRtaW4udnVlXG4vLyBtb2R1bGUgaWQgPSA2M1xuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCI8dGVtcGxhdGU+XG4gICBcblxuPGRpdiBjbGFzcz1cImxheW91dHMtYWRtaW5cIj5cblxuXG5cbiAgIDxuYXYgY2xhc3M9XCJuYXZiYXIgbmF2YmFyLWRlZmF1bHQgbmF2YmFyLXN0YXRpYy10b3BcIj5cbiAgICA8ZGl2IGNsYXNzPVwiY29udGFpbmVyLWZsdWlkXCI+XG5cdFx0PCEtLSBCcmFuZCBhbmQgdG9nZ2xlIGdldCBncm91cGVkIGZvciBiZXR0ZXIgbW9iaWxlIGRpc3BsYXkgLS0+XG5cdFx0PGRpdiBjbGFzcz1cIm5hdmJhci1oZWFkZXJcIj5cblx0XHRcdDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwibmF2YmFyLXRvZ2dsZSBuYXZiYXItdG9nZ2xlLXNpZGViYXIgY29sbGFwc2VkXCI+XG5cdFx0XHRNRU5VXG5cdFx0XHQ8L2J1dHRvbj5cblx0XHRcdDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwibmF2YmFyLXRvZ2dsZSBjb2xsYXBzZWRcIiBkYXRhLXRvZ2dsZT1cImNvbGxhcHNlXCIgZGF0YS10YXJnZXQ9XCIjYnMtZXhhbXBsZS1uYXZiYXItY29sbGFwc2UtMVwiPlxuXHRcdFx0XHQ8c3BhbiBjbGFzcz1cInNyLW9ubHlcIj5Ub2dnbGUgbmF2aWdhdGlvbjwvc3Bhbj5cblx0XHRcdFx0PHNwYW4gY2xhc3M9XCJpY29uLWJhclwiPjwvc3Bhbj5cblx0XHRcdFx0PHNwYW4gY2xhc3M9XCJpY29uLWJhclwiPjwvc3Bhbj5cblx0XHRcdFx0PHNwYW4gY2xhc3M9XCJpY29uLWJhclwiPjwvc3Bhbj5cblx0XHRcdDwvYnV0dG9uPlxuXHRcdFx0PGEgY2xhc3M9XCJuYXZiYXItYnJhbmRcIiBocmVmPVwiI1wiPlxuXHRcdFx0XHR7eyB0aGlzLiRzdG9yZS5zdGF0ZS51c2VyLmZpcnN0X25hbWUgfX1cblx0XHRcdDwvYT5cblx0XHQ8L2Rpdj5cblxuXHRcdDwhLS0gQ29sbGVjdCB0aGUgbmF2IGxpbmtzLCBmb3JtcywgYW5kIG90aGVyIGNvbnRlbnQgZm9yIHRvZ2dsaW5nIC0tPlxuXHRcdDxkaXYgY2xhc3M9XCJjb2xsYXBzZSBuYXZiYXItY29sbGFwc2VcIiBpZD1cImJzLWV4YW1wbGUtbmF2YmFyLWNvbGxhcHNlLTFcIj4gICAgICBcblx0XHRcdDxmb3JtIGNsYXNzPVwibmF2YmFyLWZvcm0gbmF2YmFyLWxlZnRcIiBtZXRob2Q9XCJHRVRcIiByb2xlPVwic2VhcmNoXCI+XG5cdFx0XHRcdDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XG5cdFx0XHRcdFx0PGlucHV0IHR5cGU9XCJ0ZXh0XCIgbmFtZT1cInFcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIHBsYWNlaG9sZGVyPVwiU2VhcmNoXCI+XG5cdFx0XHRcdDwvZGl2PlxuXHRcdFx0XHQ8YnV0dG9uIHR5cGU9XCJzdWJtaXRcIiBjbGFzcz1cImJ0biBidG4tZGVmYXVsdFwiPjxpIGNsYXNzPVwiZ2x5cGhpY29uIGdseXBoaWNvbi1zZWFyY2hcIj48L2k+PC9idXR0b24+XG5cdFx0XHQ8L2Zvcm0+XG5cdFx0XHQ8dWwgY2xhc3M9XCJuYXYgbmF2YmFyLW5hdiBuYXZiYXItcmlnaHRcIj5cblx0XHRcdFx0PGxpPlxuXHRcdFx0XHRcdDxyb3V0ZXItbGluayB0bz0nLyc+SG9tZTwvcm91dGVyLWxpbms+XG5cdFx0XHRcdDwvbGk+XG5cdFx0XHRcdDxsaSBjbGFzcz1cImRyb3Bkb3duIFwiPlxuXHRcdFx0XHRcdDxhIGhyZWY9XCIjXCIgY2xhc3M9XCJkcm9wZG93bi10b2dnbGVcIiBkYXRhLXRvZ2dsZT1cImRyb3Bkb3duXCIgcm9sZT1cImJ1dHRvblwiIGFyaWEtZXhwYW5kZWQ9XCJmYWxzZVwiPlxuXHRcdFx0XHRcdFx0QWNjb3VudFxuXHRcdFx0XHRcdFx0PHNwYW4gY2xhc3M9XCJjYXJldFwiPjwvc3Bhbj48L2E+XG5cdFx0XHRcdFx0XHQ8dWwgY2xhc3M9XCJkcm9wZG93bi1tZW51XCIgcm9sZT1cIm1lbnVcIj5cblx0XHRcdFx0XHRcdFx0PGxpIGNsYXNzPVwiZHJvcGRvd24taGVhZGVyXCI+U0VUVElOR1M8L2xpPlxuXHRcdFx0XHRcdFx0XHQ8bGkgY2xhc3M9XCJcIj5cblx0XHRcdFx0XHRcdFx0XHQ8cm91dGVyLWxpbmsgdG89J3Byb2ZpbGUnPlByb2ZpbGU8L3JvdXRlci1saW5rPlxuXHRcdFx0XHRcdFx0XHQ8L2xpPlxuXHRcdFx0XHRcdFx0XHQ8bGkgY2xhc3M9XCJkaXZpZGVyXCI+PC9saT5cblx0XHRcdFx0XHRcdFx0PGxpPjxhIHYtb246Y2xpY2s9XCJsb2dvdXRcIj5Mb2dvdXQ8L2E+PC9saT5cblx0XHRcdFx0XHRcdDwvdWw+XG5cdFx0XHRcdFx0PC9saT5cblx0XHRcdFx0PC91bD5cblx0XHRcdDwvZGl2PjwhLS0gLy5uYXZiYXItY29sbGFwc2UgLS0+XG5cdFx0PC9kaXY+PCEtLSAvLmNvbnRhaW5lci1mbHVpZCAtLT5cblx0PC9uYXY+ICBcdDxkaXYgY2xhc3M9XCJjb250YWluZXItZmx1aWQgbWFpbi1jb250YWluZXJcIj5cbiAgXHRcdDxkaXYgY2xhc3M9XCJjb2wtbWQtMiBzaWRlYmFyXCI+XG4gIFx0XHRcdDxkaXYgY2xhc3M9XCJyb3dcIj5cblx0PCEtLSB1bmNvbW1lbnQgY29kZSBmb3IgYWJzb2x1dGUgcG9zaXRpb25pbmcgdHdlZWsgc2VlIHRvcCBjb21tZW50IGluIGNzcyAtLT5cblx0PGRpdiBjbGFzcz1cImFic29sdXRlLXdyYXBwZXJcIj4gPC9kaXY+XG5cdDwhLS0gTWVudSAtLT5cblx0PGRpdiBjbGFzcz1cInNpZGUtbWVudVwiPlxuXHRcdDxuYXYgY2xhc3M9XCJuYXZiYXIgbmF2YmFyLWRlZmF1bHRcIiByb2xlPVwibmF2aWdhdGlvblwiPlxuXHRcdFx0PCEtLSBNYWluIE1lbnUgLS0+XG5cdFx0XHQ8ZGl2IGNsYXNzPVwic2lkZS1tZW51LWNvbnRhaW5lclwiPlxuXHRcdFx0XHQ8dWwgY2xhc3M9XCJuYXYgbmF2YmFyLW5hdlwiPlxuXHRcdFx0XHRcdDxsaT5cblx0XHRcdFx0XHRcdDxyb3V0ZXItbGluayB0bz0nZGFzaGJvYXJkJz48c3BhbiBjbGFzcz1cImdseXBoaWNvbiBnbHlwaGljb24tZGFzaGJvYXJkXCI+PC9zcGFuPiBEYXNoYm9hcmQ8L3JvdXRlci1saW5rPlxuXHRcdFx0XHRcdDwvbGk+XG5cdFx0XHRcdFx0PGxpPlxuXHRcdFx0XHRcdFx0PHJvdXRlci1saW5rIHRvPSd1c2Vycyc+PHNwYW4gY2xhc3M9XCJnbHlwaGljb24gZ2x5cGhpY29uLXVzZXJcIj48L3NwYW4+IFVzZXJzPC9yb3V0ZXItbGluaz5cblx0XHRcdFxuXHRcdFx0XHRcdDwvbGk+XG5cdFx0XHRcdFx0PGxpPjxhIGhyZWY9XCIjXCI+PHNwYW4gY2xhc3M9XCJnbHlwaGljb24gZ2x5cGhpY29uLWNsb3VkXCI+PC9zcGFuPiBMaW5rPC9hPjwvbGk+XG5cblx0XHRcdFx0XHQ8IS0tIERyb3Bkb3duLS0+XG5cdFx0XHRcdFx0PGxpIGNsYXNzPVwicGFuZWwgcGFuZWwtZGVmYXVsdFwiIGlkPVwiZHJvcGRvd25cIj5cblx0XHRcdFx0XHRcdDxhIGRhdGEtdG9nZ2xlPVwiY29sbGFwc2VcIiBocmVmPVwiI2Ryb3Bkb3duLWx2bDFcIj5cblx0XHRcdFx0XHRcdFx0PHNwYW4gY2xhc3M9XCJnbHlwaGljb24gZ2x5cGhpY29uLXVzZXJcIj48L3NwYW4+IFN1YiBMZXZlbCA8c3BhbiBjbGFzcz1cImNhcmV0XCI+PC9zcGFuPlxuXHRcdFx0XHRcdFx0PC9hPlxuXG5cdFx0XHRcdFx0XHQ8IS0tIERyb3Bkb3duIGxldmVsIDEgLS0+XG5cdFx0XHRcdFx0XHQ8ZGl2IGlkPVwiZHJvcGRvd24tbHZsMVwiIGNsYXNzPVwicGFuZWwtY29sbGFwc2UgY29sbGFwc2VcIj5cblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cInBhbmVsLWJvZHlcIj5cblx0XHRcdFx0XHRcdFx0XHQ8dWwgY2xhc3M9XCJuYXYgbmF2YmFyLW5hdlwiPlxuXHRcdFx0XHRcdFx0XHRcdFx0PGxpPjxhIGhyZWY9XCIjXCI+TGluazwvYT48L2xpPlxuXHRcdFx0XHRcdFx0XHRcdFx0PGxpPjxhIGhyZWY9XCIjXCI+TGluazwvYT48L2xpPlxuXHRcdFx0XHRcdFx0XHRcdFx0PGxpPjxhIGhyZWY9XCIjXCI+TGluazwvYT48L2xpPlxuXG5cdFx0XHRcdFx0XHRcdFx0XHQ8IS0tIERyb3Bkb3duIGxldmVsIDIgLS0+XG5cdFx0XHRcdFx0XHRcdFx0XHQ8bGkgY2xhc3M9XCJwYW5lbCBwYW5lbC1kZWZhdWx0XCIgaWQ9XCJkcm9wZG93blwiPlxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8YSBkYXRhLXRvZ2dsZT1cImNvbGxhcHNlXCIgaHJlZj1cIiNkcm9wZG93bi1sdmwyXCI+XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PHNwYW4gY2xhc3M9XCJnbHlwaGljb24gZ2x5cGhpY29uLW9mZlwiPjwvc3Bhbj4gU3ViIExldmVsIDxzcGFuIGNsYXNzPVwiY2FyZXRcIj48L3NwYW4+XG5cdFx0XHRcdFx0XHRcdFx0XHRcdDwvYT5cblx0XHRcdFx0XHRcdFx0XHRcdFx0PGRpdiBpZD1cImRyb3Bkb3duLWx2bDJcIiBjbGFzcz1cInBhbmVsLWNvbGxhcHNlIGNvbGxhcHNlXCI+XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cInBhbmVsLWJvZHlcIj5cblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDx1bCBjbGFzcz1cIm5hdiBuYXZiYXItbmF2XCI+XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdDxsaT48YSBocmVmPVwiI1wiPkxpbms8L2E+PC9saT5cblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0PGxpPjxhIGhyZWY9XCIjXCI+TGluazwvYT48L2xpPlxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8bGk+PGEgaHJlZj1cIiNcIj5MaW5rPC9hPjwvbGk+XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQ8L3VsPlxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cblx0XHRcdFx0XHRcdFx0XHRcdDwvbGk+XG5cdFx0XHRcdFx0XHRcdFx0PC91bD5cblx0XHRcdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdFx0XHQ8L2Rpdj5cblx0XHRcdFx0XHQ8L2xpPlxuXG5cdFx0XHRcdFx0PGxpPjxhIGhyZWY9XCIjXCI+PHNwYW4gY2xhc3M9XCJnbHlwaGljb24gZ2x5cGhpY29uLXNpZ25hbFwiPjwvc3Bhbj4gTGluazwvYT48L2xpPlxuXG5cdFx0XHRcdDwvdWw+XG5cdFx0XHQ8L2Rpdj48IS0tIC8ubmF2YmFyLWNvbGxhcHNlIC0tPlxuXHRcdDwvbmF2PlxuXG5cdDwvZGl2PlxuPC9kaXY+ICBcdFx0PC9kaXY+XG4gIFx0XHQ8ZGl2IGNsYXNzPVwiY29sLW1kLTEwIGNvbnRlbnRcIj5cbiAgXHRcdFx0PHJvdXRlci12aWV3Pjwvcm91dGVyLXZpZXc+XG4gIFx0XG4gIFx0XHQ8L2Rpdj5cbiAgXHRcdDxmb290ZXIgY2xhc3M9XCJwdWxsLWxlZnQgZm9vdGVyXCI+XG4gIFx0XHRcdDxwIGNsYXNzPVwiY29sLW1kLTEyXCI+XG4gIFx0XHRcdFx0PGhyIGNsYXNzPVwiZGl2aWRlclwiPlxuICBcdFx0XHRcdENvcHlyaWdodCAmQ09QWTsgMjAxNSA8YSBocmVmPVwiaHR0cDovL3d3dy5waW5ncG9uZy1sYWJzLmNvbVwiPkdyYXZpdGFubzwvYT5cbiAgXHRcdFx0PC9wPlxuICBcdFx0PC9mb290ZXI+XG4gIFx0PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgIFxuICBcblxuPC9kaXY+XG5cblxuPC90ZW1wbGF0ZT5cblxuXG5cbjxzY3JpcHQ+XG5leHBvcnQgZGVmYXVsdCB7XG5cblx0ZGF0YSgpe1xuXHRcdHJldHVybiB7fVxuICAgIH0sXG4gICAgY3JlYXRlZCgpe1xuICAgIH0sXG4gICAgbWV0aG9kczoge1xuICAgIFx0bG9nb3V0KCl7XG4gICAgICAgICAgICBsZXQgdGhhdCA9IHRoaXM7XG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSgnYWNjZXNzX3Rva2VuJyk7XG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSgncmVmcmVzaF90b2tlbicpO1xuICAgICAgICAgICAgdGhhdC4kc3RvcmUuc3RhdGUudXNlciA9IHt9O1xuICAgICAgICAgICAgdGhhdC4kc3RvcmUuc3RhdGUuaXNBdXRoZW50aWNhdGVkID0gZmFsc2U7XG4gICAgICAgICAgICB0aGF0LiRzdG9yZS5zdGF0ZS5hY2Nlc3NfdG9rZW4gPSBudWxsO1xuICAgICAgICAgICAgdGhhdC4kcm91dGVyLnB1c2goJy9sb2dpbicpO1xuICAgICAgICB9XG4gICAgfVxuXG5cbn07PC9zY3JpcHQ+XG5cblxuPHN0eWxlIHR5cGU9XCJ0ZXh0L2Nzc1wiIG1lZGlhPVwic2NyZWVuXCI+XG5cblxuPC9zdHlsZT5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gcmVzb3VyY2VzL2pzL3Z1ZS9sYXlvdXRzL2FkbWluLnZ1ZT84ZjE4N2U1NCIsInZhciByZW5kZXIgPSBmdW5jdGlvbigpIHtcbiAgdmFyIF92bSA9IHRoaXNcbiAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICByZXR1cm4gX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJsYXlvdXRzLWFkbWluXCIgfSwgW1xuICAgIF9jKFwibmF2XCIsIHsgc3RhdGljQ2xhc3M6IFwibmF2YmFyIG5hdmJhci1kZWZhdWx0IG5hdmJhci1zdGF0aWMtdG9wXCIgfSwgW1xuICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb250YWluZXItZmx1aWRcIiB9LCBbXG4gICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwibmF2YmFyLWhlYWRlclwiIH0sIFtcbiAgICAgICAgICBfYyhcbiAgICAgICAgICAgIFwiYnV0dG9uXCIsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcIm5hdmJhci10b2dnbGUgbmF2YmFyLXRvZ2dsZS1zaWRlYmFyIGNvbGxhcHNlZFwiLFxuICAgICAgICAgICAgICBhdHRyczogeyB0eXBlOiBcImJ1dHRvblwiIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBbX3ZtLl92KFwiXFxuXFx0XFx0XFx0TUVOVVxcblxcdFxcdFxcdFwiKV1cbiAgICAgICAgICApLFxuICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgX3ZtLl9tKDApLFxuICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgX2MoXCJhXCIsIHsgc3RhdGljQ2xhc3M6IFwibmF2YmFyLWJyYW5kXCIsIGF0dHJzOiB7IGhyZWY6IFwiI1wiIH0gfSwgW1xuICAgICAgICAgICAgX3ZtLl92KFxuICAgICAgICAgICAgICBcIlxcblxcdFxcdFxcdFxcdFwiICtcbiAgICAgICAgICAgICAgICBfdm0uX3ModGhpcy4kc3RvcmUuc3RhdGUudXNlci5maXJzdF9uYW1lKSArXG4gICAgICAgICAgICAgICAgXCJcXG5cXHRcXHRcXHRcIlxuICAgICAgICAgICAgKVxuICAgICAgICAgIF0pXG4gICAgICAgIF0pLFxuICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICBfYyhcbiAgICAgICAgICBcImRpdlwiLFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImNvbGxhcHNlIG5hdmJhci1jb2xsYXBzZVwiLFxuICAgICAgICAgICAgYXR0cnM6IHsgaWQ6IFwiYnMtZXhhbXBsZS1uYXZiYXItY29sbGFwc2UtMVwiIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIFtcbiAgICAgICAgICAgIF92bS5fbSgxKSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcInVsXCIsIHsgc3RhdGljQ2xhc3M6IFwibmF2IG5hdmJhci1uYXYgbmF2YmFyLXJpZ2h0XCIgfSwgW1xuICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICBcImxpXCIsXG4gICAgICAgICAgICAgICAgW19jKFwicm91dGVyLWxpbmtcIiwgeyBhdHRyczogeyB0bzogXCIvXCIgfSB9LCBbX3ZtLl92KFwiSG9tZVwiKV0pXSxcbiAgICAgICAgICAgICAgICAxXG4gICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgIF9jKFwibGlcIiwgeyBzdGF0aWNDbGFzczogXCJkcm9wZG93biBcIiB9LCBbXG4gICAgICAgICAgICAgICAgX3ZtLl9tKDIpLFxuICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICBcInVsXCIsXG4gICAgICAgICAgICAgICAgICB7IHN0YXRpY0NsYXNzOiBcImRyb3Bkb3duLW1lbnVcIiwgYXR0cnM6IHsgcm9sZTogXCJtZW51XCIgfSB9LFxuICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICBfYyhcImxpXCIsIHsgc3RhdGljQ2xhc3M6IFwiZHJvcGRvd24taGVhZGVyXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIlNFVFRJTkdTXCIpXG4gICAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICBcImxpXCIsXG4gICAgICAgICAgICAgICAgICAgICAge30sXG4gICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJyb3V0ZXItbGlua1wiLCB7IGF0dHJzOiB7IHRvOiBcInByb2ZpbGVcIiB9IH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiUHJvZmlsZVwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICAgICAgIDFcbiAgICAgICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJsaVwiLCB7IHN0YXRpY0NsYXNzOiBcImRpdmlkZXJcIiB9KSxcbiAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJsaVwiLCBbXG4gICAgICAgICAgICAgICAgICAgICAgX2MoXCJhXCIsIHsgb246IHsgY2xpY2s6IF92bS5sb2dvdXQgfSB9LCBbX3ZtLl92KFwiTG9nb3V0XCIpXSlcbiAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICBdKVxuICAgICAgICAgIF1cbiAgICAgICAgKVxuICAgICAgXSlcbiAgICBdKSxcbiAgICBfdm0uX3YoXCIgXCIpLFxuICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29udGFpbmVyLWZsdWlkIG1haW4tY29udGFpbmVyXCIgfSwgW1xuICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtbWQtMiBzaWRlYmFyXCIgfSwgW1xuICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcInJvd1wiIH0sIFtcbiAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImFic29sdXRlLXdyYXBwZXJcIiB9KSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwic2lkZS1tZW51XCIgfSwgW1xuICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgIFwibmF2XCIsXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJuYXZiYXIgbmF2YmFyLWRlZmF1bHRcIixcbiAgICAgICAgICAgICAgICBhdHRyczogeyByb2xlOiBcIm5hdmlnYXRpb25cIiB9XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcInNpZGUtbWVudS1jb250YWluZXJcIiB9LCBbXG4gICAgICAgICAgICAgICAgICBfYyhcInVsXCIsIHsgc3RhdGljQ2xhc3M6IFwibmF2IG5hdmJhci1uYXZcIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgIFwibGlcIixcbiAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICBfYyhcInJvdXRlci1saW5rXCIsIHsgYXR0cnM6IHsgdG86IFwiZGFzaGJvYXJkXCIgfSB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwic3BhblwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZ2x5cGhpY29uIGdseXBoaWNvbi1kYXNoYm9hcmRcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIERhc2hib2FyZFwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICAgICAgIDFcbiAgICAgICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgXCJsaVwiLFxuICAgICAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwicm91dGVyLWxpbmtcIiwgeyBhdHRyczogeyB0bzogXCJ1c2Vyc1wiIH0gfSwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcInNwYW5cIiwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImdseXBoaWNvbiBnbHlwaGljb24tdXNlclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgVXNlcnNcIilcbiAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICAgICAgICAxXG4gICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgIF92bS5fbSgzKSxcbiAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgX3ZtLl9tKDQpLFxuICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICBfdm0uX20oNSlcbiAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgKVxuICAgICAgICAgIF0pXG4gICAgICAgIF0pXG4gICAgICBdKSxcbiAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMCBjb250ZW50XCIgfSwgW19jKFwicm91dGVyLXZpZXdcIildLCAxKSxcbiAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICBfdm0uX20oNilcbiAgICBdKVxuICBdKVxufVxudmFyIHN0YXRpY1JlbmRlckZucyA9IFtcbiAgZnVuY3Rpb24oKSB7XG4gICAgdmFyIF92bSA9IHRoaXNcbiAgICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgICByZXR1cm4gX2MoXG4gICAgICBcImJ1dHRvblwiLFxuICAgICAge1xuICAgICAgICBzdGF0aWNDbGFzczogXCJuYXZiYXItdG9nZ2xlIGNvbGxhcHNlZFwiLFxuICAgICAgICBhdHRyczoge1xuICAgICAgICAgIHR5cGU6IFwiYnV0dG9uXCIsXG4gICAgICAgICAgXCJkYXRhLXRvZ2dsZVwiOiBcImNvbGxhcHNlXCIsXG4gICAgICAgICAgXCJkYXRhLXRhcmdldFwiOiBcIiNicy1leGFtcGxlLW5hdmJhci1jb2xsYXBzZS0xXCJcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIFtcbiAgICAgICAgX2MoXCJzcGFuXCIsIHsgc3RhdGljQ2xhc3M6IFwic3Itb25seVwiIH0sIFtfdm0uX3YoXCJUb2dnbGUgbmF2aWdhdGlvblwiKV0pLFxuICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICBfYyhcInNwYW5cIiwgeyBzdGF0aWNDbGFzczogXCJpY29uLWJhclwiIH0pLFxuICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICBfYyhcInNwYW5cIiwgeyBzdGF0aWNDbGFzczogXCJpY29uLWJhclwiIH0pLFxuICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICBfYyhcInNwYW5cIiwgeyBzdGF0aWNDbGFzczogXCJpY29uLWJhclwiIH0pXG4gICAgICBdXG4gICAgKVxuICB9LFxuICBmdW5jdGlvbigpIHtcbiAgICB2YXIgX3ZtID0gdGhpc1xuICAgIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICAgIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICAgIHJldHVybiBfYyhcbiAgICAgIFwiZm9ybVwiLFxuICAgICAge1xuICAgICAgICBzdGF0aWNDbGFzczogXCJuYXZiYXItZm9ybSBuYXZiYXItbGVmdFwiLFxuICAgICAgICBhdHRyczogeyBtZXRob2Q6IFwiR0VUXCIsIHJvbGU6IFwic2VhcmNoXCIgfVxuICAgICAgfSxcbiAgICAgIFtcbiAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCIgfSwgW1xuICAgICAgICAgIF9jKFwiaW5wdXRcIiwge1xuICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgICAgICAgICBhdHRyczogeyB0eXBlOiBcInRleHRcIiwgbmFtZTogXCJxXCIsIHBsYWNlaG9sZGVyOiBcIlNlYXJjaFwiIH1cbiAgICAgICAgICB9KVxuICAgICAgICBdKSxcbiAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgX2MoXG4gICAgICAgICAgXCJidXR0b25cIixcbiAgICAgICAgICB7IHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZGVmYXVsdFwiLCBhdHRyczogeyB0eXBlOiBcInN1Ym1pdFwiIH0gfSxcbiAgICAgICAgICBbX2MoXCJpXCIsIHsgc3RhdGljQ2xhc3M6IFwiZ2x5cGhpY29uIGdseXBoaWNvbi1zZWFyY2hcIiB9KV1cbiAgICAgICAgKVxuICAgICAgXVxuICAgIClcbiAgfSxcbiAgZnVuY3Rpb24oKSB7XG4gICAgdmFyIF92bSA9IHRoaXNcbiAgICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgICByZXR1cm4gX2MoXG4gICAgICBcImFcIixcbiAgICAgIHtcbiAgICAgICAgc3RhdGljQ2xhc3M6IFwiZHJvcGRvd24tdG9nZ2xlXCIsXG4gICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgaHJlZjogXCIjXCIsXG4gICAgICAgICAgXCJkYXRhLXRvZ2dsZVwiOiBcImRyb3Bkb3duXCIsXG4gICAgICAgICAgcm9sZTogXCJidXR0b25cIixcbiAgICAgICAgICBcImFyaWEtZXhwYW5kZWRcIjogXCJmYWxzZVwiXG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICBbXG4gICAgICAgIF92bS5fdihcIlxcblxcdFxcdFxcdFxcdFxcdFxcdEFjY291bnRcXG5cXHRcXHRcXHRcXHRcXHRcXHRcIiksXG4gICAgICAgIF9jKFwic3BhblwiLCB7IHN0YXRpY0NsYXNzOiBcImNhcmV0XCIgfSlcbiAgICAgIF1cbiAgICApXG4gIH0sXG4gIGZ1bmN0aW9uKCkge1xuICAgIHZhciBfdm0gPSB0aGlzXG4gICAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gICAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gICAgcmV0dXJuIF9jKFwibGlcIiwgW1xuICAgICAgX2MoXCJhXCIsIHsgYXR0cnM6IHsgaHJlZjogXCIjXCIgfSB9LCBbXG4gICAgICAgIF9jKFwic3BhblwiLCB7IHN0YXRpY0NsYXNzOiBcImdseXBoaWNvbiBnbHlwaGljb24tY2xvdWRcIiB9KSxcbiAgICAgICAgX3ZtLl92KFwiIExpbmtcIilcbiAgICAgIF0pXG4gICAgXSlcbiAgfSxcbiAgZnVuY3Rpb24oKSB7XG4gICAgdmFyIF92bSA9IHRoaXNcbiAgICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgICByZXR1cm4gX2MoXG4gICAgICBcImxpXCIsXG4gICAgICB7IHN0YXRpY0NsYXNzOiBcInBhbmVsIHBhbmVsLWRlZmF1bHRcIiwgYXR0cnM6IHsgaWQ6IFwiZHJvcGRvd25cIiB9IH0sXG4gICAgICBbXG4gICAgICAgIF9jKFxuICAgICAgICAgIFwiYVwiLFxuICAgICAgICAgIHsgYXR0cnM6IHsgXCJkYXRhLXRvZ2dsZVwiOiBcImNvbGxhcHNlXCIsIGhyZWY6IFwiI2Ryb3Bkb3duLWx2bDFcIiB9IH0sXG4gICAgICAgICAgW1xuICAgICAgICAgICAgX2MoXCJzcGFuXCIsIHsgc3RhdGljQ2xhc3M6IFwiZ2x5cGhpY29uIGdseXBoaWNvbi11c2VyXCIgfSksXG4gICAgICAgICAgICBfdm0uX3YoXCIgU3ViIExldmVsIFwiKSxcbiAgICAgICAgICAgIF9jKFwic3BhblwiLCB7IHN0YXRpY0NsYXNzOiBcImNhcmV0XCIgfSlcbiAgICAgICAgICBdXG4gICAgICAgICksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFxuICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAge1xuICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwicGFuZWwtY29sbGFwc2UgY29sbGFwc2VcIixcbiAgICAgICAgICAgIGF0dHJzOiB7IGlkOiBcImRyb3Bkb3duLWx2bDFcIiB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBbXG4gICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcInBhbmVsLWJvZHlcIiB9LCBbXG4gICAgICAgICAgICAgIF9jKFwidWxcIiwgeyBzdGF0aWNDbGFzczogXCJuYXYgbmF2YmFyLW5hdlwiIH0sIFtcbiAgICAgICAgICAgICAgICBfYyhcImxpXCIsIFtfYyhcImFcIiwgeyBhdHRyczogeyBocmVmOiBcIiNcIiB9IH0sIFtfdm0uX3YoXCJMaW5rXCIpXSldKSxcbiAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgIF9jKFwibGlcIiwgW19jKFwiYVwiLCB7IGF0dHJzOiB7IGhyZWY6IFwiI1wiIH0gfSwgW192bS5fdihcIkxpbmtcIildKV0pLFxuICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgX2MoXCJsaVwiLCBbX2MoXCJhXCIsIHsgYXR0cnM6IHsgaHJlZjogXCIjXCIgfSB9LCBbX3ZtLl92KFwiTGlua1wiKV0pXSksXG4gICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgIFwibGlcIixcbiAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwicGFuZWwgcGFuZWwtZGVmYXVsdFwiLFxuICAgICAgICAgICAgICAgICAgICBhdHRyczogeyBpZDogXCJkcm9wZG93blwiIH1cbiAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgIFwiYVwiLFxuICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIFwiZGF0YS10b2dnbGVcIjogXCJjb2xsYXBzZVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICBocmVmOiBcIiNkcm9wZG93bi1sdmwyXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwic3BhblwiLCB7IHN0YXRpY0NsYXNzOiBcImdseXBoaWNvbiBnbHlwaGljb24tb2ZmXCIgfSksXG4gICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgU3ViIExldmVsIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwic3BhblwiLCB7IHN0YXRpY0NsYXNzOiBcImNhcmV0XCIgfSlcbiAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwicGFuZWwtY29sbGFwc2UgY29sbGFwc2VcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7IGlkOiBcImRyb3Bkb3duLWx2bDJcIiB9XG4gICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcInBhbmVsLWJvZHlcIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwidWxcIiwgeyBzdGF0aWNDbGFzczogXCJuYXYgbmF2YmFyLW5hdlwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImxpXCIsIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiYVwiLCB7IGF0dHJzOiB7IGhyZWY6IFwiI1wiIH0gfSwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCJMaW5rXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJsaVwiLCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImFcIiwgeyBhdHRyczogeyBocmVmOiBcIiNcIiB9IH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiTGlua1wiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwibGlcIiwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJhXCIsIHsgYXR0cnM6IHsgaHJlZjogXCIjXCIgfSB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIkxpbmtcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgXSlcbiAgICAgICAgICBdXG4gICAgICAgIClcbiAgICAgIF1cbiAgICApXG4gIH0sXG4gIGZ1bmN0aW9uKCkge1xuICAgIHZhciBfdm0gPSB0aGlzXG4gICAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gICAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gICAgcmV0dXJuIF9jKFwibGlcIiwgW1xuICAgICAgX2MoXCJhXCIsIHsgYXR0cnM6IHsgaHJlZjogXCIjXCIgfSB9LCBbXG4gICAgICAgIF9jKFwic3BhblwiLCB7IHN0YXRpY0NsYXNzOiBcImdseXBoaWNvbiBnbHlwaGljb24tc2lnbmFsXCIgfSksXG4gICAgICAgIF92bS5fdihcIiBMaW5rXCIpXG4gICAgICBdKVxuICAgIF0pXG4gIH0sXG4gIGZ1bmN0aW9uKCkge1xuICAgIHZhciBfdm0gPSB0aGlzXG4gICAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gICAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gICAgcmV0dXJuIF9jKFwiZm9vdGVyXCIsIHsgc3RhdGljQ2xhc3M6IFwicHVsbC1sZWZ0IGZvb3RlclwiIH0sIFtcbiAgICAgIF9jKFwicFwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbC1tZC0xMlwiIH0pLFxuICAgICAgX2MoXCJoclwiLCB7IHN0YXRpY0NsYXNzOiBcImRpdmlkZXJcIiB9KSxcbiAgICAgIF92bS5fdihcIlxcbiAgXFx0XFx0XFx0XFx0Q29weXJpZ2h0IMKpIDIwMTUgXCIpLFxuICAgICAgX2MoXCJhXCIsIHsgYXR0cnM6IHsgaHJlZjogXCJodHRwOi8vd3d3LnBpbmdwb25nLWxhYnMuY29tXCIgfSB9LCBbXG4gICAgICAgIF92bS5fdihcIkdyYXZpdGFub1wiKVxuICAgICAgXSksXG4gICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgX2MoXCJwXCIpXG4gICAgXSlcbiAgfVxuXVxucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5tb2R1bGUuZXhwb3J0cyA9IHsgcmVuZGVyOiByZW5kZXIsIHN0YXRpY1JlbmRlckZuczogc3RhdGljUmVuZGVyRm5zIH1cbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikgICAgICAucmVyZW5kZXIoXCJkYXRhLXYtMjE5OTQyZTZcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTIxOTk0MmU2XCIsXCJoYXNTY29wZWRcIjpmYWxzZSxcImJ1YmxlXCI6e1widHJhbnNmb3Jtc1wiOnt9fX0hLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wJmJ1c3RDYWNoZSEuL3Jlc291cmNlcy9qcy92dWUvbGF5b3V0cy9hZG1pbi52dWVcbi8vIG1vZHVsZSBpZCA9IDY1XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsInZhciBkaXNwb3NlZCA9IGZhbHNlXG5mdW5jdGlvbiBpbmplY3RTdHlsZSAoc3NyQ29udGV4dCkge1xuICBpZiAoZGlzcG9zZWQpIHJldHVyblxuICByZXF1aXJlKFwiISF2dWUtc3R5bGUtbG9hZGVyIWNzcy1sb2FkZXI/c291cmNlTWFwIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleD97XFxcInZ1ZVxcXCI6dHJ1ZSxcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNjg0YTQ0NGFcXFwiLFxcXCJzY29wZWRcXFwiOmZhbHNlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXN0eWxlcyZpbmRleD0wJmJ1c3RDYWNoZSEuL2luZGV4LnZ1ZVwiKVxufVxudmFyIG5vcm1hbGl6ZUNvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpXG4vKiBzY3JpcHQgKi9cbnZhciBfX3Z1ZV9zY3JpcHRfXyA9IHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dLFxcXCJwbHVnaW5zXFxcIjpbXFxcInRyYW5zZm9ybS1vYmplY3QtcmVzdC1zcHJlYWRcXFwiXX0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAmYnVzdENhY2hlIS4vaW5kZXgudnVlXCIpXG4vKiB0ZW1wbGF0ZSAqL1xudmFyIF9fdnVlX3RlbXBsYXRlX18gPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi02ODRhNDQ0YVxcXCIsXFxcImhhc1Njb3BlZFxcXCI6ZmFsc2UsXFxcImJ1YmxlXFxcIjp7XFxcInRyYW5zZm9ybXNcXFwiOnt9fX0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCZidXN0Q2FjaGUhLi9pbmRleC52dWVcIilcbi8qIHRlbXBsYXRlIGZ1bmN0aW9uYWwgKi9cbiAgdmFyIF9fdnVlX3RlbXBsYXRlX2Z1bmN0aW9uYWxfXyA9IGZhbHNlXG4vKiBzdHlsZXMgKi9cbnZhciBfX3Z1ZV9zdHlsZXNfXyA9IGluamVjdFN0eWxlXG4vKiBzY29wZUlkICovXG52YXIgX192dWVfc2NvcGVJZF9fID0gbnVsbFxuLyogbW9kdWxlSWRlbnRpZmllciAoc2VydmVyIG9ubHkpICovXG52YXIgX192dWVfbW9kdWxlX2lkZW50aWZpZXJfXyA9IG51bGxcbnZhciBDb21wb25lbnQgPSBub3JtYWxpemVDb21wb25lbnQoXG4gIF9fdnVlX3NjcmlwdF9fLFxuICBfX3Z1ZV90ZW1wbGF0ZV9fLFxuICBfX3Z1ZV90ZW1wbGF0ZV9mdW5jdGlvbmFsX18sXG4gIF9fdnVlX3N0eWxlc19fLFxuICBfX3Z1ZV9zY29wZUlkX18sXG4gIF9fdnVlX21vZHVsZV9pZGVudGlmaWVyX19cbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwicmVzb3VyY2VzL2pzL3Z1ZS9kYXNoYm9hcmQvaW5kZXgudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHsgIHJldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleS5zdWJzdHIoMCwgMikgIT09IFwiX19cIn0pKSB7ICBjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi02ODRhNDQ0YVwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTY4NGE0NDRhXCIsIENvbXBvbmVudC5vcHRpb25zKVxuJyArICcgIH1cbiAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgZGlzcG9zZWQgPSB0cnVlXG4gIH0pXG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9qcy92dWUvZGFzaGJvYXJkL2luZGV4LnZ1ZVxuLy8gbW9kdWxlIGlkID0gNjZcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiLy8gc3R5bGUtbG9hZGVyOiBBZGRzIHNvbWUgY3NzIHRvIHRoZSBET00gYnkgYWRkaW5nIGEgPHN0eWxlPiB0YWdcblxuLy8gbG9hZCB0aGUgc3R5bGVzXG52YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJ2dWVcXFwiOnRydWUsXFxcImlkXFxcIjpcXFwiZGF0YS12LTY4NGE0NDRhXFxcIixcXFwic2NvcGVkXFxcIjpmYWxzZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCZidXN0Q2FjaGUhLi9pbmRleC52dWVcIik7XG5pZih0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbmlmKGNvbnRlbnQubG9jYWxzKSBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzO1xuLy8gYWRkIHRoZSBzdHlsZXMgdG8gdGhlIERPTVxudmFyIHVwZGF0ZSA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlc0NsaWVudC5qc1wiKShcIjY5ZjUzM2Q4XCIsIGNvbnRlbnQsIGZhbHNlKTtcbi8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbmlmKG1vZHVsZS5ob3QpIHtcbiAvLyBXaGVuIHRoZSBzdHlsZXMgY2hhbmdlLCB1cGRhdGUgdGhlIDxzdHlsZT4gdGFnc1xuIGlmKCFjb250ZW50LmxvY2Fscykge1xuICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJ2dWVcXFwiOnRydWUsXFxcImlkXFxcIjpcXFwiZGF0YS12LTY4NGE0NDRhXFxcIixcXFwic2NvcGVkXFxcIjpmYWxzZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCZidXN0Q2FjaGUhLi9pbmRleC52dWVcIiwgZnVuY3Rpb24oKSB7XG4gICAgIHZhciBuZXdDb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcInZ1ZVxcXCI6dHJ1ZSxcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNjg0YTQ0NGFcXFwiLFxcXCJzY29wZWRcXFwiOmZhbHNlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wJmJ1c3RDYWNoZSEuL2luZGV4LnZ1ZVwiKTtcbiAgICAgaWYodHlwZW9mIG5ld0NvbnRlbnQgPT09ICdzdHJpbmcnKSBuZXdDb250ZW50ID0gW1ttb2R1bGUuaWQsIG5ld0NvbnRlbnQsICcnXV07XG4gICAgIHVwZGF0ZShuZXdDb250ZW50KTtcbiAgIH0pO1xuIH1cbiAvLyBXaGVuIHRoZSBtb2R1bGUgaXMgZGlzcG9zZWQsIHJlbW92ZSB0aGUgPHN0eWxlPiB0YWdzXG4gbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyB1cGRhdGUoKTsgfSk7XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvdnVlLXN0eWxlLWxvYWRlciEuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyP3NvdXJjZU1hcCEuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJ2dWVcIjp0cnVlLFwiaWRcIjpcImRhdGEtdi02ODRhNDQ0YVwiLFwic2NvcGVkXCI6ZmFsc2UsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wJmJ1c3RDYWNoZSEuL3Jlc291cmNlcy9qcy92dWUvZGFzaGJvYXJkL2luZGV4LnZ1ZVxuLy8gbW9kdWxlIGlkID0gNjdcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKSh0cnVlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIlxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiXCIsXCJmaWxlXCI6XCJpbmRleC52dWVcIixcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuXG4vLyBleHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyP3NvdXJjZU1hcCEuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJ2dWVcIjp0cnVlLFwiaWRcIjpcImRhdGEtdi02ODRhNDQ0YVwiLFwic2NvcGVkXCI6ZmFsc2UsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wJmJ1c3RDYWNoZSEuL3Jlc291cmNlcy9qcy92dWUvZGFzaGJvYXJkL2luZGV4LnZ1ZVxuLy8gbW9kdWxlIGlkID0gNjhcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiPHNjcmlwdD5cblx0XG48L3NjcmlwdD5cbjx0ZW1wbGF0ZT5cblx0PGRpdiBpZD1cImFkbWluLWluZGV4LWNvbXBvbmVudFwiPlxuXHRcdFx0XHQgIDxkaXYgY2xhc3M9XCJwYW5lbCBwYW5lbC1kZWZhdWx0XCI+XG5cdDxkaXYgY2xhc3M9XCJwYW5lbC1oZWFkaW5nXCI+XG5cdFx0RGFzaGJvYXJkXG5cdDwvZGl2PlxuXHQ8ZGl2IGNsYXNzPVwicGFuZWwtYm9keVwiPlxuXHRcdCA8cD5Mb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzaWNpbmcgZWxpdC4gSXN0ZSBzYWVwZSBldCBxdWlzcXVhbSBuZXNjaXVudCBtYXhpbWUuPC9wPlxuXHQ8L2Rpdj5cbjwvZGl2PlxuXHRcdFxuXHQ8L2Rpdj5cblxuPC90ZW1wbGF0ZT5cblxuXG48c2NyaXB0PlxuZXhwb3J0IGRlZmF1bHQge1xuXG4gICAgZGF0YSgpe1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgdXNlcnM6IHt9XG4gICAgICAgIH1cbiAgICB9LFxuICAgIGNvbXB1dGVkOiB7fSxcblxuICAgIGNyZWF0ZWQoKXtcbiAgICAgICAgLy9ETyBTT01FVEhJTkcgSEVSRSBBVCBGSVJTVCBMT0FEXG4gICAgICAgIGxldCB0aGF0ID0gdGhpcztcbiAgICAgICAgYXhpb3MuZ2V0KCcvYXBpL3YxL3VzZXJzJylcbiAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgICAgICAgICAgIHRoYXQudXNlcnMgPSByZXNwb25zZS5kYXRhLmRhdGE7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLmNhdGNoKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgICAgICAgIH0pO1xuICAgIH0sXG4gICAgbWV0aG9kczoge1xuICAgICAgICBvblN1Ym1pdCgpe1xuICAgICAgICAgICAgbGV0IHRoYXQgPSB0aGlzO1xuICAgICAgICB9XG4gICAgfVxuXG5cbn07PC9zY3JpcHQ+XG48c3R5bGUgdHlwZT1cInRleHQvY3NzXCIgbWVkaWE9XCJzY3JlZW5cIj5cblx0XG48L3N0eWxlPlxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHJlc291cmNlcy9qcy92dWUvZGFzaGJvYXJkL2luZGV4LnZ1ZT9hZWYwMmFiYSIsInZhciByZW5kZXIgPSBmdW5jdGlvbigpIHtcbiAgdmFyIF92bSA9IHRoaXNcbiAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICByZXR1cm4gX3ZtLl9tKDApXG59XG52YXIgc3RhdGljUmVuZGVyRm5zID0gW1xuICBmdW5jdGlvbigpIHtcbiAgICB2YXIgX3ZtID0gdGhpc1xuICAgIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICAgIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICAgIHJldHVybiBfYyhcImRpdlwiLCB7IGF0dHJzOiB7IGlkOiBcImFkbWluLWluZGV4LWNvbXBvbmVudFwiIH0gfSwgW1xuICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJwYW5lbCBwYW5lbC1kZWZhdWx0XCIgfSwgW1xuICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcInBhbmVsLWhlYWRpbmdcIiB9LCBbXG4gICAgICAgICAgX3ZtLl92KFwiXFxuXFx0XFx0RGFzaGJvYXJkXFxuXFx0XCIpXG4gICAgICAgIF0pLFxuICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcInBhbmVsLWJvZHlcIiB9LCBbXG4gICAgICAgICAgX2MoXCJwXCIsIFtcbiAgICAgICAgICAgIF92bS5fdihcbiAgICAgICAgICAgICAgXCJMb3JlbSBpcHN1bSBkb2xvciBzaXQgYW1ldCwgY29uc2VjdGV0dXIgYWRpcGlzaWNpbmcgZWxpdC4gSXN0ZSBzYWVwZSBldCBxdWlzcXVhbSBuZXNjaXVudCBtYXhpbWUuXCJcbiAgICAgICAgICAgIClcbiAgICAgICAgICBdKVxuICAgICAgICBdKVxuICAgICAgXSlcbiAgICBdKVxuICB9XG5dXG5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbm1vZHVsZS5leHBvcnRzID0geyByZW5kZXI6IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnMgfVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKSAgICAgIC5yZXJlbmRlcihcImRhdGEtdi02ODRhNDQ0YVwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtNjg0YTQ0NGFcIixcImhhc1Njb3BlZFwiOmZhbHNlLFwiYnVibGVcIjp7XCJ0cmFuc2Zvcm1zXCI6e319fSEuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAmYnVzdENhY2hlIS4vcmVzb3VyY2VzL2pzL3Z1ZS9kYXNoYm9hcmQvaW5kZXgudnVlXG4vLyBtb2R1bGUgaWQgPSA3MFxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJ2YXIgZGlzcG9zZWQgPSBmYWxzZVxuZnVuY3Rpb24gaW5qZWN0U3R5bGUgKHNzckNvbnRleHQpIHtcbiAgaWYgKGRpc3Bvc2VkKSByZXR1cm5cbiAgcmVxdWlyZShcIiEhdnVlLXN0eWxlLWxvYWRlciFjc3MtbG9hZGVyP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXg/e1xcXCJ2dWVcXFwiOnRydWUsXFxcImlkXFxcIjpcXFwiZGF0YS12LTQzMDkzNjRmXFxcIixcXFwic2NvcGVkXFxcIjpmYWxzZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zdHlsZXMmaW5kZXg9MCZidXN0Q2FjaGUhLi9pbmRleC52dWVcIilcbn1cbnZhciBub3JtYWxpemVDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKVxuLyogc2NyaXB0ICovXG52YXIgX192dWVfc2NyaXB0X18gPSByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXSxcXFwicGx1Z2luc1xcXCI6W1xcXCJ0cmFuc2Zvcm0tb2JqZWN0LXJlc3Qtc3ByZWFkXFxcIl19IS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wJmJ1c3RDYWNoZSEuL2luZGV4LnZ1ZVwiKVxuLyogdGVtcGxhdGUgKi9cbnZhciBfX3Z1ZV90ZW1wbGF0ZV9fID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNDMwOTM2NGZcXFwiLFxcXCJoYXNTY29wZWRcXFwiOmZhbHNlLFxcXCJidWJsZVxcXCI6e1xcXCJ0cmFuc2Zvcm1zXFxcIjp7fX19IS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAmYnVzdENhY2hlIS4vaW5kZXgudnVlXCIpXG4vKiB0ZW1wbGF0ZSBmdW5jdGlvbmFsICovXG4gIHZhciBfX3Z1ZV90ZW1wbGF0ZV9mdW5jdGlvbmFsX18gPSBmYWxzZVxuLyogc3R5bGVzICovXG52YXIgX192dWVfc3R5bGVzX18gPSBpbmplY3RTdHlsZVxuLyogc2NvcGVJZCAqL1xudmFyIF9fdnVlX3Njb3BlSWRfXyA9IG51bGxcbi8qIG1vZHVsZUlkZW50aWZpZXIgKHNlcnZlciBvbmx5KSAqL1xudmFyIF9fdnVlX21vZHVsZV9pZGVudGlmaWVyX18gPSBudWxsXG52YXIgQ29tcG9uZW50ID0gbm9ybWFsaXplQ29tcG9uZW50KFxuICBfX3Z1ZV9zY3JpcHRfXyxcbiAgX192dWVfdGVtcGxhdGVfXyxcbiAgX192dWVfdGVtcGxhdGVfZnVuY3Rpb25hbF9fLFxuICBfX3Z1ZV9zdHlsZXNfXyxcbiAgX192dWVfc2NvcGVJZF9fLFxuICBfX3Z1ZV9tb2R1bGVfaWRlbnRpZmllcl9fXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcInJlc291cmNlcy9qcy92dWUvdXNlcnMvaW5kZXgudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHsgIHJldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleS5zdWJzdHIoMCwgMikgIT09IFwiX19cIn0pKSB7ICBjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi00MzA5MzY0ZlwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTQzMDkzNjRmXCIsIENvbXBvbmVudC5vcHRpb25zKVxuJyArICcgIH1cbiAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgZGlzcG9zZWQgPSB0cnVlXG4gIH0pXG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9qcy92dWUvdXNlcnMvaW5kZXgudnVlXG4vLyBtb2R1bGUgaWQgPSA3MVxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIvLyBzdHlsZS1sb2FkZXI6IEFkZHMgc29tZSBjc3MgdG8gdGhlIERPTSBieSBhZGRpbmcgYSA8c3R5bGU+IHRhZ1xuXG4vLyBsb2FkIHRoZSBzdHlsZXNcbnZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcInZ1ZVxcXCI6dHJ1ZSxcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNDMwOTM2NGZcXFwiLFxcXCJzY29wZWRcXFwiOmZhbHNlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wJmJ1c3RDYWNoZSEuL2luZGV4LnZ1ZVwiKTtcbmlmKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuaWYoY29udGVudC5sb2NhbHMpIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHM7XG4vLyBhZGQgdGhlIHN0eWxlcyB0byB0aGUgRE9NXG52YXIgdXBkYXRlID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLXN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzQ2xpZW50LmpzXCIpKFwiN2I3ZTlmMTVcIiwgY29udGVudCwgZmFsc2UpO1xuLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuaWYobW9kdWxlLmhvdCkge1xuIC8vIFdoZW4gdGhlIHN0eWxlcyBjaGFuZ2UsIHVwZGF0ZSB0aGUgPHN0eWxlPiB0YWdzXG4gaWYoIWNvbnRlbnQubG9jYWxzKSB7XG4gICBtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/c291cmNlTWFwIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlci9pbmRleC5qcz97XFxcInZ1ZVxcXCI6dHJ1ZSxcXFwiaWRcXFwiOlxcXCJkYXRhLXYtNDMwOTM2NGZcXFwiLFxcXCJzY29wZWRcXFwiOmZhbHNlLFxcXCJoYXNJbmxpbmVDb25maWdcXFwiOnRydWV9IS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wJmJ1c3RDYWNoZSEuL2luZGV4LnZ1ZVwiLCBmdW5jdGlvbigpIHtcbiAgICAgdmFyIG5ld0NvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwidnVlXFxcIjp0cnVlLFxcXCJpZFxcXCI6XFxcImRhdGEtdi00MzA5MzY0ZlxcXCIsXFxcInNjb3BlZFxcXCI6ZmFsc2UsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAmYnVzdENhY2hlIS4vaW5kZXgudnVlXCIpO1xuICAgICBpZih0eXBlb2YgbmV3Q29udGVudCA9PT0gJ3N0cmluZycpIG5ld0NvbnRlbnQgPSBbW21vZHVsZS5pZCwgbmV3Q29udGVudCwgJyddXTtcbiAgICAgdXBkYXRlKG5ld0NvbnRlbnQpO1xuICAgfSk7XG4gfVxuIC8vIFdoZW4gdGhlIG1vZHVsZSBpcyBkaXNwb3NlZCwgcmVtb3ZlIHRoZSA8c3R5bGU+IHRhZ3NcbiBtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHVwZGF0ZSgpOyB9KTtcbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy92dWUtc3R5bGUtbG9hZGVyIS4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXI/c291cmNlTWFwIS4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyP3tcInZ1ZVwiOnRydWUsXCJpZFwiOlwiZGF0YS12LTQzMDkzNjRmXCIsXCJzY29wZWRcIjpmYWxzZSxcImhhc0lubGluZUNvbmZpZ1wiOnRydWV9IS4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAmYnVzdENhY2hlIS4vcmVzb3VyY2VzL2pzL3Z1ZS91c2Vycy9pbmRleC52dWVcbi8vIG1vZHVsZSBpZCA9IDcyXG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJcXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W10sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIlwiLFwiZmlsZVwiOlwiaW5kZXgudnVlXCIsXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXI/e1widnVlXCI6dHJ1ZSxcImlkXCI6XCJkYXRhLXYtNDMwOTM2NGZcIixcInNjb3BlZFwiOmZhbHNlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCZidXN0Q2FjaGUhLi9yZXNvdXJjZXMvanMvdnVlL3VzZXJzL2luZGV4LnZ1ZVxuLy8gbW9kdWxlIGlkID0gNzNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiPHNjcmlwdD5cblx0XG48L3NjcmlwdD5cbjx0ZW1wbGF0ZT5cblx0PGRpdiBpZD1cImFkbWluLWluZGV4LWNvbXBvbmVudFwiPlxuXHRcdFx0XHQgIDxkaXYgY2xhc3M9XCJwYW5lbCBwYW5lbC1kZWZhdWx0XCI+XG5cdDxkaXYgY2xhc3M9XCJwYW5lbC1oZWFkaW5nXCI+XG5cdFx0VXNlcnNcblx0PC9kaXY+XG5cdDxkaXYgY2xhc3M9XCJwYW5lbC1ib2R5XCI+XG5cdFx0PHRhYmxlIGNsYXNzPVwidGFibGUgdGFibGUtYm9yZGVyZWQgdGFibGUtcmVzcG9uc2l2ZSB0YWJsZS1zdHJpcGVkXCI+XG5cdFx0XHQ8dGhlYWQ+XG5cdFx0XHRcdDx0cj5cblx0XHRcdFx0XHQ8dGg+TmFtZTwvdGg+XG5cdFx0XHRcdFx0PHRoPkVtYWlsPC90aD5cblx0XHRcdFx0PC90cj5cblx0XHRcdDwvdGhlYWQ+XG5cdFx0XHQ8dGJvZHk+XG5cdFx0XHRcdDx0ciB2LWZvcj1cInVzZXIgaW4gdXNlcnNcIj5cblx0XHRcdFx0XHQ8dGQ+e3sgdXNlci5uYW1lIH19PC90ZD5cblx0XHRcdFx0XHQ8dGQ+e3sgdXNlci5lbWFpbCB9fTwvdGQ+XG5cdFx0XHRcdDwvdHI+XG5cdFx0XHQ8L3Rib2R5PlxuXHRcdDwvdGFibGU+XG5cdDwvZGl2PlxuPC9kaXY+XG5cdFx0XG5cdDwvZGl2PlxuXG48L3RlbXBsYXRlPlxuXG5cbjxzY3JpcHQ+XG5leHBvcnQgZGVmYXVsdCB7XG5cbiAgICBkYXRhKCl7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICB1c2Vyczoge31cbiAgICAgICAgfVxuICAgIH0sXG4gICAgY29tcHV0ZWQ6IHt9LFxuXG4gICAgY3JlYXRlZCgpe1xuICAgICAgICAvL0RPIFNPTUVUSElORyBIRVJFIEFUIEZJUlNUIExPQURcbiAgICAgICAgbGV0IHRoYXQgPSB0aGlzO1xuICAgICAgICBheGlvcy5nZXQoJy9hcGkvdjEvdXNlcnMnKVxuICAgICAgICAgICAgLnRoZW4oZnVuY3Rpb24gKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgdGhhdC51c2VycyA9IHJlc3BvbnNlLmRhdGEuZGF0YTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAuY2F0Y2goZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgICAgICAgfSk7XG4gICAgfSxcbiAgICBtZXRob2RzOiB7XG4gICAgICAgIG9uU3VibWl0KCl7XG4gICAgICAgICAgICBsZXQgdGhhdCA9IHRoaXM7XG4gICAgICAgIH1cbiAgICB9XG5cblxufTs8L3NjcmlwdD5cbjxzdHlsZSB0eXBlPVwidGV4dC9jc3NcIiBtZWRpYT1cInNjcmVlblwiPlxuXHRcbjwvc3R5bGU+XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gcmVzb3VyY2VzL2pzL3Z1ZS91c2Vycy9pbmRleC52dWU/NjY0YzI5YzIiLCJ2YXIgcmVuZGVyID0gZnVuY3Rpb24oKSB7XG4gIHZhciBfdm0gPSB0aGlzXG4gIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgcmV0dXJuIF9jKFwiZGl2XCIsIHsgYXR0cnM6IHsgaWQ6IFwiYWRtaW4taW5kZXgtY29tcG9uZW50XCIgfSB9LCBbXG4gICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJwYW5lbCBwYW5lbC1kZWZhdWx0XCIgfSwgW1xuICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJwYW5lbC1oZWFkaW5nXCIgfSwgW192bS5fdihcIlxcblxcdFxcdFVzZXJzXFxuXFx0XCIpXSksXG4gICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJwYW5lbC1ib2R5XCIgfSwgW1xuICAgICAgICBfYyhcbiAgICAgICAgICBcInRhYmxlXCIsXG4gICAgICAgICAge1xuICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwidGFibGUgdGFibGUtYm9yZGVyZWQgdGFibGUtcmVzcG9uc2l2ZSB0YWJsZS1zdHJpcGVkXCJcbiAgICAgICAgICB9LFxuICAgICAgICAgIFtcbiAgICAgICAgICAgIF92bS5fbSgwKSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgXCJ0Ym9keVwiLFxuICAgICAgICAgICAgICBfdm0uX2woX3ZtLnVzZXJzLCBmdW5jdGlvbih1c2VyKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIF9jKFwidHJcIiwgW1xuICAgICAgICAgICAgICAgICAgX2MoXCJ0ZFwiLCBbX3ZtLl92KF92bS5fcyh1c2VyLm5hbWUpKV0pLFxuICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgIF9jKFwidGRcIiwgW192bS5fdihfdm0uX3ModXNlci5lbWFpbCkpXSlcbiAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgKVxuICAgICAgICAgIF1cbiAgICAgICAgKVxuICAgICAgXSlcbiAgICBdKVxuICBdKVxufVxudmFyIHN0YXRpY1JlbmRlckZucyA9IFtcbiAgZnVuY3Rpb24oKSB7XG4gICAgdmFyIF92bSA9IHRoaXNcbiAgICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgICByZXR1cm4gX2MoXCJ0aGVhZFwiLCBbXG4gICAgICBfYyhcInRyXCIsIFtcbiAgICAgICAgX2MoXCJ0aFwiLCBbX3ZtLl92KFwiTmFtZVwiKV0pLFxuICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICBfYyhcInRoXCIsIFtfdm0uX3YoXCJFbWFpbFwiKV0pXG4gICAgICBdKVxuICAgIF0pXG4gIH1cbl1cbnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxubW9kdWxlLmV4cG9ydHMgPSB7IHJlbmRlcjogcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnM6IHN0YXRpY1JlbmRlckZucyB9XG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpICAgICAgLnJlcmVuZGVyKFwiZGF0YS12LTQzMDkzNjRmXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi00MzA5MzY0ZlwiLFwiaGFzU2NvcGVkXCI6ZmFsc2UsXCJidWJsZVwiOntcInRyYW5zZm9ybXNcIjp7fX19IS4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCZidXN0Q2FjaGUhLi9yZXNvdXJjZXMvanMvdnVlL3VzZXJzL2luZGV4LnZ1ZVxuLy8gbW9kdWxlIGlkID0gNzVcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwidmFyIGRpc3Bvc2VkID0gZmFsc2VcbmZ1bmN0aW9uIGluamVjdFN0eWxlIChzc3JDb250ZXh0KSB7XG4gIGlmIChkaXNwb3NlZCkgcmV0dXJuXG4gIHJlcXVpcmUoXCIhIXZ1ZS1zdHlsZS1sb2FkZXIhY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4P3tcXFwidnVlXFxcIjp0cnVlLFxcXCJpZFxcXCI6XFxcImRhdGEtdi0zMzY2OTExZFxcXCIsXFxcInNjb3BlZFxcXCI6ZmFsc2UsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c3R5bGVzJmluZGV4PTAmYnVzdENhY2hlIS4vZXJyb3JzLnZ1ZVwiKVxufVxudmFyIG5vcm1hbGl6ZUNvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpXG4vKiBzY3JpcHQgKi9cbnZhciBfX3Z1ZV9zY3JpcHRfXyA9IHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dLFxcXCJwbHVnaW5zXFxcIjpbXFxcInRyYW5zZm9ybS1vYmplY3QtcmVzdC1zcHJlYWRcXFwiXX0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAmYnVzdENhY2hlIS4vZXJyb3JzLnZ1ZVwiKVxuLyogdGVtcGxhdGUgKi9cbnZhciBfX3Z1ZV90ZW1wbGF0ZV9fID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMzM2NjkxMWRcXFwiLFxcXCJoYXNTY29wZWRcXFwiOmZhbHNlLFxcXCJidWJsZVxcXCI6e1xcXCJ0cmFuc2Zvcm1zXFxcIjp7fX19IS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXRlbXBsYXRlJmluZGV4PTAmYnVzdENhY2hlIS4vZXJyb3JzLnZ1ZVwiKVxuLyogdGVtcGxhdGUgZnVuY3Rpb25hbCAqL1xuICB2YXIgX192dWVfdGVtcGxhdGVfZnVuY3Rpb25hbF9fID0gZmFsc2Vcbi8qIHN0eWxlcyAqL1xudmFyIF9fdnVlX3N0eWxlc19fID0gaW5qZWN0U3R5bGVcbi8qIHNjb3BlSWQgKi9cbnZhciBfX3Z1ZV9zY29wZUlkX18gPSBudWxsXG4vKiBtb2R1bGVJZGVudGlmaWVyIChzZXJ2ZXIgb25seSkgKi9cbnZhciBfX3Z1ZV9tb2R1bGVfaWRlbnRpZmllcl9fID0gbnVsbFxudmFyIENvbXBvbmVudCA9IG5vcm1hbGl6ZUNvbXBvbmVudChcbiAgX192dWVfc2NyaXB0X18sXG4gIF9fdnVlX3RlbXBsYXRlX18sXG4gIF9fdnVlX3RlbXBsYXRlX2Z1bmN0aW9uYWxfXyxcbiAgX192dWVfc3R5bGVzX18sXG4gIF9fdnVlX3Njb3BlSWRfXyxcbiAgX192dWVfbW9kdWxlX2lkZW50aWZpZXJfX1xuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJyZXNvdXJjZXMvanMvdnVlL2xheW91dHMvZXJyb3JzLnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7ICByZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkuc3Vic3RyKDAsIDIpICE9PSBcIl9fXCJ9KSkgeyAgY29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtMzM2NjkxMWRcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi0zMzY2OTExZFwiLCBDb21wb25lbnQub3B0aW9ucylcbicgKyAnICB9XG4gIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbiAoZGF0YSkge1xuICAgIGRpc3Bvc2VkID0gdHJ1ZVxuICB9KVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvanMvdnVlL2xheW91dHMvZXJyb3JzLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNzZcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiLy8gc3R5bGUtbG9hZGVyOiBBZGRzIHNvbWUgY3NzIHRvIHRoZSBET00gYnkgYWRkaW5nIGEgPHN0eWxlPiB0YWdcblxuLy8gbG9hZCB0aGUgc3R5bGVzXG52YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzP3NvdXJjZU1hcCEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXIvaW5kZXguanM/e1xcXCJ2dWVcXFwiOnRydWUsXFxcImlkXFxcIjpcXFwiZGF0YS12LTMzNjY5MTFkXFxcIixcXFwic2NvcGVkXFxcIjpmYWxzZSxcXFwiaGFzSW5saW5lQ29uZmlnXFxcIjp0cnVlfSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCZidXN0Q2FjaGUhLi9lcnJvcnMudnVlXCIpO1xuaWYodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG5pZihjb250ZW50LmxvY2FscykgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2Fscztcbi8vIGFkZCB0aGUgc3R5bGVzIHRvIHRoZSBET01cbnZhciB1cGRhdGUgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXNDbGllbnQuanNcIikoXCIyZmQxYzZkNFwiLCBjb250ZW50LCBmYWxzZSk7XG4vLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG5pZihtb2R1bGUuaG90KSB7XG4gLy8gV2hlbiB0aGUgc3R5bGVzIGNoYW5nZSwgdXBkYXRlIHRoZSA8c3R5bGU+IHRhZ3NcbiBpZighY29udGVudC5sb2NhbHMpIHtcbiAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwidnVlXFxcIjp0cnVlLFxcXCJpZFxcXCI6XFxcImRhdGEtdi0zMzY2OTExZFxcXCIsXFxcInNjb3BlZFxcXCI6ZmFsc2UsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAmYnVzdENhY2hlIS4vZXJyb3JzLnZ1ZVwiLCBmdW5jdGlvbigpIHtcbiAgICAgdmFyIG5ld0NvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz9zb3VyY2VNYXAhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3N0eWxlLWNvbXBpbGVyL2luZGV4LmpzP3tcXFwidnVlXFxcIjp0cnVlLFxcXCJpZFxcXCI6XFxcImRhdGEtdi0zMzY2OTExZFxcXCIsXFxcInNjb3BlZFxcXCI6ZmFsc2UsXFxcImhhc0lubGluZUNvbmZpZ1xcXCI6dHJ1ZX0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9c3R5bGVzJmluZGV4PTAmYnVzdENhY2hlIS4vZXJyb3JzLnZ1ZVwiKTtcbiAgICAgaWYodHlwZW9mIG5ld0NvbnRlbnQgPT09ICdzdHJpbmcnKSBuZXdDb250ZW50ID0gW1ttb2R1bGUuaWQsIG5ld0NvbnRlbnQsICcnXV07XG4gICAgIHVwZGF0ZShuZXdDb250ZW50KTtcbiAgIH0pO1xuIH1cbiAvLyBXaGVuIHRoZSBtb2R1bGUgaXMgZGlzcG9zZWQsIHJlbW92ZSB0aGUgPHN0eWxlPiB0YWdzXG4gbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyB1cGRhdGUoKTsgfSk7XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvdnVlLXN0eWxlLWxvYWRlciEuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyP3NvdXJjZU1hcCEuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zdHlsZS1jb21waWxlcj97XCJ2dWVcIjp0cnVlLFwiaWRcIjpcImRhdGEtdi0zMzY2OTExZFwiLFwic2NvcGVkXCI6ZmFsc2UsXCJoYXNJbmxpbmVDb25maWdcIjp0cnVlfSEuL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXN0eWxlcyZpbmRleD0wJmJ1c3RDYWNoZSEuL3Jlc291cmNlcy9qcy92dWUvbGF5b3V0cy9lcnJvcnMudnVlXG4vLyBtb2R1bGUgaWQgPSA3N1xuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKHRydWUpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiXFxuYm9keSB7IGJhY2tncm91bmQtaW1hZ2U6IHVybChkYXRhOmltYWdlL3BuZztiYXNlNjQsaVZCT1J3MEtHZ29BQUFBTlNVaEVVZ0FBQUJvQUFBQWFDQVlBQUFDcFNrek9BQUFBQkhOQ1NWUUlDQWdJZkFoa2lBQUFBQWx3U0ZsekFBQUxFZ0FBQ3hJQjB0MSsvQUFBQUJaMFJWaDBRM0psWVhScGIyNGdWR2x0WlFBeE1DOHlPUzh4TWlLcXEza0FBQUFjZEVWWWRGTnZablIzWVhKbEFFRmtiMkpsSUVacGNtVjNiM0pyY3lCRFV6Vnh0ZU0yQUFBQkhrbEVRVlJJaWIyVnl3NkVJQXhGVzVpZHIvLy9ReDlzZkczcExFeUozdEF3aTVFbUJxUm83dkhhd2lFRUVSSFM2eDdNVE14TVZ2Nit6M3RQTVVZU2tmVE0vUjBmRWFHMmJiTXYrR2M0blp6bitkTjRIQWNSRWEzcitoaTNiY3V1NjhqTHNraFZJbFcwNzN0V2FZbFE5K0Y5SXBxbVNmcStmd3NraGRPL0F3bVVUSlhyT3VhUlFOZVJrT2Q1bHE3clhtUzVJbm1FUktvRVIvUU12VUFQbFpESGNaUmhHTjRDU2VHWSthSE1xZ2NrczVSckh2L2VlaDQ1NXg1S3JNcTJ5SFFkaWJETzZuY0cvS1pXTDdNOHhEeVMxL01JTzBOSnFkVUxMUzgxWDYvWDZhUjBucUJTSmNQZVpubFpyek40NzdOS1VSbjJOdXM4c2p6bUVJSTBUZk1peXhVdXhwaFZXanBKa2J4MGJ0VW5zaFJpaFZ2NzBCdjhJdFhxNkFzb2kvWmlDYlU2WWdBQUFBQkpSVTVFcmtKZ2dnPT0pO1xcbn1cXG4uZXJyb3ItdGVtcGxhdGUge3BhZGRpbmc6IDQwcHggMTVweDt0ZXh0LWFsaWduOiBjZW50ZXI7XFxufVxcbi5lcnJvci1hY3Rpb25zIHttYXJnaW4tdG9wOjE1cHg7bWFyZ2luLWJvdHRvbToxNXB4O1xcbn1cXG4uZXJyb3ItYWN0aW9ucyAuYnRuIHsgbWFyZ2luLXJpZ2h0OjEwcHg7XFxufVxcblxcdFxcblwiLCBcIlwiLCB7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCIvVXNlcnMvdmlsbGFtb3JuYW50b25pb2pyL0RldmVsb3BtZW50L2dyZXlmb3gvbWF0Y2h1cC93ZWIvcmVzb3VyY2VzL2pzL3Z1ZS9sYXlvdXRzL3Jlc291cmNlcy9qcy92dWUvbGF5b3V0cy9lcnJvcnMudnVlPzRjMmQxMjc4XCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCI7QUFZQSxPQUFBLDhvQkFBQTtDQUFBO0FBQ0EsaUJBQUEsbUJBQUEsbUJBQUE7Q0FBQTtBQUNBLGdCQUFBLGdCQUFBLG1CQUFBO0NBQUE7QUFDQSxzQkFBQSxrQkFBQTtDQUFBXCIsXCJmaWxlXCI6XCJlcnJvcnMudnVlXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIjx0ZW1wbGF0ZT5cXG48ZGl2IGlkPVxcXCJsYXlvdXRzLWVycm9yc1xcXCI+XFxuICAgIDxyb3V0ZXItdmlldz48L3JvdXRlci12aWV3PlxcbjwvZGl2PlxcbjwvdGVtcGxhdGU+XFxuXFxuXFxuPHNjcmlwdD5cXG48L3NjcmlwdD5cXG5cXG48c3R5bGUgdHlwZT1cXFwidGV4dC9jc3NcXFwiIG1lZGlhPVxcXCJzY3JlZW5cXFwiPlxcblxcbmJvZHkgeyBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoZGF0YTppbWFnZS9wbmc7YmFzZTY0LGlWQk9SdzBLR2dvQUFBQU5TVWhFVWdBQUFCb0FBQUFhQ0FZQUFBQ3BTa3pPQUFBQUJITkNTVlFJQ0FnSWZBaGtpQUFBQUFsd1NGbHpBQUFMRWdBQUN4SUIwdDErL0FBQUFCWjBSVmgwUTNKbFlYUnBiMjRnVkdsdFpRQXhNQzh5T1M4eE1pS3FxM2tBQUFBY2RFVllkRk52Wm5SM1lYSmxBRUZrYjJKbElFWnBjbVYzYjNKcmN5QkRVelZ4dGVNMkFBQUJIa2xFUVZSSWliMlZ5dzZFSUF4Rlc1aWRyLy8vUXg5c2ZHM3BMRXlKM3RBd2k1RW1CcVJvN3ZIYXdpRUVFUkhTNng3TVRNeE1WdjYrejN0UE1VWVNrZlRNL1IwZkVhRzJiYk12K0djNG5aem4rZE40SEFjUkVhM3IraGkzYmN1dTY4akxza2hWSWxXMDczdFdhWWxROStGOUlwcW1TZnErZndza2hkTy9Bd21VVEpYck91YVJRTmVSa09kNWxxN3JYbVM1SW5tRVJLb0VSL1FNdlVBUGxaREhjWlJoR040Q1NlR1krYUhNcWdja3M1UnJIdi9lZWg0NTV4NUtyTXEyeUhRZGliRE82bmNHL0taV0w3TTh4RHlTMS9NSU8wTkpxZFVMTFM4MVg2L1g2YVIwbnFCU0pjUGVabmxacnpONDc3TktVUm4yTnVzOHNqem1FSUkwVGZNaXl4VXV4cGhWV2pwSmtieDBidFVuc2hSaWhWdjcwQnY4SXRYcTZBc29pL1ppQ2JVNllnQUFBQUJKUlU1RXJrSmdnZz09KTt9XFxuLmVycm9yLXRlbXBsYXRlIHtwYWRkaW5nOiA0MHB4IDE1cHg7dGV4dC1hbGlnbjogY2VudGVyO31cXG4uZXJyb3ItYWN0aW9ucyB7bWFyZ2luLXRvcDoxNXB4O21hcmdpbi1ib3R0b206MTVweDt9XFxuLmVycm9yLWFjdGlvbnMgLmJ0biB7IG1hcmdpbi1yaWdodDoxMHB4OyB9XFxuXFx0XFxuPC9zdHlsZT5cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlcj9zb3VyY2VNYXAhLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc3R5bGUtY29tcGlsZXI/e1widnVlXCI6dHJ1ZSxcImlkXCI6XCJkYXRhLXYtMzM2NjkxMWRcIixcInNjb3BlZFwiOmZhbHNlLFwiaGFzSW5saW5lQ29uZmlnXCI6dHJ1ZX0hLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT1zdHlsZXMmaW5kZXg9MCZidXN0Q2FjaGUhLi9yZXNvdXJjZXMvanMvdnVlL2xheW91dHMvZXJyb3JzLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNzhcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwidmFyIHJlbmRlciA9IGZ1bmN0aW9uKCkge1xuICB2YXIgX3ZtID0gdGhpc1xuICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gIHJldHVybiBfYyhcImRpdlwiLCB7IGF0dHJzOiB7IGlkOiBcImxheW91dHMtZXJyb3JzXCIgfSB9LCBbX2MoXCJyb3V0ZXItdmlld1wiKV0sIDEpXG59XG52YXIgc3RhdGljUmVuZGVyRm5zID0gW11cbnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxubW9kdWxlLmV4cG9ydHMgPSB7IHJlbmRlcjogcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnM6IHN0YXRpY1JlbmRlckZucyB9XG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpICAgICAgLnJlcmVuZGVyKFwiZGF0YS12LTMzNjY5MTFkXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0zMzY2OTExZFwiLFwiaGFzU2NvcGVkXCI6ZmFsc2UsXCJidWJsZVwiOntcInRyYW5zZm9ybXNcIjp7fX19IS4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCZidXN0Q2FjaGUhLi9yZXNvdXJjZXMvanMvdnVlL2xheW91dHMvZXJyb3JzLnZ1ZVxuLy8gbW9kdWxlIGlkID0gODBcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwidmFyIGRpc3Bvc2VkID0gZmFsc2VcbnZhciBub3JtYWxpemVDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKVxuLyogc2NyaXB0ICovXG52YXIgX192dWVfc2NyaXB0X18gPSBudWxsXG4vKiB0ZW1wbGF0ZSAqL1xudmFyIF9fdnVlX3RlbXBsYXRlX18gPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXIvaW5kZXg/e1xcXCJpZFxcXCI6XFxcImRhdGEtdi0wZjgyNzA4MFxcXCIsXFxcImhhc1Njb3BlZFxcXCI6ZmFsc2UsXFxcImJ1YmxlXFxcIjp7XFxcInRyYW5zZm9ybXNcXFwiOnt9fX0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCZidXN0Q2FjaGUhLi80MDQudnVlXCIpXG4vKiB0ZW1wbGF0ZSBmdW5jdGlvbmFsICovXG4gIHZhciBfX3Z1ZV90ZW1wbGF0ZV9mdW5jdGlvbmFsX18gPSBmYWxzZVxuLyogc3R5bGVzICovXG52YXIgX192dWVfc3R5bGVzX18gPSBudWxsXG4vKiBzY29wZUlkICovXG52YXIgX192dWVfc2NvcGVJZF9fID0gbnVsbFxuLyogbW9kdWxlSWRlbnRpZmllciAoc2VydmVyIG9ubHkpICovXG52YXIgX192dWVfbW9kdWxlX2lkZW50aWZpZXJfXyA9IG51bGxcbnZhciBDb21wb25lbnQgPSBub3JtYWxpemVDb21wb25lbnQoXG4gIF9fdnVlX3NjcmlwdF9fLFxuICBfX3Z1ZV90ZW1wbGF0ZV9fLFxuICBfX3Z1ZV90ZW1wbGF0ZV9mdW5jdGlvbmFsX18sXG4gIF9fdnVlX3N0eWxlc19fLFxuICBfX3Z1ZV9zY29wZUlkX18sXG4gIF9fdnVlX21vZHVsZV9pZGVudGlmaWVyX19cbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwicmVzb3VyY2VzL2pzL3Z1ZS9lcnJvcnMvNDA0LnZ1ZVwiXG5pZiAoQ29tcG9uZW50LmVzTW9kdWxlICYmIE9iamVjdC5rZXlzKENvbXBvbmVudC5lc01vZHVsZSkuc29tZShmdW5jdGlvbiAoa2V5KSB7ICByZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkuc3Vic3RyKDAsIDIpICE9PSBcIl9fXCJ9KSkgeyAgY29uc29sZS5lcnJvcihcIm5hbWVkIGV4cG9ydHMgYXJlIG5vdCBzdXBwb3J0ZWQgaW4gKi52dWUgZmlsZXMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtMGY4MjcwODBcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi0wZjgyNzA4MFwiLCBDb21wb25lbnQub3B0aW9ucylcbicgKyAnICB9XG4gIG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbiAoZGF0YSkge1xuICAgIGRpc3Bvc2VkID0gdHJ1ZVxuICB9KVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvanMvdnVlL2Vycm9ycy80MDQudnVlXG4vLyBtb2R1bGUgaWQgPSA4MVxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJ2YXIgcmVuZGVyID0gZnVuY3Rpb24oKSB7XG4gIHZhciBfdm0gPSB0aGlzXG4gIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgcmV0dXJuIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiZXJyb3JzLTQwNC1jb21wb25lbnRcIiB9LCBbXG4gICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb250YWluZXJcIiB9LCBbXG4gICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcInJvd1wiIH0sIFtcbiAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtbWQtMTJcIiB9LCBbXG4gICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJlcnJvci10ZW1wbGF0ZVwiIH0sIFtcbiAgICAgICAgICAgIF9jKFwiaDFcIiwgW192bS5fdihcIlxcbiAgICAgICAgICAgICAgICAgICAgT29wcyFcIildKSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcImgyXCIsIFtfdm0uX3YoXCJcXG4gICAgICAgICAgICAgICAgICAgIDQwNCBOb3QgRm91bmRcIildKSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImVycm9yLWRldGFpbHNcIiB9LCBbXG4gICAgICAgICAgICAgIF92bS5fdihcbiAgICAgICAgICAgICAgICBcIlxcbiAgICAgICAgICAgICAgICAgICAgU29ycnksIGFuIGVycm9yIGhhcyBvY2N1cmVkLCBSZXF1ZXN0ZWQgcGFnZSBub3QgZm91bmQhXFxuICAgICAgICAgICAgICAgIFwiXG4gICAgICAgICAgICAgIClcbiAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICBcImRpdlwiLFxuICAgICAgICAgICAgICB7IHN0YXRpY0NsYXNzOiBcImVycm9yLWFjdGlvbnNcIiB9LFxuICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICBcInJvdXRlci1saW5rXCIsXG4gICAgICAgICAgICAgICAgICB7IHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcHJpbWFyeSBidG4tbGdcIiwgYXR0cnM6IHsgdG86IFwiL1wiIH0gfSxcbiAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJzcGFuXCIsIHsgc3RhdGljQ2xhc3M6IFwiZ2x5cGhpY29uIGdseXBoaWNvbi1ob21lXCIgfSksXG4gICAgICAgICAgICAgICAgICAgIF92bS5fdihcIlxcbiAgICAgICAgICAgICAgICAgICAgICAgIFRha2UgTWUgSG9tZSBcIilcbiAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICBcInJvdXRlci1saW5rXCIsXG4gICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZGVmYXVsdCBidG4tbGdcIixcbiAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHsgdG86IFwiY29udGFjdFwiIH1cbiAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgIF9jKFwic3BhblwiLCB7IHN0YXRpY0NsYXNzOiBcImdseXBoaWNvbiBnbHlwaGljb24tZW52ZWxvcGVcIiB9KSxcbiAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIENvbnRhY3QgU3VwcG9ydFwiKVxuICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgMVxuICAgICAgICAgICAgKVxuICAgICAgICAgIF0pXG4gICAgICAgIF0pXG4gICAgICBdKVxuICAgIF0pXG4gIF0pXG59XG52YXIgc3RhdGljUmVuZGVyRm5zID0gW11cbnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxubW9kdWxlLmV4cG9ydHMgPSB7IHJlbmRlcjogcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnM6IHN0YXRpY1JlbmRlckZucyB9XG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpICAgICAgLnJlcmVuZGVyKFwiZGF0YS12LTBmODI3MDgwXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0wZjgyNzA4MFwiLFwiaGFzU2NvcGVkXCI6ZmFsc2UsXCJidWJsZVwiOntcInRyYW5zZm9ybXNcIjp7fX19IS4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCZidXN0Q2FjaGUhLi9yZXNvdXJjZXMvanMvdnVlL2Vycm9ycy80MDQudnVlXG4vLyBtb2R1bGUgaWQgPSA4MlxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJpbXBvcnQgVnVlIGZyb20gJ3Z1ZSc7XG5pbXBvcnQgVnVleCBmcm9tICd2dWV4JztcblxuVnVlLnVzZShWdWV4KTtcblxuZXhwb3J0IGNvbnN0IHN0b3JlID0gbmV3IFZ1ZXguU3RvcmUoe1xuICAgIHN0YXRlOiB7XG4gICAgICAgIHVzZXI6IHt9LFxuICAgICAgICBhY2Nlc3NfdG9rZW46IG51bGwsXG4gICAgICAgIGlzQXV0aGVudGljYXRlZDogZmFsc2VcbiAgICB9XG59KTtcblxuZXhwb3J0IGRlZmF1bHQgc3RvcmU7XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2pzL3N0b3JlLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==