import VueRouter from 'vue-router';

const routes = [
    {
        path: '/',
        component: require('./vue/layouts/pages'),
        meta: {requiresAuth: false},
        children: [
            {
                path: '',
                component: require('./vue/pages/home'),
                meta: {requiresAuth: false}
            }
        ]
    },
     {
        path: '/about',
        component: require('./vue/layouts/pages'),
        meta: {requiresAuth: false},
        children: [
            {
                path: '',
                component: require('./vue/pages/about'),
                meta: {requiresAuth: false}
            }
        ]
    },
     {
        path: '/contact',
        component: require('./vue/layouts/pages'),
        meta: {requiresAuth: false},
        children: [
            {
                path: '',
                component: require('./vue/pages/contact'),
                meta: {requiresAuth: false}
            }
        ]
    },
    {
        path: '/login',
        component: require('./vue/layouts/portal'),
        meta: {requiresAuth: false},
        children: [
            {
                path: '',
                component: require('./vue/auth/login'),
                meta: {requiresAuth: false}
            }
        ]
    },
     {
        path: '/dashboard',
        component: require('./vue/layouts/admin'),
        meta: {requiresAuth: true},
        children: [
            {
                path: '',
                component: require('./vue/dashboard/index'),
                meta: {requiresAuth: true}
            }
        ]
    },
    {
        path: '/users',
        component: require('./vue/layouts/admin'),
        meta: {requiresAuth: true},
        children: [
            {
                path: '',
                component: require('./vue/users/index'),
                meta: {requiresAuth: true}
            }
        ]
    },
    {
        path: '*',
        component: require('./vue/layouts/errors'),
        meta: {requiresAuth: false},
        children: [
            {
                path: '',
                component: require('./vue/errors/404'),
                meta: {requiresAuth: false}
            }
        ]
    }
];

const router = new VueRouter({
    linkActiveClass: "active",
    mode: 'history',
    routes
});

router.beforeEach((to, from, next) => {
    let accessToken = localStorage.getItem('access_token');
    if (to.meta.requiresAuth) {
        if (!accessToken || accessToken === null) {
            next({
                path: '/login',
                query: {redirect: to.fullPath}
            });
        }
    }
    next();


});

export default router;